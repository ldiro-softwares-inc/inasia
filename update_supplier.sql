ALTER TABLE Suppliers ADD nomenclatures longtext NULL;
CREATE TABLE _supplier_categorie (supplier_id INT NOT NULL, categorie_id VARCHAR(255) NOT NULL, INDEX IDX_7389AB242ADD6D8C (supplier_id), INDEX IDX_7389AB24BCF5E72D (categorie_id), PRIMARY KEY(supplier_id, categorie_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE _supplier_categorie ADD CONSTRAINT FK_7389AB242ADD6D8C FOREIGN KEY (supplier_id) REFERENCES Suppliers (id);
ALTER TABLE _supplier_categorie ADD CONSTRAINT FK_7389AB24BCF5E72D FOREIGN KEY (categorie_id) REFERENCES Categories (id);