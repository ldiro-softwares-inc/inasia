# Installation

## Prérequis

PHP > 5.6

MYSQL 5.6 (utilisé en production) ou MYSQL 7 configuré avec only-full-group-by disable https://stackoverflow.com/questions/23921117/disable-only-full-group-by

## Cloner le depot 

https://gitlab.com/ldiro-softwares-inc/inasia

## Ajout d'une bdd MYSQL + import du dump

inAsiaDev.sql.zip + les différents fichiers SQL

## Installer les dépendances

php -d memory_limit=-1 composer.phar install

Lorsque les dépendences sont téléchargées, renseigner le parameters.yml

    database_driver: pdo_mysql
    database_host: localhost
    database_port: 
    database_name: 
    database_user: 
    database_password: 
    
## Installer les assets

php app/console assets:install htdocs --symlink 

php app/console assetic:dump htdocs

Pour que les upload des images fonctionnent il faut créer manuellement le dossier :
/htdocs/tmp 
    
## Lancer le serveur web dans le dossier htdocs

php -S localhost:80 -t htdocs/ 

## Login

http://localhost:80/app_dev.php/

Le user demo est 

login : olivier

password : 123456

## Notes 

Lorsque qu'on clique sur "Consulter" l'erreur "Unable to find Supplychain file" signifie que l'algo doit retourner un fichier uploadé (GED) qui n'est pas présent dans l'environnement de dev. Ce n'est don pas un BUG.