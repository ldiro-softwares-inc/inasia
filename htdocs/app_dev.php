<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;
/*
if (!in_array($_SERVER['REMOTE_ADDR'], array('62.61.232.142'))) {
    header('HTTP/1.0 403 Forbidden');
    exit('');
}
*/
$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
Debug::enable();

require_once __DIR__.'/../app/AppKernel.php';

$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);

