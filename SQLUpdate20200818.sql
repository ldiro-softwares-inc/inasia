-- branch customer_add_supplier_exception
CREATE TABLE _customers_suppliers_exception (customer_id INT NOT NULL, supplier_id INT NOT NULL, INDEX IDX_32E8F7B89395C3F3 (customer_id), INDEX IDX_32E8F7B82ADD6D8C (supplier_id), PRIMARY KEY(customer_id, supplier_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE _customers_suppliers_exception ADD CONSTRAINT FK_32E8F7B89395C3F3 FOREIGN KEY (customer_id) REFERENCES Customers (id);
ALTER TABLE _customers_suppliers_exception ADD CONSTRAINT FK_32E8F7B82ADD6D8C FOREIGN KEY (supplier_id) REFERENCES Suppliers (id);