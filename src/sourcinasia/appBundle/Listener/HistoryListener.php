<?php

namespace sourcinasia\appBundle\Listener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use sourcinasia\appBundle\Entity\History;
use sourcinasia\appBundle\Entity\Supplier;

class HistoryListener implements EventSubscriber {

    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
        $this->container = $container;
    }

    public function getSubscribedEvents() {
        return array(
            'postPersist',
            'postUpdate',
        );
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $this->archive($args, "Update");
    }

    public function postPersist(LifecycleEventArgs $args) {
        $this->archive($args, "Persist");
    }

    public function archive(LifecycleEventArgs $args, $event) {
        $entity = $args->getEntity();

        if ($entity instanceof Supplier) {
            $History = new History();
            $History->SetTitle(($event == "Persist") ? 'HISTORY_NEW_SUPPLER': 'HISTORY_EDIT_SUPPLER');
            $History->SetDescription('HISTORY_NEW_SUPPLER_DESCRIPTION' . $entity->getName());
            $History->SetSupplier($entity);
        }

        if (!empty($History)) {
            if ($event == "Persist")
                $History->SetIco('<i class="icon-plus-sign"></i>');
            elseif ($event == "Update")
                $History->SetIco('<i class="icon-pencil"></i>');
            $History->SetDate(new \DateTime());
            if ($this->container->get('security.token_storage')->getToken()){ // Need to bypass user token for units tests //Todo correct this problem
                $History->SetUser($this->container->get('security.token_storage')->getToken()->getUser());
            }
            $em = $args->getEntityManager();
            $em->persist($History);
            $em->flush();
        }
    }

}
