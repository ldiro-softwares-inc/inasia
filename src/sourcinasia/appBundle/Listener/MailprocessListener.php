<?php

namespace sourcinasia\appBundle\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class MailprocessListener implements EventSubscriberInterface
{

    protected $doctrine;
    protected $container;
    protected $translator;
    protected $context;
    protected $rooter;
    private $mailingfields;

    public function __construct(
        $versionProduction,
        \Doctrine\Bundle\DoctrineBundle\Registry $doctrine,
        $container,
        $translator,
        $context,
        $rooter,
        $mailer,
        $template
    )
    {
        $this->versionProduction = $versionProduction;
        $this->doctrine = $doctrine;
        $this->container = $container;
        $this->translator = $translator;
        $this->context = $context;
        $this->rooter = $rooter;
        $this->mailer = $mailer;
        $this->template = $template;
    }

    static public function getSubscribedEvents()
    {
        return array(
            'sendmail' => array('OnSendMail'),
            'sendmessage' => array('OnSendMessage'),
            'sendemailbase' => array('OnSendEmailBase'),
            'sendemailuseraccess' => array('OnSendEmailUserAccess'),
        );
    }

    public function OnSendEmailUserAccess(GenericEvent $event)
    {
        $ContactUser = $event['ContactUser'];
        $sale_contact = $ContactUser->getContact()->getCustomer()->getMainsaler()->getName() . ' Email: <a href="mailto:' . $ContactUser->getContact()->getCustomer()->getMainsaler()->getEmail() . '">' . $ContactUser->getContact()->getCustomer()->getMainsaler()->getEmail() . '</a>';
        $this->sendmail(
            $ContactUser->getContact()->getCustomer()->GetLocale(),
            array($ContactUser->getEmail(), $event['User']->getEmail(), 'f.aubert@inasia-corp.com'),
            $this->translator->trans($event['TITLE'], array(), 'messages', $ContactUser->getContact()->getCustomer()->GetLocale()),
            str_replace(array('{login}', '{password}'), array($ContactUser->getEmail(), $event['Password']), $this->translator->trans($event['MESSAGE'], array(), 'messages', $ContactUser->getContact()->getCustomer()->GetLocale()))
            . str_replace('{saler_contact}', $sale_contact, $this->translator->trans('MAIL_FOOTER_SOURCINASIA_SALERINCHARGE', array(), 'messages', $ContactUser->getContact()->getCustomer()->GetLocale()))
            . $this->translator->trans('MAIL_FOOTER_SOURCINASIA_SIGNATURE', array(), 'messages', $ContactUser->getContact()->getCustomer()->GetLocale())
        );
    }

    public function OnSendMail(GenericEvent $event)
    {
        $cadencier = $event->getSubject();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $this->PopulateSourcinasiaMailing($cadencier, $user);

        switch ($event['step']) {
            case 'SALERUSERCOMFIRMSELECTION':
                $this->sendordermail(
                    $cadencier->getCustomer()->GetLocale(),
                    $this->doctrine->getRepository('appBundle:Contact')->GetAdminContactEmailByCustomer(
                        $cadencier->getCustomer()->GetId()
                    ),
                    'STEP0',
                    'MAIL_CONFIRMATION_SELECTIONSALER_ADMINCLIENT'
                );
                $this->PopulateCustomerMailing($cadencier, $user);
                $this->sendordermail(
                    $user->getLocale(),
                    $user->getEmail(),
                    'STEP0',
                    'MAIL_CONFIRMATION_SELECTIONSALER_USERCLIENT'
                );
                break;
            case 'CUSTOMERCONFIRMSELECTION':
                if ($this->context->isGranted('ROLE_CLIENT')) {
                    $this->sendordermail(
                        $cadencier->getCustomer()->getLocale(),
                        array_merge(array($cadencier->getCustomer()->getMainsaler()->GetEmail()), $this->GetRoleEmails('ROLE_ADMIN')),
                        'STEP11',
                        'MAIL_CONFIRMATION_SELECTION_ADMINCLIENT'
                    );
                }

                $owners = $cadencier->getCustomer()->getOwners();
                $emails = array();
                foreach ($owners as $owner) {
                    $emails[] = $owner->GetEmail();
                }

                $this->sendordermail(
                    $cadencier->getCustomer()->getMainsaler()->GetLocale(),
                    $emails,
                    'STEP10',
                    'MAIL_CONFIRMATION_SELECTION_SALER'
                );
                break;
            case 'SALERCONFIRMSELECTION':
                $this->sendordermail(
                    'en',
                    array_merge($this->GetRoleEmails('ROLE_PRODUCTION'), array('s.barea@inasia-corp.com')),
                    'STEP11',
                    'MAIL_CONFIRMATION_SELECTION_PRODUCTION'
                );
                break;
            case 'SENDPITOSUPPLIER':
                break;
            case 'PRODUCTIONCONFIRMPRICES':
                $this->sendordermail(
                    'en',
                    array_merge($this->GetRoleEmails('ROLE_ADMIN'), array('s.barea@inasia-corp.com')),
                    'STEP13',
                    'MAIL_CONFIRMATION_PRICES_ADMIN'
                );
                break;
            case 'PRICESREJECTED':
                $this->sendordermail(
                    'en',
                    $this->GetRoleEmails('ROLE_PRODUCTION'),
                    'PI SUPPLIER REJECTED',
                    'MAIL_CONFIRMATION_SELECTION_REJECTED'
                );
                break;
            case 'ADMINCONFIRMPRICES':
                $this->sendordermail(
                    $cadencier->getCustomer()->getMainsaler()->GetLocale(),
                    $cadencier->getCustomer()->getMainsaler()->GetEmail(),
                    'STEP14',
                    'MAIL_CONFIRMATION_GENPI_SALER'
                );
                break;
            case 'SALERCOMFIRMPI':
                $this->sendordermail(
                    'en',
                    $this->GetRoleEmails('ROLE_ADMIN'),
                    'STEP15',
                    'MAIL_CONFIRMATION_GENPI_ADMIN'
                );
                break;
            case 'ADMINCOMFIRMPI':
                $this->sendordermail(
                    $cadencier->getCustomer()->getMainsaler()->GetLocale(),
                    $cadencier->getCustomer()->getMainsaler()->GetEmail(),
                    'STEP2',
                    'MAIL_CONFIRMATION_WAITINGPI_SALER'
                );
                //$this->sendordermail($cadencier->getCustomer()->GetLocale(), $this->doctrine->getRepository('appBundle:Contact')->GetAdminContactEmailByCustomer($cadencier->getCustomer()->GetId()), 'STEP2', 'MAIL_CONFIRMATION_WAITINGPI_CUSTOMER');
                break;
            case 'CUSTOMERCONFIRMPI':
                $this->sendordermail(
                    'en',
                    $this->GetRoleEmails('ROLE_PRODUCTION'),
                    'STEP3',
                    'MAIL_CONFIRMATION_PICUSTOMER_PRODUCTION'
                );
                $this->sendordermail($cadencier->getCustomer()->GetLocale(), $this->doctrine->getRepository('appBundle:Contact')->GetAdminContactEmailByCustomer($cadencier->getCustomer()->GetId()), 'STEP3', 'MAIL_CONFIRMATION_PICUSTOMER_CUSTOMER');
                break;
            case 'LOADERORDER':
                $this->sendordermail(
                    $cadencier->getCustomer()->getMainsaler()->GetLocale(),
                    $cadencier->getCustomer()->getMainsaler()->GetEmail(),
                    'STEP4',
                    'MAIL_CONFIRMATION_LOADEDORDER_SALER'
                );
                $this->sendordermail($cadencier->getCustomer()->GetLocale(), $this->doctrine->getRepository('appBundle:Contact')->GetAdminContactEmailByCustomer($cadencier->getCustomer()->GetId()), 'STEP4', 'MAIL_CONFIRMATION_LOADEDORDER_CUSTOMER');
                break;
            case 'DELIVEREDORDER':
                $this->sendordermail(
                    $cadencier->getCustomer()->getMainsaler()->GetLocale(),
                    $cadencier->getCustomer()->getMainsaler()->GetEmail(),
                    'STEP5',
                    'MAIL_CONFIRMATION_DELIVEREDORDER_SALER'
                );
                $this->sendordermail($cadencier->getCustomer()->GetLocale(), $this->doctrine->getRepository('appBundle:Contact')->GetAdminContactEmailByCustomer($cadencier->getCustomer()->GetId()), 'STEP5', 'MAIL_CONFIRMATION_DELIVEREDORDER_SALER');
                break;
            case 'VALIDATION':
                $this->sendordermail(
                    $cadencier->getCustomer()->getMainsaler()->GetLocale(),
                    array_merge($this->GetRoleEmails('ROLE_ADMIN'), $this->GetRoleEmails('ROLE_ADMINSUPPLYCHAIN')),
                    'SUPPLYCHAIN_VALIDATION',
                    $this->template->render('appBundle:Emails:waitingdocument.html.twig', array('Supplychain' => $event['Supplychain']))
                );
                break;
            case 'Message':
                $this->sendordermail(
                    $cadencier->getCustomer()->getMainsaler()->GetLocale(),
                    $this->GetRoleEmails('ROLE_ADMINSUPPLYCHAIN'),
                    'SUPPLYCHAIN_VALIDATION',
                    'MAIL_SUPPLYCHAIN_VALIDATION'
                );
                break;
            default :
                break;
        }
    }

    public function OnSendMessage(GenericEvent $event)
    {
        $chat = $event->getSubject();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $header = "";
        $title = "";
        $mainsaler= "";

        if (method_exists($user, 'getContact')) {
            $contact = $user->getContact();
            if (method_exists($user->getContact(), 'getCustomer')) {
                $cutomer = $user->getContact()->getCustomer();
                if ($user->getContact()->getCustomer()) {
                    $mainsaler = $user->getContact()->getCustomer()->getMainsaler();
                    $header .= "COMPANY : " . $cutomer->getName() . "<br/>";
                    $header .= "USER : " . $contact->getName() . "<br/>";
                    $title = $cutomer->getName() . ' - ';
                }
            }
        }

        switch ($chat->getType()) {
            case 1: // SALE
                $emails = array("f.aubert@inasia-corp.com");
                if ($mainsaler) {
                    $this->sendmail(
                        'en',
                        $mainsaler->getEmail(),
                        "MESSAGE - " . $title . $chat->getTitle(),
                        $header . $chat->getMessage()
                    );
                }
                break;
            case 2: // PRODUCTION
                $this->sendmail(
                    "en",
                    $this->GetRoleEmails('ROLE_ADMINSUPPLYCHAIN'),
                    "MESSAGE - " . $title . $chat->getTitle(),
                    $header . $chat->getMessage()
                );
                break;
            case 3: // SUPPORT
                $this->sendmail(
                    'fr',
                    array("alert@site-internet-reunion.re", "f.aubert@inasia-corp.com"),
                    "MESSAGE - " . $title . $chat->getTitle(),
                    $header . $chat->getMessage()
                );
                break;
        }

        $this->sendmail(
            $user->getLocale(),
            $user->getEmail(),
            'TITLE_MESSAGE_CONFIRMATION',
            'MAIL_MESSAGE_CONFIRMATION'
        );
    }

    public function OnSendEmailBase(GenericEvent $event)
    {
        $message = $event->getSubject();

        if (!is_array($message['TO'])) {
            $message['TO'] = (array)$message['TO'];
        }

        $this->sendmail(
            '',
            $message['TO'],
            $message['TITLE'],
            $this->AddFormatHtml($message['MESSAGE']),
            'info@inasia-corp.com',
            false
        );
    }

    private function PopulateSourcinasiaMailing($cadencier, $user)
    {
        $this->mailingfields['{Commercial}'] = $user->getName();
        $this->mailingfields['{customer_name}'] = $cadencier->GetCustomer()->GetName();
        $this->mailingfields['{user_name}'] = $cadencier->GetUser()->GetName();
        $this->mailingfields['{num_commande}'] = $cadencier->GetId();
        $this->mailingfields['{order_link}'] = '<a href="https://market.inasia-corp.com' . $this->rooter->generate(
                'command_show',
                array('id' => $cadencier->getId())
            ) . '">https://market.inasia-corp.com</a>';
        $this->mailingfields['{action}'] = 'MAIL_ACTION_CONFIRM';
    }

    private function PopulateCustomerMailing($cadencier, $user)
    {
        $this->mailingfields['{Commercial}'] = $user->getName();
        $this->mailingfields['{customer_name}'] = $cadencier->GetCustomer()->GetName();
        $this->mailingfields['{saler_contact}'] = $cadencier->getCustomer()->getMainsaler()->getName() . ' Email: <a href="mailto:' . $cadencier->getCustomer()->getMainsaler()->getEmail() . '">' . $cadencier->getCustomer()->getMainsaler()->getEmail() . '</a>';;
        $this->mailingfields['{user_name}'] = $cadencier->GetUser()->GetName();
        $this->mailingfields['{num_commande}'] = $cadencier->GetId();
        $this->mailingfields['{order_link}'] = '<a href="https://market.inasia-corp.com' . $this->rooter->generate(
                'command_show',
                array('id' => $cadencier->getId())
            ) . '">market.inasia-corp.com</a>';
        $this->mailingfields['{action}'] = 'MAIL_ACTION_CONFIRM';
    }

    private function sendordermail($locale = "en", $to = array(), $title = "", $message = "")
    {
        if (strtolower($locale) == "fr") {
            $locale = "fr_FR";
        } else {
            $locale = "en_EN";
        }

        if (array_key_exists('{num_commande}', $this->mailingfields)) {
            $title = $this->translator->trans(
                    'ENTITY_ORDER',
                    array(),
                    'messages',
                    $locale
                ) . ':' . $this->mailingfields['{num_commande}'] . ' | ' . html_entity_decode(
                    $this->translator->trans($title, array(), 'messages', $locale)
                );
        } else {
            $title = $this->translator->trans($title, array(), 'messages', $locale);
        }

        if (array_key_exists('{action}', $this->mailingfields)) {
            $this->mailingfields['{action}'] = $this->translator->trans(
                $this->mailingfields['{action}'],
                array(),
                'messages',
                $locale
            );
        }

        $message = $this->translator->trans('MAIL_HEADER_NUMORDER', array(), 'messages', $locale)
            . $this->translator->trans('MAIL_HEADER_HI', array(), 'messages', $locale)
            . $this->translator->trans($message, array(), 'messages', $locale);

        if (array_key_exists('{selection_link}', $this->mailingfields)) {
            $message .= $this->translator->trans(
                'MAIL_FOOTER_SOURCINASIA_DOWNLOADSELECTION',
                array(),
                'messages',
                $locale
            );
        }

        $message .= $this->translator->trans('MAIL_FOOTER_SOURCINASIA_FOLLOWORDER', array(), 'messages', $locale);

        if (array_key_exists('{saler_contact}', $this->mailingfields)) {
            $message .= $this->translator->trans('MAIL_FOOTER_SOURCINASIA_SALERINCHARGE', array(), 'messages', $locale);
        }

        $message .= $this->translator->trans('MAIL_FOOTER_SOURCINASIA_SIGNATURE', array(), 'messages', $locale);

        if (!empty($this->mailingfields)) {
            $message = str_replace(array_keys($this->mailingfields), array_values($this->mailingfields), $message);
        }


        if (is_array($to)) {
            if ($this->versionProduction) {
                foreach (array_unique($to) as $email) {
                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $this->mailer->send(
                            \Swift_Message::newInstance()
                                ->setSubject($title)
                                ->setFrom('order@inasia-corp.com')
                                ->setTo($email)
                                ->setBody($this->translator->trans($message), 'text/html')
                        );
                    }
                }
            }
            $this->mailer->send(
                \Swift_Message::newInstance()
                    ->setSubject('CC:' . $title)
                    ->setFrom('order@inasia-corp.com')
                    ->setTo('follow@site-internet-reunion.re')
                    ->setBody($this->translator->trans($message), 'text/html')
            );
        }
    }

    private function sendmail(
        $locale = "en",
        $to = array(),
        $title = "",
        $message = "",
        $from = "contact@inasia-corp.com",
        $nl2br = true
    )
    {
        if (strtolower($locale) == "fr") {
            $locale = "fr_FR";
        } else {
            $locale = "en_EN";
        }

        if (is_array($to)) {
            foreach (array_unique($to) as $email) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

                    if (!$this->versionProduction) {
                        $email = "f.aubert@inasia-corp.com";
                    }

                    $this->mailer->send(
                        \Swift_Message::newInstance()
                            ->setSubject($title)
                            ->setFrom($from)
                            ->setTo($email)
                            ->setBody($nl2br ? nl2br($message) : $message, 'text/html')
                    );
                }
            }
        }

        /*  $this->mailer->send(
              \Swift_Message::newInstance()
                  ->setSubject('CC:' . $title)
                  ->setFrom('order@inasia-corp.com')
                  ->setTo($from)
                  ->setBody($nl2br ? nl2br($message) : $message, 'text/html')
          );*/

        $this->mailer->send(
            \Swift_Message::newInstance()
                ->setSubject('CC:' . $title)
                ->setFrom('order@inasia-corp.com')
                ->setTo('follow@site-internet-reunion.re')
                ->setBody($nl2br ? nl2br($message) : $message, 'text/html')
        );
    }

    private function sendordermailresalercustomer($locale = "en", $to = array(), $title = "", $message = "")
    {
        if (strtolower($locale) == "fr") {
            $locale = "fr_FR";
        } else {
            $locale = "en_EN";
        }

        $title = html_entity_decode($this->translator->trans($title, array(), 'messages', $locale));

        $message = $this->translator->trans('MAIL_HEADER_HI', array(), 'messages', $locale)
            . utf8_decode($this->translator->trans($message, array(), 'messages', $locale));

        if (!empty($this->mailingfields)) {
            $message = str_replace(array_keys($this->mailingfields), array_values($this->mailingfields), $message);
        }

        if ($this->versionProduction) {
            if (is_array($to)) {
                foreach (array_unique($to) as $email) {
                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $this->mailer->send(
                            \Swift_Message::newInstance()
                                ->setSubject($title)
                                ->setFrom('order@inasia-corp.com')
                                ->setTo($email)
                                ->setBody($this->translator->trans($message), 'text/html')
                        );
                    }
                }
            }
        }
    }

    private function GetRoleEmails($role)
    {
        $emailsRequest = $this->doctrine->getRepository('appBundle:Cadencier')->findByRole($role);
        $emails = array();
        foreach ($emailsRequest as $email) {
            $emails[] = $email['email'];
        }

        return $emails;
    }


    private function AddFormatHtml($content)
    {
        return '<!DOCTYPE html>
<html lang="en">
<head>
<title>SOURCINASIA</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
    .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
    body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* RESET STYLES */
    body{margin:0; padding:0;}
    img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
    table{border-collapse:collapse !important;}
    body{height:100% !important; margin:0; padding:0; width:100% !important;}

    /* iOS BLUE LINKS */
    .appleBody a {color:#68440a; text-decoration: none;}
        .appleFooter a {color:#999999; text-decoration: none;}

            /* MOBILE STYLES */
            @media screen and (max-width: 525px) {

                /* ALLOWS FOR FLUID TABLES */
                table[class="wrapper"]{
                    width:100% !important;
                }

        /* ADJUSTS LAYOUT OF LOGO IMAGE */
        td[class="logo"]{
                    text-align: left;
          padding: 20px 0 20px 0 !important;
        }

        td[class="logo"] img{
                    margin:0 auto!important;
        }

        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
        td[class="mobile-hide"]{
                    display:none;}

        img[class="mobile-hide"]{
                    display: none !important;
        }

        img[class="img-max"]{
                    max-width: 100% !important;
          height:auto !important;
        }

        /* FULL-WIDTH TABLES */
        table[class="responsive-table"]{
                    width:100%!important;
                }

        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
        td[class="padding"]{
                    padding: 10px 5% 15px 5% !important;
        }

        td[class="padding-copy"]{
                    padding: 10px 5% 10px 5% !important;
          text-align: center;
        }

        td[class="padding-meta"]{
                    padding: 30px 5% 0px 5% !important;
          text-align: center;
        }

        td[class="no-pad"]{
                    padding: 0 0 20px 0 !important;
        }

        td[class="no-padding"]{
                    padding: 0 !important;
        }

        td[class="section-padding"]{
                    padding: 50px 15px 50px 15px !important;
        }

        td[class="section-padding-bottom-image"]{
                    padding: 50px 15px 0 15px !important;
        }

        /* ADJUST BUTTONS ON MOBILE */
        td[class="mobile-wrapper"]{
                    padding: 10px 5% 15px 5% !important;
        }

        table[class="mobile-button-container"]{
                    margin:0 auto;
            width:100% !important;
        }

        a[class="mobile-button"]{
                    width:80% !important;
                    padding: 15px !important;
            border: 0 !important;
            font-size: 16px !important;
        }

    }
</style>
</head>
<body style="margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="center" style="padding: 30px 15px 70px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="90%" style="padding:0 0 20px 0;" class="responsive-table">
                <!-- TITLE -->
                <tr>
                    <td align="left" style=" font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #333333;" class="padding-copy">
           ' . $content . '
</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</html>';
    }

}
