<?php

namespace sourcinasia\appBundle\Listener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use sourcinasia\appBundle\Entity\Historycustomer;
use sourcinasia\appBundle\Entity\User;

class UserLogin {

    protected $doctrine;
    protected $container;

    public function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine, $container) {
        $this->doctrine = $doctrine;
        $this->container = $container;
    }

    public function onLogin(InteractiveLoginEvent $event) {
        $em = $this->doctrine->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if ($user->getContact()) {
            
            if ($user->getDomainerestriction() && $request->server->get('HTTP_HOST')!=$user->getDomainerestriction() ){
             header('Location: logout');       
            }
            
            $Historycustomer = new Historycustomer();
            $Historycustomer->setIco('<i class="icon-circle-arrow-right"></i>');
            $Historycustomer->setTitle('User login');
            $Historycustomer->setDescription('User login');
            $Historycustomer->setDate(new \DateTime());
            $Historycustomer->setUser($user);
            $Historycustomer->setCustomer($user->getContact()->getCustomer());
            $em->persist($Historycustomer);
            $em->flush();
        }
    }

}
