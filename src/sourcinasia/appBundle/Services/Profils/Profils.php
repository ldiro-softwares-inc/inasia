<?php

namespace sourcinasia\appBundle\Services\Profils;

//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\Security\Core\SecurityContext;
use sourcinasia\appBundle\Entity\Customer;

class Profils {

    protected $context;
    protected $session;

    /**
     * @param SecurityContext $context
     */
    public function __construct($token, $context, $session) {
        $this->session = $session;
        $this->token = $token;
        $this->context = $context;
        $this->get();
    }

    public function get($init = false) {
        if ($this->token->getToken()) {
            if ($this->context->isGranted('ROLE_CLIENT')) {
                $this->initProfil(true);
            }
        }

        if (!$this->session->get('profil') or $init) {
            $this->initProfil();
        }

        return $this->session->get('profil');
    }

    public function initProfil($reset = false) {
        //$HTTP_HOST = str_replace('.sourcinasia.com', '', $_SERVER['HTTP_HOST']);

        /* $profil = array();
          if (in_array($HTTP_HOST, array('betasales', 'sales'))) {
          $profil['mode'] = 'presentation';
          } else {
          $profil['mode'] = 'buy';
          } */
        $profil['mode'] = '';
        $profil['cutomer_name'] = 'INASIA';
        $profil['customer_nomenclatures'] = array();
        $profil['customer_nomenclaturerestriction'] = array();
        $profil['customer_supplierrestriction'] = array();
        $profil['customer_supplierexception'] = array();
        $profil['customer_zone'] = array();
        $profil['customer_logo'] = '/bundles/app/images/logoh.png';
        $profil['customer_coef'] = 0;
        $profil['commercial_coef'] = 10;
        $profil['commercial_compagnymargecoef'] = 10;
        $profil['cutomer_id'] = 0;
        $profil['commercialtools'] = false;
        $profil['contact_name'] = "";
        $profil['contact_phone'] = "";
        $profil['contact_mobile'] = "";
        $profil['contact_fax'] = "";
        $profil['contact_job'] = "";
        $profil['contact_qq'] = "";
        $profil['contact_skype'] = "";
        $profil['contact_saler'] = false;
        $profil['contact_type'] = 0;

        if ($this->token->getToken()) {
            if ($this->context->isGranted('ROLE_USER')) {
                $user = $this->token->getToken()->getUser();
                $this->setUser($user);
                if ($this->context->isGranted('ROLE_CLIENT')) {
                    if ($user->getContact()->GetCustomer()) {
                        if (method_exists($user->getContact(), 'GetCustomer')) {
                            $this->setCustomer($user->getContact()->GetCustomer());
                            $this->setContact($user->getContact());
                        } else {
                            $this->securityalert();
                        }
                    } else
                        $this->securityalert();
                } elseif ($this->context->isGranted('ROLE_ALLOWIP')) {
                    if ($reset)
                        $this->session->set('profil', $profil);
                } else {
                    if (!$this->ip()) {
                        $this->securityalert();
                    }
                    if ($this->context->isGranted('ROLE_COMMERCIAL') && $reset)
                        $this->session->set('profil', $profil);
                }
            } else
                $this->session->set('profil', $profil);
        } else
            $this->session->set('profil', $profil);
    }

    public function logout() {
        $this->initProfil(true);
    }

    public function setMode($mode = 'buy') {
        $profil = $this->session->get('profil');

        if ($profil['contact_type'] == 3) {
            $profil['mode'] = "presentation";
        } elseif ($profil['contact_type'] == 4) {
            $profil['mode'] = "commercial";
        } elseif ($profil['contact_type'] == 2) {
            $profil['mode'] = "commercial";
        } elseif ($profil['contact_type'] == 1) {
            $profil['mode'] = "commercial";
        } elseif ($profil['commercialtools'] && in_array($mode, array('buy', 'presentation', 'commercial'))) {
            $profil['mode'] = $mode;
        } else
            $profil['mode'] = 'buy';

        $this->session->set('profil', $profil);
    }

    public function setCommercialcoef($p) {
        $profil = $this->session->get('profil');
        if ($profil['contact_saler']) {
            $this->initProfil(true);
        } else {
            $p = (int) $p;
            if ($p) {
                $profil['commercial_coef'] = $p;
                $this->session->set('profil', $profil);
            }
        }
    }

    public function setCustomer(Customer $customer, $donotrecordsession = false) {
        if ($this->context->isGranted('ROLE_COMMERCIAL') || $this->context->isGranted('ROLE_PRODUCTION') || $this->context->isGranted('ROLE_SOURCING')) {

            $profil = $this->session->get('profil');
            $profil['contact_type'] = $customer->GetActivity()->getId() == 6 ? 4 : 0;
            $profil['cutomer_name'] = $customer->GetName();
            $profil['cutomer_id'] = $customer->Getid();
            $profil['commercialtools'] = $customer->GetCommercialtools();
            $profil['customer_nomenclatures'] = explode(',', $customer->GetNomenclatures());
            $profil['customer_supplierrestriction'] = $this->implodeEntities($customer->getSuppliersrestriction());
            $profil['customer_supplierexception'] = $this->implodeEntities($customer->getSuppliersexception());
            if ($customer->Getlogo())
                $profil['customer_logo'] = $customer->Getlogo()->getImage();
            else
                $profil['customer_logo'] = '/bundles/app/images/logoh.png';
            $profil['customer_zone'] = $this->implodeEntities($customer->getZones());
            $profil['customer_coef'] = $customer->GetMargecoef();

            if ($donotrecordsession)
                return $profil;

            $this->session->set('profil', $profil);

            if (empty($profil['mode']))
                $this->setMode();
        } elseif ($this->context->isGranted('ROLE_CLIENT')) {
            $profil = $this->session->get('profil');

            $profil['cutomer_name'] = $customer->GetName();
            $profil['cutomer_id'] = $customer->Getid();
            $profil['commercialtools'] = $customer->GetCommercialtools();
            $profil['customer_nomenclatures'] = explode(',', $customer->GetNomenclatures());
            $profil['customer_supplierrestriction'] = $this->implodeEntities($customer->getSuppliersrestriction());
            $profil['customer_zone'] = $this->implodeEntities($customer->getZones());
            $profil['customer_coef'] = $customer->GetMargecoef();
            $profil['commercial_compagnymargecoef'] = $customer->GetCustomermargecoef();

            if ($customer->Getlogo())
                $profil['customer_logo'] = $customer->Getlogo()->getImage();
            else
                $profil['customer_logo'] = '/bundles/app/images/logoh.png';

            $this->session->set('profil', $profil);


            if ($profil['contact_type'] == 3) {
                $this->setMode('presentation');
            } elseif ($profil['contact_type'] == 4) {
                $this->setMode('commercial');
            } elseif ($profil['contact_type'] == 2) {
                $this->setMode('commercial');
            } elseif ($profil['contact_type'] == 1) {
                $this->setMode('commercial');
            } elseif (empty($profil['mode']))
                $this->setMode();
        }

        return $profil;
    }

    public function setContact($contact) {
        if ($this->context->isGranted('ROLE_CLIENT')) {
            $profil = $this->session->get('profil');

            $profil['contact_name'] = $contact->GetName();
            $profil['contact_phone'] = $contact->GetPhone();
            $profil['contact_mobile'] = $contact->GetMobile();
            $profil['contact_fax'] = $contact->GetFax();
            $profil['contact_job'] = $contact->GetJob();
            $profil['contact_qq'] = $contact->GetQq();
            $profil['contact_skype'] = $contact->GetSkype();
            $profil['contact_type'] = $contact->GetType();

            if ($profil['contact_type'] == 3) {
                $profil['contact_saler'] = true;
            } elseif ($profil['contact_type'] == 4) {
                $profil['contact_saler'] = false;
            } elseif ($profil['contact_type'] == 2) {
                $profil['contact_saler'] = true;
            } elseif ($profil['contact_type'] == 1) {
                $profil['contact_saler'] = true;
            } else
                $profil['contact_saler'] = false;

            $this->session->set('profil', $profil);
        }

        return $profil;
    }

    public function setUser($user) {
        $profil = $this->session->get('profil');

        if ($profil['contact_saler'] && $this->context->isGranted('ROLE_CLIENT')) {
            $profil['commercial_coef'] = $user->getContact()->getCustomer()->getCustomermargecoef();
        } else {
            $profil['commercial_coef'] = $user->getCustomermargecoef();
        }

        $profil['contact_locale'] = $user->getLocale();
        $this->session->set('profil', $profil);
    }

    public function GetCustomerid() {
        if ($this->context->isGranted('ROLE_COMMERCIAL')) {
            $profil = $this->session->get('profil');
            if ($profil['cutomer_id'])
                return $profil['cutomer_id'];
        } elseif ($this->context->isGranted('ROLE_USER')) {
            $user = $this->token->getToken()->getUser();
            if (method_exists($user, 'getContact')) {
                if (method_exists($user->getContact(), 'getCustomer'))
                    return $user->getContact()->getCustomer()->getId();
            } else
                throw $this->createNotFoundException('No User connected');
        } else
            throw $this->createNotFoundException('No User connected');
    }

    private function implodeEntitiesNomenclature($entites) {
        $ids = array();
        foreach ($entites as $entite)
            $ids[$entite->getDepartment() . $entite->getShelves() . $entite->getFamilly()] = $entite->getDepartment() . $entite->getShelves() . $entite->getFamilly();
        return $ids;
    }

    /*
     * Service: profil, ExportController
     */

    private function implodeEntities($entites) {
        $ids = array();
        foreach ($entites as $entite)
            $ids[$entite->getId()] = $entite->getId();
        return $ids;
    }

    private function update($profil) {
        $this->session->set('profil', $profil);
        $this->profil = $profil;
    }

    protected function ip() {
        $ip = __DIR__ . '/../../ac';

        if (file_exists($ip))
            $ip = fgets(fopen($ip, "r"));

        return md5(trim($ip)) === md5(trim($_SERVER['REMOTE_ADDR']));
    }

    protected function securityalert() {

        $message = date('l jS \of F Y h:i:s A') . '<br/> IP address : <a href="http://www.localiser-ip.com/?ip=' . $_SERVER['REMOTE_ADDR'] . '">' . $_SERVER['REMOTE_ADDR'] . '</a>';

        if ($this->token->getToken())
            $message .= ' <br/> User :  ' . $this->token->getToken()->getUser()->getId() . ' - ' . $this->token->getToken()->getUser()->getUsername();

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        mail('f.aubert@inasia-corp.com', 'INASIA - Alert', $message, $headers);

        $this->session->getFlashBag()->add('notice', 'Security Alert : Your IP address (' . $_SERVER['REMOTE_ADDR'] . ') and your contact were sent to admin. Please contact us to if you need to connect : <a href="contact@inasia-corp.com">contact</a>');

        header('Location: /logout');
        die();
    }

}
