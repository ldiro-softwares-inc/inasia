<?php

namespace sourcinasia\appBundle\Services\Getwords;

class Getwords {

    function num_words($number) {
        $ones = array('', 'ONE', 'TWO', 'THREE', 'FOUR', 'FIVE', 'SIX', 'SEVEN', 'EIGHT', 'NINE');
        $tens = array('', 'ELEVEN', 'TWELVE', 'THIRTEEN', 'FOURTEEN', 'FIFTEEN', 'SIXTEEN', 'SEVENTEEN', 'EIGHTEEN', 'NINETEEN');
        $tens2 = array('', 'TEN', 'TWENTY', 'THIRTY', 'FORTY', 'FIFTY', 'SIXTY', 'SEVENTY', 'EIGHTY', 'NINETY');
        $tens3 = array('', 'hundred', 'thousand', 'million', 'billion', 'trillion');

        $numlenght = strlen($number);
        $numarray = str_split($number, 1);


        if ($number < 10) {
            return $ones[intval($number)];
        }

        //tens 
        if ($numlenght == 2 && $number < 20 && $numarray[1] <> 0) {
            return $tens[$number - 10];
        }


        if ($numlenght == 2 && $number < 20 && $numarray[1] == 0) {
            return $tens2[$numarray[0]];
        }

        if ($numlenght == 2 && $number > 19 && $numarray[1] == 0) {
            return $tens2[$numarray[0]];
        }

        if ($numlenght == 2 && $number > 19 && $numarray[1] <> 0) {
            return $tens2[$numarray[0]] . " " . $ones[$numarray[1]];
        }


        //hundreds 
        if ($numlenght == 3) {
            $x = $numarray[1] . $numarray[2];
            return $ones[$numarray[0]] . " HUNDRED " . $this->tens($x);
        }

        //THOUSANDS 
        if ($numlenght == 4) {
            $y = $numarray[1] . $numarray[2] . $numarray[3];
            $z = $numarray[2] . $numarray[3];
            if (intval($y) > 99) {
                return $ones[$numarray[0]] . " THOUSAND " . $this->hundreds($y);
            } else {
                return $ones[$numarray[0]] . " THOUSAND " . $this->tens($z);
            }
        }


        //tensthousand 

        if ($numlenght == 5) {
            $v = $numarray[0] . $numarray[1];
            $y = $numarray[2] . $numarray[3] . $numarray[4];
            $z = $numarray[3] . $numarray[4];

            if (intval($y) > 99) {
                return $this->tens($v) . " THOUSAND " . $this->hundreds($y);
            } else {
                return $this->tens($v) . " THOUSAND " . $this->tens($z);
            }
        }

        if ($numlenght == 6) {
            $v = $numarray[1] . $numarray[2];
            $y = $numarray[3] . $numarray[4] . $numarray[5];
            $z = $numarray[4] . $numarray[5];

            if (intval($y) > 99) {
                return $ones[$numarray[0]] . " HUNDRED " . $this->tens($v) . " THOUSAND " . $this->hundreds($y);
            } else {
                return $ones[$numarray[0]] . " HUNDRED " . $this->tens($v) . " THOUSAND " . $this->tens($z);
            }
        }
    }

    function tens($number) {
        $ones = array('', 'ONE', 'TWO', 'THREE', 'FOUR', 'FIVE', 'SIX', 'SEVEN', 'EIGHT', 'NINE');
        $tens = array('', 'ELEVEN', 'TWELVE', 'THIRTEEN', 'FOURTEEN', 'FIFTEEN', 'SIXTEEN', 'SEVENTEEN', 'EIGHTEEN', 'NINETEEN');
        $tens2 = array('', 'TEN', 'TWENTY', 'THIRTY', 'FORTY', 'FIFTY', 'SIXTY', 'SEVENTY', 'EIGHTY', 'NINETY');
        $tens3 = array('', 'hundred', 'thousand', 'million', 'billion', 'trillion');

        $numlenght = strlen($number);
        $numarray = str_split($number, 1);

        if ($number < 10) {
            return $ones[intval($number)];
        }
        if ($numlenght == 2 && $number < 20 && $numarray[1] <> 0) {
            return $tens[$number - 10];
        }

        if ($numlenght == 2 && $number < 20 && $numarray[1] == 0) {
            return $tens2[$numarray[0]];
        }

        if ($numlenght == 2 && $number > 19 && $numarray[1] == 0) {
            return $tens2[$numarray[0]];
        }

        if ($numlenght == 2 && $number > 19 && $numarray[1] <> 0) {
            return $tens2[$numarray[0]] . " " . $ones[$numarray[1]];
        }
    }

    function hundreds($number) {
        $ones = array('', 'ONE', 'TWO', 'THREE', 'FOUR', 'FIVE', 'SIX', 'SEVEN', 'EIGHT', 'NINE');
        $tens = array('', 'ELEVEN', 'TWELVE', 'THIRTEEN', 'FOURTEEN', 'FIFTEEN', 'SIXTEEN', 'SEVENTEEN', 'EIGHTEEN', 'NINETEEN');
        $tens2 = array('', 'TEN', 'TWENTY', 'THIRTY', 'FORTY', 'FIFTY', 'SIXTY', 'SEVENTY', 'EIGHTY', 'NINETY');
        $tens3 = array('', 'hundred', 'thousand', 'million', 'billion', 'trillion');

        $numlenght = strlen($number);
        $numarray = str_split($number, 1);

        if ($number < 10) {
            return $ones[intval($number)];
        }

        //tens 

        if ($numlenght == 2 && $number < 20 && $numarray[1] <> 0) {
            return $tens[$number - 10];
        }
        if ($numlenght == 2 && $number < 20 && $numarray[1] == 0) {
            return $tens2[$numarray[0]];
        }

        if ($numlenght == 2 && $number > 19 && $numarray[1] == 0) {
            return $tens2[$numarray[0]];
        }

        if ($numlenght == 2 && $number > 19 && $numarray[1] <> 0) {
            return $tens2[$numarray[0]] . " " . $ones[$numarray[1]];
        }

        //hundreds 
        if ($numlenght == 3) {
            $x = $numarray[1] . $numarray[2];
            return $ones[$numarray[0]] . " HUNDRED " . $this->tens($x);
        }
    }

}

?>