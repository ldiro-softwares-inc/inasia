<?php

namespace sourcinasia\appBundle\Services\Import;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use sourcinasia\appBundle\Entity\Codebar;

class Importxls {

    public function __construct(\Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
    }

    public $ext;
    public $file;
    public $filename;
    public $url;
    public $destination;
    public $post;
    public $allnomenclatures;
    public $defaultpol;
    public $datas;
    public $images;
    public $pols;
    public $currencies;
    public $units;

    public function importRequest($file, $post, $defaultpol, $allnomenclatures) {
        $this->defaultpol = $defaultpol;
        $this->allnomenclatures = $allnomenclatures;
        $this->post = $post;
        $this->url = __SOURCINASIA__ . 'tmp/';
        $this->file = $file['attachment']->getData();
        $this->ext = substr($this->file->getClientOriginalName(), -5);
        $this->makedirectroy();
    }

    public function importsimpleRequest($file, $post) {
        $this->post = $post;
        $this->url = __SOURCINASIA__ . 'tmp/';
        $this->file = $file['attachment']->getData();
        $this->ext = substr($this->file->getClientOriginalName(), -5);
        $this->makedirectroy();
    }

    public function moveTmpDir() {
        $this->filename = md5($this->file->getClientOriginalName()) . '.xlsx';
        $this->destination = $this->url . $this->filename;
        $this->file->move($this->url, $this->filename);
    }

    public function isallowExt() {
        return $this->ext == '.xlsx';
    }

    public function setPol($pol) {
        $this->pols = $pol;
    }

    public function setunits($units) {
        $this->units = $units;
    }

    public function setcurrencies($currencies) {
        $this->currencies = $currencies;
    }

    private function makedirectroy($dir = false) {
        $dir = ($dir) ? $dir : $this->url;
        if (!is_dir($dir)) {
            mkdir($dir); //  Note:  /htdocs/tmp must be created if it doesn't exist
            $fichier = fopen($dir . 'index.html', 'w+');
            fputs($fichier, "");
            fclose($fichier);
        }
    }

    public function ExctactRows() {

        $line = 0;
        $col = 0;
        $datas = array();
        $header = array();
        $supplieritemnumber = array();

        $objPHPExcel = \PHPExcel_IOFactory::load($this->destination);
        $sheet = $objPHPExcel->getSheet(0);

        $startline = (int) $this->post['startline'] ? (int) $this->post['startline'] : 1;

        foreach ($sheet->getRowIterator() as $line => $row) {
            $col = 0;
            $remove = true;
            if ($line == $startline)
                foreach ($row->getCellIterator() as $col => $cell) {
                    $header[$col] = str_replace(' ', '_', trim(strtolower($cell->getFormattedValue())));
                    $remove = false;
                } elseif ($line > $startline)
                foreach ($row->getCellIterator() as $col => $cell) {
                    $col = trim($col);
                    if (array_key_exists($col, $header)) {
                        if ($cell->getFormattedValue()) {
                            switch (strtolower($header[$col])) {
                                case 'fob_unit_price_usd':
                                    $value = (float) round(trim($cell->getValue()), 3);
                                    break;
                                case 'exw_unit_price':
                                    $value = (float) round(trim($cell->getValue()), 3);
                                    break;
                                default:
                                    $value = trim($cell->getFormattedValue());
                                    break;
                            }
                            $datas[$line - $startline - 1][$header[$col]] = $value;
                            $remove = false;
                        }
                    }
                }
            if (true) {
                if ($line > $startline) {
                    if ($remove)
                        unset($datas[$line - $startline - 1]);
                    else {
                        if (!empty($datas[$line - $startline - 1]['nomenclature'])) {
                            if (array_key_exists($datas[$line - $startline - 1]['nomenclature'], $this->allnomenclatures))
                                $datas[$line - $startline - 1]['nomenclature'] = "<b>" . $this->allnomenclatures[$datas[$line - $startline - 1]['nomenclature']] . '</b><br/>' . $datas[$line - $startline - 1]['nomenclature'];
                            else
                                $datas[$line - $startline - 1]['nomenclature'] = "";
                        } else
                            $datas[$line - $startline - 1]['nomenclature'] = "";
                    }
                }


                if (!empty($datas[$line - $startline - 1]['supplier_item_number'])) {
                    if (!in_array($datas[$line - $startline - 1]['supplier_item_number'], $supplieritemnumber))
                        $supplieritemnumber[] = $datas[$line - $startline - 1]['supplier_item_number'];
                    else
                        $datas[$line - $startline - 1]['supplier_item_number'] = "";
                }


                if (!empty($datas[$line - $startline - 1]['moq']))
                    $datas[$line - $startline - 1]['moq'] = (int) $datas[$line - $startline - 1]['moq'];

                if (!empty($datas[$line - $startline - 1]['moq_packing']))
                    $datas[$line - $startline - 1]['moq_packing'] = (int) $datas[$line - $startline - 1]['moq_packing'];

                if (!empty($datas[$line - $startline - 1]['cbm_packing']))
                    $datas[$line - $startline - 1]['cbm_packing'] = (float) $datas[$line - $startline - 1]['cbm_packing'];



                if (!empty($datas[$line - $startline - 1]['fob']))
                    $datas[$line - $startline - 1]['fob'] = (float) $datas[$line - $startline - 1]['fob'];

                if (!empty($datas[$line - $startline - 1]['exw_unit_price']))
                    $datas[$line - $startline - 1]['exw_unit_price'] = (float) $datas[$line - $startline - 1]['exw_unit_price'];

                if (!empty($datas[$line - $startline - 1]['pcb']))
                    $datas[$line - $startline - 1]['pcb'] = (int) $datas[$line - $startline - 1]['pcb'];



                if (!empty($datas[$line - $startline - 1]['qty_20_gp']))
                    $datas[$line - $startline - 1]['qty_20_gp'] = (int) $datas[$line - $startline - 1]['qty_20_gp'];

                if (!empty($datas[$line - $startline - 1]['qty_40_gp']))
                    $datas[$line - $startline - 1]['qty_40_gp'] = (int) $datas[$line - $startline - 1]['qty_40_gp'];

                if (!empty($datas[$line - $startline - 1]['qty_40_hq']))
                    $datas[$line - $startline - 1]['qty_40_hq'] = (int) $datas[$line - $startline - 1]['qty_40_hq'];

                if (!empty($datas[$line - $startline - 1]['nw_package']))
                    $datas[$line - $startline - 1]['nw_package'] = (float) $datas[$line - $startline - 1]['nw_package'];

                if (!empty($datas[$line - $startline - 1]['gw_package']))
                    $datas[$line - $startline - 1]['gw_package'] = (float) $datas[$line - $startline - 1]['gw_package'];

                if (!empty($datas[$line - $startline - 1]['nw_product']))
                    $datas[$line - $startline - 1]['nw_product'] = (float) $datas[$line - $startline - 1]['nw_product'];

                if (!empty($datas[$line - $startline - 1]['gw_product']))
                    $datas[$line - $startline - 1]['gw_product'] = (float) $datas[$line - $startline - 1]['gw_product'];

                if (!empty($datas[$line - $startline - 1]['nomenclature'])) {
                    if (!empty($datas[$line - $startline - 1]['ce_standard'])) {
                        if (strtoupper($datas[$line - $startline - 1]['ce_standard']) == 'YES')
                            $datas[$line - $startline - 1]['ce_standard'] = 'YES';
                        elseif (strtoupper($datas[$line - $startline - 1]['ce_standard']) == 'N/A')
                            $datas[$line - $startline - 1]['ce_standard'] = 'N/A';
                        else
                            $datas[$line - $startline - 1]['ce_standard'] = 'NO';
                    } else {
                        $datas[$line - $startline - 1]['ce_standard'] = 'N/A';
                    }
                }

                if (!empty($datas[$line - $startline - 1]['moq_packing_unit'])) {
                    $value = strtoupper($datas[$line - $startline - 1]['moq_packing_unit']);

                    $units = $this->units['moqpackingunit'];
                    if (array_key_exists($value, $units))
                        $datas[$line - $startline - 1]['moq_packing_unit'] = $units[$value];
                    else
                        $datas[$line - $startline - 1]['moq_packing_unit'] = "";
                }

                if (!empty($datas[$line - $startline - 1]['currency'])) {
                    $value = strtoupper($datas[$line - $startline - 1]['currency']);

                    if (array_key_exists($value, $this->currencies))
                        $datas[$line - $startline - 1]['currency'] = $value;
                    else
                        $datas[$line - $startline - 1]['currency'] = "";
                }

                if (!empty($datas[$line - $startline - 1]['moq_unit'])) {
                    $value = strtoupper($datas[$line - $startline - 1]['moq_unit']);

                    $units = $this->units['moqunit'];

                    if (array_key_exists($value, $units))
                        $datas[$line - $startline - 1]['moq_unit'] = $units[$value];
                    else
                        $datas[$line - $startline - 1]['moq_unit'] = "";
                }


                if ((!empty($datas[$line - $startline - 1]['fob_unit_price_usd']))) {

                    if ((!empty($datas[$line - $startline - 1]['fob_port']))) {
                        $value = strtoupper($datas[$line - $startline - 1]['fob_port']);
                        if (array_key_exists($value, $this->pols))
                            $datas[$line - $startline - 1]['fob_pol'] = $this->pols[$value];
                        else
                            $datas[$line - $startline - 1]['fob_pol'] = $this->defaultpol;
                    }else {
                        $datas[$line - $startline - 1]['fob_pol'] = $this->defaultpol;
                    }
                }
            }
        }

        return array('datas' => $datas, 'supplieritemnumber' => $supplieritemnumber);
    }

    public function ExctactImages($json = true) {
        $tmpdir = date('Ymdhis') . rand(1, 1000);
        $startline = 1;
        $datas = array();
        $objPHPExcel = \PHPExcel_IOFactory::load($this->destination);
        foreach ($objPHPExcel->getActiveSheet()->getDrawingCollection() as $key => $drawing) {
            $drawing->setName($drawing->getCoordinates());
            $line = (int) preg_replace('/[^0-9]/', "", strtolower($drawing->getCoordinates())) - $startline;
            $filemane = md5(date('Ymdhis') . rand(1, 1000) . $drawing->getCoordinates()) . '.' . $drawing->getExtension();
            $file = '/tmp/' . $tmpdir . '/' . $filemane;
            $this->makedirectroy(__SOURCINASIA__ . 'htdocs/tmp/' . $tmpdir . '/');
            copy($drawing->getPath(), __SOURCINASIA__ . 'htdocs' . $file);
            //Rotate 
            $img = new \claviska\SimpleImage(__SOURCINASIA__ . 'htdocs' . $file);
            $img->autoOrient()->toFile(__SOURCINASIA__ . 'htdocs' . $file);
            $datas[$line][] = array('line' => $line, 'filename' => $filemane, 'url' => $file);
        }
        ksort($datas);

        if ($json)
            return '[' . json_encode($datas) . ']';
        else
            return $datas;
    }

    /*
     * Copie pour SupplierController : extractAction
     */

    public function ExctactImagesDirectory() {
        $tmpdir = date('Ymdhis') . rand(1, 1000);
        $startline = 1;
        $datas = array();
        $objPHPExcel = \PHPExcel_IOFactory::load($this->destination);
        foreach ($objPHPExcel->getActiveSheet()->getDrawingCollection() as $key => $drawing) {
            $drawing->setName($drawing->getCoordinates());
            $line = (int) preg_replace("[^0-9]", "", strtolower($drawing->getCoordinates())) - $startline;
            $filemane = md5(date('Ymdhis') . rand(1, 1000) . $drawing->getCoordinates()) . '.' . $drawing->getExtension();
            $file = '/tmp/' . $tmpdir . '/' . $filemane;
            $this->makedirectroy(__SOURCINASIA__ . 'htdocs/tmp/' . $tmpdir . '/');
            copy($drawing->getPath(), __SOURCINASIA__ . 'htdocs' . $file);
            $datas[$line][] = array('line' => $line, 'filename' => $filemane, 'url' => $file);
        }
        ksort($datas);

        return __SOURCINASIA__ . 'htdocs/tmp/' . $tmpdir . '/';
    }

    public function getJsonPostedDatas($datas, $images) {
        $this->datas = json_decode($datas);
        $this->datas = $this->datas[0];

        $imagejson = json_decode($images);
        foreach ($imagejson[0] as $key => $value)
            $this->images[$key] = $value;
    }

    public function PrepareEntites($data, $product, $offer, $supplier, $doctrine) {

        if (!array_key_exists('Currency', $data))
            $offer->setCurrency($doctrine
                            ->getRepository('appBundle:Currency')
                            ->findOneBy(array('id' => 1)));

        foreach ($data as $car => $value) {
            switch ($car) {
                case 'nomenclature':
                    $value = explode('<br/>', $value);
                    if (count($value) == 2)
                        $product->setCategorie($doctrine
                                        ->getRepository('appBundle:Categorie')
                                        ->findOneBy(array('id' => $value[1])));
                    break;
                case 'supplier_item_number':
                    $offer->SetItemnumber($value);
                    break;
                case 'short_description_en':
                    $product->setSmalldescription($value);
                    break;
                case 'description_en':
                    $product->SetDescription($value);
                    break;
                case 'short_description_fr':
                    $product->setFrenchsmalldescription($value);
                    break;
                case 'description_fr':
                    $product->setFrenchdescription($value);
                    break;
                case 'ce_standard':
                    if ($value == 'YES')
                        $product->setCe('YES');
                    elseif ($value == 'NO')
                        $product->setCe('NO');
                    else
                        $product->setCe('N/A');
                    break;
                case 'color':
                    $product->setColoris($value);
                    break;
                case 'material':
                    $product->SetMaterial($value);
                    break;
                case 'packing':
                    $product->SetPacking($value);
                    break;
                case 'comments':
                    $product->SetComments($value);
                    break;
                case 'product_size_cm':
                    $product->setProductsize($value);
                    break;
                case 'packing_size_cm':
                    $product->setPackingsize($value);
                    break;
                case 'nw_product':
                    $product->SetNwproduct($value);
                    break;
                case 'gw_product':
                    $product->SetGwproduct($value);
                    break;
                case 'nw_package':
                    $product->SetNwpackage($value);
                    break;
                case 'gw_package':
                    $product->SetGwpackage($value);
                    break;
                case 'qty_20_gp':
                    $product->setQty20gp($value);
                    break;
                case 'qty_40_gp':
                    $product->setQty40gp($value);
                    break;
                case 'qty_40_hq':
                    $product->setQty40hq($value);
                    break;
                case 'pcb':
                    $product->SetPcb($value);
                    break;
                case 'moq_packing':
                    $offer->SetMoqpacking($value);
                    break;
                case 'moq_packing_unit':
                    $value = strtoupper($value);
                    $units = $this->units['moqpackingunit'];
                    if (array_key_exists($value, $units))
                        $offer->setMoqpackingunit(
                                $doctrine->getRepository('appBundle:Unit')
                                        ->findOneBy(array('title' => $units[$value])));
                    break;
                case 'moq':
                    $offer->setMoq($value);
                    break;
                case 'moq_unit':
                    $value = strtoupper($value);
                    $units = $this->units['moqunit'];
                    if (array_key_exists($value, $units))
                        $offer->setMoqunit($doctrine->getRepository('appBundle:Unit')
                                        ->findOneBy(array('title' => $units[$value])));
                    break;
                case 'gencod':
                    if (!$product->getCodebar()) {
                        if ($this->em->getRepository('appBundle:Codebar')->IsUnique($value)) {

                            $gendis = substr($value, 0, 8);
                            $chrono = substr($value, 8, 4);
                            if (strlen($value) == 13 && $gendis == "48951813") {

                                if ($chrono < $this->em->getRepository('appBundle:Codebar')->getChrono($value)) {
                                    $codebar = new Codebar();
                                    $codebar->setCodebar($value);
                                    $product->setCodebar($codebar);
                                }
                            } else {
                                $codebar = new Codebar();
                                $codebar->setCodebar($value);
                                $product->setCodebar($codebar);
                            }
                        }
                    }

                    break;
                case 'cbm_packing':
                    $product->setCbm($value);
                    break;
                case 'fob_unit_price_usd':
                    $offer->setFob($value);
                    break;
                case 'fob_pol':
                    if ($value) {
                        $offer->setPol($doctrine
                                        ->getRepository('appBundle:Pol')
                                        ->findOneBy(array('title' => $value)));
                    } else
                        $offer->setPol($supplier->getpol());
                    break;
                case 'exw_unit_price':
                    $offer->setExw($value);

                    $currencychange = $doctrine
                            ->getRepository('appBundle:Currency')
                            ->findOneBy(array('title' => $data->currency));

                    if ($currencychange)
                        if ($currencychange->getUsd())
                            $offer->setExwusd($value * $currencychange->getUsd());

                    break;
                case 'currency':
                    $offer->setCurrency($doctrine
                                    ->getRepository('appBundle:Currency')
                                    ->findOneBy(array('title' => $value)));
                    break;
            }
        }

        //is there a main offer ?
        $offer->SetProduct($product);
        $offer->SetSupplier($supplier);
        $offer->SetExwusdmodificationdate(new \dateTime());
        $offer->SetFobmodificationdate(new \dateTime());
        $offer->setMainoffer(1);
        $offer->setDeal(0);
        $offer->SetModified(new \DateTime());
        return array($product, $offer);
    }

}
