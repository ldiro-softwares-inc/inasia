<?php

namespace sourcinasia\appBundle\Services\Import;

use Doctrine\ORM\EntityManager;
use sourcinasia\appBundle\Entity\Container;
use sourcinasia\appBundle\Entity\Incoterm;
use sourcinasia\appBundle\Entity\Supplier;
use sourcinasia\appBundle\Entity\Suppliercategory;
use sourcinasia\appBundle\Entity\Contact;

class ImportCsv
{

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $datas
     * @param $className
     */
    public function importSuppliers($datas)
    {

        $customFields = ["supplier_category", "contact_name", "contact_office_phone", "contact_mobile", "contact_email", "contact_qq"];
        $incoterms = ["cif", "ddp", "exw", "fob"];
        $container = ["20gp", "40gp", "40hq", "lcl"];

        $created = 0;
        $updated = 0;

        foreach (json_decode($datas, true) as $line) {

            $Supplier = $this->em->getRepository('appBundle:Supplier')->find($line['id']);
            if ($Supplier) {
                $updated++;
            } else {
                $Supplier = new Supplier();
                $created++;
            }

            $contactToImport = [];
            foreach ($line as $key => $value) {
                if ($key != 'id' && $key != 'paymenterms') { //remove paymenterms when we will manage relationships
                    if (in_array($key, $customFields)) {
                        if ('supplier_category' === $key) {
                            if ($value && $value != "") {
                                if (!$Suppliercategory = $this->em->getRepository('appBundle:Suppliercategory')->findOneBy(['title' => $value])) {
                                    $Suppliercategory = new Suppliercategory();
                                    $Suppliercategory->setTitle($value);
                                    $this->em->persist($Supplier);
                                    $this->em->flush();
                                }
                                $Supplier->setSupplierCategory($Suppliercategory);
                            }
                        } else {
                            if (strpos($key, 'contact_') === 0) {
                                $explodeKey = explode('_', $key, 2);
                                $contact_key = array_pop($explodeKey);
                                $contactToImport[$contact_key] = $value;
                            }
                        }
                    } elseif (in_array($key, $container)) {
                        $supplierContainer = $this->em->getRepository('appBundle:Container')->findOneBy(['title' => strtoupper($key)]);

                        if (!empty($value)) {

                            if ($supplierContainer instanceof Container) {

                                if (!$Supplier->hasContainer($supplierContainer)) {
                                    $Supplier->addContainer($supplierContainer);
                                }
                            }
                        } else {

                            if ($supplierContainer instanceof Container) {
                                $Supplier->removeContainer($supplierContainer);

                            }
                        }

                    } elseif (in_array($key, $incoterms)) {

                        $supplierIncoterm = $this->em->getRepository('appBundle:Incoterm')->findOneBy(['title' => strtoupper($key)]);

                        if (!empty($value)) {

                            if ($supplierIncoterm instanceof Incoterm) {

                                if (!$Supplier->hasIncoterm($supplierIncoterm)) {
                                    $Supplier->addIncoterm($supplierIncoterm);
                                }
                            }

                        } else {

                            if ($supplierIncoterm instanceof Incoterm) {
                                $Supplier->removeIncoterm($supplierIncoterm);
                            }
                        }

                    } else {
                        $setter = 'set' . ucfirst($key);
                        $Supplier->$setter($value);
                    }
                }
            }

            $this->em->persist($Supplier);
            $this->em->flush();

            // Persist new Contact
            if (
                array_key_exists("email", $contactToImport) && $contactToImport["email"] &&
                array_key_exists("name", $contactToImport) && $contactToImport["name"] &&
                array_key_exists("office_phone", $contactToImport) &&
                array_key_exists("qq", $contactToImport)
            ) {
                $Contact = new Contact();
                $Contact->setEmail($contactToImport["email"]);
                $Contact->setName($contactToImport["name"]);
                $Contact->setPhone($contactToImport["office_phone"]);
                $Contact->setMobile($contactToImport["mobile"]);
                $Contact->setQq($contactToImport["qq"]);
                $Contact->setSupplier($Supplier);
                $this->em->persist($Contact);
                $this->em->flush();
            }
        }

        return [$created, $updated];
    }
}
