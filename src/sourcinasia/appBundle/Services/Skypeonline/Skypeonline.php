<?php

namespace sourcinasia\appBundle\Services\Skypeonline;

use Symfony\Component\HttpFoundation\Session\Session;

class Skypeonline {

    protected $session;

    /**
     * @param SecurityContext $context
     */
    public function __construct($session) {
        $this->session = $session;
    }

    function is_online($username) {

        /*         * *************************************
          Possible status  values:
          NUM        TEXT                DESCRIPTION
         * 0     UNKNOWN             Not opted in or no data available. 
         * 1     OFFLINE                 The user is Offline 
         * 2     ONLINE                  The user is Online 
         * 3     AWAY                    The user is Away 
         * 4     NOT AVAILABLE       The user is Not Available 
         * 5     DO NOT DISTURB  The user is Do Not Disturb (DND) 
         * 6     INVISIBLE               The user is Invisible or appears Offline 
         * 7     SKYPE ME                The user is in Skype Me mode
         * ************************************** */

        $skype = $this->session->get('skype_' . $username);

        if (empty($skype))
            $skype = $this->setData('skype_' . $username);
        elseif (date_diff(new \Datetime(), $skype['time'])->format('i') > 5)
            $skype = $this->setData('skype_' . $username);

        return $skype['statut'];
    }

    private function setData($username) {
        preg_match('/xml:lang="en">(.*)</', file_get_contents('http://mystatus.skype.com/' . $username . '.xml'), $match);

        $skype = array(
            'time' => new \DateTime(),
            'statut' => strtolower($match[1]) == "online"
        );
        $this->session->set($username, $skype);

        return $skype;
    }

}

