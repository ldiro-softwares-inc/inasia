<?php

namespace sourcinasia\appBundle\Services\Read;

class ReadCsv {

    public $fieldseparator = ";";
    public $lineseparator = "\n";

    public function readOld($csv) {
        $file = fopen($csv, "r");
        $size = filesize($csv);
        if (!$size) {
            exit;
        }
        $csvContent = fread($file, $size);
        fclose($file);

        $datas = [];
        $fieldsName = [];

        foreach (explode($this->lineseparator, $csvContent) as $index => $line) {
        	$data = [];
            foreach (explode($this->fieldseparator, $line) as $subIndex => $cel) {
	        	if($index == 0) {
        			$fieldsName[] = strtolower(trim($cel));
        		} else {
        			if($cel == 'NULL') {
        				$cel = null;
        			}
        			$cel = str_replace(array('"'), '', $cel);
                	$data[$fieldsName[$subIndex]] = trim($cel);
        		}
            }
            if($data) 
            	$datas[] = $data;
        }
        array_pop($datas); //last line seems to be empty (idk why) so i removed it 
        return $datas;
    }

    public function read($file){
        $arrResult  = array();
        $handle     = fopen($file, "r");
        if(empty($handle) === false) {
            $header = null;

            while(($data = fgetcsv($handle, 1000, $this->fieldseparator)) !== FALSE){
                if($header){
                    $data = array_combine($header, $data);
                    $arrResult[] = $data;
                }else{
                    $header = array_map('strtolower', $data);
                }
            }
            fclose($handle);
        }
        return $arrResult;
    }

}
