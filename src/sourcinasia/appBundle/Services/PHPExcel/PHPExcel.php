<?php

namespace sourcinasia\appBundle\Services\PHPExcel;

class PHPExcel
{

    private $obj;
    private $col = 0;
    private $colletter = 'A';
    private $row = 1;
    private $coord = 'A1';
    private $coordbeforeretour = 'A1';
    private $cl = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private $colMax = "1";
    private $colMaxLetter = "A";
    private $translator;

    /*
     *  ////////////////////////////////////////////// PUBLIC ///////////////////////////////////////////
     */

    public function displayheader($cadencier)
    {
        $this->obj->getActiveSheet()->setCellValue('K19', 'Date : ' . date('d-m-Y'));
        $this->obj->getActiveSheet()->setCellValue('K21', 'TITLE_INCOTERM : ' . $cadencier->getIncoterm()->GetTitle());
        $this->obj->getActiveSheet()->setCellValue(
            'K23', $this->translator->trans('TITLE_DELECERVYTIME') . ' : ' . (int)($cadencier->getSupplier()->getDeleverytime() + 10) . ' ' . $this->translator->trans('TITLE_DAYS')
        );
        $this->obj->getActiveSheet()->setCellValue(
            'K25', $this->translator->trans('TITLE_VALIDITYOFFER') . ' : ' . $cadencier->getSupplier()->getValidityoffer() . 'days'
        );
        $this->obj->getActiveSheet()->setCellValue(
            'K27', $this->translator->trans('TITLE_PAYMENTTERM') . ' : ' . $cadencier->getCustomerpaymenterms()->getTitle()
        );
        $this->obj->getActiveSheet()->setCellValue(
            'N19', $this->translator->trans('TITLE_REFERENCE_SUPPLIER') . ' : ' . $cadencier->getSupplier()->getId()
        );

        return $this;
    }

    public function displayPiElement($cadencier, $user)
    {
        $this->obj->getActiveSheet()->setCellValue('E3', 'PROFORMA INVOICE No:');
        $this->obj->getActiveSheet()->setCellValue('J3', $cadencier->GetId());
        $this->obj->getActiveSheet()->setCellValue('M5', str_replace('<br />', ' - ', nl2br($cadencier->GetComment())));
        if ($cadencier->getStepValidation() instanceof \datetime) {
            $this->obj->getActiveSheet()->setCellValue(
                'E10', 'Date : ' . $cadencier->getStepValidation()->format('d-m-Y')
            );
        } else {
            $this->obj->getActiveSheet()->setCellValue('E10', 'Date : ' . date('d-m-Y'));
        }
        $this->obj->getActiveSheet()->setCellValue(
            'E11', $this->translator->trans('TITLE_REFERENCE_SUPPLIER') . ' : ' . $cadencier->getSupplier()->getId()
        );
        $this->obj->getActiveSheet()->setCellValue('M4', $this->translator->trans('TITLE_COMMENTS'));
        $this->obj->getActiveSheet()->setCellValue('R4', $this->translator->trans('TITLE_SIGNATURE'));
        $this->obj->getActiveSheet()->setCellValue(
            'R11', str_replace(array('<br/>', '  '), ' ', $this->translator->trans('BANK_INFORMATION'))
        );

        $this->obj->getActiveSheet()->setCellValue(
            'E12', 'Incoterm : ' . $cadencier->getFinalincoterm() . '  ' . $cadencier->getFinalpol()
        );

        $this->obj->getActiveSheet()->setCellValue(
            'E13', $this->translator->trans('TITLE_DELECERVYTIME') . ' : ' . (int)($cadencier->getDeleverytime() + 10) . ' ' . $this->translator->trans('TITLE_DAYS')
        );

        $this->obj->getActiveSheet()->setCellValue(
            'E14', $this->translator->trans('TITLE_VALIDITYOFFER') . ' : ' . $cadencier->getSupplier()->getValidityoffer() . ' ' . $this->translator->trans('TITLE_DAYS')
        );
        if ($cadencier->GetCustomer()) {
            $this->obj->getActiveSheet()->setCellValue(
                'E15', $this->translator->trans('TITLE_PAYMENTTERM') . ' : ' . $cadencier->getCustomerpaymenterms()->getTitle()
            );
        }

        if ($cadencier->GetCustomer()) {
            if ($cadencier->GetCustomer()->getExportsalername() && $cadencier->GetCustomer()->getExportsaleremail()) {
                $commercialname = $cadencier->GetCustomer()->getExportsalername();
                $commercialemail = $cadencier->GetCustomer()->getExportsaleremail();
            } else {
                $commercial = $cadencier->GetCustomer()->GetMainsaler();
                if ($commercial) {
                    $commercialname = $commercial->GetName() . ' ' . $commercial->getPhone();
                    $commercialemail = $commercial->GetEmail();
                }
            }
            $this->obj->getActiveSheet()->setCellValue(
                'A17', $this->translator->trans('TITLE_SALER') . ' : ' . $commercialname
            );
            $this->obj->getActiveSheet()->setCellValue('F17', 'Email : ' . $commercialemail);
        }

        if ($cadencier->GetCustomer()) {
            $this->obj->getActiveSheet()->setCellValue('E4', $this->translator->trans('TITLE_CUSTOMER') . ' : ');
            $this->obj->getActiveSheet()->setCellValue('G5', $cadencier->GetCustomer()->GetName());
            $this->obj->getActiveSheet()->setCellValue('G6', $cadencier->GetCustomer()->GetAddress());
            $this->obj->getActiveSheet()->setCellValue('G7', $cadencier->GetCustomer()->GetCity());
            $this->obj->getActiveSheet()->setCellValue(
                'G8', $cadencier->GetCustomer()->GetPc() . ' ' . $cadencier->GetCustomer()->GetCountry()->GetTitle()
            );
        }

        return $this;
    }

    public function displayPlElement($command, $user)
    {
        $this->obj->getActiveSheet()->setCellValue('E3', 'PACKING LIST');
        $this->obj->getActiveSheet()->setCellValue('J3', $command->GetTcnum());
        $this->obj->getActiveSheet()->setCellValue('E10', 'Date : ' . date('d-m-Y'));
        $invoices = "/";
        foreach ($command->getCadenciers() as $cadencier) {

            foreach ($cadencier->getInvoicecustomers() as $invoice) {
                if ($invoice->getService() == 0) {
                    $invoices .= $invoice->getNumber() . '/';
                }
            }
            /* foreach ($cadencier->getInvoicesuppliers() as $invoice)
              $invoices .= $invoice->getNumber().'/'; */
        }

        $this->obj->getActiveSheet()->setCellValue('E11', 'CI No: ' . $invoices);
        $this->obj->getActiveSheet()->setCellValue(
            'E12', "CONTAINER" . ' : ' . $command->GetTcnum() . " VESSEL" . ' : ' . $command->GetVessel()
        );
        $this->obj->getActiveSheet()->setCellValue('E13', "SEAL" . ' : ' . $command->GetSealnum());
        $this->obj->getActiveSheet()->setCellValue('E14', "SHIPPED ON BOARD" . ' : ' . $command->GetEtd()->format('d-m-Y'));
        $this->obj->getActiveSheet()->setCellValue(
            'E15', $this->translator->trans('TITLE_PAYMENTTERM') . ' : ' . $cadencier->getCustomerpaymenterms()->getTitle()
        );

        if ($cadencier->GetCustomer()) {
            if ($cadencier->GetCustomer()->getExportsalername() && $cadencier->GetCustomer()->getExportsaleremail()) {
                $commercialname = $cadencier->GetCustomer()->getExportsalername();
                $commercialemail = $cadencier->GetCustomer()->getExportsaleremail();
            } else {
                $commercial = $cadencier->GetCustomer()->GetMainsaler();
                if ($commercial) {
                    $commercialname = $commercial->GetName() . ' ' . $commercial->GetPhone();
                    $commercialemail = $commercial->GetEmail();
                }
            }
            //$this->obj->getActiveSheet()->setCellValue('A17', $this->translator->trans('TITLE_SALER') . ' : ' . $commercialname);
            //$this->obj->getActiveSheet()->setCellValue('D17', 'EMAIL : ' . $commercialemail);
        }

        if ($cadencier->GetCustomer()) {
            $this->obj->getActiveSheet()->setCellValue('E4', $this->translator->trans('TITLE_CUSTOMER') . ' : ');
            $this->obj->getActiveSheet()->setCellValue('G5', $cadencier->GetCustomer()->GetName());
            $this->obj->getActiveSheet()->setCellValue('G6', $cadencier->GetCustomer()->GetAddress());
            $this->obj->getActiveSheet()->setCellValue('G7', $cadencier->GetCustomer()->GetCity());
            $this->obj->getActiveSheet()->setCellValue(
                'G8', $cadencier->GetCustomer()->GetPc() . ' ' . $cadencier->GetCustomer()->GetCountry()->GetTitle()
            );
        }

        return $this;
    }

    public function displayCiElement($ci, $user)
    {

        $cadencier = $ci->getCadencier();
        $ci = $this->doctrine->getRepository('appBundle:Invoicecustomer')->findOneById($ci);
        $this->obj->getActiveSheet()->setCellValue(
            'C3', 'COMMERCIAL INVOICE : ' . $ci->getNumber()
        );

        $this->obj->getActiveSheet()->setCellValue('C10', 'Date : ' . $ci->getDate()->format('d-m-Y'));

        $this->obj->getActiveSheet()->setCellValue('C11', "SOURCE" . ' : ' . $cadencier->getSupplier()->GetId() . ' - ORDER :' . $cadencier->getId());
        $this->obj->getActiveSheet()->setCellValue(
            'C12', "Incoterm" . ' : ' . $cadencier->getFinalincoterm() . '  ' . $cadencier->getFinalpol()
        );

        if (!$ci->getService()) {
            $this->obj->getActiveSheet()->setCellValue(
                'C13', "SEAL" . ' : ' . $cadencier->getCommand()->GetSealnum() . "   CONTAINER" . ' : ' . $cadencier->getCommand()->GetTcnum()
            );
            $this->obj->getActiveSheet()->setCellValue(
                'C14', "ETD/ETA" . ' : ' . $cadencier->GetCommand()->GetEtd()->format('d-m-Y') . '/' . $cadencier->GetCommand()->GetEta()->format('d-m-Y')
            );
        } else {
            $this->obj->getActiveSheet()->setCellValue(
                'C13', ""
            );
            $this->obj->getActiveSheet()->setCellValue(
                'C14', ""
            );
        }

        $this->obj->getActiveSheet()->setCellValue(
            'C15', $this->translator->trans('TITLE_PAYMENTTERM') . ' : ' . $cadencier->getCustomerpaymenterms()->getTitle()
        );

        if ($cadencier->GetCustomer()) {
            if ($cadencier->GetCustomer()->getExportsalername() && $cadencier->GetCustomer()->getExportsaleremail()) {
                $commercialname = $cadencier->GetCustomer()->getExportsalername();
                $commercialemail = $cadencier->GetCustomer()->getExportsaleremail();
            } else {
                $commercial = $cadencier->GetCustomer()->GetMainsaler();
                if ($commercial) {
                    $commercialname = $commercial->GetName() . ' ' . $commercial->getPhone();
                    $commercialemail = $commercial->GetEmail();
                }
            }
            //  $this->obj->getActiveSheet()->setCellValue('A17', $this->translator->trans('TITLE_SALER') . ' : ' . $commercialname);
            //   $this->obj->getActiveSheet()->setCellValue('D17', 'EMAIL : ' . $commercialemail);
        }

        if ($cadencier->GetCustomer()) {
            $this->obj->getActiveSheet()->setCellValue('C4', $this->translator->trans('Customer ') . ' : ');
            $this->obj->getActiveSheet()->setCellValue('D5', $cadencier->GetCustomer()->GetName());
            $this->obj->getActiveSheet()->setCellValue('D6', $cadencier->GetCustomer()->GetAddress());
            $this->obj->getActiveSheet()->setCellValue('D7', $cadencier->GetCustomer()->GetCity());
            $this->obj->getActiveSheet()->setCellValue(
                'D8', $cadencier->GetCustomer()->GetPc() . ' ' . $cadencier->GetCustomer()->GetCountry()->GetTitle()
            );
        }

        return $this;
    }

    public function displaySelectionheader($cadencier, $user)
    {
        $this->obj->getActiveSheet()->setCellValue('I3', 'QUOTATION SHEET No.');
        $this->obj->getActiveSheet()->setCellValue('N3', $cadencier->GetId());
        $this->obj->getActiveSheet()->setCellValue('Q5', str_replace('<br />', ' - ', nl2br($cadencier->GetComment())));
        $this->obj->getActiveSheet()->setCellValue('I10', 'Date : ' . date('d-m-Y'));
        $this->obj->getActiveSheet()->setCellValue(
            'I11', $this->translator->trans('TITLE_REFERENCE_SUPPLIER') . ' : ' . $cadencier->getSupplier()->getId()
        );
        $this->obj->getActiveSheet()->setCellValue('Q4', $this->translator->trans('TITLE_COMMENTS'));

        if (($cadencier->GetPol()) && ($cadencier->getIncoterm()->GetId() == 2)) {
            $this->obj->getActiveSheet()->setCellValue(
                'I12', 'Incoterm : ' . $cadencier->getIncoterm()->GetTitle() . '  ' . $cadencier->GetPol()->GetTitle()
            );
        } else {
            $this->obj->getActiveSheet()->setCellValue('I12', 'Incoterm : ' . $cadencier->getIncoterm()->GetTitle() . '  ');
        }

        $this->obj->getActiveSheet()->setCellValue(
            'I13', $this->translator->trans('TITLE_DELECERVYTIME') . ' : ' . (int)($cadencier->getDeleverytime() + 10) . ' ' . $this->translator->trans('TITLE_DAYS')
        );

        $this->obj->getActiveSheet()->setCellValue(
            'I14', $this->translator->trans('TITLE_VALIDITYOFFER') . ' : ' . $cadencier->getSupplier()->getValidityoffer() . ' ' . $this->translator->trans('TITLE_DAYS')
        );
        if ($cadencier->GetCustomer()) {
            $this->obj->getActiveSheet()->setCellValue(
                'I15', $this->translator->trans('TITLE_PAYMENTTERM') . ' : ' . $cadencier->getCustomerpaymenterms()->getTitle()
            );
        }

        if ($cadencier->GetCustomer()) {
            if ($cadencier->GetCustomer()->getExportsalername() && $cadencier->GetCustomer()->getExportsaleremail()) {
                $commercialname = $cadencier->GetCustomer()->getExportsalername();
                $commercialemail = $cadencier->GetCustomer()->getExportsaleremail();
            } else {
                $commercial = $cadencier->GetCustomer()->GetMainsaler();
                if ($commercial) {
                    $commercialname = $commercial->GetName() . ' ' . $commercial->getPhone();
                    $commercialemail = $commercial->GetEmail();
                }
            }
            $this->obj->getActiveSheet()->setCellValue(
                'A17', $this->translator->trans('TITLE_SALER') . ' : ' . $commercialname
            );
            $this->obj->getActiveSheet()->setCellValue('F17', 'Email : ' . $commercialemail);
        }

        if ($cadencier->GetCustomer() && $cadencier->getCustomer()->GetActivity()->getId() == 6) {
            $this->obj->getActiveSheet()->setCellValue('I4', '');
            $this->obj->getActiveSheet()->setCellValue('I5', $cadencier->getFinalcustomer());
        } elseif ($cadencier->GetCustomer()) {
            $this->obj->getActiveSheet()->setCellValue('I4', $this->translator->trans('TITLE_CUSTOMER') . ' : ');
            $this->obj->getActiveSheet()->setCellValue('J5', $cadencier->GetCustomer()->GetName());
            $this->obj->getActiveSheet()->setCellValue('J6', $cadencier->GetCustomer()->GetAddress());
            $this->obj->getActiveSheet()->setCellValue(
                'J7', $cadencier->GetCustomer()->GetPc() . ' ' . $cadencier->GetCustomer()->GetCity()
            );
            $this->obj->getActiveSheet()->setCellValue('J8', $cadencier->GetCustomer()->GetCountry()->GetTitle());
        } else {
            if ($cadencier->getFinalcustomer()) {
                $this->obj->getActiveSheet()->setCellValue('I4', '');
                $this->obj->getActiveSheet()->setCellValue('I5', $cadencier->getFinalcustomer());
            }
        }

        return $this;
    }

    public function displaySelectionheadercustomer($cadencier, $user)
    {
        $this->obj->getActiveSheet()->setCellValue('I3', 'QUOTATION SHEET No.');
        $this->obj->getActiveSheet()->setCellValue('N3', $cadencier->GetId());
        $this->obj->getActiveSheet()->setCellValue('Q5', str_replace('<br />', ' - ', nl2br($cadencier->GetComment())));
        $this->obj->getActiveSheet()->setCellValue('I10', 'Date : ' . date('d-m-Y'));
        $this->obj->getActiveSheet()->setCellValue(
            'I11', $this->translator->trans('TITLE_REFERENCE_SUPPLIER') . ' : ' . $cadencier->getSupplier()->getId()
        );
        $this->obj->getActiveSheet()->setCellValue('Q4', $this->translator->trans('TITLE_COMMENTS'));

        if ($cadencier->GetPol()) {
            $this->obj->getActiveSheet()->setCellValue(
                'I12', 'Incoterm : ' . $cadencier->getIncoterm()->GetTitle() . '  ' . $cadencier->GetPol()->GetTitle()
            );
        } else {
            $this->obj->getActiveSheet()->setCellValue('I12', 'Incoterm : ' . $cadencier->getIncoterm()->GetTitle() . '  ');
        }

        $this->obj->getActiveSheet()->setCellValue(
            'I13', $this->translator->trans('TITLE_DELECERVYTIME') . ' : ' . (int)($cadencier->getDeleverytime() + 10) . 'days'
        );

        $this->obj->getActiveSheet()->setCellValue(
            'I14', $this->translator->trans('TITLE_VALIDITYOFFER') . ' : ' . $cadencier->getSupplier()->getValidityoffer() . 'days'
        );
        if ($cadencier->GetCustomer()) {
            $this->obj->getActiveSheet()->setCellValue(
                'I15', $this->translator->trans('TITLE_PAYMENTTERM') . ' : ' . $cadencier->getCustomerpaymenterms()->GetTitle()
            );
        }

        $this->obj->getActiveSheet()->setCellValue(
            'A17', $this->translator->trans('TITLE_SALER') . ' : ' . $user->getName() . ' ' . $user->getPhone()
        );
        $this->obj->getActiveSheet()->setCellValue('F17', 'Email : ' . $user->getEmail());

        $image = $user->getContact()->getCustomer()->getLogo()->getImage();

        if ($image) {
            $image = __SOURCINASIA__ . 'htdocs/' . $image;
            if (file_exists($image)) {
                $info = getimagesize($image);
                if (in_array($info['mime'], array('image/gif', 'image/jpeg', 'image/png'))) {
                    $objDrawing = new \PHPExcel_Worksheet_Drawing();
                    $objDrawing->setName('INASIA');
                    $objDrawing->setDescription('INASIA');
                    $objDrawing->setPath($image);
                    $objDrawing->setHeight(200);
                    $objDrawing->setCoordinates('A1');
                    $objDrawing->setOffsetX(0);
                    $objDrawing->setWorksheet($this->obj->getActiveSheet());
                }
            }
        }

        $this->obj->getActiveSheet()->setCellValue('A10', $user->getContact()->getCustomer()->getName());
        $this->obj->getActiveSheet()->setCellValue(
            'A11', $user->getContact()->getCustomer()->getAddress() . ' ' . $user->getContact()->getCustomer()->getPc()
        );
        $this->obj->getActiveSheet()->setCellValue('A12', $user->getContact()->getCustomer()->getCity());

        if ($cadencier->getFinalcustomer()) {
            $this->obj->getActiveSheet()->setCellValue('I4', '');
            $this->obj->getActiveSheet()->setCellValue('I5', $cadencier->getFinalcustomer());
        }

        return $this;
    }

    public function displaySupplierheader($cadencier)
    {
        $this->obj->getActiveSheet()->setCellValue('I3', 'PI REQUEST No.');
        $this->obj->getActiveSheet()->setCellValue('N3', $cadencier->GetId());
        $this->obj->getActiveSheet()->setCellValue('I10', 'Date : ' . date('d-m-Y'));
        $this->obj->getActiveSheet()->setCellValue(
            'I11', $this->translator->trans('TITLE_REFERENCE_SUPPLIER') . ' : ' . $cadencier->getSupplier()->getId()
        );
        $this->obj->getActiveSheet()->setCellValue('Q4', $this->translator->trans('TITLE_COMMENTS'));
        if ($cadencier->GetPol()) {
            $this->obj->getActiveSheet()->setCellValue(
                'I12', 'Incoterm : ' . $cadencier->getIncoterm()->GetTitle() . '  ' . $cadencier->GetPol()->GetTitle()
            );
        } else {
            $this->obj->getActiveSheet()->setCellValue('I12', 'Incoterm : ' . $cadencier->getIncoterm()->GetTitle() . '  ');
        }

        $this->obj->getActiveSheet()->setCellValue(
            'I13', $this->translator->trans('TITLE_DELECERVYTIME') . ' : ' . (int)($cadencier->getSupplier()->getDeleverytime()) . 'days'
        );

        $this->obj->getActiveSheet()->setCellValue(
            'I14', $this->translator->trans('TITLE_VALIDITYOFFER') . ' : ' . $cadencier->getSupplier()->getValidityoffer() . 'days'
        );

        if ($cadencier->getSupplierpaymenterms()) {
            $this->obj->getActiveSheet()->setCellValue(
                'I15', $this->translator->trans('TITLE_PAYMENTTERM') . ' : ' . $cadencier->getSupplier()->getPaymenterms()->GetTitle()
            );
        } else {
            $this->obj->getActiveSheet()->setCellValue('I15', $this->translator->trans('TITLE_PAYMENTTERM') . ' : ');
        }

        $this->obj->getActiveSheet()->setCellValue('A17', $this->translator->trans('TITLE_SALER') . ' : SUPPLY CHAIN');
        $this->obj->getActiveSheet()->setCellValue('F17', 'Email : supplychain@inasia-corp.com');

        if ($cadencier->GetCustomer()) {
            $this->obj->getActiveSheet()->setCellValue('I4', $this->translator->trans('TITLE_SUPPLIER') . ' : ' . $cadencier->getSupplier()->GetName());
            foreach ($cadencier->getSupplier()->getAddresses() as $adresse) {
                if ($adresse->GetType() == "Office") {
                    $this->obj->getActiveSheet()->setCellValue(
                        'I5', $adresse->GetAddress() . ' ' . $adresse->GetCity()->getTitle()
                    );
                }
            }
        }

        return $this;
    }

    /*
     * SOURCING
     */

    public function exportcadencierRender($offers)
    {

        $obligatoire = array('background-color' => 'F28A8C');
        $css = array('cel-align' => 'center', 'width' => 'auto', 'wrap' => true);
        $obligatoire = array_merge($css, $obligatoire);

        $this->setRowHeight('75pt');

        $this->set('INASIA N')->style($css);
        $this->set('Supplier Item Number')->style($css);
        $this->set('Nomenclature')->style($css);
        $this->set('GENCOD')->style($css);
        $this->set('Short Description EN')->style($obligatoire);
        $this->set('Short Description FR')->style($obligatoire);
        $this->set('Description EN')->style($obligatoire);
        $this->set('Description FR')->style($obligatoire);
        $this->set('Color')->style($css);
        $this->set('Material')->style($css);
        $this->set('Packing')->style($css);
        $this->set('Comments')->style($css);
        $this->set('CBM Packing')->style($obligatoire);
        $this->set('PCB')->style($obligatoire);
        $this->set('Packing Size cm')->style($css);
        $this->set('Product Size cm')->style($css);
        $this->set('NW product')->style($css);
        $this->set('GW product')->style($css);
        $this->set('NW Package')->style($css);
        $this->set('GW Package')->style($css);
        $this->set('QTY 20 GP')->style($css);
        $this->set('QTY 40 GP')->style($css);
        $this->set('QTY 40 HQ')->style($css);
        $this->set('CE Standard')->style($css);
        $this->set('EXW Unit Price')->style($obligatoire);
        $this->set('Currency')->style($obligatoire);
        $this->set('FOB Unit Price USD')->style($obligatoire);
        $this->set('FOB PORT')->style($css);
        $this->set('MOQ')->style($obligatoire);
        $this->set('MOQ UNIT')->style($obligatoire);
        $this->set('MOQ Packing')->style($obligatoire);
        $this->set('MOQ Packing Unit')->style($obligatoire);
        $this->retour();

        $css = array('cel-align' => 'center');
        foreach ($offers as $key => $offer) {
            $this->setRowHeight('50pt');
            $this->set($offer->getProduct()->getId())->style($css);
            $this->set($offer->getItemnumber())->style($css);
            $this->set($offer->getProduct()->getCategorie()->getId())->style($css);
            if ($offer->GetProduct()->GetCodebar()) {
                $this->set(" " . $offer->GetProduct()->GetCodebar()->GetCodebar())->style($css);
            } else {
                $this->set('')->style($css);
            }
            $this->set($offer->getProduct()->getSmalldescription())->style($css);
            $this->set($offer->getProduct()->getFrenchsmalldescription())->style($css);
            $this->set($offer->getProduct()->getDescription())->style($css);
            $this->set($offer->getProduct()->getFrenchdescription())->style($css);
            $this->set($offer->getProduct()->getColoris())->style($css);
            $this->set($offer->getProduct()->getMaterial())->style($css);
            $this->set($offer->getProduct()->getPacking())->style($css);
            $this->set($offer->getProduct()->getComments())->style($css);
            $this->set($offer->getProduct()->getCbm())->style($css);
            $this->set($offer->getProduct()->getPcb())->style($css);
            $this->set($offer->getProduct()->getPackingsize())->style($css);
            $this->set($offer->getProduct()->getProductsize())->style($css);
            $this->set($offer->getProduct()->getNwproduct())->style($css);
            $this->set($offer->getProduct()->getGwproduct())->style($css);
            $this->set($offer->getProduct()->getNwpackage())->style($css);
            $this->set($offer->getProduct()->getGwpackage())->style($css);
            $this->set($offer->getProduct()->getQty20gp())->style($css);
            $this->set($offer->getProduct()->getQty40gp())->style($css);
            $this->set($offer->getProduct()->getQty40hq())->style($css);
            $this->set($offer->getProduct()->getCe())->style($css);
            $this->set($offer->getExw())->style($css);
            $this->set($offer->getCurrency()->GetTitle())->style($css);
            $this->set($offer->getFob())->style($css);
            if ($offer->getPol()) {
                $this->set($offer->getPol()->GetTitle())->style($css);
            } else {
                $this->set('')->style($css);
            }
            $this->set($offer->getMoq())->style($css);
            $this->set($offer->getMoqunit()->GetTitle())->style($css);
            $this->set($offer->getMoqpacking())->style($css);
            if ($offer->getMoqpackingunit()) {
                $this->set($offer->getMoqpackingunit()->GetTitle())->style($css);
            } else {
                $this->set('')->style($css);
            }
            $this->retour();
        }

        //$this->obj->getActiveSheet()->getProtection()->setSheet(true);

        //$this->setUnProtection('A2:' . $this->coordbeforeretour);

        return $this;
    }

    /*
     * PI SUPPLIER
     */

    public function exportproductionupdatepriceRender($offers, $cadencier)
    {
        $selection = $cadencier->getSelection();
        $locale = $this->translator->getLocale();
        $css = array('cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64');

        $this->set('Supplier Num')->style($css);
        $this->set('INASIA Num')->style($css);
        //$this->set('Nomenclature')->style($css);
        $this->set('Picture')->style($css);
        $this->set('Short Description EN')->style($css);
        $this->set('Description EN')->style($css);
        $this->set('Color')->style($css);
        $this->set('Material')->style($css);
        $this->set('Packing')->style($css);
        $this->set('Comments')->style($css);
        $this->set('CBM Packing')->style($css);
        $this->set('PCB')->style($css);
        $this->set('Packing Size cm')->style($css);
        $this->set('Product Size cm')->style($css);
        $this->set('NW product')->style($css);
        $this->set('GW product')->style($css);
        $this->set('NW Package')->style($css);
        $this->set('GW Package')->style($css);
        $this->set('QTY 20 GP')->style($css);
        $this->set('QTY 40 GP')->style($css);
        $this->set('QTY 40 HQ')->style($css);
        $this->set('CE Standard')->style($css);
        $this->set('QTY')->style($css);
        if ($cadencier->GetIncoterm()->GetTitle() == 'FOB') {
            $this->set('FOB Unit Price USD')->style($css);
        } else {
            $this->set('EXW Unit Price')->style($css);
            $this->set('Currency')->style($css);
        }
        $this->set('FOB PORT')->style($css);

        $this->retour();

        $css = array('cel-align' => 'center', 'wrap' => true);
        foreach ($offers as $key => $offer) {
            $this->setRowHeight('50pt');
            $this->set($offer->getItemnumber())->style($css);
            $this->set($offer->getProduct()->getId())->style($css);
            //$this->set($offer->getProduct()->getCategorie()->getId())->style($css);
            $this->setCelImage(str_replace('original.jpg', 'small.jpg', $offer->getProduct()->getMainimage()))->style($css);
            $this->set($offer->getProduct()->getSmalldescription())->style($css);
            $this->set($offer->getProduct()->getDescription())->style($css);
            $this->set($offer->getProduct()->getColoris())->style($css);
            $this->set($offer->getProduct()->getMaterial())->style($css);
            $this->set($offer->getProduct()->getPacking())->style($css);
            $this->set($offer->getProduct()->getComments())->style($css);
            $this->set($offer->getProduct()->getCbm())->style($css);
            $this->set($offer->getProduct()->getPcb())->style($css);
            $this->set($offer->getProduct()->getPackingsize())->style($css);
            $this->set($offer->getProduct()->getProductsize())->style($css);
            $this->set($offer->getProduct()->getNwproduct())->style($css);
            $this->set($offer->getProduct()->getGwproduct())->style($css);
            $this->set($offer->getProduct()->getGwpackage())->style($css);
            $this->set($offer->getProduct()->getNwpackage())->style($css);
            $this->set($offer->getProduct()->getQty20gp())->style($css);
            $this->set($offer->getProduct()->getQty40gp())->style($css);
            $this->set($offer->getProduct()->getQty40hq())->style($css);
            $this->set($offer->getProduct()->getCe())->style($css);
            $this->set($selection['o' . $offer->getId()]['qty'])->style(
                array_merge($css, array('background-color' => 'fbde64'))
            );
            if ($cadencier->GetIncoterm()->GetTitle() == 'FOB') {
                $this->set($offer->getFob())->style(array_merge($css, array('background-color' => 'fbde64')));
            } else {
                $this->set($offer->getExw())->style(array_merge($css, array('background-color' => 'fbde64')));
                $this->set($offer->getCurrency()->GetTitle())->style(
                    array_merge($css, array('background-color' => 'fbde64'))
                );
            }
            if ($offer->getPol()) {
                $this->set($offer->getPol()->GetTitle())->style($css);
            } else {
                $this->set('')->style($css);
            }
            $this->retour();
        }

        return $this;
    }

    /*
     * COMMERCIAL EXPORT FOR CUSTOMER
     */

    public function exportQuotationCustomerRender($offers, $cadencier, $profil, $issaler = false, $insertqty = false)
    {

        $locale = $this->translator->getLocale();
        $selection = $cadencier->getSelection();
        $css = array('cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64');
        $this->setRowHeight('50pt');

        $this->set($this->translator->trans('TITLE_ITEM_NO'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));

        if ($issaler) {
            $this->set('Description EN')->style(array('cel-align' => 'center', 'width' => '60px', 'wrap' => true, 'background-color' => 'fbde64'));
            $this->set('Description FR')->style(array('cel-align' => 'center', 'width' => '60px', 'wrap' => true, 'background-color' => 'fbde64'));
        } else {
            $this->set('Description')->style(array('cel-align' => 'center', 'width' => '60px', 'wrap' => true, 'background-color' => 'fbde64'));
        }
        $this->set($this->translator->trans('TITLE_PHOTO'))->style(array('cel-align' => 'center', 'width' => '30px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_PRODUCTSIZE'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_PACKINGSIZE'))->style(array('cel-align' => 'center', 'width' => '20px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_MATERIAL'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_COLORIS'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_PACKING'))->style(array('cel-align' => 'center', 'width' => '20px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_COMMENTS'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_NWPRODUCT'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_NGPRODUCT'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_NWPACKAGE'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_NGPACKAGE'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_PCB'))->style($css);
        $this->set('CBM')->style($css);
        $this->set('MOQ')->style($css);
        $this->set($this->translator->trans('ENTITY_OFFER_MOQUNIT'))->style($css);
        $this->set($this->translator->trans('TITLE_UNIT_PRICE'))->style($css);
        $this->set($this->translator->trans('TITLE_ORDER_QUANTITY'))->style($css);
        $this->set('Total CBM')->style($css);
        $this->set('Total USD')->style($css);

        $this->retour();
        $css = array('cel-align' => 'center', 'wrap' => true);
        foreach ($offers as $key => $offer) {
            $this->setRowHeight('100pt');

            $this->set($offer->getProduct()->getId())->style($css);

            $descrptioncolomn = $this->coord;

            if ($issaler) {
                $this->set($offer->getProduct()->getDescription())->style($css);
                $this->set($offer->getProduct()->getFrenchdescription())->style($css);
            } else {
                if ($locale == "fr") {
                    $this->set($offer->getProduct()->getFrenchdescription())->style($css);
                } else {
                    $this->set($offer->getProduct()->getDescription())->style($css);
                }
            }

            $this->setCelImage(str_replace('original.jpg', 'small.jpg', $offer->getProduct()->getMainimage()))->style($css);
            $this->set($offer->getProduct()->getProductsize())->style($css);
            $this->set($offer->getProduct()->getPackingsize())->style($css);
            $this->set($offer->getProduct()->getMaterial())->style($css);
            $this->set($offer->getProduct()->getColoris())->style($css);
            $this->set($offer->getProduct()->getPacking())->style($css);
            $this->set($offer->getProduct()->getComments())->style($css);
            $this->set($offer->getProduct()->getNwproduct())->style($css);
            $this->set($offer->getProduct()->getGwproduct())->style($css);
            $this->set($offer->getProduct()->getNwpackage())->style($css);
            $this->set($offer->getProduct()->getGwpackage())->style($css);
            $coordpcb = $this->coord;
            $this->set($offer->getProduct()->getPcb())->style($css);
            $coordcbm = $this->coord;
            $this->set($offer->getProduct()->getCbm())->style($css);
            $this->set($offer->getMoq($profil['cutomer_id']))->style($css);
            $this->set($offer->getMoqunit()->GetTitle())->style($css);
            $coordprice = $this->coord;
            if ($cadencier->GetIncoterm()->GetTitle() == 'FOB') {
                $this->setCelFormat('CURRENCY_USD');
                $this->set($offer->getSaleprice('fob', $profil))->style($css);
            } else {
                $this->setCelFormat('CURRENCY_USD');
                $this->set($offer->getSaleprice('exw', $profil))->style($css);
            }
            $coordqty = $this->coord;
            $coordqtyletter = $this->colletter;
            if ($insertqty && array_key_exists('o' . $offer->getId(), $selection)) {
                $this->set($selection['o' . $offer->getId()]['qty'])->style($css);
            } else {
                $this->set('0')->style($css);
            }
            $coordtotalcbm = $this->colletter;
            $this->setCelCalValue('=' . $coordqty . '*' . $coordcbm . '/' . $coordpcb)->setFormat('0.000')->style($css);
            $coordtotalusd = $this->colletter;
            $this->setCelCalValue('=' . $coordqty . '*' . $coordprice)->setCelFormat('CURRENCY_USD')->style($css);
            $this->retour();
        }

        $start = $this->row - count($offers) - 1;

        $totalLine = $this->row - count($offers) - 1;

        /*$this->setCelCalValue(
             '=sum(' . $coordtotalcbm . (int)($totalLine + 1) . ':' . $coordtotalcbm . (int)($this->row - 1) . ')', $coordtotalcbm . (int)($totalLine - 1)
         )->style($css);
         $this->setCelCalValue(
             '=sum(' . $coordtotalusd . (int)($totalLine + 1) . ':' . $coordtotalusd . (int)($this->row - 1) . ')', $coordtotalusd . (int)($totalLine - 1)
         )->style($css);*/

        //$this->obj->getActiveSheet()->setAutoFilter('A' . (string) $start . ':V' . $this->row);
        return $this;
    }

    /*
     * PI CUSTOMER
     */

    public function exportQuotationPiCustomerRender($offers, $cadencier, $profil, $issaler = false, $insertqty = false)
    {

        $locale = $this->translator->getLocale();
        $selection = $cadencier->getSelection();
        $css = array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64');

        $this->setRowHeight('50pt');

        $this->set($this->translator->trans('TITLE_ITEM_NO'))->style($css);

        $this->set('Description')->style(array('width' => 60, 'cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('TITLE_PHOTO'))->style(array('width' => 30, 'cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_PRODUCTSIZE'))->style(array('width' => 15, 'cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_PACKINGSIZE'))->style(array('width' => 15, 'cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_MATERIAL'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_COLORIS'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_PACKING'))->style(array('width' => 20, 'cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_COMMENTS'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_NWPRODUCT'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_NGPRODUCT'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_NWPACKAGE'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_NGPACKAGE'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_PCB'))->style($css);
        $this->set('CBM')->style($css);
        $this->set($this->translator->trans('TITLE_UNIT_PRICE') . ' ' . $cadencier->getFinaldevis())->style($css);
        $this->set($this->translator->trans('TITLE_ORDER_QUANTITY'))->style($css);
        $this->set('Total CBM')->style($css);
        $this->set('Total' . ' ' . $cadencier->getFinaldevis())->style(array('width' => 30, 'cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64'));

        $this->retour();
        $css = array('cel-align' => 'center', 'wrap' => true);
        foreach ($offers as $key => $offer) {
            $this->setRowHeight('100pt');

            $this->set($offer->getProduct()->getId())->style($css);

            $descrptioncolomn = $this->coord;
            if ($locale == "fr") {
                $this->set($offer->getProduct()->getFrenchdescription())->style($css);
            } else {
                $this->set($offer->getProduct()->getDescription())->style($css);
            }

            $this->setCelImage(str_replace('original.jpg', 'small.jpg', $offer->getProduct()->getMainimage()))->style($css);
            $this->set($offer->getProduct()->getProductsize())->style($css);
            $this->set($offer->getProduct()->getPackingsize())->style($css);
            $this->set($offer->getProduct()->getMaterial())->style($css);
            $this->set($offer->getProduct()->getColoris())->style($css);
            $this->set($offer->getProduct()->getPacking())->style($css);
            $this->set($offer->getProduct()->getComments())->style($css);
            $this->set($offer->getProduct()->getNwproduct())->style($css);
            $this->set($offer->getProduct()->getGwproduct())->style($css);
            $this->set($offer->getProduct()->getNwpackage())->style($css);
            $this->set($offer->getProduct()->getGwpackage())->style($css);
            $coordpcb = $this->coord;
            $this->set($offer->getProduct()->getPcb())->style($css);
            $coordcbm = $this->coord;
            $this->set($offer->getProduct()->getCbm())->style($css);
            $coordprice = $this->coord;
            $coordpricecol = $this->colletter;
            $this->set($selection['o' . $offer->getId()]['finalprice'])->style($css);
            $coordqty = $this->coord;
            $coordqtyletter = $this->colletter;
            if ($insertqty && array_key_exists('o' . $offer->getId(), $selection)) {
                $this->set($selection['o' . $offer->getId()]['qty'])->style($css);
            } else {
                $this->set('0')->style($css);
            }
            $coordtotalcbm = $this->colletter;
            //formule total CBM faux: Qty * CBM / PCB
            $this->setCelCalValue('=' . $coordqty . '*' . $coordcbm . '/' . $coordpcb)->style($css);
            $coordtotalusd = $this->colletter;
            $this->setCelCalValue('=' . $coordqty . '*' . $coordprice)->setCelFormat('CURRENCY_USD')->style($css);
            $this->retour();
        }

        $start = $this->row - count($offers) - 1;
        $totalLine = $this->row - count($offers) - 1;
        $totalservice = $coordtotalusd . $this->row;

        if (is_array($cadencier->getServicedescription())) {
            foreach ($cadencier->getServicedescription() as $service) {
                if (array_key_exists('description', $service) && array_key_exists('value', $service)) {
                    $this->set("SERVICES", "A" . $this->row)->style($css);
                    $this->set(1, $coordqtyletter . $this->row)->style(array('cel-align' => 'center', 'wrap' => true), $coordqtyletter . $this->row);
                    $this->set($service['description'], "B" . $this->row)->style(array('cel-align' => 'center', 'wrap' => true), "B" . $this->row);
                    $this->set($service['value'], $coordpricecol . $this->row)->style(array('cel-align' => 'center', 'wrap' => true), $coordpricecol . $this->row);
                    $this->setCelCalValue(
                        '=' . $coordqtyletter . $this->row . '*' . $coordpricecol . $this->row, $coordtotalusd . (int)($this->row)
                    )->setCelFormat('CURRENCY_USD', $coordtotalusd . $this->row)->style(array('cel-align' => 'center', 'wrap' => true), $coordtotalusd . $this->row);
                    $this->retour();
                }
            }
        }


        if ((float)$cadencier->GetFinaltva()) {
            $this->set("TOTAL HT", $coordtotalcbm . $this->row)->style($css);
        } else {
            $this->set("TOTAL", $coordtotalcbm . $this->row)->style($css);
        }
        $total = $coordtotalusd . (int)($this->row);
        $this->setCelCalValue(
            '=sum(' . $coordtotalusd . (int)($totalLine + 1) . ':' . $coordtotalusd . (int)($this->row - 1) . ')', $coordtotalusd . (int)($this->row)
        )->setCelFormat('CURRENCY_USD', $coordtotalusd . (int)($this->row))->style($css, $coordtotalusd . (int)($this->row));
        $this->retour();

        if ((float)$cadencier->GetFinaltva()) {
            $this->set('TVA ' . (float)$cadencier->GetFinaltva() . '%', $coordtotalcbm . (int)($this->row))->style($css);
            $tva = $coordtotalusd . (int)($this->row);
            $this->setCelCalValue(
                '=' . $total . '*' . (float)$cadencier->GetFinaltva() . '/100', $coordtotalusd . (int)($this->row)
            )->style($css, $coordtotalusd . (int)($this->row));
            $this->retour();
            $this->set("Total TTC", $coordtotalcbm . $this->row)->style($css);
            $this->setCelCalValue('=' . $total . '+' . $tva, $coordtotalusd . (int)($this->row))->style($css, $coordtotalusd . (int)($this->row));
        }

        /* $totalLine = $this->row - count($offers) - 1;
          $this->set('Total', $coordqtyletter . $this->row)->style($css);
          $this->setCelCalValue('=sum(' . $coordtotalcbm . (int) ($totalLine + 1 ) . ':' . $coordtotalcbm . (int) ($this->row - 1) . ')', $coordtotalcbm . (int) ($this->row))->style($css);
          $totalproduit = $coordtotalusd . (int) ($this->row);
          $this->setCelCalValue('=sum(' . $coordtotalusd . (int) ($totalLine + 1) . ':' . $coordtotalusd . (int) ($this->row - 1) . ')', $coordtotalusd . (int) ($this->row))->style($css);
          $this->retour();
          $this->set($cadencier->GetServicedescription())->style($css);
          $this->setCelCalValue('=sum(' . $totalproduit . '+' . $totalservice . ')', $coordtotalusd . (int) ($this->row + 1))->style($css); */


        return $this;
    }

    public function exportQuotationCiCustomerRender($offers, $ci)
    {

        $cadencier = $ci->getCadencier();
        $totalsum = 0;
        $locale = $this->translator->getLocale();
        $selection = $cadencier->getSelection();
        $css = array('width' => 15, 'cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64');
        $this->setRowHeight('50pt');
        $this->set($this->translator->trans('TITLE_ITEM_NO'))->style($css);
        $this->set('Description')->style(
            array('width' => 100, 'cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64')
        );

        if ($ci->getService() == 0) {
            $this->set('Barcode')->style($css);
            $this->set('Material')->style($css);
            $this->set('Color')->style($css);
            $this->set('PCB')->style($css);
            $this->set('Total CBM')->style($css);
        }
        $this->set($this->translator->trans('TITLE_ORDER_QUANTITY'))->style($css);
        $this->set($this->translator->trans('TITLE_UNIT_PRICE') . ' ' . $cadencier->getFinaldevis())->style($css);
        $this->set('Total' . ' ' . $cadencier->getFinaldevis())->style($css);
        $this->retour();
        $css = array('cel-align' => 'center', 'wrap' => true);

        if ($ci->getService() == 0) {
            foreach ($offers as $key => $offer) {
                $this->set($offer->getProduct()->getId())->style($css);
                if ($locale == "fr") {
                    $this->set($offer->getProduct()->getFrenchdescription())->style($css);
                } else {
                    $this->set($offer->getProduct()->getDescription())->style($css);
                }
                if (!$offer->GetProduct()->GetCodebar()) {
                    $this->session->getFlashBag()->add('warning', 'Please check barcode');
                    die('Please check barcode');
                }

                if ($offer->GetProduct()->GetCodebar()->GetCodebar()) {
                    $this->set('#' . $offer->GetProduct()->GetCodebar()->GetCodebar())->style($css);
                    /*  $this->setCelImage($this->gencode->get($offer->GetProduct()->GetCodebar()->GetCodebar()))->style(
                          array('cel-align' => 'center', 'wrap' => true, 'width' => 10)
                      );*/
                }

                $this->set($offer->getProduct()->getMaterial())->style($css);
                $this->set($offer->getProduct()->getColoris())->style($css);
                $this->set($offer->getProduct()->getPcb())->style($css);
                $this->set(
                    $offer->getProduct()->GetCbm() / $offer->getProduct()->GetPcb() * $selection['o' . $offer->getId()]['qty']
                )->style($css);

                $descrptioncolomn = $this->coord;
                $coordqty = $this->coord;
                $this->set($selection['o' . $offer->getId()]['qty'])->style($css);
                $coordprice = $this->coord;
                $this->setCelFormat('CURRENCY_USD');
                $this->set($selection['o' . $offer->getId()]['finalprice'])->style($css);
                $this->setCelFormat('CURRENCY_USD');
                $this->setCelCalValue('=' . $coordqty . '*' . $coordprice)->style($css);
                $this->retour();

                $start = $this->row - count($offers) - 1;
                $totalLine = $this->row - count($offers) - 1;
                $totalservice = "F" . $this->row;

                $totalsum += $selection['o' . $offer->getId()]['finalprice'] * $selection['o' . $offer->getId()]['qty'];
            }

            if ((float)$cadencier->GetFinaltva()) {
                $this->set("Total HT", "G" . $this->row)->setCelAlignment(array('V', 'H'), 'CENTER', "G" . $this->row);
            } else {
                $this->set("Total", "G" . $this->row)->setCelAlignment(array('V', 'H'), 'CENTER', "G" . $this->row);
            }

            $total = "J" . (int)($this->row);
            $this->setCelCalValue(
                '=sum(' . "H" . (int)($totalLine + 1) . ':' . "H" . (int)($this->row - 1) . ')', "H" . (int)($this->row)
            )->setCelAlignment(array('V', 'H'), 'CENTER', "H" . $this->row);
            $this->setCelFormat('CURRENCY_USD', "H" . (int)($this->row));
            $this->setCelCalValue(
                '=sum(' . "J" . (int)($totalLine + 1) . ':' . "J" . (int)($this->row - 1) . ')', "J" . (int)($this->row)
            )->setCelAlignment(array('V', 'H'), 'CENTER', "J" . $this->row);
            $this->setCelFormat('CURRENCY_USD', "J" . (int)($this->row));
            $this->retour();

            if ((float)$cadencier->GetFinaltva()) {
                $this->set('TVA ' . (float)$cadencier->GetFinaltva() . '%', "G" . (int)($this->row))->setCelAlignment(
                    array('V', 'H'), 'CENTER', "G" . $this->row
                );
                $tva = "J" . (int)($this->row);
                $this->setCelCalValue(
                    '=' . $total . '*' . (float)$cadencier->GetFinaltva() . '/100', "J" . (int)($this->row)
                )->style($css);
                $this->setCelFormat('CURRENCY_USD', "J" . (int)($this->row));
                $this->retour();
                $this->set("TOTAL TTC", "G" . $this->row)->setCelAlignment(array('V', 'H'), 'CENTER', "G" . $this->row);
                $this->setCelCalValue('=' . $total . '+' . $tva, "J" . (int)($this->row))->style($css);
                $this->setCelFormat('CURRENCY_USD', "J" . (int)($this->row));
            } else {
                $tva = 0;
            }

            $this->retour();
            $this->set("MADE IN", $this->coord, false)->style($css);
            $this->retour();
            $this->set(
                "SAY ONLY TOTAL " . $this->Getwords->num_words((float)number_format(($totalsum + $tva), 2, ',', ''))
                , $this->coord, false);

            $this->retour();
        } else {
            foreach ($cadencier->GetServicedescription() as $service) {
                if (array_key_exists('description', $service) && array_key_exists('value', $service)) {
                    $this->setRowHeight('100pt');
                    $this->set("Service")->style($css);
                    $descrptioncolomn = $this->coord;
                    $this->set($service['description'])->style($css);
                    $coordqty = $this->coord;
                    $this->set(1)->style($css);
                    $coordprice = $this->coord;
                    $this->set($service['value'])->style($css);
                    $this->setCelCalValue('=' . $coordqty . '*' . $coordprice)->style($css);
                    $this->retour();
                    $totalsum += $service['value'];
                }
            }
            $start = $this->row - count($cadencier->GetServicedescription()) - 1;
            $totalLine = $this->row - count($cadencier->GetServicedescription()) - 1;
            $totalservice = "F" . $this->row;

            if ((float)$cadencier->GetFinaltva()) {
                $this->set("TOTAL HT", "C" . $this->row)->style($css);
            } else {
                $this->set("TOTAL", "C" . $this->row)->style($css);
            }

            $total = "E" . (int)($this->row);
            $this->setCelCalValue(
                '=sum(' . "E" . (int)($totalLine + 1) . ':' . "E" . (int)($this->row - 1) . ')', "E" . (int)($this->row)
            )->style($css);
            $this->setCelCalValue(
                '=sum(' . "D" . (int)($totalLine + 1) . ':' . "D" . (int)($this->row - 1) . ')', "D" . (int)($this->row)
            )->style($css);
            $this->retour();

            if ((float)$cadencier->GetFinaltva()) {
                $this->set('TVA ' . (float)$cadencier->GetFinaltva() . '%', "C" . (int)($this->row))->setCelAlignment(
                    array('V', 'H'), 'CENTER', "C" . $this->row
                );
                $tva = "E" . (int)($this->row);
                $this->setCelCalValue(
                    '=' . $total . '*' . (float)$cadencier->GetFinaltva() . '/100', "E" . (int)($this->row)
                )->style($css);
                $this->retour();
                $this->set("Total TTC", "C" . $this->row)->setCelAlignment(array('V', 'H'), 'CENTER', "C" . $this->row);
                $this->setCelCalValue('=' . $total . '+' . $tva, "E" . (int)($this->row))->style($css);
            } else {
                $tva = 0;
            }

            $this->retour();
            $this->set("MADE IN", $this->coord, false)->style($css);
            $this->retour();
            $this->set(
                "SAY ONLY TOTAL " . $this->Getwords->num_words((float)number_format(($totalsum + $tva), 2, ',', ''))
                , $this->coord, false);
            $this->retour();
        }


        return $this;
    }

    /*
     * EXPORT EXCEL SHIPPING MARK (add a column with an image, 8/2018)
     */

    public
    function exportshippingmarkRender($offers, $cadencierId, $customerName)
    {
        $locale = $this->translator->getLocale();
        $css = array('cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64', 'width' => 20);
        $this->setRowHeight('50pt');
/* New code 16 Oct 2018 */
        $this->set('Order No:')->style($css);
        $this->set('Importer:')->style($css);
        $this->set('Photo')->style($css);
        $this->set('Descr.:')->style($css);
        $this->set('Descr.:')->style($css);
        $this->set('Code EAN:')->style($css);
        $this->set('Color:')->style($css);
        $this->set('Qty:')->style($css);
        $this->set('Inasia item No')->style($css);
        $this->set('Supplier Item No')->style($css);
        $this->set('N.W.:')->style($css);
        $this->set('G.W.:')->style($css);
        $this->set('MEAS.:')->style($css);

        /* Old code up to 16 Oct 2018 */
/*
        $this->set('LIBELLE FR')->style($css);
        $this->set('LIBELLE EN')->style($css);
        $this->set('BAR CODE')->style($css);
        $this->set('COLORIS')->style($css);
        $this->set('PCB')->style($css);
        $this->set('INASIA ITEM NUMBER')->style($css);
        $this->set('SUPPLIER ITEM NUMBER')->style($css);
        $this->set('N.W. PACKAGE (KGS)')->style($css);
        $this->set('G.W. PACKAGE (KGS)')->style($css);
        $this->set('MEAS (PACKAGE SIZE)')->style($css);
        $this->set('PHOTO')->style($css);
*/

        $this->retour();
        $css = array('cel-align' => 'center', 'wrap' => true);

        foreach ($offers as $key => $offer) {
            $this->setRowHeight('100pt');

            // col A, B
            $this->set($cadencierId)->style($css);
            $this->set($customerName)->style($css);
            // col C: photo doesn't show up because they haven't been uploaded locally
            $this->setCelImage(str_replace('original.jpg', 'small.jpg', $offer->getProduct()->getMainimage()))->style($css);
//            $this->setCelImage('https://market.inasia-corp.com/' . str_replace('original.jpg', 'small.jpg', $offer->getProduct()->getMainimage()))->style($css);
            // cols D, E
            $this->set($offer->getProduct()->getfrenchsmalldescription())->style($css);
            $this->set($offer->getProduct()->getsmalldescription())->style($css);

            if (!$offer->GetProduct()->GetCodebar()) {
                $this->session->getFlashBag()->add('warning', 'Please check barcode');
                die('Please check barcode');
            }
// replace barcode image with a number, col F
            if ($offer->GetProduct()->GetCodebar()->GetCodebar()) {
                $this->set('#'.$offer->GetProduct()->GetCodebar()->GetCodebar())->style($css);
/*
                $this->setCelImage($this->gencode->get($offer->GetProduct()->GetCodebar()->GetCodebar()))->style(
                    array('cel-align' => 'center', 'wrap' => true, 'width' => 10)
                );
*/
            }
// col G
            $this->set($offer->GetProduct()->GetColoris())->style($css);
            $this->set($offer->GetProduct()->GetPcb())->style($css);
            // cols I, J, K, L, M
            $this->set($offer->getProduct()->getId())->style($css);
            $this->set($offer->getItemnumber())->style($css);
            $this->set($offer->getProduct()->getNwpackage())->style($css);
            $this->set($offer->getProduct()->getGwpackage())->style($css);
            $this->set($offer->getProduct()->getPackingsize())->style($css);

            $this->retour();
        }

        return $this;
    }

    /*
     * PL CUSTOMER
     */

    public
    function exportQuotationPlCustomerRender($command, $profil, $issaler = false, $insertqty = false)
    {
        $locale = $this->translator->getLocale();
        $css = array('cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64');
        $this->setRowHeight('50pt');

        $this->set($this->translator->trans('TITLE_ITEM_NO'))->style($css);
        $this->set('Description Of Goods')->style($css);
        $this->set('Qty/ Pcs')->style($css);
        $this->set('Package/Ctns')->style($css);
        $this->set('Cbm (M3)')->style($css);
        $this->set('Total CBM (M3)')->style($css);
        $this->set('Net Weight (kgs)')->style($css);
        $this->set('Gross Weight (kgs)')->style($css);
        $this->set('Total Net Weight (kgs)')->style($css);
        $this->set('Total Gross Weight (kgs)')->style($css);
        $this->retour();
        $css = array('cel-align' => 'center', 'wrap' => true);
        $total = 0;

        $nb = 0;
        foreach ($command->getCadenciers() as $cadencier) {
            $selection = $cadencier->getSelection();

            $offers = $this->doctrine->getRepository('appBundle:Offer')
                ->getOffersSelection($cadencier->getOfferslist());

            $nb = $nb + count($offers);

            foreach ($offers as $key => $offer) {
                $this->setRowHeight('100pt');
                $this->set($offer->getProduct()->getId())->style($css);
                $descrptioncolomn = $this->coord;
                $this->set($offer->getProduct()->getDescription())->style($css);
                $qtycol = $this->colletter;
                $this->set($selection['o' . $offer->getId()]['qty'])->style($css);
                $packagecol = $this->colletter;
                $packagePerCtns = round($selection['o' . $offer->getId()]['qty'] / $offer->getProduct()->getPcb(), 0, PHP_ROUND_HALF_UP);
                $this->set($packagePerCtns)->style($css);
                $this->set($offer->getProduct()->getCbm())->style($css);
                $cbmcol = $this->colletter;
                // "formule total CBM faux"
//                $this->set($selection['o' . $offer->getId()]['qty'] * $offer->getProduct()->getCbm())->style($css);
// total CBM calculation fix:
                $this->set($packagePerCtns * $offer->getProduct()->getCbm())->style($css);

                $this->setCelFormat('TWODEC')->set($offer->getProduct()->getNwpackage())->style($css); // net weight: NW package
                $this->setCelFormat('TWODEC')->set($offer->getProduct()->getGwpackage())->style($css); // gross weight: GW package
                $totalncol = $this->colletter;
                // total net weight: qty should be package/ctns
                $this->set(round($selection['o' . $offer->getId()]['qty'] / $offer->getProduct()->getPcb(), 0, PHP_ROUND_HALF_UP) * $offer->getProduct()->getNwpackage())->style($css);
                $totalgcol = $this->colletter;
                // total gross weight
                $this->set(round($selection['o' . $offer->getId()]['qty'] / $offer->getProduct()->getPcb(), 0, PHP_ROUND_HALF_UP) * $offer->getProduct()->getGwpackage())->style($css);
                $this->retour();

                $total += round(
                    $selection['o' . $offer->getId()]['qty'] / $offer->getProduct()->getPcb(), 0, PHP_ROUND_HALF_UP
                );
            }
        }
        $totalLine = $this->row - $nb - 1;
        $css = array('cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64');
        $this->setRowHeight('50pt');
        $this->set("")->style($css);
        $this->set("")->style($css);
        $this->set("")->style($css);
        $this->set("")->style($css);
        $this->set("")->style($css);
        $this->setCelCalValue(
            '=sum(' . $qtycol . (int)($totalLine + 1) . ':' . $qtycol . (int)($this->row - 1) . ')', $qtycol . (int)($this->row)
        )->style($css);
        $this->setCelCalValue(
            '=sum(' . $packagecol . (int)($totalLine + 1) . ':' . $packagecol . (int)($this->row - 1) . ')', $packagecol . (int)($this->row)
        )->style($css);
        $this->setCelCalValue(
            '=sum(' . $cbmcol . (int)($totalLine + 1) . ':' . $cbmcol . (int)($this->row - 1) . ')', $cbmcol . (int)($this->row)
        )->style($css);
        $this->setCelCalValue(
            '=sum(' . $totalncol . (int)($totalLine + 1) . ':' . $totalncol . (int)($this->row - 1) . ')', $totalncol . (int)($this->row)
        )->style($css);
        $this->setCelCalValue(
            '=sum(' . $totalgcol . (int)($totalLine + 1) . ':' . $totalgcol . (int)($this->row - 1) . ')', $totalgcol . (int)($this->row)
        )->style($css);
        $this->retour();

        $css = array('cel-align' => 'center', 'wrap' => true);
        $this->set("Shipping Mark:")->style($css);
        $this->retour();
        $this->set("MADE IN", $this->coord, false)->style($css);
        $this->retour();
        $this->set(
            "SAY ONLY TOTAL " . $this->Getwords->num_words((float)number_format(($total), 2, ',', '')) . " PACKAGES"
            , $this->coord, false)->style($css);
        $this->retour();

        return $this;
    }

    /*
     * EXPORT FOR FIXPIRCE
     */

    public
    function exportfixedpriceRender($offers, $idcustomer)
    {

        $profil["cutomer_id"] = $idcustomer;
        $profil["customer_coef"] = 0;
        $profil["contact_type"] = 0;
        $profil["mode"] = "buy";

        $css = array('cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64');
        $this->setRowHeight('50pt');
        $this->set('SUPPLIER')->style($css);
        $this->set('SUPPLIER ITEM NO')->style($css);
        $this->set('INASIA ITEM NO')->style($css);
        $this->set('Description EN')->style($css);
        $this->set('Description FR')->style($css);
        $this->set('MOQ')->style($css);
        $this->set('FREE MOQ2')->style($css);
        $this->set('MOQ UNIT')->style($css);
        $this->set('DIFF')->style($css);
        $this->set('FOB Unit Price USD')->style($css);
        $this->set('FREE FOB Unit Price USD')->style($css);
        $this->set('DIFF')->style($css);
        $this->set('EXW Unit Price USD')->style($css);
        $this->set('FREE EXW Unit Price USD')->style($css);
        $this->set('DIFF')->style($css);

        $this->retour();

        $css = array('cel-align' => 'center', 'wrap' => true);
        foreach ($offers as $key => $offer) {
            $this->setRowHeight('100pt');
            $this->set($offer->getSupplier()->getId())->style($css);
            $this->set($offer->getItemnumber())->style($css);
            $this->set($offer->getProduct()->getId())->style($css);
            $this->set($offer->getProduct()->getDescription())->style($css);
            $this->set($offer->getProduct()->getFrenchdescription())->style($css);
            $moq1 = $this->coord;
            $this->set($offer->getMoq($profil['cutomer_id']))->style($css);
            $moq2 = $this->coord;
            $this->set($offer->getMoq())->style($css);
            $this->set($offer->getMoqunit()->GetTitle())->style($css);
            $this->setCelCalValue("=(" . $moq1 . '-' . $moq2 . ')*100/' . $moq2)->style($css);
            $fob1 = $this->coord;
            $this->set($offer->getSaleprice('fob', $profil))->style($css);
            $fob2 = $this->coord;
            $this->set($offer->getSaleprice('fob', $profil, false))->style($css);
            $this->setCelCalValue("=(" . $fob1 . '-' . $fob2 . ')*100/' . $fob2)->style($css);
            $exw1 = $this->coord;
            $this->set($offer->getSaleprice('exw', $profil))->style($css);
            $exw2 = $this->coord;
            $this->set($offer->getSaleprice('exw', $profil, false))->style($css);
            $this->setCelCalValue("=(" . $exw1 . '-' . $exw2 . ')*100/' . $exw2)->style($css);
            $this->retour();
        }
        $start = $this->row - count($offers) - 1;

        return $this;
    }

    public
    function displayCatalogHeader($user, $customer)
    {
        $this->obj->getActiveSheet()->setCellValue('I3', 'Quotation sheet No : ' . date('hms'));
        $this->obj->getActiveSheet()->setCellValue('I12', 'Date : ' . date('d-m-Y'));

        if ($customer) {
            $this->obj->getActiveSheet()->setCellValue('I4', $this->translator->trans('TITLE_CUSTOMER') . ' : ');
            $this->obj->getActiveSheet()->setCellValue('J5', $customer->GetName());
            $this->obj->getActiveSheet()->setCellValue('J6', $customer->GetAddress());
            $this->obj->getActiveSheet()->setCellValue('J7', $customer->GetCity());
            $this->obj->getActiveSheet()->setCellValue(
                'J8', $customer->GetPc() . ' ' . $customer->GetCountry()->GetTitle()
            );
        }

        $this->obj->getActiveSheet()->setCellValue('A17', 'Saler : ' . $user->getName() . ' ' . $user->getPhone());
        $this->obj->getActiveSheet()->setCellValue('F17', 'Email : ' . $user->GetEmail());

        return $this;
    }

    public
    function exportCatalogCustomer($offers, $fob, $exw, $access, $profil)
    {

        $locale = $this->translator->getLocale();

        $css = array('cel-align' => 'center', 'width' => '20px', 'wrap' => true, 'background-color' => 'fbde64');
        $this->setRowHeight('40pt');
        $this->set($this->translator->trans('ENTITY_PRODUCT_SOURCINASIAITEMNUMBER'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        if ($access != "ROLE_USER") {
            $this->set($this->translator->trans('ENTITY_PRODUCT_SUPPLIERITEMNUMBER'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        }
        $this->set($this->translator->trans('ENTITY_OFFER_MAINOFFER'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));

        if ($access == "ROLE_SOURCING") {
            $this->set($this->translator->trans('ENTITY_COMMAND_FACTORY'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        }

        $this->set($this->translator->trans('ENTITY_PRODUCT_NOMENCLATURE'))->style(array('cel-align' => 'center', 'width' => '30px', 'wrap' => true, 'background-color' => 'fbde64'));
        //$this->set($this->translator->trans('TITLE_BARCODE'))->style($css);
        $this->set($this->translator->trans('TITLE_PHOTO'))->style(array('cel-align' => 'center', 'width' => '30px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_SMALLDESCRIPTION'))->style(array('cel-align' => 'center', 'width' => '30px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_DESCRIPTION'))->style(array('cel-align' => 'center', 'width' => '60px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_FRENSHSMALLDESCRIPTION'))->style(array('cel-align' => 'center', 'width' => '30px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_FRENSHDESCRIPTION'))->style(array('cel-align' => 'center', 'width' => '60px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_COLORIS'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_MATERIAL'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_PACKING'))->style(array('cel-align' => 'center', 'width' => '30px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('TITLE_COMMENTS'))->style($css);
        $this->set($this->translator->trans('ENTITY_COMMAND_CBM'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('TITLE_PCB'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_PRODUCTSIZE'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_NWPRODUCT'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_NGPRODUCT'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_NWPACKAGE'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_NGPACKAGE'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_QTY20GP'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_QTY40GP'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_QTY40HQ'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));

        if ($fob) {
            $this->set('FOB PORT')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        }

        $this->set('MOQ')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set('MOQ UNIT')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));

        if ($access != "ROLE_SOURCING") {
            if ($access == "ROLE_ADMIN" || $access == "ROLE_COMMERCIAL") {
                if ($fob) {
                    $this->set('FOB INASIA Price')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
                }
                if ($exw) {
                    $this->set('EXW INASIA Price')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
                }
                if ($access == "ROLE_ADMIN") {
                    $this->set('% marge')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
                }
            }

            if ($profil['cutomer_id']) {
                if ($fob) {
                    $this->set('FOB Price')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
                }
                if ($exw) {
                    $this->set('EXW Price')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
                }
                if ($access == "ROLE_ADMIN" || $access == "ROLE_COMMERCIAL") {
                    $this->set('% marge')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
                }
            }
        }

        $this->retour();

        $css = array('cel-align' => 'center', 'wrap' => true);
        foreach ($offers as $key => $offer) {
            $this->set($offer->getProduct()->getId())->style($css);
            if ($access != "ROLE_USER") {
                $this->set($offer->getItemnumber())->style($css);
            }
            if ($offer->getProduct()->getOffers()) {
                foreach ($offer->getProduct()->getOffers() as $offertmp) {
                    if ($offertmp->getMainoffer()) {
                        $mainOffer = $offertmp;
                    }
                }

                if ($mainOffer) {
                    $offer = $mainOffer;
                }
            }

            $this->set($offertmp->getSupplier()->getId())->style($css);

            if ($access == "ROLE_SOURCING") {
                $this->set($offer->getSupplier()->getId())->style($css);
            }

            if ($locale == "fr") {
                $this->set($offer->getProduct()->getCategorie()->getNom())->style($css);
            } else {
                $this->set($offer->getProduct()->getCategorie()->getName())->style($css);
            }

            /* if ($offer->GetProduct()->GetCodebar()) {
                 $this->set(" " . $offer->GetProduct()->GetCodebar()->GetCodebar())->style($css);
             } else {
                 $this->set('')->style($css);
             }*/

            $this->setCelImage(str_replace('original.jpg', 'small.jpg', $offer->getProduct()->getMainimage()))->style($css);

            $this->set($offer->getProduct()->getSmalldescription())->warp()->style($css);
            $this->set($offer->getProduct()->getDescription())->warp()->style($css);
            $this->set($offer->getProduct()->getFrenchsmalldescription())->style($css);
            $this->set($offer->getProduct()->getFrenchdescription())->style($css);

            $this->set($offer->getProduct()->getColoris())->style($css);
            $this->set($offer->getProduct()->getMaterial())->style($css);
            $this->set($offer->getProduct()->getPacking())->style($css);
            $this->set($offer->getProduct()->getComments())->style($css);
            $this->set($offer->getProduct()->getCbm())->style($css);
            $this->set($offer->getProduct()->getPcb())->style($css);
            $this->set($offer->getProduct()->getProductsize())->style($css);
            $this->set($offer->getProduct()->getNwproduct())->style($css);
            $this->set($offer->getProduct()->getGwproduct())->style($css);
            $this->set($offer->getProduct()->getGwpackage())->style($css);
            $this->set($offer->getProduct()->getNwpackage())->style($css);
            $this->set($offer->getProduct()->getQty20gp())->style($css);
            $this->set($offer->getProduct()->getQty40gp())->style($css);
            $this->set($offer->getProduct()->getQty40hq())->style($css);
            if ($fob) {
                if ($offer->getPol()) {
                    $this->set($offer->getPol()->GetTitle())->style($css);
                } else {
                    $this->set('')->style($css);
                }
            }
            $this->set($offer->getMoq())->style($css);

            if ($offer->getMoqpackingunit()) {
                $this->set($offer->getMoqunit()->GetTitle())->style($css);
            } else {
                $this->set('')->style($css);
            }

            if ($fob) {
                $price = $offer->getSaleprice('fob', $profil);
                $this->setCelFormat('CURRENCY_USD');
                $this->set($price ? round($price, 2) : '-')->style($css);
            }
            if ($exw) {
                $price = $offer->getSaleprice('exw', $profil);
                $this->setCelFormat('CURRENCY_USD');
                $this->set($price ? round($price, 2) : '-')->style($css);
            }

            $letter = $this->colletter;
            $this->retour();
        }

        //$this->obj->getActiveSheet()->getProtection()->setSheet(true);
        $this->obj->getActiveSheet()->getPageSetup()->setPrintArea('A1:' . $letter . $this->row);
        $this->obj->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $this->obj->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->obj->getActiveSheet()->getPageSetup()->setFitToPage(true);
        $this->obj->getActiveSheet()->getPageSetup()->setFitToWidth(1);
        $this->obj->getActiveSheet()->getPageSetup()->setFitToHeight(0);

        return $this;
    }

    public function exportCatalog($offers, $fob, $exw, $access, $profil)
    {

        $locale = $this->translator->getLocale();


        $css = array('cel-align' => 'center', 'width' => '20px', 'wrap' => true, 'background-color' => 'fbde64');
        $this->setRowHeight('40pt');
        $this->set($this->translator->trans('ENTITY_PRODUCT_SOURCINASIAITEMNUMBER'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        if ($access != "ROLE_USER") {
            $this->set($this->translator->trans('ENTITY_PRODUCT_SUPPLIERITEMNUMBER'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        }
        $this->set($this->translator->trans('ENTITY_OFFER_MAINOFFER'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_COMMAND_FACTORY'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_NOMENCLATURE'))->style(array('cel-align' => 'center', 'width' => '30px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('TITLE_BARCODE'))->style($css);
        $this->set($this->translator->trans('TITLE_PHOTO'))->style(array('cel-align' => 'center', 'width' => '30px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_SMALLDESCRIPTION'))->style(array('cel-align' => 'center', 'width' => '30px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_DESCRIPTION'))->style(array('cel-align' => 'center', 'width' => '60px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_FRENSHSMALLDESCRIPTION'))->style(array('cel-align' => 'center', 'width' => '30px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_FRENSHDESCRIPTION'))->style(array('cel-align' => 'center', 'width' => '60px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_COLORIS'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_MATERIAL'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_PACKING'))->style(array('cel-align' => 'center', 'width' => '30px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('TITLE_COMMENTS'))->style($css);
        $this->set($this->translator->trans('ENTITY_COMMAND_CBM'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('TITLE_PCB'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_PACKINGSIZE'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_PRODUCTSIZE'))->style($css);
        $this->set($this->translator->trans('ENTITY_PRODUCT_NWPRODUCT'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_NGPRODUCT'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_NWPACKAGE'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_NGPACKAGE'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_QTY20GP'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('ENTITY_PRODUCT_QTY40GP'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));;
        $this->set($this->translator->trans('ENTITY_PRODUCT_QTY40HQ'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set($this->translator->trans('CE'))->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));

        if ($fob) {
            // $this->set('FOB Unit Price USD')->style($css);
            $this->set('FOB PORT')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        }

        $this->set('MOQ')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set('MOQ UNIT')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set('MOQ Packing')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
        $this->set('MOQ Packing Unit')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));

        if ($access != "ROLE_SOURCING") {
            if ($access == "ROLE_ADMIN" || $access == "ROLE_COMMERCIAL") {
                if ($fob) {
                    $this->set('FOB INASIA Price')->style(array('cel-align' => 'center', 'width' => '15px', 'wrap' => true, 'background-color' => 'fbde64'));
                }
                if ($exw) {
                    $this->set('EXW INASIA Price')->style(array('cel-align' => 'center', 'width' => '15px', 'wrap' => true, 'background-color' => 'fbde64'));
                }
                if ($access == "ROLE_ADMIN") {
                    $this->set('% marge')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
                    if ($fob) {
                        $this->set('FOB supplier price')->style(array('cel-align' => 'center', 'width' => '15px', 'wrap' => true, 'background-color' => 'fbde64'));
                    }
                    if ($exw) {
                        $this->set('EXW supplier price')->style(array('cel-align' => 'center', 'width' => '15px', 'wrap' => true, 'background-color' => 'fbde64'));
                    }
                }
            }

            if ($profil['cutomer_id']) {
                if ($fob) {
                    $this->set('FOB Customer Price')->style(array('cel-align' => 'center', 'width' => '15px', 'wrap' => true, 'background-color' => 'fbde64'));
                }
                if ($exw) {
                    $this->set('EXW Customer Price')->style(array('cel-align' => 'center', 'width' => '15px', 'wrap' => true, 'background-color' => 'fbde64'));
                }
                if ($access == "ROLE_ADMIN" || $access == "ROLE_COMMERCIAL") {
                    $this->set('% marge')->style(array('cel-align' => 'center', 'width' => '10px', 'wrap' => true, 'background-color' => 'fbde64'));
                }
            }
        }

        $this->retour();

        $css = array('cel-align' => 'center', 'wrap' => true);
        foreach ($offers as $key => $offer) {
            $this->set($offer->getProduct()->getId())->style($css);
            if ($access != "ROLE_USER") {
                $this->set($offer->getItemnumber())->style($css);
            }
            if ($offer->getProduct()->getOffers()) {
                foreach ($offer->getProduct()->getOffers() as $offertmp) {
                    if ($offertmp->getMainoffer()) {
                        $mainOffer = $offertmp;
                    }
                }

                if ($mainOffer) {
                    $offer = $mainOffer;
                }
            }

            $this->set($offertmp->getSupplier()->getId())->style($css);
            $this->set($offer->getSupplier()->getId())->style($css);
            $this->set($offer->getProduct()->getCategorie()->getId())->style($css);
            if ($offer->GetProduct()->GetCodebar()) {
                $this->set(" " . $offer->GetProduct()->GetCodebar()->GetCodebar())->style($css);
            } else {
                $this->set('')->style($css);
            }

            $this->setCelImage(str_replace('original.jpg', 'small.jpg', $offer->getProduct()->getMainimage()))->style($css);
            $this->set($offer->getProduct()->getSmalldescription())->warp()->style($css);
            $this->set($offer->getProduct()->getDescription())->warp()->style($css);
            $this->set($offer->getProduct()->getFrenchsmalldescription())->style($css);
            $this->set($offer->getProduct()->getFrenchdescription())->style($css);
            $this->set($offer->getProduct()->getColoris())->style($css);
            $this->set($offer->getProduct()->getMaterial())->style($css);
            $this->set($offer->getProduct()->getPacking())->style($css);
            $this->set($offer->getProduct()->getComments())->style($css);
            $this->set($offer->getProduct()->getCbm())->style($css);
            $this->set($offer->getProduct()->getPcb())->style($css);
            $this->set($offer->getProduct()->getPackingsize())->style($css);
            $this->set($offer->getProduct()->getProductsize())->style($css);
            $this->set($offer->getProduct()->getNwproduct())->style($css);
            $this->set($offer->getProduct()->getGwproduct())->style($css);
            $this->set($offer->getProduct()->getGwpackage())->style($css);
            $this->set($offer->getProduct()->getNwpackage())->style($css);
            $this->set($offer->getProduct()->getQty20gp())->style($css);
            $this->set($offer->getProduct()->getQty40gp())->style($css);
            $this->set($offer->getProduct()->getQty40hq())->style($css);
            $this->set($offer->getProduct()->getCe())->style($css);
            if ($fob) {
                if ($offer->getPol()) {
                    $this->set($offer->getPol()->GetTitle())->style($css);
                } else {
                    $this->set('')->style($css);
                }
            }
            $this->set($offer->getMoq())->style($css);
            $this->set($offer->getMoqunit()->GetTitle())->style($css);
            $this->set($offer->getMoqpacking())->style($css);
            if ($offer->getMoqpackingunit()) {
                $this->set($offer->getMoqpackingunit()->GetTitle())->style($css);
            } else {
                $this->set('')->style($css);
            }
            if ($access != "ROLE_SOURCING") {
                if ($access == "ROLE_ADMIN" || $access == "ROLE_COMMERCIAL") {
                    if ($fob) {
                        $price = $offer->GetSourcinasiaprice('fob');
                        $this->setCelFormat('CURRENCY_USD');
                        $this->set($price ? round($price, 2) : '-')->style($css);
                    }
                    if ($exw) {
                        $price = $offer->GetSourcinasiaprice('exw');
                        $this->setCelFormat('CURRENCY_USD');
                        $this->set($price ? round($price, 2) : '-')->style($css);
                    }
                    if ($access == "ROLE_ADMIN") {
                        $this->set($offer->getProduct()->getCategorie()->getMargesourcin())->style($css);
                        if ($fob) {
                            $fobsupplierpricecel = $this->coord;
                            $price = $offer->GetSourcingprice('fob');
                            $this->setCelFormat('CURRENCY_USD');
                            $this->set($price ? round($price, 2) : '-')->style($css);
                        }
                        if ($exw) {
                            $fobsupplierpricecel = $this->coord;
                            $price = $offer->GetSourcingprice('exw');
                            $this->setCelFormat('CURRENCY_USD');
                            $this->set($price ? round($price, 2) : '-')->style($css);
                        }
                    }
                }
                if ($profil['cutomer_id']) {
                    if ($fob) {
                        $focel = $this->coord;
                        $price = $offer->getSaleprice('fob', $profil);
                        $this->setCelFormat('CURRENCY_USD');
                        $this->set($price ? round($price, 2) : '-')->style($css);
                    }
                    if ($exw) {
                        $focel = $this->coord;
                        $price = $offer->getSaleprice('exw', $profil);
                        $this->setCelFormat('CURRENCY_USD');
                        $this->set($price ? round($price, 2) : '-')->style($css);
                    }
                    if ($access == "ROLE_ADMIN" || $access == "ROLE_COMMERCIAL") {
                        $this->set("=100*(" . $focel . "-" . $fobsupplierpricecel . ") /" . $focel);
                    }
                }
            }

            $letter = $this->colletter;
            $this->retour();
        }

        //   $this->obj->getActiveSheet()->getProtection()->setSheet(true);
        $this->obj->getActiveSheet()->getPageSetup()->setPrintArea('A1:' . $letter . $this->row);
        $this->obj->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $this->obj->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->obj->getActiveSheet()->getPageSetup()->setFitToPage(true);
        $this->obj->getActiveSheet()->getPageSetup()->setFitToWidth(1);
        $this->obj->getActiveSheet()->getPageSetup()->setFitToHeight(0);
        return $this;
    }

    public
    function hideEmptyCol(
        $colomns = "", $start
    )
    {
        $colomns = explode(',', $colomns);
        foreach ($colomns as $colomn) {
            $empty = true;
            for ($i = $start; $i < $this->row; $i++) {
                $value = $this->obj->getActiveSheet()->getCell($colomn . $i)->getFormattedValue();
                if (!empty($value)) {
                    $empty = false;
                }
            }

            if ($empty) {
                $this->setColVisible(false, $colomn);
            }
        }
    }

    /*
     * ////////////////////////////////////////////// CLASSES ///////////////////////////////////////////
     */

    public
    function __construct(
        $translator, $Getwords, \Doctrine\ORM\EntityManager $doctrine, $session, $gencode
    )
    {
        $this->translator = $translator;
        $this->Getwords = $Getwords;
        $this->doctrine = $doctrine;
        $this->session = $session;
        $this->gencode = $gencode;
    }

    public
    function load(
        $template = false
    )
    {
        $file = dirname(__FILE__) . "/../../Resources/masters/" . $template;
        if ($template && file_exists($file)) {
            $this->obj = \PHPExcel_IOFactory::createReader('Excel2007');
            $this->obj = $this->obj->load($file);
        } else {
            $this->obj = new \PHPExcel();
            $this->obj->getProperties()->setCreator("INASIA")
                ->setLastModifiedBy("INASIA")
                ->setTitle("INASIA")
                ->setSubject("INASIA")
                ->setDescription("")
                ->setKeywords("")
                ->setCategory("INASIA");
        }

        $this->obj->setActiveSheetIndex(0);

        return $this;
    }

    public
    function output($filename, $format = "xls")
    {
        $this->obj->getActiveSheet()->getPageSetup()->setPrintArea('A1:' . $this->colMaxLetter . $this->row);
        $this->obj->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $this->obj->getActiveSheet()->getPageSetup()->setFitToPage(true);

        //  $objPHPExcel->getActiveSheet()    ->setBreak( 'A10' , PHPExcel_Worksheet::BREAK_ROW );
        if ($format == "xls") {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');

            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');

            $objWriter = \PHPExcel_IOFactory::createWriter($this->obj, 'Excel2007');
            $objWriter->setPreCalculateFormulas(true);
        } else {
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
            header('Cache-Control: max-age=0');


            $rendererName = \PHPExcel_Settings::PDF_RENDERER_MPDF;
            $rendererLibrary = 'MPDF56';
            $rendererLibraryPath = dirname(__FILE__) . '/../PdfGen/' . $rendererLibrary;
            if (!\PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                die('Please set the $rendererName and $rendererLibraryPath values' . PHP_EOL . ' as appropriate for your directory structure');
            }
            $this->obj->getActiveSheet()->setShowGridLines(false);
            $objWriter = new \PHPExcel_Writer_PDF($this->obj);
        }
        $objWriter->save('php://output');
        exit;
    }

    public
    function arrayToXls(
        $array
    )
    {
        if (count($array)) {
            $css = array('cel-align' => 'center', 'wrap' => true, 'background-color' => 'fbde64');
            $this->setRowHeight('50pt');
            foreach ($array[0] as $th => $val) {
                $this->setColWidth('27pt');
                $this->set($th)->style($css);
            }
            $this->retour();

            $css = array('cel-align' => 'center', 'wrap' => true);
            foreach ($array as $row) {
                $this->setRowHeight('25pt');
                foreach ($row as $name => $cel) {
                    if (is_object($cel) && get_class($cel) == 'DateTime') {
                        $this->set($cel->format('d-m-Y'))->style($css);
                    } elseif ($name == "CiNumber") {
                        $this->set('N' . $cel)->style($css);
                    } elseif (is_float($cel)) {
                        $this->obj->getActiveSheet()->getStyle($this->GetCoord())->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
                        $this->set($cel)->style($css);
                    } else {
                        $this->set($cel)->style($css);
                    }
                }
                $this->retour();
            }

        }

        return $this;
    }

    private
    function GetCoord()
    {
        //Manipulation des colonnes... A optimiser..
        if ($this->col <= 25) {
            $this->colletter = $this->cl[$this->col];
        } else {
            if ($this->col > 25 && $this->col < 51) {
                $this->colletter = 'A' . $this->cl[$this->col - 26];
            } else {
                die('too much col. Max 51.');
            }
        }

        $this->coord = $this->colletter . $this->row;

        return $this->coord;
    }

    private
    function retour()
    {
        $this->col = 0;
        $this->coordbeforeretour = $this->coord;
        $this->row = (int)$this->row + 1;
        $this->GetCoord();
    }

    public
    function setInitline(
        $line
    )
    {
        $this->col = 0;
        $this->row = $line;
        $this->GetCoord();

        return $this;
    }

    private
    function next()
    {
        $this->col = (int)$this->col + 1;
        $this->GetCoord();
        $this->colMax = ($this->col > $this->colMax) ? $this->col : $this->colMax;
        if ($this->colMax <= 25) {
            $this->colMaxLetter = $this->cl[$this->colMax];
        } else {
            if ($this->colMax > 25 && $this->colMax < 51) {
                $this->colMaxLetter = 'A' . $this->cl[$this->colMax - 26];
            } else {
                die('too much col. Max 51.');
            }
        }
    }

    private
    function style(
        $params = array(), $coord = false
    )
    {
        if (array_key_exists('background-color', $params)) {
            $this->setCelColor($params['background-color']);
        }

        if (array_key_exists('height', $params)) {
            $this->setRowHeight($params['height']);
        }

        if (array_key_exists('wrap', $params)) {
            $this->setCelWrap($params['wrap']);
        }

        if (array_key_exists('width', $params)) {
            if ($params['width'] == 'auto') {
                $this->setColAutoWidth();
            } else {
                $this->setColWidth($params['width']);
            }
        }

        if (array_key_exists('cel-align', $params)) {
            $this->setCelAlignment(array('V', 'H'), 'CENTER', $coord);
        }

        $this->next();

        return $this;
    }

    /*
     *  ////////////////////////////////////////////// PHPEXCEL FRAMEWORK PUBLIC ///////////////////////////////////////////
     */

////////// DOCUMENT
////////// SHEET

    public
    function selectSheet(
        $index = 0
    )
    {
        $this->obj->setActiveSheetIndex(0);

        return $this;
    }

    public
    function addSheet(
        $id, $title
    )
    {
        $this->obj->createSheet(null, $id);
        $this->obj->setActiveSheetIndex($id);
        $this->obj->getActiveSheet()->setTitle($title);
        $this->obj->setActiveSheetIndex($id);
        $this->col = 0;
        $this->colletter = 'A';
        $this->row = 1;
        $this->coord = 'A1';
        $this->coordbeforeretour = 'A1';

        return $this;
    }


    public
    function removeSheet(
        $id
    )
    {
        $this->obj->removeSheetByIndex($id);

        return $this;
    }

    public
    function setSheetName(
        $name
    )
    {
        $this->obj->getActiveSheet()->setTitle($name);

        return $this;
    }

    public
    function setHeader(
        $value
    )
    {
        $this->obj->getActiveSheet()->getHeaderFooter()->setOddHeader($value);

        return $this;
    }

    public
    function setFooter(
        $value
    )
    {
        $this->obj->getActiveSheet()->getHeaderFooter()->setOddFooter($value);

        return $this;
    }

    public
    function setOrientation(
        $value = 'LANDSCAPE'
    )
    {
        $formats = array(
            'LANDSCAPE' => \PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE,
        );

        if (array_key_exists($value, $formats)) {
            $this->obj->getActiveSheet()->getPageSetup()->setOrientation($formats[$value]);
        }

        return $this;
    }

    public
    function setPaperSize(
        $value
    )
    {
        $formats = array(
            'A4' => \PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4,
        );

        if (array_key_exists($value, $formats)) {
            $this->obj->getActiveSheet()->getPageSetup()->setPaperSize($formats[$value]);
        }

        return $this;
    }

    public
    function setUnProtection(
        $coords
    )
    {
        $this->obj->getActiveSheet()
            ->getStyle($coords)
            ->getProtection()->setLocked(
                \PHPExcel_Style_Protection::PROTECTION_UNPROTECTED
            );

        return $this;
    }

////////// CEL
    public
    function set(
        $value, $coord = false, $border = true
    )
    {
        if (empty($coord)) {
            $coord = $this->coord;
        }
        $this->obj->getActiveSheet()->setCellValue($coord, $value);

        if ($border) {
            $this->obj->getActiveSheet()->getStyle($coord)->applyFromArray(array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            ));
        }
        return $this;
    }

    public
    function setCelCalValue(
        $value, $coord = false
    )
    {
        // =SUM(C2:C4)  =AVERAGE(B2:C4)
        if (empty($coord)) {
            $coord = $this->coord;
        }
        $this->obj->getActiveSheet()->setCellValue($coord, $value);
        $this->obj->getActiveSheet()->getCell($coord)->getCalculatedValue($value);
        $this->obj->getActiveSheet()->setCellValue($coord, $value);

        $this->obj->getActiveSheet()->getStyle($coord)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )));
        return $this;
    }

    public
    function setCelFont(
        $value, $coord = false
    )
    {
        if (empty($coord)) {
            $coord = $this->coord;
        }
        $this->obj->getActiveSheet()->setCellValue($coord, $value);

        return $this;
    }

    public
    function setCelSize(
        $value, $coord = false
    )
    {
        if (empty($coord)) {
            $coord = $this->coord;
        }
        $this->obj->getActiveSheet()->getStyle($coord)->getFont()->setSize((int)$value);

        return $this;
    }

    public
    function setCelBold(
        $boolean = true, $coord = false
    )
    {
        if (empty($coord)) {
            $coord = $this->coord;
        }
        $this->obj->getActiveSheet()->getStyle($coord)->getFont()->setBold((boolean)$boolean);

        return $this;
    }

    public
    function setCelWrap(
        $boolean = true, $coord = false
    )
    {
        if (empty($coord)) {
            $coord = $this->coord;
        }

        $this->obj->getActiveSheet()->getStyle($coord)->getAlignment()->setWrapText($boolean);

        return $this;
    }

    public function warp()
    {

        if (empty($coord)) {
            $coord = $this->coord;
        }
        $this->obj->getActiveSheet()->getStyle($coord)->getAlignment()->setWrapText(true);

        return $this;
    }

    public
    function setCelHyperlink(
        $value, $link, $coord = false
    )
    {
        if (empty($coord)) {
            $coord = $this->coord;
        }
        $this->obj->getActiveSheet()->setCellValue($coord, $value);
        $this->obj->getActiveSheet()->getCell($coord)->getHyperlink()->setUrl($link);

        return $this;
    }

    public
    function setCelBorder(
        $coords = false
    )
    {
        if ($coords) {
            $this->obj->getActiveSheet()->getStyle($coords)->applyFromArray(
                array(
                    'borders' => array(
                        'outline' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                        ),
                    ),
                )
            );
        }

        return $this;
    }

    public
    function setCelAlignment(
        $axes = array("H", 'V'), $value = "LEFT", $coord = false
    )
    {
        if (empty($coord)) {
            $coord = $this->coord;
        }
        $formats = array(
            'RIGHT' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            'LEFT' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            'JUSTIFY' => \PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY,
            'CENTER' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        );
        if (array_key_exists($value, $formats)) {
            if (in_array('H', $axes)) {
                $this->obj->getActiveSheet()->getStyle($coord)->getAlignment()->setHorizontal($formats[$value]);
            }

            if (in_array('V', $axes)) {
                $this->obj->getActiveSheet()->getStyle($coord)->getAlignment()->setVertical($formats[$value]);
            }
        }

        return $this;
    }

    public
    function setCelImage(
        $image, $height = false, $coord = false
    )
    {
        if ($image) {
            $image = __SOURCINASIA__ . 'htdocs/' . $image;
            if (file_exists($image)) {
                $info = getimagesize($image);
                if (in_array($info['mime'], array('image/gif', 'image/jpeg', 'image/png'))) {
                    if (empty($coord)) {
                        $coord = $this->coord;
                    }
                    if (empty($height)) {
                        $height = 100;
                    }
                    $objDrawing = new \PHPExcel_Worksheet_Drawing();
                    $objDrawing->setName('INASIA');
                    $objDrawing->setDescription('INASIA');
                    $objDrawing->setPath($image);
                    $objDrawing->setHeight($height);
                    $objDrawing->setCoordinates($coord);
                    $objDrawing->setOffsetX(0);
                    $objDrawing->setWorksheet($this->obj->getActiveSheet());

                    return $this;
                }

                return $this->set('');
            } else {
                return $this->set('');
            }
        } else {
            return $this->set('');
        }
    }

    public
    function setCelColor(
        $color, $coord = false
    )
    {
        if (empty($coord)) {
            $coord = $this->coord;
        }
        $this->obj->getActiveSheet()->getStyle($coord)->getFill()
            ->applyFromArray(
                array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array('rgb' => $color)
                )
            );

        return $this;
    }

    public
    function setCelFormat(
        $format = 'DATETIME', $coord = false
    )
    {
        if (empty($coord)) {
            $coord = $this->coord;
        }
        $formats = array(
            'YYYYMMDD2' => \PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2,
            'TIME4' => \PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4,
            'DATETIME' => \PHPExcel_Style_NumberFormat::FORMAT_DATE_DATETIME,
            'CURRENCY_USD' => \PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
            'TWODEC' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00
        );

        if (array_key_exists($format, $formats)) {
            $this->obj->getActiveSheet()->getStyle($coord)->getNumberFormat()->setFormatCode($formats[$format]);
        }

        return $this;
    }

    public
    function setFormat(
        $format, $coord = false
    )
    {
        if (empty($coord)) {
            $coord = $this->coord;
        }

        $this->obj->getActiveSheet()->getStyle($coord)->getNumberFormat()->setFormatCode($format);

        return $this;
    }


////////// COL

    public
    function setColVisible(
        $visible = true, $col = false
    )
    {
        if (empty($col)) {
            $col = $this->colletter;
        }
        $this->obj->getActiveSheet()->getColumnDimension($col)->setVisible($visible);

        return $this;
    }

    public
    function setColWidth(
        $width, $col = false
    )
    {
        if (empty($col)) {
            $col = $this->colletter;
        }
        if (!empty($width)) {
            $this->obj->getActiveSheet()->getColumnDimension($col)->setWidth((int)$width);
        }

        return $this;
    }

    public
    function setColAutoWidth(
        $boolean = true, $col = false
    )
    {
        if (empty($col)) {
            $col = $this->colletter;
        }
        $this->obj->getActiveSheet()->getColumnDimension($col)->setAutoSize((boolean)$boolean);

        return $this;
    }

////////// Row
    public
    function setRowHeight(
        $height, $row = false
    )
    {
        if (empty($row)) {
            $row = $this->row;
        }
        $this->obj->getActiveSheet()->getRowDimension($row)->setRowHeight((int)$height);

        return $this;
    }

}
