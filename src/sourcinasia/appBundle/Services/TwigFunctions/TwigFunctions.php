<?php

namespace sourcinasia\appBundle\Services\TwigFunctions;

class TwigFunctions extends \Twig_Extension {

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter ('diffToDate', array($this, 'diffToToday')),
        );
    }

    public function diffToToday($date1) {
        $today = new \DateTime();
        $interval = $date1->diff($today);
        return $interval->format('%a');
    }

    public function getName() {
        return 'extensions';
    }

}

?>