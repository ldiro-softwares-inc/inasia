<?php

namespace sourcinasia\appBundle\Services\PdfGen;

class PdfGen {

    public $variables = array();
    public $html = "";
    public $page = "A4";
    public $header = 10;
    public $footer = 5;
    public $paddingtop = 10;
    public $paddingbottom = 20;
    public $paddingright = 5;
    public $paddingleft = 5;
    public $WatermarkText = "";

    function __construct() {
        $this->variables['{{today}}'] = date('d-m-Y');
    }

    /*     * ********************************     Functions         *************** */

    public function load($template) {
        $template = dirname(__FILE__) . "/templates/" . $template;
        if (file_exists($template))
            $this->html = file_get_contents($template);
        return $this;
    }

    public function set($variables = array()) {
        foreach ($variables as $key => $value)
            if (property_exists($this, $key))
                $this->$key = $value;
        return $this;
    }

    public function replace($variables = array()) {
        $this->variables = array_merge($this->variables, $variables);
        $this->html = str_replace(array_keys($this->variables), array_values($this->variables), $this->html);
        return $this;
    }

    public function output($filename = '') {
        //print $this->html;die;
        $pdf = new \mPDF('win-1252', $this->page, '', '', $this->paddingleft, $this->paddingright, $this->paddingtop, $this->paddingbottom, $this->header, $this->footer);
        $pdf->useOnlyCoreFonts = true;    // false is default
        $pdf->SetTitle("INASIA CORP.Ltd");
        $pdf->SetAuthor("INASIA CORP.Ltd");
        $pdf->SetWatermarkText($this->WatermarkText);
        $pdf->showWatermarkText = (!empty($this->WatermarkText));
        $pdf->watermark_font = 'DejaVuSansCondensed';
        $pdf->watermarkTextAlpha = 0.1;
        $pdf->SetDisplayMode('fullpage');
        $pdf->WriteHTML($this->html);
        $pdf->Output($filename, 'D');
        exit;
    }

}
