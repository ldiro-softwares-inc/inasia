<?php

namespace sourcinasia\appBundle\Services\Export;

use sourcinasia\appBundle\Entity\Cadencier;

class Export
{
    public $dev = true;
    public $rows = array();
    public $services = array();
    public $values = array();

    public function __construct($translator, $Getwords, \Doctrine\ORM\EntityManager $doctrine, $session, $gencode, $templating)
    {
        ini_set('max_execution_time', 6000000);
        $this->translator = $translator;
        $this->Getwords = $Getwords;
        $this->doctrine = $doctrine;
        $this->session = $session;
        $this->gencode = $gencode;
        $this->templating = $templating;
    }

    public function catalog($type, $name, $offers, $customer, $profil)
    {
        $this->export = $this->init('appBundle:Pdf:selection.html.twig', 'INASIA-SELECTION.xlsx', $type, $name);

        list($saler, $email) = $this->getCommercial($customer);
        $this->values = array(
            array('number', 'H1', date('hms')),
            array('date', 'F3', new CelDate(new \datetime())),
            array('customerName', 'I3', $customer->GetName()),
            array('customerAddress1', 'I4', $customer->GetAddress() . ' ' . $customer->GetPc()),
            array('customerAddress2', 'I5', $customer->GetCity() . ' ' . $customer->GetCountry()->getTitle()),
            array('commercialName', 'I7', $saler),
            array('commercialEmail', 'I8', $email),
            array('txtUnitprice', 'M10', 'UNIT PRICE'),
            array('txtSelection', 'G1', 'SELECTION'),
        );

        $fob = $exw = true;
        if ($fob && !$exw) {
            $this->values[] = array('txtIncoterm', 'F5', 'fob');
        } elseif (!$fob && $exw) {
            $this->values[] = array('txtIncoterm', 'F5', 'exw');
        } else {
            $this->values[] = array('txtIncoterm', 'F5', 'exw / fob');
        }
        foreach ($offers as $k => $offer) {
            $this->rows[$k]['soucinasiaNumber'] = $offer->getProduct()->getId();
            $this->rows[$k]['tradeNumber'] = $offer->getSupplier()->getId();
            $this->rows[$k]['description'] = $offer->getProduct()->getDescription();
            $this->rows[$k]['image'] = $offer->getProduct()->getMainimage() ? new CelImage($offer->getProduct()->getMainimage(), $this->dev) : null;
            $this->rows[$k]['productSize'] = $offer->getProduct()->getProductsize();
            $this->rows[$k]['material'] = $offer->getProduct()->getMaterial();
            $this->rows[$k]['packing'] = $offer->getProduct()->getPacking();
            $this->rows[$k]['comments'] = $offer->getProduct()->getComments();
            $this->rows[$k]['pcb'] = $offer->getProduct()->getPcb();
            $this->rows[$k]['cbm'] = $offer->getProduct()->getCbm();
            $this->rows[$k]['moq'] = $offer->getMoq($profil['cutomer_id']);
            $this->rows[$k]['moqUnit'] = $offer->getMoqunit()->GetTitle();
            $exw = $exw || $offer->getSaleprice(strtolower('exw'), $profil) != '0.00';
            $fob = $fob || $offer->getSaleprice(strtolower('fob'), $profil) != '0.00';

            if ($fob && !$exw) {
                $this->rows[$k]['price'] = new CelCurrency($offer->getSaleprice('fob', $profil));
            } elseif (!$fob && $exw) {
                $this->rows[$k]['price'] = new CelCurrency($offer->getSaleprice('exw', $profil));
            } else {
                $exw = $offer->getSaleprice(strtolower('exw'), $profil) != '0.00';
                $fob = $offer->getSaleprice(strtolower('fob'), $profil) != '0.00';
                if ($fob) {
                    $this->rows[$k]['price'] = new CelCurrency($offer->getSaleprice('fob', $profil));
                } else {
                    $this->rows[$k]['price'] = new CelCurrency($offer->getSaleprice('exw', $profil));
                }
            }
            $this->rows[$k][] = 1;
            $this->rows[$k][] = new CelFormule('=+N@*J@/I@');
            $this->rows[$k][] = new CelFormule('=+N@*M@');
        }

        return $this->output($name, 12);
    }

    public function qs($type, $name, $cadencier, $offers, $profil)
    {
        $this->export = $this->init('appBundle:Pdf:qs.html.twig', 'INASIA-QS.xlsx', $type, $name);
        list($saler, $email) = $this->getCommercial($cadencier->getCustomer());
        $this->values = array(
            array('number', 'G1', $cadencier->GetId()),
            array('date', 'E3', new CelDate(new \datetime())),
            array('supplier', 'E4', $cadencier->getSupplier()->getId()),
            array('incoterm', 'E5', $cadencier->GetIncoterm()->GetTitle()),
            array('deliveryTime', 'E6', ($cadencier->getSupplier()->getDeleverytime() + 10) . ' DAYS'),
            array('validityOffer', 'E7', $cadencier->getSupplier()->getValidityoffer() . ' ' . strtoupper($this->translator->trans('TITLE_DAYS'))),
            array('paymentTerm', 'J6', $cadencier->getSupplier()->getValidityoffer() . ' DAYS'),
            array('customerName', 'H3', $cadencier->GetCustomer() ? $cadencier->GetCustomer()->GetName() : ''),
            array('paymenTerms', 'E8', $cadencier->getCustomerpaymenterms()->getTitle()),
            array('customerAddress1', 'H4', $cadencier->GetCustomer() ? $cadencier->GetCustomer()->GetAddress() . "\n" . $cadencier->GetCustomer()->GetPc() . "\n" . $cadencier->GetCustomer()->GetCity() . "\n" . $cadencier->GetCustomer()->GetCountry()->getTitle() : ''),
            array('customerAddress2', '', ''),
            array('customerComments', '', $cadencier->getComment()),
            array('commercialName', 'H7', $saler),
//            array('commercialName', '', $cadencier->GetCustomer() ? $cadencier->GetCustomer()->GetName() : ''),
            array('commercialEmail', 'H8', $email),
        );

        $fob = $exw = true;
        $selection = $cadencier->getSelection();
        foreach ($offers as $k => $offer) {
            if (array_key_exists('o' . $offer->getId(), $selection)) {
                $this->rows[$k]['soucinasiaNumber'] = $offer->getProduct()->getId();
                $this->rows[$k]['description'] = $offer->getProduct()->getDescription();
                $this->rows[$k]['image'] = $offer->getProduct()->getMainimage() ? new CelImage($offer->getProduct()->getMainimage(), $this->dev) : null;
                $this->rows[$k]['productSize'] = $offer->getProduct()->getProductsize();
                $this->rows[$k]['material'] = $offer->getProduct()->getMaterial();
                $this->rows[$k]['packing'] = $offer->getProduct()->getPacking();
                $this->rows[$k]['comments'] = $offer->getProduct()->getComments();
                $this->rows[$k]['pcb'] = $offer->getProduct()->getPcb();
                $this->rows[$k]['cbm'] = $offer->getProduct()->getCbm();
                $this->rows[$k]['moq'] = $offer->getMoq($profil['cutomer_id']);
                $this->rows[$k]['moqUnit'] = $offer->getMoqunit()->GetTitle();

                if ($cadencier->getIncoterm()) {
                    if ($cadencier->getIncoterm()->getTitle() == "FOB") {
                        $this->rows[$k]['price'] = new CelCurrency($offer->getSaleprice('fob', $profil));
                    } elseif ($cadencier->getIncoterm()->getTitle() == "EXW") {
                        $this->rows[$k]['price'] = new CelCurrency($offer->getSaleprice('exw', $profil));
                    }
                } else { // deprected - cadencier must have incoterm
                    $exw = $exw || $offer->getSaleprice(strtolower('exw'), $profil) != '0.00';
                    $fob = $fob || $offer->getSaleprice(strtolower('fob'), $profil) != '0.00';

                    if ($fob && !$exw) {
                        $this->rows[$k]['price'] = new CelCurrency($offer->getSaleprice('fob', $profil));
                    } elseif (!$fob && $exw) {
                        $this->rows[$k]['price'] = new CelCurrency($offer->getSaleprice('exw', $profil));
                    } else {
                        $exw = $offer->getSaleprice(strtolower('exw'), $profil) != '0.00';
                        $fob = $offer->getSaleprice(strtolower('fob'), $profil) != '0.00';
                        if ($fob) {
                            $this->rows[$k]['price'] = new CelCurrency($offer->getSaleprice('fob', $profil));
                        } else {
                            $this->rows[$k]['price'] = new CelCurrency($offer->getSaleprice('exw', $profil));
                        }
                    }
                }

                $this->rows[$k]['qty'] = $selection['o' . $offer->getId()]['qty'];
                $this->rows[$k][] = new CelFormule('=+M@*I@/H@');
                $this->rows[$k][] = new CelFormule('=+M@*L@');
            }
        }

        return $this->output($name . '-' . $cadencier->getId(), 12);
    }

    public function po($type, $name, $cadencier, $offers, $profil)
    {
        $this->export = $this->init('appBundle:Pdf:qs.html.twig', 'INASIA-PO-REQUEST.xlsx', $type, $name);
        list($saler, $email) = $this->getCommercial($cadencier->getCustomer());
        $this->values = array(
            array('number', 'H1', $cadencier->GetId()),
            array('date', 'J1', new CelDate(new \datetime())),
            array('supplier', 'J3', "Supplier No: " . $cadencier->getSupplier()->getId()),
            array('incoterm', 'J5', "Incoterm: " . $cadencier->GetIncoterm()->GetTitle()),
//            array('deliveryTime', 'E6', ($cadencier->getSupplier()->getDeleverytime() + 10) . ' DAYS'),
//            array('validityOffer', 'E7', $cadencier->getSupplier()->getValidityoffer() . ' ' . strtoupper($this->translator->trans('TITLE_DAYS'))),
//            array('paymentTerm', 'E6', $cadencier->getSupplier()->getValidityoffer() . ' DAYS'),
//            array('customerName', 'H3', $cadencier->GetCustomer() ? $cadencier->GetCustomer()->GetName() : ''),
            array('paymenTerms', 'J6', "Payment Term: " . $cadencier->getSupplierpaymenterms()->getTitle()),
//            array('customerAddress1', 'H4', $cadencier->GetCustomer() ? $cadencier->GetCustomer()->GetAddress() . "\n" . $cadencier->GetCustomer()->GetPc() . "\n" . $cadencier->GetCustomer()->GetCity() . "\n" . $cadencier->GetCustomer()->GetCountry()->getTitle() : ''),
//            array('customerAddress2', '', '' ),
//            array('customerComments', '', $cadencier->getComment()),
//            array('commercialName', 'H7', $saler),
//            array('commercialName', '', $cadencier->GetCustomer() ? $cadencier->GetCustomer()->GetName() : ''),
//            array('commercialEmail', 'H8', $email),
            array('supplierName', 'A2', $cadencier->getSupplier()->getName())
        );

        $fob = $exw = true;
        $selection = $cadencier->getSelection();
        foreach ($offers as $k => $offer) {
            $this->rows[$k]['soucinasiaNumber'] = $offer->getProduct()->getId();
            $this->rows[$k]['supplierItemNumber'] = $offer->getItemnumber();
            $this->rows[$k]['image'] = $offer->getProduct()->getMainimage() ? new CelImage($offer->getProduct()->getMainimage(), $this->dev) : null;
            $this->rows[$k]['description'] = $offer->getProduct()->getDescription();
            $this->rows[$k]['packing'] = $offer->getProduct()->getPacking();
            $this->rows[$k]['material'] = $offer->getProduct()->getMaterial();
            $this->rows[$k]['productSize'] = $offer->getProduct()->getProductsize();
            $this->rows[$k]['colors'] = $offer->getProduct()->getColoris();
            $this->rows[$k]['packingSize'] = $offer->getProduct()->getPackingsize();
            $this->rows[$k]['nwpackage'] = $offer->getProduct()->getNwpackage();
            $this->rows[$k]['gwpackage'] = $offer->getProduct()->getGwpackage(); // col K
            $this->rows[$k]['moq'] = $offer->getMoq($profil['cutomer_id']);  // col L

//            $this->rows[$k]['comments'] = $offer->getProduct()->getComments();
            $this->rows[$k]['pcb'] = $offer->getProduct()->getPcb(); // col M
            $this->rows[$k]['cbm'] = $offer->getProduct()->getCbm(); // col N
            //           $this->rows[$k]['moqUnit'] = $offer->getMoqunit()->GetTitle(); // col ?
            $this->rows[$k]['qty'] = $selection['o' . $offer->getId()]['qty']; // col O

            $this->rows[$k][] = new CelFormule('=+(O@/M@)*N@'); // col P

            $exw = $exw || $offer->getExw() != '0.00';
            $fob = $fob || $offer->getFob() != '0.00';
            /* Col Q */

            if ($cadencier->getIncoterm()->getTitle() == "FOB") {
                $this->rows[$k]['price'] = new CelCurrency($offer->getFob());
            } else {
                $this->rows[$k]['price'] = new CelCurrency($offer->getExw(), $offer->getCurrency()->getTitle());
            }

            /*  if ($fob && !$exw) {
                  $this->rows[$k]['price'] = new CelCurrency($offer->getFob(), $offer->getCurrency());
              } elseif (!$fob && $exw) {
                  $this->rows[$k]['price'] = new CelCurrency($offer->getExw(), $offer->getCurrency());
              } else {
                  $exw = $offer->getExw() != '0.00';
                  $fob = $offer->getFob() != '0.00';
                  if ($fob) {
                      $this->rows[$k]['price'] = new CelCurrency($offer->getFob(), $offer->getCurrency());
                  } else {
                      $this->rows[$k]['price'] = new CelCurrency($offer->getExw(), $offer->getCurrency());
                  }
              }*/

//            $this->rows[$k][] = new CelFormule('=+M@*I@/H@');

            if ($cadencier->getIncoterm()->getTitle() == "FOB") {
                $totalCel = new CelCurrency('=+O@*Q@');
            } else {
                $totalCel = new CelCurrency('=+O@*Q@', $offer->getCurrency()->getTitle());
            }

            $totalCel->formulas = true;
            $this->rows[$k][] = $totalCel;
        }

        $this->export = $this->export->setCellSize('A2', 12);

        return $this->output($name . '-' . $cadencier->getId(), 11);
    }


    public function pi($type, $name, $cadencier, $offers, $profil)
    {
        $this->export = $this->init('appBundle:Pdf:pi.html.twig', 'INASIA-PI.xlsx', $type, $name);
        $this->tmpPiCi($cadencier);
        $this->values[] = array('number', 'G1', $cadencier->getId());
        foreach ($offers as $k => $offer) {
            $this->rows[$k]['soucinasiaNumber'] = $offer->getProduct()->getId();
            $this->rows[$k]['description'] = $offer->getProduct()->getDescription();
            $this->rows[$k]['image'] = $offer->getProduct()->getMainimage() ? new CelImage($offer->getProduct()->getMainimage(), $this->dev) : null;
            $this->rows[$k]['barcode'] = new Cel($offer->GetProduct()->GetCodebar() ? '#' . $offer->GetProduct()->GetCodebar()->GetCodebar() : null);
            $this->rows[$k]['pcb'] = $offer->getProduct()->getPcb();
            $this->rows[$k]['cbm'] = $offer->getProduct()->getCbm();

            $selection = $cadencier->getSelection();
            if (array_key_exists('finalprice', $selection['o' . $offer->getId()])) {
                $unitPrice = $selection['o' . $offer->getId()]['finalprice'];
            } else {
                $unitPrice = $offer->getSaleprice((($cadencier->GetIncoterm()->GetTitle() == 'FOB') ? 'fob' : 'exw'), $profil);
            }

            $this->rows[$k]['price'] = new CelCurrency($unitPrice, $cadencier->getFinaldevis());
            $this->rows[$k]['qty'] = $cadencier->getSelection()['o' . $offer->getId()]['qty'];
            $this->rows[$k][] = new CelFormule('=+H@*F@/E@');
            $this->rows[$k][] = new CelFormule('=+H@*G@', $cadencier->getFinaldevis());
        }
        if (is_array($cadencier->getServicedescription())) {
            foreach ($cadencier->getServicedescription() as $k => $service) {
                $this->services[$k]['description'] = $service['description'];
                $this->services[$k]['value'] = (float)$service['value'];
            }
        }
        /* missing "sayonly" added */
        $totalRow = sizeof($offers) + 12;
        $totalCell = 'A' . strval($totalRow);

        return $this->output($name . '-' . $cadencier->getId(), 12, $totalCell);
    }

    public function ci($type, $name, $invoice, $offers, $profil)
    {
        $cadencier = $invoice->getCadencier();
        $this->export = $this->init('appBundle:Pdf:ci.html.twig', 'Inasia-Commercial-Invoice-Sheet.xlsx', $type, $name);
        $this->ciHeaders($invoice);
        $this->values[] = array('number', 'G1', '#' . $cadencier->getInvoicecustomers()->first()->getNumber());
        $selection = $cadencier->getSelection();
        foreach ($offers as $k => $offer) {
            $this->rows[$k]['soucinasiaNumber'] = $offer->getProduct()->getId();
            $this->rows[$k]['description'] = $offer->getProduct()->getDescription();
            $this->rows[$k]['barcode'] = new Cel($offer->GetProduct()->GetCodebar() ? '#' . $offer->GetProduct()->GetCodebar()->GetCodebar() : null);

            $this->rows[$k]['color'] = $offer->getProduct()->getColoris();
            $this->rows[$k]['material'] = $offer->getProduct()->getMaterial();
            $this->rows[$k]['size'] = $offer->getProduct()->getProductsize();

            $unitPrice = $selection['o' . $offer->getId()]['finalprice'];
//            $unitPrice = $offer->getSaleprice((($cadencier->GetIncoterm()->GetTitle() == 'FOB') ? 'fob' : 'exw'), $profil);
            $this->rows[$k]['price'] = new CelCurrency($unitPrice, $cadencier->getFinaldevis());
            $this->rows[$k]['qty'] = $cadencier->getSelection()['o' . $offer->getId()]['qty'];
            $this->rows[$k][] = new CelFormule('=+H@*G@', $cadencier->getFinaldevis());
        }
        /*To add an extra line: */
        $nextRow = sizeof($this->rows);
        $this->rows[$nextRow]['soucinasiaNumber'] = '';

        $this->values[] = array('total', 'I16', new CelFormule('=SUM(I13:I14)'));
        $this->values[] = array('deposit', 'I17', $this->calculateDeposit($invoice));
        $this->values[] = array('totalQty', 'H16', new CelFormule('=SUM(H13:H14)'));
        return $this->output($name . '-' . $cadencier->getId(), 14, 'A19');

        /*
        $this->export = $this->init('appBundle:Pdf:ci.html.twig', 'INASIA-CI.xlsx', $type, $name);
        $this->tmpPiCi($cadencier);
        $this->values[] = array('number', 'G1', '#'.$cadencier->getInvoicecustomers()->first()->getNumber());
        $selection = $cadencier->getSelection();
        foreach ($offers as $k => $offer) {
            $this->rows[$k]['soucinasiaNumber'] = $offer->getProduct()->getId();
            $this->rows[$k]['description'] = $offer->getProduct()->getDescription();
            $this->rows[$k]['image'] = $offer->getProduct()->getMainimage() ? new CelImage($offer->getProduct()->getMainimage(), $this->dev) : null;
            $this->rows[$k]['barcode'] = new Cel($offer->GetProduct()->GetCodebar() ? '#' . $offer->GetProduct()->GetCodebar()->GetCodebar() : null);
            $this->rows[$k]['pcb'] = $offer->getProduct()->getPcb();
            $this->rows[$k]['cbm'] = $offer->getProduct()->getCbm();
            $unitPrice = $selection['o' . $offer->getId()]['finalprice'];
//            $unitPrice = $offer->getSaleprice((($cadencier->GetIncoterm()->GetTitle() == 'FOB') ? 'fob' : 'exw'), $profil);
            $this->rows[$k]['price'] = new CelCurrency($unitPrice, $cadencier->getFinaldevis());
            $this->rows[$k]['qty'] = $cadencier->getSelection()['o' . $offer->getId()]['qty'];
            $this->rows[$k][] = new CelFormule('=+H@*F@/E@');
            $this->rows[$k][] = new CelFormule('=+H@*G@', $cadencier->getFinaldevis());
        }
        $nextRow = sizeof($this->rows);
        $this->rows[$nextRow]['soucinasiaNumber'] = '';
        $this->rows[$nextRow]['description'] = "MADE IN CHINA";
//        $this->rows[$nextRow+6]['bankinfo'] = $this->translator->trans('BANK_INFORMATION');
        $bankRow = sizeof($offers) + 17;
        return $this->output($name . '-' . $cadencier->getId(), 12, 'A17', $bankRow);
*/

    }

    public function piService($type, $name, $Invoice)
    {
        $this->export = $this->init('appBundle:Pdf:pis.html.twig', 'INASIA-PIS.xlsx', $type, $name);
        $this->tmpPiCiService($Invoice);
//        $this->values[] = array('number', 'G1', $Invoice->getNumber());
        $services = json_decode($Invoice->getDescription());
        foreach ($services as $k => $service) {
            $this->services[$k]['description'] = $service->description;
            $this->services[$k]['value'] = (float)$service->value;
        }

        return $this->output($name . '-' . $Invoice->getId(), 12);
    }

    public function ciService($type, $name, $Invoice)
    {
        $this->export = $this->init('appBundle:Pdf:cis.html.twig', 'Inasia-Commercial-Invoice-Services.xlsx', $type, $name);

//        $this->export = $this->init('appBundle:Pdf:cis.html.twig', 'INASIA-CIS.xlsx', $type, $name);
//        $this->tmpPiCiService($Invoice);
        $this->ciServiceHeaders($Invoice);
//        $this->values[] = array('number', 'G1', $Invoice->getNumber());
        $serviceCount = 0;
        if ($Invoice->getDescription()) {
            $services = json_decode($Invoice->getDescription());
            foreach ($services as $k => $service) {
                $this->services[$k]['description'] = $service->description;
                $this->services[$k]['value'] = (float)$service->value;
                $serviceCount++;
            }
        }
        $cadencier = $Invoice->getCadencier();
        if (is_array($cadencier->getServicedescription())) {
            foreach ($cadencier->getServicedescription() as $k => $service) {
                $this->services[$k]['description'] = $service['description'];
                $this->services[$k]['value'] = (float)$service['value'];
                $serviceCount++;
            }
        }

        // to add an extra line
        $nextRow = sizeof($this->services);
        $this->services[$nextRow]['description'] = '';


        $this->values[] = array('total', 'I12', new CelFormule('=SUM(I10:I11)'));
        $this->values[] = array('totalQty', 'J12', new CelFormule('=SUM(J10:J11)'));

        //        $bankRow = $serviceCount + 17;
        return $this->output($name . '-' . $Invoice->getId(), 11, 'A15');
    }

    ## Templates

    /**
     * Headers for the new template of 2/2019
     * @param $cadencier
     */
    private function ciHeaders($invoice)
    {
        $cadencier = $invoice->getCadencier();
        $this->values = array(
            array('date', 'E3', new CelDate($invoice->getDate())),
            array('orderNo', 'E4', $cadencier->getId()),
            array('supplier', 'E5', $cadencier->getSupplier()->getId()),
            array('incoterm', 'E6', $cadencier->getFinalincoterm()),
            array('containerNo', 'E7', $cadencier->getCommand()->GetTcnum()),
            array('sealNo', 'E8', $cadencier->getCommand()->GetSealnum()),
            array('etdeta', 'E9', $cadencier->GetCommand()->GetEtd()->format('d-m-Y') . '/' . $cadencier->GetCommand()->GetEta()->format('d-m-Y')),
            array('paymentTerm', 'E10', $cadencier->getCustomerpaymenterms()->getTitle()),
            array('customerName', 'H3', $cadencier->GetCustomer()->GetName()),

            array('customerAddress', 'H5', $cadencier->GetCustomer()->GetAddress() . ' ' . $cadencier->GetCustomer()->GetPc() . "\n" . $cadencier->GetCustomer()->GetCity() . ' ' . $cadencier->GetCustomer()->GetCountry()->getTitle()),
            /*
                        array('customerAddress', 'H5', $cadencier->GetCustomer()->GetAddress() ),
                        array('customerAddress1', 'H6', $cadencier->GetCustomer()->GetPc()),
                        array('customerAddress2', 'H7', $cadencier->GetCustomer()->GetCity()),
                        array('customerAddress3', 'H8', $cadencier->GetCustomer()->GetCountry()->getTitle())
            */
        );
    }

    /**
     * Headers for the new template of 2/2019
     * @param $cadencier
     */
    private function ciServiceHeaders($invoice)
    {
        list($saler, $email) = $this->getCommercial($invoice->getCustomer() ? $invoice->getCustomer() : $invoice->getCadencier()->getCustomer());
        $this->values = array(
            array('number', 'G1', '#' . $invoice->getNumber()),
            array('date', 'E3', new CelDate($invoice->getDate())),
            array('orderNo', 'E4', $invoice->getCadencier()->getId()),
            array('supplier', 'E5', $invoice->getCadencier()->getSupplier()->getId()),
            // array('validityOffer', 'E6', $invoice->getCadencier()->getSupplier()->getValidityoffer()),
            array('paymentTerm', 'E7', $invoice->getCadencier()->getCustomerpaymenterms()->getTitle()),

            array('customerName', 'H3', $invoice->getCustomer() ? $invoice->getCustomer()->GetName() : $invoice->getCadencier()->getCustomer()->GetName()),
            array('customerAddress', 'H4', $invoice->getCustomer() ? $invoice->getCustomer()->GetAddress() . ' ' . $invoice->getCustomer()->GetPc() . "\n" . $invoice->getCustomer()->GetCity() . ' ' . $invoice->getCustomer()->GetCountry()->getTitle()
                : $invoice->getCadencier()->getCustomer()->GetAddress() . ' ' . $invoice->getCadencier()->getCustomer()->GetPc() . "\n" . $invoice->getCadencier()->getCustomer()->GetCity() . ' ' . $invoice->getCadencier()->getCustomer()->GetCountry()->getTitle()),
//            array('customerAddress1', '', $invoice->getCustomer() ? $invoice->getCustomer()->GetAddress() . ' ' . $invoice->getCustomer()->GetPc() : $invoice->getCadencier()->getCustomer()->GetAddress() . ' ' . $invoice->getCadencier()->getCustomer()->GetPc()),
//            array('customerAddress2', '', $invoice->getCustomer() ? $invoice->getCustomer()->GetCity() . ' ' . $invoice->getCustomer()->GetCountry()->getTitle() : $invoice->getCadencier()->getCustomer()->GetCity() . ' ' . $invoice->getCadencier()->getCustomer()->GetCountry()->getTitle()),

        );
    }

    private function tmpPiCi($cadencier)
    {
        list($saler, $email) = $this->getCommercial($cadencier->getCustomer());

        // if delevery updated, take the value, otherwise take the supplier's
        if( ((int) $cadencier->getDeleverytime()) > 0 ) {
            $deleverytime = $cadencier->getDeleverytime(); // +10 too ?
        } else {
            $deleverytime = $cadencier->getSupplier()->getDeleverytime() + 10;
        }

        // if finalDevis present use it, otherwise, take the current value
        if( !empty($cadencier->getFinalincoterm()) ) {
            $incoterm = $cadencier->getFinalincoterm();
        } else {
            $incoterm = $cadencier->GetIncoterm()->GetTitle();
        }

        $this->values = array(
            array('date', 'E3', new CelDate($cadencier->getStepProduction() ? $cadencier->getStepProduction() : new \datetime())),
            array('supplier', 'E4', $cadencier->getSupplier()->getId()),
            array('incoterm', 'E5', $incoterm),
            array('deliveryTime', 'E6', ($deleverytime + 10) . ' DAYS'),
            array('validityOffer', 'E7', $cadencier->getSupplier()->getValidityoffer() . ' ' . strtoupper($this->translator->trans('TITLE_DAYS'))),
            array('paymentTerm', 'E8', $cadencier->getCustomerpaymenterms()->getTitle()),
            array('customerName', 'H3', $cadencier->GetCustomer()->GetName()),
            array('customerAddress', 'H4', $cadencier->GetCustomer()->GetAddress() . "\n" . $cadencier->GetCustomer()->GetPc() . "\n" . $cadencier->GetCustomer()->GetCity() . "\n" . $cadencier->GetCustomer()->GetCountry()->getTitle()),
            array('customerAddress1', '', $cadencier->GetCustomer()->GetAddress() . ' ' . $cadencier->GetCustomer()->GetPc()),
            array('customerAddress2', '', $cadencier->GetCustomer()->GetCity() . ' ' . $cadencier->GetCustomer()->GetCountry()->getTitle()),
            array('commercialName', '', $saler),
            array('commercialEmail', '', $email),
        );
    }

    private function tmpPiCiService($Invoice)
    {
        list($saler, $email) = $this->getCommercial($Invoice->getCustomer() ? $Invoice->getCustomer() : $Invoice->getCadencier()->getCustomer());
        $this->values = array(
            array('number', 'G1', '#' . $Invoice->getNumber()),
            array('date', 'E3', new CelDate(new \datetime())),
            array('service', 'E4', true), // ?
            // Invoices_customer allows NULL values for the customer column
            array('customerName', 'H3', $Invoice->getCustomer() ? $Invoice->getCustomer()->GetName() : $Invoice->getCadencier()->getCustomer()->GetName()),
            array('customerAddress', 'H4', $Invoice->getCustomer() ? $Invoice->getCustomer()->GetAddress() . ' ' . $Invoice->getCustomer()->GetPc() . "\n" . $Invoice->getCustomer()->GetCity() . ' ' . $Invoice->getCustomer()->GetCountry()->getTitle()
                : $Invoice->getCadencier()->getCustomer()->GetAddress() . ' ' . $Invoice->getCadencier()->getCustomer()->GetPc() . "\n" . $Invoice->getCadencier()->getCustomer()->GetCity() . ' ' . $Invoice->getCadencier()->getCustomer()->GetCountry()->getTitle()),
            array('customerAddress1', '', $Invoice->getCustomer() ? $Invoice->getCustomer()->GetAddress() . ' ' . $Invoice->getCustomer()->GetPc() : $Invoice->getCadencier()->getCustomer()->GetAddress() . ' ' . $Invoice->getCadencier()->getCustomer()->GetPc()),
            array('customerAddress2', '', $Invoice->getCustomer() ? $Invoice->getCustomer()->GetCity() . ' ' . $Invoice->getCustomer()->GetCountry()->getTitle() : $Invoice->getCadencier()->getCustomer()->GetCity() . ' ' . $Invoice->getCadencier()->getCustomer()->GetCountry()->getTitle()),
            array('commercialName', '', $saler),
            array('commercialEmail', '', $email),
        );
    }

    ## functions

    private function init($pdf, $xls, $type, $name)
    {
        if ($type == "pdf") {
            return new ExportPdf($pdf, $name, true, $this->templating, $this->Getwords);
        } else {
            return new ExportExcel($xls, $name, true, $this->translator, $this->Getwords);
        }
    }

    private function output($name, $line, $sayonly = false, $bankInfoRow = null)
    {
        return $this->export->inject($this->rows, $this->services, $this->values, $line, $sayonly, $bankInfoRow)->output($name);
    }

    private function getCommercial($customer)
    {
        if (!$customer) {
            return array('', '');
        }
        if ($customer->getExportsalername() && $customer->getExportsaleremail()) {
            $commercialname = $customer->getExportsalername();
            $commercialemail = $customer->getExportsaleremail();
        } else {
            $commercial = $customer->GetMainsaler();
            if ($commercial) {
                $commercialname = $commercial->GetName() . ' ' . $commercial->getPhone();
                $commercialemail = $commercial->GetEmail();
            }
        }
        return array($commercialname, $commercialemail);
    }

    private function calculateDeposit($invoice)
    {
        $deposit = 0;
        foreach ($invoice->getPayments() as $payment) {
            if ($payment->getType()->getTitle() == 'BANK SLIP DEPOSIT')
                $deposit += $payment->getAmount();
        }
        return $deposit;
    }
}
