<?php

namespace sourcinasia\appBundle\Services\Export;

class Cel
{

    protected $cel;
    public $translate = true;
    public $format = false;
    public $formulas = false;

    public function __construct($cel, $formulas = null)
    {
        $this->cel = $cel;
        $this->formulas = $formulas;
    }

    public function getValue()
    {
        return $this->cel;
    }

}
