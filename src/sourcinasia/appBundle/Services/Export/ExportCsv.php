<?php

namespace sourcinasia\appBundle\Services\Export;

use sourcinasia\appBundle\Entity\Repository\ContainersRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Doctrine\ORM\EntityManager;

class ExportCsv
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * ExportCsv constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function exportAllSuppliers()
    {
        $suppliers = $this->em->getRepository('appBundle:Supplier')->findAllWithContainer();

        $cotnainer_titles = $this->em->getRepository('appBundle:Container')->getAllFromColumn("title");

        $incoterm_titles = $this->em->getRepository('appBundle:Incoterm')->getAllFromColumn("title");

        $fieldsName = [
            'id',
            'name',
            'website',
            'comments',
            'mark',
            'note',
            'deleverytime',
            'offername',
            'infobank',
            'productshidden',
            'exportlicence',
            'businesslicence',
            'offernamefr',
            'validityoffer',
            'shopNumber',
            'contact_name',
            'contact_office_phone',
            'contact_mobile',
            'contact_email',
            'contact_qq',
            'supplier_category',

        ];


        $fieldsName = array_merge($fieldsName,  $incoterm_titles, $cotnainer_titles);

        return $this->fileWriterWithArraySet($suppliers, $fieldsName, 'export-all-suppliers');
    }

    /**
     * @param array $entities
     * @param array $fieldsName
     * @param string $fileName
     * @return Response
     */
    private function fileWriter($entities, $fieldsName, $fileName) {
        $resultSet = [];
        $delimiter = ';';

        $resultSet[] = implode($delimiter, $fieldsName);

        foreach ($entities as $entity) {
            $entityDatas = [];
            foreach ($fieldsName as $fieldName) {
                $getter = 'get'.ucfirst($fieldName);
                $data = $entity->$getter();

                $data = preg_replace("/[\n\r;]/"," ",$data);
                $entityDatas[] = $data;
            }
            $resultSet[] = implode($delimiter, $entityDatas);
        }

        $content = implode("\n", $resultSet);
        $response = new Response($content);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition','attachment; filename="'.$fileName.'".csv"');

        return $response;
    }

    /**
     *
     * when we want to export csv from array
     *
     * @param array $dataSet
     * @param array $fieldsName
     * @param string $fileName
     * @param string $delimiter
     * @return Response
     */
    private function fileWriterWithArraySet($dataSet, $fieldsName, $fileName, $delimiter = ',')
    {

        $resultSet = [];

        $resultSet[] = implode($delimiter, $fieldsName);

        /**
         *  Here we loop on $dataset and check
         *  if value has linebreak then replace it with space.
         */

        foreach ($dataSet as $item) {
            $entityDatas = [];
            foreach ($fieldsName as $fieldName) {
                $data = (array_key_exists($fieldName, $item))? $item[$fieldName]: '';
                $data = preg_replace("/[\n\r;]/"," ",$data);
                $entityDatas[] = $data;
            }
            $resultSet[] = implode($delimiter, $entityDatas);
        }

        $content = implode("\n", $resultSet);

        $response = new Response($content);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $fileName . '.csv"');

        return $response;
    }

}
