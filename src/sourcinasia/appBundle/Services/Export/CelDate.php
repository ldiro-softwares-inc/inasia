<?php

namespace sourcinasia\appBundle\Services\Export;

class CelDate extends Cel
{
    public function __construct(\datetime $cel)
    {
        parent::__construct($cel);
        $this->format = \PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2;
    }

    public function getValue()
    {
        return $this->cel->format('d/m/Y');
    }

    public function getDate()
    {
        return $this->cel;
    }
}