<?php

namespace sourcinasia\appBundle\Services\Export;

use claviska\SimpleImage;

class ExportPdf
{
    public $variables = array();
    public $html = "";
    public $page = "A4";
    public $header = 10;
    public $footer = 5;
    public $imgNbr = 0;
    public $paddingtop = 10;
    public $paddingbottom = 20;
    public $paddingright = 5;
    public $paddingleft = 5;
    public $WatermarkText = "";

    public function __construct($source, $name, $dev, $templating, $Getwords)
    {
        $this->source = $source;
        $this->dev = $dev;
        $this->templating = $templating;
        $this->Getwords = $Getwords;
    }

    public function output($filename = '')
    {
        $pdf = new \mPDF('win-1252', $this->page, '', '', $this->paddingleft, $this->paddingright, $this->paddingtop, $this->paddingbottom, $this->header, $this->footer);
        $pdf->useOnlyCoreFonts = true;
        $pdf->SetTitle("INASIA CORP.Ltd");
        $pdf->SetAuthor("INASIA CORP.Ltd");
        $pdf->SetWatermarkText($this->WatermarkText);
        $pdf->showWatermarkText = (!empty($this->WatermarkText));
        $pdf->watermark_font = 'DejaVuSansCondensed';
        $pdf->watermarkTextAlpha = 0.1;
        $pdf->SetDisplayMode('fullpage');
        $pdf->WriteHTML($this->html);
        $pdf->Output($filename . '[' . date('d-m-Y') . ']' . '.pdf', 'I');
    }

    private function get($cel)
    {
        if (is_object($cel)) {
            if ($cel instanceof CelImage && $cel->getValue()) {
                return $this->setImage($cel->getValue());
            } elseif ($cel instanceof CelCurrency) {
                return number_format($cel->getValue(), 2, ',', '.') . $cel->symbol;
            } elseif ($cel instanceof CelFormule) {
                return null;
            } else {
                return $cel->getValue();
            }
        }
        return $cel;
    }

    private function setImage($image)
    {
        $tmpFile = __SOURCINASIA__ . 'tmp/' . md5($this->imgNbr++) . '.jpg';

        if ($this->dev) {
            file_put_contents($tmpFile, file_get_contents($image));
        } else {
            copy($image, $tmpFile);
        }

        if (file_exists($tmpFile)) {
            $info = getimagesize($tmpFile);
            if (in_array($info['mime'], array('image/gif', 'image/jpeg', 'image/png'))) {
                $img = new SimpleImage($tmpFile);
                $img->fittowidth(320)->bestfit(90, 90)->toFile($tmpFile, null, 70);
                return $tmpFile;
            }
        }
    }

    public function inject(array $rows, array $services, array $values, $row = 1, $sayonly = false)
    {
        // injection du tableau
        $datas = array();
        $datas['TotalUsd'] = 0;
        $datas['TotalCbm'] = 0;
        $datas['TotalQty'] = 0;
        $datas['rows'] = array();
        $datas['services'] = array();
        foreach ($rows as $r => $row) {
            foreach ($row as $k => $cel) {
                $datas['rows'][$r][$k] = $this->get($cel);
            }

            if (array_key_exists('qty', $row)) {
                if (array_key_exists('price', $row)) {
                    $datas['TotalUsd'] = $datas['TotalUsd'] + $row['price']->getValue() * $row['qty'];
                }
                $datas['TotalQty'] = $datas['TotalQty'] + $row['qty'];
            }

            if (array_key_exists('qty', $row) && array_key_exists('cbm', $row) && array_key_exists('pcb', $row)) {
                $datas['TotalCbm'] = $datas['TotalCbm'] + ($row['cbm'] / $row['pcb'] * $row['qty']);
            }

        }

        foreach ($services as $r => $service) {
            $datas['services'][] = $service;
            $datas['TotalUsd'] += $service['value'];
        }

        //injection values
        foreach ($values as $value) {
            if ($value[2] instanceof CelDate) {
                $datas[$value[0]] = $value[2]->getDate();
            } else {
                $datas[$value[0]] = $value[2];
            }
        }

        $datas['sayonly'] = $this->Getwords->num_words(number_format(($datas['TotalUsd']), 2, '.', ''));
        $this->html = $this->templating->render($this->source, $datas);
        return $this;
    }
}
