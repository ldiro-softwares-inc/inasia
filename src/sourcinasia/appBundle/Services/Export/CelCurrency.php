<?php

namespace sourcinasia\appBundle\Services\Export;

class CelCurrency extends Cel
{
    public function __construct($cel, $currency = 'usd')
    {
        parent::__construct($cel);

        switch (mb_strtolower($currency)) {
            case 'thb':
                $this->format = '[$THB ]#,##0.00_-';
                $this->symbol = 'THB';
                $this->text = 'THB';
                break;
            case 'cny':
                $this->format = '[$CNY ]#,##0.00_-';
                $this->symbol = 'CNY';
                $this->text = 'CNY';
                break;
            case 'rmb':
                $this->format = '[$RMB ]#,##0.00_-';
                $this->symbol = 'RMB';
                $this->text = 'RMB';
                break;
            case 'eur':
                $this->format = \PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE;
                $this->symbol = '€';
                $this->text = 'EUR';
                break;
            case 'euro':
                $this->format = \PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE;
                $this->symbol = '€';
                $this->text = 'EUR';
                break;
            case '€':
                $this->format = \PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE;
                $this->symbol = '€';
                $this->text = 'EUR';
                break;
            default:
                $this->format = \PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE;
                $this->symbol = '$';
                $this->text = 'USD';
        }

    }

}
