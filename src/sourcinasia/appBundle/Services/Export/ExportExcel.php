<?php

namespace sourcinasia\appBundle\Services\Export;
use claviska\SimpleImage;

class ExportExcel
{

    protected $obj;
    private $col = 'A';
    private $row = '1';
    private $matrice;
    private $imgNbr = 0;

    public function __construct($source, $name, $dev, $translator, $Getwords)
    {
        $this->load($source);
        $this->setSheetName($name);
        $this->dev = $dev;
        $this->translator = $translator;
        $this->Getwords = $Getwords;
        $this->matrice = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    }

    public function load($template = false)
    {
        $file = dirname(__FILE__) . "/../../Resources/masters/" . $template;
        if ($template && file_exists($file)) {
            $this->obj = \PHPExcel_IOFactory::createReader('Excel2007');
            $this->obj = $this->obj->load($file);
//            $newSheet = clone $this->obj->getActiveSheet();
//            $newSheet->setTitle('cloned');
//            $this->obj->addSheet($newSheet);

        } else {
            $this->obj = new \PHPExcel();
            $this->obj->getProperties()->setCreator("INASIA")
                ->setLastModifiedBy("INASIA")
                ->setTitle("INASIA")
                ->setSubject("INASIA")
                ->setDescription("")
                ->setKeywords("")
                ->setCategory("INASIA");
        }

        $this->obj->setActiveSheetIndex(0);
        return $this;
    }

    public function setSheetName($name)
    {
        $this->obj->getActiveSheet()->setTitle($name);
        return $this;
    }

    public function getSheetName() {
        return $this->obj->getActiveSheet()->getTitle();
    }

    public function setInitline($line)
    {
        $this->setCol('A');
        $this->setRow($line);
        return $this;
    }

    public function output($filename, $format = "xls")
    {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '[' . date('d-m-Y') . ']' . '.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $this->obj->getActiveSheet()->getPageSetup()->setPrintArea('A1:' . $this->obj->getActiveSheet()->getHighestColumn() . $this->row);

//        $sheetCount = $this->obj->getSheetCount();
//        $this->obj->removeSheetByIndex($sheetCount - 1);

        $objWriter = \PHPExcel_IOFactory::createWriter($this->obj, 'Excel2007');
        $objWriter->setPreCalculateFormulas(true);
        $objWriter->save('php://output');
        exit;
    }

    public function inject(array $rows, array $services, array $values, $row = 1, $sayonly = false, $bankInfoRow = null)
    {
        // injections des values
        foreach ($values as $key => $value) {
            if ($value[1]) {
                if ($value[0] == 'orderNo') {
                   $this->obj->getActiveSheet()->setCellValueExplicit($value[1],$value[2],\PHPExcel_Cell_DataType::TYPE_STRING);
                   $this->obj->getActiveSheet()->getStyle($value[1])->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                } else {
                    $this->set($value[2], $value[1]);
                }
            }
        }

        // add 1 more for CI
        if ((count($rows) + count($services)) != 2) {
            $this->obj->getActiveSheet()->insertNewRowBefore($row, ((count($rows) + count($services)) - 2));
        }

        // injections des rows
        $TotalUsd = 0;
        $this->setInitline($row - 1);
        foreach ($rows as $record) {
            if (array_key_exists('image', $record) && isset($record['image'])) {
                $this->obj->getActiveSheet()->getRowDimension($this->getRow())->setRowHeight(112);
            }
            foreach ($record as $key => $cel) {
                if ($key == 'material' || $key == 'description' || $key = 'packing') {
                    $this->obj->getActiveSheet()->getStyle($this->cursorCoord())->getAlignment()->setWrapText(true);
                }

                $this->set($cel);
                $this->cursorNext();
            }
            $this->cursorRetourChariot();
            if (array_key_exists('qty', $record) and array_key_exists('price', $record)) {
                $TotalUsd = $TotalUsd + $record['price']->getValue() * $record['qty'];
            }

        }

        foreach ($services as $service) {
            $blankColumns = 5;
            if ($this->getSheetName() == 'Commercial Invoice Service') {
                $blankColumns = 6;
                $currentRow = $this->getRow();
                $this->obj->getActiveSheet()->mergeCells('B'.$currentRow.':G'.$currentRow);
            }
            if (array_key_exists('value', $service)) {
                $this->set('Services')->cursorNext()->set($service['description'])->cursorNext($blankColumns)->set($service['value'])->cursorNext()->set(1)->cursorNext(7-$blankColumns)->set($service['value']);
                $this->cursorRetourChariot();
                $TotalUsd = $TotalUsd + $service['value'];
            }
        }

        if ($sayonly) {
            $total = explode(',', number_format($TotalUsd, 2, ',', ''));
            if (array_key_exists(0, $total)) {
                $total1 = $this->Getwords->num_words($total[0]);
            }
            if (array_key_exists(1, $total)) {
                $total2 = " AND CENTS " . $this->Getwords->num_words($total[1]);
            } else {
                $total2 = "";
            }
            $sayonlyrow = $this->getRow() + 4;

//            $this->set("SAY ONLY TOTAL ". $total1 . $total2, $sayonly);

            $this->obj->getActiveSheet()->setCellValue('A'.$sayonlyrow , "SAY ONLY TOTAL ". $total1 . $total2);

        }
        if ($bankInfoRow) {
            $this->obj->getActiveSheet()->setCellValue('A'.$bankInfoRow, str_replace('<br/>', "\r",  preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $this->translator->trans('BANK_INFORMATION'))));
            $this->obj->getActiveSheet()->getRowDimension($bankInfoRow)->setRowHeight(80);
        }
// cell background fix for CI export, D3:D10, G3:G10
        if ($this->getSheetName() == 'Commercial Invoice') {
/*
            $this->obj->getActiveSheet()->getStyle('D3:D10')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('7F7F7F7F');
            $this->obj->getActiveSheet()->getStyle('G3:G10')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('7F7F7F7F');
*/

            // $this->obj->getActiveSheet()->getStyle('D3:D10')->applyFromArray(
            //     array(
            //         'fill' => array (
            //             'type' => \PHPExcel_Style_Fill::FILL_GRADIENT_PATH,
            //             'rotation' => 180,
            //             'startcolor' => array(
            //                 'argb' => '00000000'
            //             ),
            //             'endcolor' => array(
            //                 'argb' => 'FFFFFFFF'
            //             )
            //         )
            //     )
            // );
            $this->obj->getActiveSheet()->getStyle('G3:G10')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('7F7F7F7F');
            $this->obj->getActiveSheet()->mergeCells('H5:L8');

        }


        return $this;
    }

    public function set($cel, $coord = false)
    {
        $value = false;
        if (is_object($cel)) {
            if ($cel instanceof CelImage && $cel->getValue()) {
                $this->setImage($cel->getValue());
            } else {
                if ($cel->format) {
                    $this->obj->getActiveSheet()->getStyle($this->cursorCoord())->getNumberFormat()->setFormatCode($cel->format);
                }

                $value = $cel->getValue();

                if ($cel->formulas) {
                    $value = str_replace('@', $this->getRow(), $value);
                } else {
                    if ($cel->translate) {
                        $value = $this->translator->trans($value);
                    }
                }
            }
        } else {
            if ($trans = $this->translator->trans($cel)) {
                $value = $trans;
            } else {
                $value = $cel;
            }
        }

        $this->obj->getActiveSheet()->setCellValue(($coord ? $coord : $this->cursorCoord()), (string)$value);

        return $this;
    }

    public function setCellSize($cellCoord, $fontSize) {
        $this->obj->getActiveSheet()->getStyle($cellCoord)->getFont()->setSize($fontSize);
        return $this;
    }

    private function setImage($image, $coord = false)
    {
        $tmpFile = __SOURCINASIA__ . 'tmp/' . md5($this->imgNbr++) . '.jpg';

        if ($this->dev) { // add error handling:
            file_put_contents($tmpFile, @file_get_contents($image));
        } else {
            copy($image, $tmpFile);
        }

        if (file_exists($tmpFile)) {
            $info = $tmpFile ? @getimagesize($tmpFile) : 0;
            if (in_array($info['mime'], array('image/gif', 'image/jpeg', 'image/png'))) {
                $img = new SimpleImage($tmpFile);
                $img->fittowidth(162)->bestfit(162, 143)->toFile($tmpFile,null, 70);
                $objDrawing = new \PHPExcel_Worksheet_Drawing();
                $objDrawing->setName('INASIA');
                $objDrawing->setDescription('INASIA');
                $objDrawing->setPath($tmpFile);
                $objDrawing->setCoordinates(($coord ? $coord : $this->cursorCoord()));
                $objDrawing->setOffsetX(2);
                $objDrawing->setOffsetY(2);
                $objDrawing->setWorksheet($this->obj->getActiveSheet());
            }
        }
        return $this;
    }

    private function cursorSetValue($value)
    {
        $this->set($value);
        return $this;
    }

    private function cursorStyle($coord = false)
    {
        return $this->obj->getActiveSheet()->getStyle($coord ? $coord : $this->cursorCoord());
    }

    private function cursorRetourChariot()
    {
        $this->setCol('A');
        $this->setRow($this->getRow() + 1);
    }

    private function cursorCoord()
    {
        return $this->getCol() . $this->getRow();
    }

    private function cursorNext($i = 1)
    {
        for ($j = 0; $j < $i; $j++) {
            $col = array_search($this->getCol(), $this->matrice);
            $this->setCol($this->matrice[$col + 1]);
        }
        return $this;
    }

    private function setRow($line = 1)
    {
        $this->row = $line;
    }

    private function getRow()
    {
        return $this->row;
    }

    private function setCol($letter = 'A')
    {
        $this->col = $letter;
    }

    private function getCol()
    {
        return $this->col;
    }

}
