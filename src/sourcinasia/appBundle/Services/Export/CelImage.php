<?php

namespace sourcinasia\appBundle\Services\Export;

class CelImage extends Cel
{

    public function __construct($cel, $dev = false)
    {
        if ($dev) {
            $cel = 'https://market.inasia-corp.com/' . $cel;
        }
        parent::__construct(str_replace('original.jpg', 'small.jpg', $cel));
        $this->translate = false;
    }

}
