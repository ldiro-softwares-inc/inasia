<?php

namespace sourcinasia\appBundle\Services\ImageRotate;


use Doctrine\ORM\EntityManagerInterface;
use sourcinasia\appBundle\Entity\Contact;
use sourcinasia\appBundle\Entity\Image;
use sourcinasia\appBundle\Entity\Supplier;
use sourcinasia\appBundle\Entity\Suppliercategory;
use Symfony\Component\BrowserKit\Request;

class ImageRotate
{
    /**
     * @var $em
     */
    protected $em;


    /**
     * ImageRotateService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param $image
     * @param $source
     * @param $destination
     * @return Image|void
     */

    public function uploadImage($image, $source, $destination)    {

        $image = ($image instanceof Image)? $image : new Image();

        if (!empty($image) && !empty($filepath)) {
            copy($source, $destination);
            unlink($source);

            $image->setImage($image->MyDirectory() . md5($image->getFilename()) . '/original.jpg');
            $image->setImageKey(md5($image->getFilename()));
            $image->setCreated(new \DateTime());
            return $this->rotateImage($image->getFullImagePath(),$image);

        } else {
            $fileName = date('ymdhis') /*. $image->image->getClientOriginalName()*/;
            $image->setFilename($fileName);
            $image->image->move($image->getTmpUploadRootDir(), $fileName);
            $image->setImage($image->MyDirectory() . md5($fileName) . '/original.jpg');
            $image->setImageKey(md5($image->getFilename()));

            return $this->rotateImage($image->getFullImagePath(),$image);
        }
        return $image;
    }

    /**
     * @param $source
     * @param $angle
     */

    public function rotateImage( $source,$angle) {

        if (null === $source)
            return;

        $info = getimagesize($source);
        $img = new \claviska\SimpleImage();

        switch ($info['mime']) {
            case 'image/gif':

                $img->fromFile($source)->rotate($angle)->toFile($source);
                break;
            case 'image/jpeg':
                $img->fromFile($source)->rotate($angle)->toFile($source);
                break;
            case 'image/png':
                $img->fromFile($source)->rotate($angle)->toFile($source);
                break;
            default:
                break;
        }
    }


}
