<?php

namespace sourcinasia\appBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use sourcinasia\appBundle\Entity\Supplier;

/**
 * Class DocumentRepository
 * @package sourcinasia\appBundle\Entity\Repository
 */
class DocumentRepository extends EntityRepository
{
    public function GetSupplierDocumentsOfType(Supplier $supplier,String $type){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d')
            ->from('appBundle:Document', 'd')
            ->where('d.documentType = :documentType')
            ->andWhere('d.supplier = :supplier')
            ->orderBy('d.id', 'DESC')
            ->setParameters(array('documentType' => $type, 'supplier' => $supplier));

        return $qb->getQuery()->getResult(Query::HYDRATE_OBJECT);
    }
}
