<?php

namespace sourcinasia\appBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * SupplychainRepository
 *
 */
class SupplychainRepository extends EntityRepository {

    public function search($request) {
        $params = array();
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')
                ->from('appBundle:Cadencier', 'c');
        $qb->andWhere('c.command is not NULL');
        $qb->andWhere('c.state >1');
        $qb->orderBy('c.created', 'ASC');
        $qb->setParameters($params);

        return $qb->getQuery()->getResult();
    }

    public function getCustomerDocuments($cadencier) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('supplychain.id, supplychaintype.id as supplychaintypeid')
                ->from('appBundle:Supplychain', 'supplychain')
                ->leftjoin('supplychain.supplychaintype', 'supplychaintype');
        $qb->andWhere('supplychain.validate  = 1');
        $qb->andWhere('supplychain.supplychaintype in (37,38)');
        $qb->andWhere('supplychain.cadencier = :cadencier');
        $qb->setParameters(array('cadencier' => $cadencier));
        return $qb->getQuery()->getArrayResult();
    }

    public function getCustomerDocument($idsupplychain, $cadencier, $customer) {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('supplychain')
                ->from('appBundle:Supplychain', 'supplychain')
                ->leftjoin('supplychain.cadencier', 'cadencier');
        $qb->andWhere('supplychain.validate  = 1');
        $qb->andWhere('supplychain.supplychaintype in (37,38)');
        $qb->andWhere('supplychain.cadencier = :cadencier');
        $qb->andWhere('supplychain.id = :idsupplychain');
        $qb->andWhere('cadencier.customer = :customer');
        $qb->setParameters(array('cadencier' => $cadencier, 'idsupplychain' => $idsupplychain, 'customer' => $customer));
        return $qb->getQuery()->getSingleResult();
    }

}
