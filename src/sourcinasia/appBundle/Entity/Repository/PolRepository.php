<?php

namespace sourcinasia\appBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CadencierRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PolRepository extends EntityRepository {
    /*
     * SupplierController.php|importAction
     */

    public function getArrayDatas() {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('p.title')->from('appBundle:Pol', 'p');
        $polresult = array();
        foreach ($query->getQuery()->getArrayResult() as $pol)
            $polresult[$pol['title']] = $pol['title'];
        return $polresult;
    }

    /*
     * ProductController.php|filtersAction
     */

    public function getPols($filters) {
        print_r($filters); die;

        return $polresult;
    }

}
