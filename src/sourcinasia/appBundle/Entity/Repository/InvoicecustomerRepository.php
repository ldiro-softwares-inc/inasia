<?php

namespace sourcinasia\appBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * InvoicecustomerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class InvoicecustomerRepository extends EntityRepository {

    public function GetAdminExport($start, $stop) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('Invoicecustomer.id as Chrono, cadencier.loadingdate as LoadingDate, mainsaler.name as Saler, customer.name as Customer, Invoicecustomer.amount as TotalValue')
                ->from('appBundle:Invoicecustomer', 'Invoicecustomer')
                ->leftjoin('Invoicecustomer.cadencier', 'cadencier')
                ->leftjoin('cadencier.customer', 'customer')
                ->leftjoin('cadencier.user', 'user')
                ->leftjoin('customer.mainsaler', 'mainsaler')
                ->where('cadencier.locked = :locked')
                ->andwhere('cadencier.created BETWEEN :start and :stop')
                ->groupby('Invoicecustomer.id')
                ->setParameters(array('locked' => 1, 'start' => $start, 'stop' => $stop));

        $result = $qb->getQuery()->getArrayResult();

        foreach ($result as $k => $payment) {
            $result[$k]['CiNumber'] = $payment['LoadingDate']->format('Ymd') . sprintf("%06d", $payment['CiNumber']);
        }

        return $result;
    }


}
