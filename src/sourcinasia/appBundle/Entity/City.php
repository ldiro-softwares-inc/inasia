<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 */
class City {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \sourcinasia\appBundle\Entity\Provinc
     */
    private $province;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return City
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set province
     *
     * @param \sourcinasia\appBundle\Entity\Province $province
     * @return City
     */
    public function setProvince(\sourcinasia\appBundle\Entity\Province $province = null)
    {
        $this->province = $province;
    
        return $this;
    }

    /**
     * Get province
     *
     * @return \sourcinasia\appBundle\Entity\Province 
     */
    public function getProvince()
    {
        return $this->province;
    }
    
    public function getAddressname(){
         return $this->getProvince()->getCountry()->GetTitle() . '>' . $this->getProvince()->GetTitle() . '>' . $this->title;
    }
}
