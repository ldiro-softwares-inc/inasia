<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Command
 */
class Command {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $shipper;

    /**
     * @var string
     */
    private $consignee;

    /**
     * @var string
     */
    private $notify;

    /**
     * @var string
     */
    private $freightpayableat;

    /**
     * @var string
     */
    private $bookingnumber;

    /**
     * @var \DateTime
     */
    private $etd;

    /**
     * @var \DateTime
     */
    private $eta;

    /**
     * @var string
     */
    private $vessel;

    /**
     * @var string
     */
    private $tcnum;

    /**
     * @var string
     */
    private $sealnum;
    
    /**
     * @var string
     */
    private $descriptionofgoods;
    /**
     * @var boolean
     */
    private $locked;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $stepLoading;

    /**
     * @var \DateTime
     */
    private $stepOnboard;

    /**
     * @var \DateTime
     */
    private $stepDelivery;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cadenciers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $supplychains;

    /**
     * @var \sourcinasia\appBundle\Entity\Shippingcompany
     */
    private $shippingcompany;

    /**
     * @var \sourcinasia\appBundle\Entity\Pod
     */
    private $pod;

    /**
     * @var \sourcinasia\appBundle\Entity\Container
     */
    private $container;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cadenciers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supplychains = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shipper
     *
     * @param string $shipper
     * @return Command
     */
    public function setShipper($shipper)
    {
        $this->shipper = $shipper;

        return $this;
    }

    /**
     * Get shipper
     *
     * @return string 
     */
    public function getShipper()
    {
        return $this->shipper;
    }

    /**
     * Set consignee
     *
     * @param string $consignee
     * @return Command
     */
    public function setConsignee($consignee)
    {
        $this->consignee = $consignee;

        return $this;
    }

    /**
     * Get consignee
     *
     * @return string 
     */
    public function getConsignee()
    {
        return $this->consignee;
    }

    /**
     * Set notify
     *
     * @param string $notify
     * @return Command
     */
    public function setNotify($notify)
    {
        $this->notify = $notify;

        return $this;
    }

    /**
     * Get notify
     *
     * @return string 
     */
    public function getNotify()
    {
        return $this->notify;
    }

    /**
     * Set freightpayableat
     *
     * @param string $freightpayableat
     * @return Command
     */
    public function setFreightpayableat($freightpayableat)
    {
        $this->freightpayableat = $freightpayableat;

        return $this;
    }

    /**
     * Get freightpayableat
     *
     * @return string 
     */
    public function getFreightpayableat()
    {
        return $this->freightpayableat;
    }

    /**
     * Set bookingnumber
     *
     * @param string $bookingnumber
     * @return Command
     */
    public function setBookingnumber($bookingnumber)
    {
        $this->bookingnumber = $bookingnumber;

        return $this;
    }

    /**
     * Get bookingnumber
     *
     * @return string 
     */
    public function getBookingnumber()
    {
        return $this->bookingnumber;
    }

    /**
     * Set etd
     *
     * @param \DateTime $etd
     * @return Command
     */
    public function setEtd($etd)
    {
        $this->etd = $etd;

        return $this;
    }

    /**
     * Get etd
     *
     * @return \DateTime 
     */
    public function getEtd()
    {
        return $this->etd;
    }

    /**
     * Set eta
     *
     * @param \DateTime $eta
     * @return Command
     */
    public function setEta($eta)
    {
        $this->eta = $eta;

        return $this;
    }

    /**
     * Get eta
     *
     * @return \DateTime 
     */
    public function getEta()
    {
        return $this->eta;
    }

    /**
     * Set vessel
     *
     * @param string $vessel
     * @return Command
     */
    public function setVessel($vessel)
    {
        $this->vessel = $vessel;

        return $this;
    }

    /**
     * Get vessel
     *
     * @return string 
     */
    public function getVessel()
    {
        return $this->vessel;
    }

    /**
     * Set tcnum
     *
     * @param string $tcnum
     * @return Command
     */
    public function setTcnum($tcnum)
    {
        $this->tcnum = $tcnum;

        return $this;
    }

    /**
     * Get tcnum
     *
     * @return string 
     */
    public function getTcnum()
    {
        return $this->tcnum;
    }

    /**
     * Set sealnum
     *
     * @param string $sealnum
     * @return Command
     */
    public function setSealnum($sealnum)
    {
        $this->sealnum = $sealnum;

        return $this;
    }

    /**
     * Get sealnum
     *
     * @return string 
     */
    public function getSealnum()
    {
        return $this->sealnum;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     * @return Command
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean 
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Command
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Command
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set stepLoading
     *
     * @param \DateTime $stepLoading
     * @return Command
     */
    public function setStepLoading($stepLoading)
    {
        $this->stepLoading = $stepLoading;

        return $this;
    }

    /**
     * Get stepLoading
     *
     * @return \DateTime 
     */
    public function getStepLoading()
    {
        return $this->stepLoading;
    }

    /**
     * Set stepOnboard
     *
     * @param \DateTime $stepOnboard
     * @return Command
     */
    public function setStepOnboard($stepOnboard)
    {
        $this->stepOnboard = $stepOnboard;

        return $this;
    }

    /**
     * Get stepOnboard
     *
     * @return \DateTime 
     */
    public function getStepOnboard()
    {
        return $this->stepOnboard;
    }

    /**
     * Set stepDelivery
     *
     * @param \DateTime $stepDelivery
     * @return Command
     */
    public function setStepDelivery($stepDelivery)
    {
        $this->stepDelivery = $stepDelivery;

        return $this;
    }

    /**
     * Get stepDelivery
     *
     * @return \DateTime 
     */
    public function getStepDelivery()
    {
        return $this->stepDelivery;
    }

    /**
     * Add cadenciers
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadenciers
     * @return Command
     */
    public function addCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadenciers)
    {
        $this->cadenciers[] = $cadenciers;

        return $this;
    }

    /**
     * Remove cadenciers
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadenciers
     */
    public function removeCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadenciers)
    {
        $this->cadenciers->removeElement($cadenciers);
    }

    /**
     * Get cadenciers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCadenciers()
    {
        return $this->cadenciers;
    }

    /**
     * Add supplychains
     *
     * @param \sourcinasia\appBundle\Entity\Supplychain $supplychains
     * @return Command
     */
    public function addSupplychain(\sourcinasia\appBundle\Entity\Supplychain $supplychains)
    {
        $this->supplychains[] = $supplychains;

        return $this;
    }

    /**
     * Remove supplychains
     *
     * @param \sourcinasia\appBundle\Entity\Supplychain $supplychains
     */
    public function removeSupplychain(\sourcinasia\appBundle\Entity\Supplychain $supplychains)
    {
        $this->supplychains->removeElement($supplychains);
    }

    /**
     * Get supplychains
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplychains()
    {
        return $this->supplychains;
    }

    /**
     * Set shippingcompany
     *
     * @param \sourcinasia\appBundle\Entity\Shippingcompany $shippingcompany
     * @return Command
     */
    public function setShippingcompany(\sourcinasia\appBundle\Entity\Shippingcompany $shippingcompany = null)
    {
        $this->shippingcompany = $shippingcompany;

        return $this;
    }

    /**
     * Get shippingcompany
     *
     * @return \sourcinasia\appBundle\Entity\Shippingcompany 
     */
    public function getShippingcompany()
    {
        return $this->shippingcompany;
    }

    /**
     * Set pod
     *
     * @param \sourcinasia\appBundle\Entity\Pod $pod
     * @return Command
     */
    public function setPod(\sourcinasia\appBundle\Entity\Pod $pod = null)
    {
        $this->pod = $pod;

        return $this;
    }

    /**
     * Get pod
     *
     * @return \sourcinasia\appBundle\Entity\Pod 
     */
    public function getPod()
    {
        return $this->pod;
    }

    /**
     * Set container
     *
     * @param \sourcinasia\appBundle\Entity\Container $container
     * @return Command
     */
    public function setContainer(\sourcinasia\appBundle\Entity\Container $container = null)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * Get container
     *
     * @return \sourcinasia\appBundle\Entity\Container 
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Set descriptionofgoods
     *
     * @param string $descriptionofgoods
     * @return Command
     */
    public function setDescriptionofgoods($descriptionofgoods)
    {
        $this->descriptionofgoods = $descriptionofgoods;

        return $this;
    }

    /**
     * Get descriptionofgoods
     *
     * @return string 
     */
    public function getDescriptionofgoods()
    {
        return $this->descriptionofgoods;
    }
    
    
}
