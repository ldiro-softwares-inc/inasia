<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Unit
 */
class Unit
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var boolean
     */
    private $moqpackingunit;

    /**
     * @var boolean
     */
    private $moqunit;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Unit
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set moqpackingunit
     *
     * @param boolean $moqpackingunit
     * @return Unit
     */
    public function setMoqpackingunit($moqpackingunit)
    {
        $this->moqpackingunit = $moqpackingunit;
    
        return $this;
    }

    /**
     * Get moqpackingunit
     *
     * @return boolean 
     */
    public function getMoqpackingunit()
    {
        return $this->moqpackingunit;
    }

    /**
     * Set moqunit
     *
     * @param boolean $moqunit
     * @return Unit
     */
    public function setMoqunit($moqunit)
    {
        $this->moqunit = $moqunit;
    
        return $this;
    }

    /**
     * Get moqunit
     *
     * @return boolean 
     */
    public function getMoqunit()
    {
        return $this->moqunit;
    }
}
