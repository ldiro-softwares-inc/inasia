<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Suppliercategory
 */
class Suppliercategory
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var int
     */
    protected $margin_rate;

    /**
     * @ORM\OneToMany(targetEntity="sourcinasia\appBundle\Entity\Supplier", mappedBy="supplierCategory")
     */
    protected $suppliers;

    /**
     * @var \DateTime
     */
    protected $created_at;

    /**
     * Category constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->title       = null;
        $this->margin_rate = 1;
        $this->created_at  = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Suppliercategory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getMarginRate()
    {
        return $this->margin_rate;
    }

    /**
     * @param int $margin_rate
     *
     * @return Suppliercategory
     */
    public function setMarginRate($margin_rate)
    {
        $this->margin_rate = $margin_rate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSuppliers()
    {
        return $this->suppliers;
    }

    /**
     * @param $suppliers
     *
     * @return Suppliercategory
     */
    public function setSuppliers($suppliers)
    {
        $this->suppliers = $suppliers;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     *
     * @return Suppliercategory
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }
}