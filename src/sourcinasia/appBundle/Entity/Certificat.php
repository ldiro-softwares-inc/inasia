<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Certificat
 */
class Certificat {

   
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \sourcinasia\appBundle\Entity\Norme
     */
    private $norme;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Certificat
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set norme
     *
     * @param \sourcinasia\appBundle\Entity\Norme $norme
     * @return Certificat
     */
    public function setNorme(\sourcinasia\appBundle\Entity\Norme $norme = null)
    {
        $this->norme = $norme;

        return $this;
    }

    /**
     * Get norme
     *
     * @return \sourcinasia\appBundle\Entity\Norme 
     */
    public function getNorme()
    {
        return $this->norme;
    }
}
