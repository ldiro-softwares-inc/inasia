<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoicecustomer
 */
class Invoicecustomer {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var \sourcinasia\appBundle\Entity\Cadencier
     */
    private $cadencier;

    /**
     * @var \sourcinasia\appBundle\Entity\Customer
     */
    private $customer;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $payments;

    /**
     * @var boolean
     */
    private $service;

    /**
     * @var integer
     */
    private $chrono;

    /**
     * @var string
     */
    private $description;

    /**
     * @var boolean
     */
    private $confirm;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplychain
     */
    private $ci;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplychain
     */
    private $pi;



    /**
     * Constructor
     */
    public function __construct() {
        $this->payments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function setId() {
        return $this->id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Invoicecustomer
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Invoicecustomer
     */
    public function setAmount($amount) {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount() {
        return $this->amount;
    }

    /**
     * Set cadencier
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadencier
     * @return Invoicecustomer
     */
    public function setCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadencier = null) {
        $this->cadencier = $cadencier;

        return $this;
    }

    /**
     * Get cadencier
     *
     * @return \sourcinasia\appBundle\Entity\Cadencier 
     */
    public function getCadencier() {
        return $this->cadencier;
    }

    /**
     * Set customer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customer
     * @return Invoicecustomer
     */
    public function setCustomer(\sourcinasia\appBundle\Entity\Customer $customer = null) {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \sourcinasia\appBundle\Entity\Customer 
     */
    public function getCustomer() {
        return $this->customer;
    }

    /**
     * Add payments
     *
     * @param \sourcinasia\appBundle\Entity\Payment $payments
     * @return Invoicecustomer
     */
    public function addPayment(\sourcinasia\appBundle\Entity\Payment $payments) {
        $this->payments[] = $payments;

        return $this;
    }

    /**
     * Remove payments
     *
     * @param \sourcinasia\appBundle\Entity\Payment $payments
     */
    public function removePayment(\sourcinasia\appBundle\Entity\Payment $payments) {
        $this->payments->removeElement($payments);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPayments() {
        return $this->payments;
    }

    public function getNumber() {
        if ($this->getService()) {
            if ($this->getCadencier() && $this->getDate() && $this->getChrono())
                return $this->getDate()->format('Ymd') . sprintf("%04d", $this->getChrono());
            elseif ($this->getCadencier() && $this->getDate() && !$this->getChrono())
                return 'PLEASE ENTER CHRONO FOR ORDER';
        } else {
            if ($this->getCadencier() && $this->getCadencier()->getLoadingdate() && $this->getChrono())
                return $this->getDate()->format('Ymd') . sprintf("%04d", $this->getChrono());
            elseif ($this->getCadencier() && $this->getCadencier()->getLoadingdate() && !$this->getChrono())
                return 'PLEASE ENTER CHRONO FOR ORDER';
            else
                return 'WAITING FOR LOADING DATE';
        }
    }


    /**
     * Set service
     *
     * @param boolean $service
     * @return Invoicecustomer
     */
    public function setService($service) {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return boolean 
     */
    public function getService() {
        return $this->service;
    }

    /**
     * Set chrono
     *
     * @param integer $chrono
     * @return Invoicecustomer
     */
    public function setChrono($chrono) {
        $this->chrono = $chrono;

        return $this;
    }

    /**
     * Get chrono
     *
     * @return integer 
     */
    public function getChrono() {
        return $this->chrono;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Invoicecustomer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set confirm
     *
     * @param boolean $confirm
     *
     * @return Invoicecustomer
     */
    public function setConfirm($confirm)
    {
        $this->confirm = $confirm;

        return $this;
    }

    /**
     * Get confirm
     *
     * @return boolean
     */
    public function getConfirm()
    {
        return $this->confirm;
    }

    /**
     * Set ci
     *
     * @param \sourcinasia\appBundle\Entity\Supplychain $ci
     *
     * @return Invoicecustomer
     */
    public function setCi(\sourcinasia\appBundle\Entity\Supplychain $ci = null)
    {
        $this->ci = $ci;

        return $this;
    }

    /**
     * Get ci
     *
     * @return \sourcinasia\appBundle\Entity\Supplychain
     */
    public function getCi()
    {
        return $this->ci;
    }

    /**
     * Set pi
     *
     * @param \sourcinasia\appBundle\Entity\Supplychain $pi
     *
     * @return Invoicecustomer
     */
    public function setPi(\sourcinasia\appBundle\Entity\Supplychain $pi = null)
    {
        $this->pi = $pi;

        return $this;
    }

    /**
     * Get pi
     *
     * @return \sourcinasia\appBundle\Entity\Supplychain
     */
    public function getPi()
    {
        return $this->pi;
    }


}
