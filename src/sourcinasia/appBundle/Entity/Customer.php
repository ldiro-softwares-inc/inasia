<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customer
 */
class Customer {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $siret;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var boolean
     */
    private $xlsexport;

    /**
     * @var boolean
     */
    private $commercialtools;

    /**
     * @var integer
     */
    private $margecoef;

    /**
     * @var integer
     */
    private $customermargecoef;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $nomenclatures;

    /**
     * @var string
     */
    private $pc;



    /**
     * @var string
     */
    private $exportsalername;

    /**
     * @var string
     */
    private $exportsaleremail;

    /**
     * @var string
     */
    private $exportfile;

    /**
     * @var \sourcinasia\appBundle\Entity\Logo
     */
    private $logo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $historiescustomer;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $histories;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $contacts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cadenciers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $chats;

    /**
     * @var \sourcinasia\appBundle\Entity\Country
     */
    private $country;

    /**
     * @var \sourcinasia\appBundle\Entity\Activity
     */
    private $activity;

    /**
     * @var \sourcinasia\appBundle\Entity\Paymentcondition
     */
    private $paymentcondition;

    /**
     * @var \sourcinasia\appBundle\Entity\User
     */
    private $mainsaler;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $quotationsheets;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $products;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $categories;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $categoriesrestriction;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $suppliersrestriction;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $suppliersexception;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $zones;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $owners;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $catalogs;

    /**
     * Constructor
     */
    public function __construct() {
        $this->historiescustomer = new \Doctrine\Common\Collections\ArrayCollection();
        $this->histories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contacts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cadenciers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->chats = new \Doctrine\Common\Collections\ArrayCollection();
        $this->quotationsheets = new \Doctrine\Common\Collections\ArrayCollection();
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categoriesrestriction = new \Doctrine\Common\Collections\ArrayCollection();
        $this->suppliersrestriction = new \Doctrine\Common\Collections\ArrayCollection();
        $this->zones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->owners = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Customer
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set siret
     *
     * @param string $siret
     * @return Customer
     */
    public function setSiret($siret) {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string 
     */
    public function getSiret() {
        return $this->siret;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return Customer
     */
    public function setLocale($locale) {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * Set xlsexport
     *
     * @param boolean $xlsexport
     * @return Customer
     */
    public function setXlsexport($xlsexport) {
        $this->xlsexport = $xlsexport;

        return $this;
    }

    /**
     * Get xlsexport
     *
     * @return boolean 
     */
    public function getXlsexport() {
        return $this->xlsexport;
    }

    /**
     * Set commercialtools
     *
     * @param boolean $commercialtools
     * @return Customer
     */
    public function setCommercialtools($commercialtools) {
        $this->commercialtools = $commercialtools;

        return $this;
    }

    /**
     * Get commercialtools
     *
     * @return boolean 
     */
    public function getCommercialtools() {
        return $this->commercialtools;
    }

    /**
     * Set margecoef
     *
     * @param integer $margecoef
     * @return Customer
     */
    public function setMargecoef($margecoef) {
        $this->margecoef = $margecoef;

        return $this;
    }

    /**
     * Get margecoef
     *
     * @return integer 
     */
    public function getMargecoef() {
        return $this->margecoef;
    }

    /**
     * Set customermargecoef
     *
     * @param integer $customermargecoef
     * @return Customer
     */
    public function setCustomermargecoef($customermargecoef) {
        $this->customermargecoef = $customermargecoef;

        return $this;
    }

    /**
     * Get customermargecoef
     *
     * @return integer 
     */
    public function getCustomermargecoef() {
        return $this->customermargecoef;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Customer
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Customer
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set nomenclatures
     *
     * @param string $nomenclatures
     * @return Customer
     */
    public function setNomenclatures($nomenclatures) {
        $this->nomenclatures = $nomenclatures;

        return $this;
    }

    /**
     * Get nomenclatures
     *
     * @return string 
     */
    public function getNomenclatures() {
        return $this->nomenclatures;
    }

    /**
     * Set pc
     *
     * @param string $pc
     * @return Customer
     */
    public function setPc($pc) {
        $this->pc = $pc;

        return $this;
    }

    /**
     * Get pc
     *
     * @return string 
     */
    public function getPc() {
        return $this->pc;
    }


    /**
     * Set exportfile
     *
     * @param string $exportfile
     * @return Customer
     */
    public function setExportfile($exportfile) {
        $this->exportfile = $exportfile;

        return $this;
    }

    /**
     * Get exportfile
     *
     * @return string 
     */
    public function getExportfile() {
        return $this->exportfile;
    }

    /**
     * Set logo
     *
     * @param \sourcinasia\appBundle\Entity\Logo $logo
     * @return Customer
     */
    public function setLogo(\sourcinasia\appBundle\Entity\Logo $logo = null) {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return \sourcinasia\appBundle\Entity\Logo 
     */
    public function getLogo() {
        return $this->logo;
    }

    /**
     * Add historiescustomer
     *
     * @param \sourcinasia\appBundle\Entity\Historycustomer $historiescustomer
     * @return Customer
     */
    public function addHistoriescustomer(\sourcinasia\appBundle\Entity\Historycustomer $historiescustomer) {
        $this->historiescustomer[] = $historiescustomer;

        return $this;
    }

    /**
     * Remove historiescustomer
     *
     * @param \sourcinasia\appBundle\Entity\Historycustomer $historiescustomer
     */
    public function removeHistoriescustomer(\sourcinasia\appBundle\Entity\Historycustomer $historiescustomer) {
        $this->historiescustomer->removeElement($historiescustomer);
    }

    /**
     * Get historiescustomer
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoriescustomer() {
        return $this->historiescustomer;
    }

    /**
     * Add histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     * @return Customer
     */
    public function addHistory(\sourcinasia\appBundle\Entity\History $histories) {
        $this->histories[] = $histories;

        return $this;
    }

    /**
     * Remove histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     */
    public function removeHistory(\sourcinasia\appBundle\Entity\History $histories) {
        $this->histories->removeElement($histories);
    }

    /**
     * Get histories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistories() {
        return $this->histories;
    }

    /**
     * Add contacts
     *
     * @param \sourcinasia\appBundle\Entity\Contact $contacts
     * @return Customer
     */
    public function addContact(\sourcinasia\appBundle\Entity\Contact $contacts) {
        $this->contacts[] = $contacts;

        return $this;
    }

    /**
     * Remove contacts
     *
     * @param \sourcinasia\appBundle\Entity\Contact $contacts
     */
    public function removeContact(\sourcinasia\appBundle\Entity\Contact $contacts) {
        $this->contacts->removeElement($contacts);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContacts() {
        return $this->contacts;
    }

    /**
     * Add cadenciers
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadenciers
     * @return Customer
     */
    public function addCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadenciers) {
        $this->cadenciers[] = $cadenciers;

        return $this;
    }

    /**
     * Remove cadenciers
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadenciers
     */
    public function removeCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadenciers) {
        $this->cadenciers->removeElement($cadenciers);
    }

    /**
     * Get cadenciers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCadenciers() {
        return $this->cadenciers;
    }

    /**
     * Add chats
     *
     * @param \sourcinasia\appBundle\Entity\Chat $chats
     * @return Customer
     */
    public function addChat(\sourcinasia\appBundle\Entity\Chat $chats) {
        $this->chats[] = $chats;

        return $this;
    }

    /**
     * Remove chats
     *
     * @param \sourcinasia\appBundle\Entity\Chat $chats
     */
    public function removeChat(\sourcinasia\appBundle\Entity\Chat $chats) {
        $this->chats->removeElement($chats);
    }

    /**
     * Get chats
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChats() {
        return $this->chats;
    }

    /**
     * Set country
     *
     * @param \sourcinasia\appBundle\Entity\Country $country
     * @return Customer
     */
    public function setCountry(\sourcinasia\appBundle\Entity\Country $country = null) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \sourcinasia\appBundle\Entity\Country 
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set activity
     *
     * @param \sourcinasia\appBundle\Entity\Activity $activity
     * @return Customer
     */
    public function setActivity(\sourcinasia\appBundle\Entity\Activity $activity = null) {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \sourcinasia\appBundle\Entity\Activity 
     */
    public function getActivity() {
        return $this->activity;
    }

    /**
     * Set paymentcondition
     *
     * @param \sourcinasia\appBundle\Entity\Paymentcondition $paymentcondition
     * @return Customer
     */
    public function setPaymentcondition(\sourcinasia\appBundle\Entity\Paymentcondition $paymentcondition = null) {
        $this->paymentcondition = $paymentcondition;

        return $this;
    }

    /**
     * Get paymentcondition
     *
     * @return \sourcinasia\appBundle\Entity\Paymentcondition 
     */
    public function getPaymentcondition() {
        return $this->paymentcondition;
    }

    /**
     * Set mainsaler
     *
     * @param \sourcinasia\appBundle\Entity\User $mainsaler
     * @return Customer
     */
    public function setMainsaler(\sourcinasia\appBundle\Entity\User $mainsaler = null) {
        $this->mainsaler = $mainsaler;

        return $this;
    }

    /**
     * Get mainsaler
     *
     * @return \sourcinasia\appBundle\Entity\User 
     */
    public function getMainsaler() {
        return $this->mainsaler;
    }

    /**
     * Add quotationsheets
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $quotationsheets
     * @return Customer
     */
    public function addQuotationsheet(\sourcinasia\appBundle\Entity\Cadencier $quotationsheets) {
        $this->quotationsheets[] = $quotationsheets;

        return $this;
    }

    /**
     * Remove quotationsheets
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $quotationsheets
     */
    public function removeQuotationsheet(\sourcinasia\appBundle\Entity\Cadencier $quotationsheets) {
        $this->quotationsheets->removeElement($quotationsheets);
    }

    /**
     * Get quotationsheets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getQuotationsheets() {
        return $this->quotationsheets;
    }

    /**
     * Add products
     *
     * @param \sourcinasia\appBundle\Entity\Product $products
     * @return Customer
     */
    public function addProduct(\sourcinasia\appBundle\Entity\Product $products) {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \sourcinasia\appBundle\Entity\Product $products
     */
    public function removeProduct(\sourcinasia\appBundle\Entity\Product $products) {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts() {
        return $this->products;
    }

    /**
     * Add categories
     *
     * @param \sourcinasia\appBundle\Entity\Categorie $categories
     * @return Customer
     */
    public function addCategory(\sourcinasia\appBundle\Entity\Categorie $categories) {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \sourcinasia\appBundle\Entity\Categorie $categories
     */
    public function removeCategory(\sourcinasia\appBundle\Entity\Categorie $categories) {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories() {
        return $this->categories;
    }

    /**
     * Add categoriesrestriction
     *
     * @param \sourcinasia\appBundle\Entity\Categorie $categoriesrestriction
     * @return Customer
     */
    public function addCategoriesrestriction(\sourcinasia\appBundle\Entity\Categorie $categoriesrestriction) {
        $this->categoriesrestriction[] = $categoriesrestriction;

        return $this;
    }

    /**
     * Remove categoriesrestriction
     *
     * @param \sourcinasia\appBundle\Entity\Categorie $categoriesrestriction
     */
    public function removeCategoriesrestriction(\sourcinasia\appBundle\Entity\Categorie $categoriesrestriction) {
        $this->categoriesrestriction->removeElement($categoriesrestriction);
    }

    /**
     * Get categoriesrestriction
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategoriesrestriction() {
        return $this->categoriesrestriction;
    }

    /**
     * Add suppliersrestriction
     *
     * @param \sourcinasia\appBundle\Entity\Supplier $suppliersrestriction
     * @return Customer
     */
    public function addSuppliersrestriction(\sourcinasia\appBundle\Entity\Supplier $suppliersrestriction) {
        $this->suppliersrestriction[] = $suppliersrestriction;

        return $this;
    }

    /**
     * Remove suppliersrestriction
     *
     * @param \sourcinasia\appBundle\Entity\Supplier $suppliersrestriction
     */
    public function removeSuppliersrestriction(\sourcinasia\appBundle\Entity\Supplier $suppliersrestriction) {
        $this->suppliersrestriction->removeElement($suppliersrestriction);
    }

    /**
     * Get suppliersrestriction
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSuppliersrestriction() {
        return $this->suppliersrestriction;
    }

    /**
     * Add zones
     *
     * @param \sourcinasia\appBundle\Entity\Zone $zones
     * @return Customer
     */
    public function addZone(\sourcinasia\appBundle\Entity\Zone $zones) {
        $this->zones[] = $zones;

        return $this;
    }

    /**
     * Remove zones
     *
     * @param \sourcinasia\appBundle\Entity\Zone $zones
     */
    public function removeZone(\sourcinasia\appBundle\Entity\Zone $zones) {
        $this->zones->removeElement($zones);
    }

    /**
     * Get zones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getZones() {
        return $this->zones;
    }

    /**
     * Add owners
     *
     * @param \sourcinasia\appBundle\Entity\User $owners
     * @return Customer
     */
    public function addOwner(\sourcinasia\appBundle\Entity\User $owners) {
        $this->owners[] = $owners;

        return $this;
    }

    /**
     * Remove owners
     *
     * @param \sourcinasia\appBundle\Entity\User $owners
     */
    public function removeOwner(\sourcinasia\appBundle\Entity\User $owners) {
        $this->owners->removeElement($owners);
    }

    /**
     * Get owners
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOwners() {
        return $this->owners;
    }

    /**
     * Set exportsalername
     *
     * @param string $exportsalername
     * @return Customer
     */
    public function setExportsalername($exportsalername) {
        $this->exportsalername = $exportsalername;

        return $this;
    }

    /**
     * Get exportsalername
     *
     * @return string 
     */
    public function getExportsalername() {
        return $this->exportsalername;
    }

    /**
     * Set exportsaleremail
     *
     * @param string $exportsaleremail
     * @return Customer
     */
    public function setExportsaleremail($exportsaleremail) {
        $this->exportsaleremail = $exportsaleremail;

        return $this;
    }

    /**
     * Get exportsaleremail
     *
     * @return string 
     */
    public function getExportsaleremail() {
        return $this->exportsaleremail;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $invoices;


    /**
     * Add invoices
     *
     * @param \sourcinasia\appBundle\Entity\Invoicecustomer $invoices
     * @return Customer
     */
    public function addInvoice(\sourcinasia\appBundle\Entity\Invoicecustomer $invoices)
    {
        $this->invoices[] = $invoices;

        return $this;
    }

    /**
     * Remove invoices
     *
     * @param \sourcinasia\appBundle\Entity\Invoicecustomer $invoices
     */
    public function removeInvoice(\sourcinasia\appBundle\Entity\Invoicecustomer $invoices)
    {
        $this->invoices->removeElement($invoices);
    }

    /**
     * Get invoices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInvoices()
    {
        return $this->invoices;
    }
    /**
     * @var integer
     */
    private $deleverytime;


    /**
     * Set deleverytime
     *
     * @param integer $deleverytime
     * @return Customer
     */
    public function setDeleverytime($deleverytime)
    {
        $this->deleverytime = $deleverytime;

        return $this;
    }

    /**
     * Get deleverytime
     *
     * @return integer 
     */
    public function getDeleverytime()
    {
        return $this->deleverytime;
    }
    /**
     * @var boolean
     */
    private $leads;


    /**
     * Set leads
     *
     * @param boolean $leads
     *
     * @return Customer
     */
    public function setLeads($leads)
    {
        $this->leads = $leads;

        return $this;
    }

    /**
     * Get leads
     *
     * @return boolean
     */
    public function getLeads()
    {
        return $this->leads;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $historysale;


    /**
     * Add historysale
     *
     * @param \sourcinasia\appBundle\Entity\Historysale $historysale
     *
     * @return Customer
     */
    public function addHistorysale(\sourcinasia\appBundle\Entity\Historysale $historysale)
    {
        $this->historysale[] = $historysale;

        return $this;
    }

    /**
     * Remove historysale
     *
     * @param \sourcinasia\appBundle\Entity\Historysale $historysale
     */
    public function removeHistorysale(\sourcinasia\appBundle\Entity\Historysale $historysale)
    {
        $this->historysale->removeElement($historysale);
    }

    /**
     * Get historysale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistorysale()
    {
        return $this->historysale;
    }

    /**
     * Add catalog
     *
     * @param \sourcinasia\appBundle\Entity\Catalog $catalog
     *
     * @return Customer
     */
    public function addCatalog(\sourcinasia\appBundle\Entity\Catalog $catalog)
    {
        $this->catalogs[] = $catalog;

        return $this;
    }

    /**
     * Remove catalog
     *
     * @param \sourcinasia\appBundle\Entity\Catalog $catalog
     */
    public function removeCatalog(\sourcinasia\appBundle\Entity\Catalog $catalog)
    {
        $this->catalogs->removeElement($catalog);
    }

    /**
     * Get catalogs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCatalogs()
    {
        return $this->catalogs;
    }

    /**
     * Add suppliersexception
     *
     * @param \sourcinasia\appBundle\Entity\Supplier $suppliersexception
     * @return Customer
     */
    public function addSuppliersexception(\sourcinasia\appBundle\Entity\Supplier $suppliersexception) {
        $this->suppliersexception[] = $suppliersexception;

        return $this;
    }

    /**
     * Remove suppliersexception
     *
     * @param \sourcinasia\appBundle\Entity\Supplier $suppliersexception
     */
    public function removeSuppliersexception(\sourcinasia\appBundle\Entity\Supplier $suppliersexception) {
        $this->suppliersexception->removeElement($suppliersexception);
    }

    /**
     * Get suppliersexception
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSuppliersexception() {
        return $this->suppliersexception;
    }
}
