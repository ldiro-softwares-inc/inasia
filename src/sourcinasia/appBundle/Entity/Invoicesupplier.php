<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoicesupplier
 */
class Invoicesupplier {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplychain
     */
    private $document;

    /**
     * @var \sourcinasia\appBundle\Entity\Cadencier
     */
    private $cadencier;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplier
     */
    private $supplier;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $payments;

    /**
     * Constructor
     */
    public function __construct() {
        $this->payments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Invoicesupplier
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Invoicesupplier
     */
    public function setAmount($amount) {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount() {
        return $this->amount;
    }

    /**
     * Set document
     *
     * @param \sourcinasia\appBundle\Entity\Supplychain $document
     * @return Invoicesupplier
     */
    public function setDocument(\sourcinasia\appBundle\Entity\Supplychain $document = null) {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \sourcinasia\appBundle\Entity\Supplychain 
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Set cadencier
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadencier
     * @return Invoicesupplier
     */
    public function setCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadencier = null) {
        $this->cadencier = $cadencier;

        return $this;
    }

    /**
     * Get cadencier
     *
     * @return \sourcinasia\appBundle\Entity\Cadencier 
     */
    public function getCadencier() {
        return $this->cadencier;
    }

    /**
     * Set supplier
     *
     * @param \sourcinasia\appBundle\Entity\Supplier $supplier
     * @return Invoicesupplier
     */
    public function setSupplier(\sourcinasia\appBundle\Entity\Supplier $supplier = null) {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \sourcinasia\appBundle\Entity\Supplier 
     */
    public function getSupplier() {
        return $this->supplier;
    }

    /**
     * Add payments
     *
     * @param \sourcinasia\appBundle\Entity\Payment $payments
     * @return Invoicesupplier
     */
    public function addPayment(\sourcinasia\appBundle\Entity\Payment $payments) {
        $this->payments[] = $payments;

        return $this;
    }

    /**
     * Remove payments
     *
     * @param \sourcinasia\appBundle\Entity\Payment $payments
     */
    public function removePayment(\sourcinasia\appBundle\Entity\Payment $payments) {
        $this->payments->removeElement($payments);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPayments() {
        return $this->payments;
    }

    public function getNumber() {
        return $this->getdate()->format('Ym') . sprintf("%06d", $this->getId());
    }

}
