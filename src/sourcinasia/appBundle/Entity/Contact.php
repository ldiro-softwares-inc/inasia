<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 */
class Contact {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $mobile;

    /**
     * @var string
     */
    private $fax;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $job;

    /**
     * @var string
     */
    private $qq;

    /**
     * @var string
     */
    private $skype;

    /**
     * @var \DateTime
     */
    private $exportdate;

    /**
     * @var integer
     */
    private $exportnbrmax;

    /**
     * @var integer
     */
    private $exportnbr;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var \DateTime
     */
    private $unsubscribe;

    /**
     * @var \DateTime
     */
    private $lastemail;

    /**
     * @var string
     */
    private $token;

    /**
     * @var integer
     */
    private $unsubscribeReason;

    /**
     * @var integer
     */
    private $frequencenews;

    /**
     * @var \sourcinasia\appBundle\Entity\User
     */
    private $user;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplier
     */
    private $supplier;

    /**
     * @var \sourcinasia\appBundle\Entity\Customer
     */
    private $customer;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Contact
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Contact
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Contact
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return Contact
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set job
     *
     * @param string $job
     *
     * @return Contact
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return string
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set qq
     *
     * @param string $qq
     *
     * @return Contact
     */
    public function setQq($qq)
    {
        $this->qq = $qq;

        return $this;
    }

    /**
     * Get qq
     *
     * @return string
     */
    public function getQq()
    {
        return $this->qq;
    }

    /**
     * Set skype
     *
     * @param string $skype
     *
     * @return Contact
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set exportdate
     *
     * @param \DateTime $exportdate
     *
     * @return Contact
     */
    public function setExportdate($exportdate)
    {
        $this->exportdate = $exportdate;

        return $this;
    }

    /**
     * Get exportdate
     *
     * @return \DateTime
     */
    public function getExportdate()
    {
        return $this->exportdate;
    }

    /**
     * Set exportnbrmax
     *
     * @param integer $exportnbrmax
     *
     * @return Contact
     */
    public function setExportnbrmax($exportnbrmax)
    {
        $this->exportnbrmax = $exportnbrmax;

        return $this;
    }

    /**
     * Get exportnbrmax
     *
     * @return integer
     */
    public function getExportnbrmax()
    {
        return $this->exportnbrmax;
    }

    /**
     * Set exportnbr
     *
     * @param integer $exportnbr
     *
     * @return Contact
     */
    public function setExportnbr($exportnbr)
    {
        $this->exportnbr = $exportnbr;

        return $this;
    }

    /**
     * Get exportnbr
     *
     * @return integer
     */
    public function getExportnbr()
    {
        return $this->exportnbr;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Contact
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set unsubscribe
     *
     * @param \DateTime $unsubscribe
     *
     * @return Contact
     */
    public function setUnsubscribe($unsubscribe)
    {
        $this->unsubscribe = $unsubscribe;

        return $this;
    }

    /**
     * Get unsubscribe
     *
     * @return \DateTime
     */
    public function getUnsubscribe()
    {
        return $this->unsubscribe;
    }

    /**
     * Set lastemail
     *
     * @param \DateTime $lastemail
     *
     * @return Contact
     */
    public function setLastemail($lastemail)
    {
        $this->lastemail = $lastemail;

        return $this;
    }

    /**
     * Get lastemail
     *
     * @return \DateTime
     */
    public function getLastemail()
    {
        return $this->lastemail;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Contact
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set unsubscribeReason
     *
     * @param integer $unsubscribeReason
     *
     * @return Contact
     */
    public function setUnsubscribeReason($unsubscribeReason)
    {
        $this->unsubscribeReason = $unsubscribeReason;

        return $this;
    }

    /**
     * Get unsubscribeReason
     *
     * @return integer
     */
    public function getUnsubscribeReason()
    {
        return $this->unsubscribeReason;
    }

    /**
     * Set frequencenews
     *
     * @param integer $frequencenews
     *
     * @return Contact
     */
    public function setFrequencenews($frequencenews)
    {
        $this->frequencenews = $frequencenews;

        return $this;
    }

    /**
     * Get frequencenews
     *
     * @return integer
     */
    public function getFrequencenews()
    {
        return $this->frequencenews;
    }

    /**
     * Set user
     *
     * @param \sourcinasia\appBundle\Entity\User $user
     *
     * @return Contact
     */
    public function setUser(\sourcinasia\appBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \sourcinasia\appBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set supplier
     *
     * @param \sourcinasia\appBundle\Entity\Supplier $supplier
     *
     * @return Contact
     */
    public function setSupplier(\sourcinasia\appBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \sourcinasia\appBundle\Entity\Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set customer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customer
     *
     * @return Contact
     */
    public function setCustomer(\sourcinasia\appBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \sourcinasia\appBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $newsletters;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->newsletters = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add newsletter
     *
     * @param \sourcinasia\appBundle\Entity\Newsletter $newsletter
     *
     * @return Contact
     */
    public function addNewsletter(\sourcinasia\appBundle\Entity\Newsletter $newsletter)
    {
        $this->newsletters[] = $newsletter;

        return $this;
    }

    /**
     * Remove newsletter
     *
     * @param \sourcinasia\appBundle\Entity\Newsletter $newsletter
     */
    public function removeNewsletter(\sourcinasia\appBundle\Entity\Newsletter $newsletter)
    {
        $this->newsletters->removeElement($newsletter);
    }

    /**
     * Get newsletters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNewsletters()
    {
        return $this->newsletters;
    }
}
