<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Norme
 */
class Norme {

   
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $certificats;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->certificats = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Norme
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add certificats
     *
     * @param \sourcinasia\appBundle\Entity\Certificat $certificats
     * @return Norme
     */
    public function addCertificat(\sourcinasia\appBundle\Entity\Certificat $certificats)
    {
        $this->certificats[] = $certificats;

        return $this;
    }

    /**
     * Remove certificats
     *
     * @param \sourcinasia\appBundle\Entity\Certificat $certificats
     */
    public function removeCertificat(\sourcinasia\appBundle\Entity\Certificat $certificats)
    {
        $this->certificats->removeElement($certificats);
    }

    /**
     * Get certificats
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCertificats()
    {
        return $this->certificats;
    }
}
