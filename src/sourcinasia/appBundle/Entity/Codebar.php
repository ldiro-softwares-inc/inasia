<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Codebar
 */
class Codebar {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $codebar;

    /**
     * @var \sourcinasia\appBundle\Entity\Product
     */
    private $product;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set codebar
     *
     * @param integer $codebar
     * @return Codebar
     */
    public function setCodebar($codebar) {
        $this->codebar = $codebar;

        return $this;
    }

    /**
     * Get codebar
     *
     * @return integer 
     */
    public function getCodebar() {
        return $this->codebar;
    }

    /**
     * Set product
     *
     * @param \sourcinasia\appBundle\Entity\Product $product
     * @return Codebar
     */
    public function setProduct(\sourcinasia\appBundle\Entity\Product $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \sourcinasia\appBundle\Entity\Product 
     */
    public function getProduct() {
        return $this->product;
    }

}
