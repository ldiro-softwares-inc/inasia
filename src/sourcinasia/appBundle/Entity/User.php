<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 */
class User extends BaseUser {

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var \sourcinasia\appBundle\Entity\Contact
     */
    private $contact;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $histories;

    /**
     * @var \sourcinasia\appBundle\Entity\Logo
     */
    private $logo;

    /**
     * @var integer
     */
    private $modeonlogin;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var integer
     */
    private $customermargecoef;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $domainerestriction;

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->histories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set contact
     *
     * @param \sourcinasia\appBundle\Entity\Contact $contact
     * @return User
     */
    public function setContact(\sourcinasia\appBundle\Entity\Contact $contact = null) {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \sourcinasia\appBundle\Entity\Contact 
     */
    public function getContact() {
        return $this->contact;
    }

    /**
     * Set logo
     *
     * @param \sourcinasia\appBundle\Entity\Logo $logo
     * @return User
     */
    public function setLogo(\sourcinasia\appBundle\Entity\Logo $logo = null) {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return \sourcinasia\appBundle\Entity\Logo 
     */
    public function getLogo() {
        return $this->logo;
    }

    /**
     * Set modeonlogin
     *
     * @param integer $modeonlogin
     * @return User
     */
    public function setModeonlogin($modeonlogin) {
        $this->modeonlogin = $modeonlogin;

        return $this;
    }

    /**
     * Get modeonlogin
     *
     * @return integer 
     */
    public function getModeonlogin() {
        return $this->modeonlogin;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return User
     */
    public function setLocale($locale) {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * Set customermargecoef
     *
     * @param integer $customermargecoef
     * @return User
     */
    public function setCustomermargecoef($customermargecoef) {
        $this->customermargecoef = $customermargecoef;

        return $this;
    }

    /**
     * Get customermargecoef
     *
     * @return integer 
     */
    public function getCustomermargecoef() {
        return $this->customermargecoef;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set domainerestriction
     *
     * @param string $domainerestriction
     * @return User
     */
    public function setDomainerestriction($domainerestriction) {
        $this->domainerestriction = $domainerestriction;

        return $this;
    }

    /**
     * Get domainerestriction
     *
     * @return string 
     */
    public function getDomainerestriction() {
        return $this->domainerestriction;
    }


    /**
     * Add histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     * @return User
     */
    public function addHistory(\sourcinasia\appBundle\Entity\History $histories)
    {
        $this->histories[] = $histories;

        return $this;
    }

    /**
     * Remove histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     */
    public function removeHistory(\sourcinasia\appBundle\Entity\History $histories)
    {
        $this->histories->removeElement($histories);
    }

    /**
     * Get histories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistories()
    {
        return $this->histories;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cadenciers;


    /**
     * Add cadenciers
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadenciers
     * @return User
     */
    public function addCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadenciers)
    {
        $this->cadenciers[] = $cadenciers;

        return $this;
    }

    /**
     * Remove cadenciers
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadenciers
     */
    public function removeCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadenciers)
    {
        $this->cadenciers->removeElement($cadenciers);
    }

    /**
     * Get cadenciers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCadenciers()
    {
        return $this->cadenciers;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $useractivities;


    /**
     * Add useractivities
     *
     * @param \sourcinasia\appBundle\Entity\Useractivity $useractivities
     * @return User
     */
    public function addUseractivity(\sourcinasia\appBundle\Entity\Useractivity $useractivities)
    {
        $this->useractivities[] = $useractivities;

        return $this;
    }

    /**
     * Remove useractivities
     *
     * @param \sourcinasia\appBundle\Entity\Useractivity $useractivities
     */
    public function removeUseractivity(\sourcinasia\appBundle\Entity\Useractivity $useractivities)
    {
        $this->useractivities->removeElement($useractivities);
    }

    /**
     * Get useractivities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUseractivities()
    {
        return $this->useractivities;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $historiescustomers;


    /**
     * Add historiescustomers
     *
     * @param \sourcinasia\appBundle\Entity\Historycustomer $historiescustomers
     * @return User
     */
    public function addHistoriescustomer(\sourcinasia\appBundle\Entity\Historycustomer $historiescustomers)
    {
        $this->historiescustomers[] = $historiescustomers;

        return $this;
    }

    /**
     * Remove historiescustomers
     *
     * @param \sourcinasia\appBundle\Entity\Historycustomer $historiescustomers
     */
    public function removeHistoriescustomer(\sourcinasia\appBundle\Entity\Historycustomer $historiescustomers)
    {
        $this->historiescustomers->removeElement($historiescustomers);
    }

    /**
     * Get historiescustomers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoriescustomers()
    {
        return $this->historiescustomers;
    }

    public function getExpireDate()
    {
        return $this->expiresAt;
    }



    /**
     * @var string
     */
    private $token;


    /**
     * Set token
     *
     * @param string $token
     *
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
    /**
     * @var string
     */
    private $phone;


    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }
}
