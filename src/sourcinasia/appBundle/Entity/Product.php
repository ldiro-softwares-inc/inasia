<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 */
class Product {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $mainimage;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \sourcinasia\appBundle\Entity\Codebar
     */
    private $Codebar;

    /**
     * @var string
     */
    private $smalldescription;

    /**
     * @var string
     */
    private $frenchsmalldescription;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $frenchdescription;

    /**
     * @var string
     */
    private $coloris;

    /**
     * @var string
     */
    private $material;

    /**
     * @var string
     */
    private $packing;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var float
     */
    private $cbm;

    /**
     * @var float
     */
    private $pcb;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var string
     */
    private $packingsize;

    /**
     * @var string
     */
    private $productsize;

    /**
     * @var integer
     */
    private $nwproduct;

    /**
     * @var integer
     */
    private $gwproduct;

    /**
     * @var integer
     */
    private $nwpackage;

    /**
     * @var integer
     */
    private $gwpackage;

    /**
     * @var integer
     */
    private $qty20gp;

    /**
     * @var integer
     */
    private $qty40gp;

    /**
     * @var integer
     */
    private $qty40hq;

    /**
     * @var string
     */
    private $ce;

    /**
     * @var integer
     */
    private $hits;

    /**
     * @var string
     */
    private $exclusivity;

    /**
     * @var string
     */
    private $commandesarchive;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $historysales;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $offers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $histories;

    /**
     * @var \sourcinasia\appBundle\Entity\Categorie
     */
    private $categorie;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $customers;

    /**
     * @var string
     */
    private $archive;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $catalogs;

    /**
     * Constructor
     */
    public function __construct() {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
        $this->offers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->histories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->customers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->historysales = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set mainimage
     *
     * @param string $mainimage
     * @return Product
     */
    public function setMainimage($mainimage) {
        $this->mainimage = $mainimage;

        return $this;
    }

    /**
     * Get mainimage
     *
     * @return string 
     */
    public function getMainimage() {
        return $this->mainimage;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set smalldescription
     *
     * @param string $smalldescription
     * @return Product
     */
    public function setSmalldescription($smalldescription) {
        $this->smalldescription = $smalldescription;

        return $this;
    }

    /**
     * Get smalldescription
     *
     * @return string 
     */
    public function getSmalldescription() {
        return $this->smalldescription;
    }

    /**
     * Set frenchsmalldescription
     *
     * @param string $frenchsmalldescription
     * @return Product
     */
    public function setFrenchsmalldescription($frenchsmalldescription) {
        $this->frenchsmalldescription = $frenchsmalldescription;

        return $this;
    }

    /**
     * Get frenchsmalldescription
     *
     * @return string 
     */
    public function getFrenchsmalldescription() {
        return $this->frenchsmalldescription;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set frenchdescription
     *
     * @param string $frenchdescription
     * @return Product
     */
    public function setFrenchdescription($frenchdescription) {
        $this->frenchdescription = $frenchdescription;

        return $this;
    }

    /**
     * Get frenchdescription
     *
     * @return string 
     */
    public function getFrenchdescription() {
        return $this->frenchdescription;
    }

    /**
     * Set coloris
     *
     * @param string $coloris
     * @return Product
     */
    public function setColoris($coloris) {
        $this->coloris = $coloris;

        return $this;
    }

    /**
     * Get coloris
     *
     * @return string 
     */
    public function getColoris() {
        return $this->coloris;
    }

    /**
     * Set material
     *
     * @param string $material
     * @return Product
     */
    public function setMaterial($material) {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return string 
     */
    public function getMaterial() {
        return $this->material;
    }

    /**
     * Set packing
     *
     * @param string $packing
     * @return Product
     */
    public function setPacking($packing) {
        $this->packing = $packing;

        return $this;
    }

    /**
     * Get packing
     *
     * @return string 
     */
    public function getPacking() {
        return $this->packing;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Product
     */
    public function setComments($comments) {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * Set cbm
     *
     * @param float $cbm
     * @return Product
     */
    public function setCbm($cbm) {
        $this->cbm = $cbm;

        return $this;
    }

    /**
     * Get cbm
     *
     * @return float 
     */
    public function getCbm() {
        return $this->cbm;
    }

    /**
     * Set pcb
     *
     * @param float $pcb
     * @return Product
     */
    public function setPcb($pcb) {
        $this->pcb = $pcb;

        return $this;
    }

    /**
     * Get pcb
     *
     * @return float 
     */
    public function getPcb() {
        return $this->pcb;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Product
     */
    public function setModified($modified) {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified() {
        return $this->modified;
    }

    /**
     * Set packingsize
     *
     * @param string $packingsize
     * @return Product
     */
    public function setPackingsize($packingsize) {
        $this->packingsize = $packingsize;

        return $this;
    }

    /**
     * Get packingsize
     *
     * @return string 
     */
    public function getPackingsize() {
        return $this->packingsize;
    }

    /**
     * Set productsize
     *
     * @param string $productsize
     * @return Product
     */
    public function setProductsize($productsize) {
        $this->productsize = $productsize;

        return $this;
    }

    /**
     * Get productsize
     *
     * @return string 
     */
    public function getProductsize() {
        return $this->productsize;
    }

    /**
     * Set nwproduct
     *
     * @param integer $nwproduct
     * @return Product
     */
    public function setNwproduct($nwproduct) {
        $this->nwproduct = $nwproduct;

        return $this;
    }

    /**
     * Get nwproduct
     *
     * @return integer 
     */
    public function getNwproduct() {
        return $this->nwproduct;
    }

    /**
     * Set gwproduct
     *
     * @param integer $gwproduct
     * @return Product
     */
    public function setGwproduct($gwproduct) {
        $this->gwproduct = $gwproduct;

        return $this;
    }

    /**
     * Get gwproduct
     *
     * @return integer 
     */
    public function getGwproduct() {
        return $this->gwproduct;
    }

    /**
     * Set nwpackage
     *
     * @param integer $nwpackage
     * @return Product
     */
    public function setNwpackage($nwpackage) {
        $this->nwpackage = $nwpackage;

        return $this;
    }

    /**
     * Get nwpackage
     *
     * @return integer 
     */
    public function getNwpackage() {
        return $this->nwpackage;
    }

    /**
     * Set gwpackage
     *
     * @param integer $gwpackage
     * @return Product
     */
    public function setGwpackage($gwpackage) {
        $this->gwpackage = $gwpackage;

        return $this;
    }

    /**
     * Get gwpackage
     *
     * @return integer 
     */
    public function getGwpackage() {
        return $this->gwpackage;
    }

    /**
     * Set qty20gp
     *
     * @param integer $qty20gp
     * @return Product
     */
    public function setQty20gp($qty20gp) {
        $this->qty20gp = $qty20gp;

        return $this;
    }

    /**
     * Get qty20gp
     *
     * @return integer 
     */
    public function getQty20gp() {
        return $this->qty20gp;
    }

    /**
     * Set qty40gp
     *
     * @param integer $qty40gp
     * @return Product
     */
    public function setQty40gp($qty40gp) {
        $this->qty40gp = $qty40gp;

        return $this;
    }

    /**
     * Get qty40gp
     *
     * @return integer 
     */
    public function getQty40gp() {
        return $this->qty40gp;
    }

    /**
     * Set qty40hq
     *
     * @param integer $qty40hq
     * @return Product
     */
    public function setQty40hq($qty40hq) {
        $this->qty40hq = $qty40hq;

        return $this;
    }

    /**
     * Get qty40hq
     *
     * @return integer 
     */
    public function getQty40hq() {
        return $this->qty40hq;
    }

    /**
     * Set ce
     *
     * @param string $ce
     * @return Product
     */
    public function setCe($ce) {
        $this->ce = $ce;

        return $this;
    }

    /**
     * Get ce
     *
     * @return string 
     */
    public function getCe() {
        return $this->ce;
    }

    /**
     * Set hits
     *
     * @param integer $hits
     * @return Product
     */
    public function setHits($hits) {
        $this->hits = $hits;

        return $this;
    }

    /**
     * Get hits
     *
     * @return integer 
     */
    public function getHits() {
        return $this->hits;
    }

    /**
     * Set commandesarchive
     *
     * @param string $commandesarchive
     * @return Product
     */
    public function setCommandesarchive($commandesarchive) {
        $this->commandesarchive = json_encode($commandesarchive);

        return $this;
    }

    /**
     * Get commandesarchive
     *
     * @return string 
     */
    public function getCommandesarchive() {
        return json_decode($this->commandesarchive, true);
    }

    /**
     * Add images
     *
     * @param \sourcinasia\appBundle\Entity\Image $images
     * @return Product
     */
    public function addImage(\sourcinasia\appBundle\Entity\Image $images) {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \sourcinasia\appBundle\Entity\Image $images
     */
    public function removeImage(\sourcinasia\appBundle\Entity\Image $images) {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages() {
        return $this->images;
    }

    /**
     * Add offers
     *
     * @param \sourcinasia\appBundle\Entity\Offer $offers
     * @return Product
     */
    public function addOffer(\sourcinasia\appBundle\Entity\Offer $offers) {
        $this->offers[] = $offers;

        return $this;
    }

    /**
     * Remove offers
     *
     * @param \sourcinasia\appBundle\Entity\Offer $offers
     */
    public function removeOffer(\sourcinasia\appBundle\Entity\Offer $offers) {
        $this->offers->removeElement($offers);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOffers() {
        return $this->offers;
    }

    /**
     * Add histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     * @return Product
     */
    public function addHistory(\sourcinasia\appBundle\Entity\History $histories) {
        $this->histories[] = $histories;

        return $this;
    }

    /**
     * Remove histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     */
    public function removeHistory(\sourcinasia\appBundle\Entity\History $histories) {
        $this->histories->removeElement($histories);
    }

    /**
     * Get histories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistories() {
        return $this->histories;
    }

    /**
     * Set categorie
     *
     * @param \sourcinasia\appBundle\Entity\Categorie $categorie
     * @return Product
     */
    public function setCategorie(\sourcinasia\appBundle\Entity\Categorie $categorie = null) {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \sourcinasia\appBundle\Entity\Categorie 
     */
    public function getCategorie() {
        return $this->categorie;
    }

    /**
     * Add customers
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customers
     * @return Product
     */
    public function addCustomer(\sourcinasia\appBundle\Entity\Customer $customers) {
        if (!$this->customers->contains($customers))
            $this->customers[] = $customers;

        return $this;
    }

    /**
     * Remove customers
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customers
     */
    public function removeCustomer(\sourcinasia\appBundle\Entity\Customer $customers) {
        $this->customers->removeElement($customers);
    }

    /**
     * Get customers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomers() {
        return $this->customers;
    }

    /**
     * Set exclusivity
     *
     * @param string $exclusivity
     * @return Product
     */
    public function setExclusivity($exclusivity) {
        $this->exclusivity = implode(',', $exclusivity);

        return $this;
    }

    /**
     * Get exclusivity
     *
     * @return string 
     */
    public function getExclusivity() {
        return explode(',', $this->exclusivity);
    }

    /**
     * Add historysales
     *
     * @param \sourcinasia\appBundle\Entity\Historysale $historysales
     * @return Product
     */
    public function addHistorysale(\sourcinasia\appBundle\Entity\Historysale $historysales) {
        $this->historysales[] = $historysales;

        return $this;
    }

    /**
     * Remove historysales
     *
     * @param \sourcinasia\appBundle\Entity\Historysale $historysales
     */
    public function removeHistorysale(\sourcinasia\appBundle\Entity\Historysale $historysales) {
        $this->historysales->removeElement($historysales);
    }

    /**
     * Get historysales
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistorysales() {
        return $this->historysales;
    }

    /**
     * Set Codebar
     *
     * @param \sourcinasia\appBundle\Entity\Codebar $codebar
     * @return Product
     */
    public function setCodebar(\sourcinasia\appBundle\Entity\Codebar $codebar = null) {
        $this->Codebar = $codebar;

        return $this;
    }

    /**
     * Get Codebar
     *
     * @return \sourcinasia\appBundle\Entity\Codebar 
     */
    public function getCodebar() {
        return $this->Codebar;
    }

    /**
     * Set archive
     *
     * @param string $archive
     * @return Product
     */
    public function setArchive($archive) {
        $this->archive = json_encode($archive);

        return $this;
    }

    /**
     * Get archive
     *
     * @return string 
     */
    public function getArchive() {
        return (array) json_decode($this->archive, true);
    }

    /**
     * @var boolean
     */
    private $hidden;


    /**
     * Set hidden
     *
     * @param boolean $hidden
     *
     * @return Offer
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @var boolean
     */
    private $public;


    /**
     * Set public
     *
     * @param boolean $public
     *
     * @return Product
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Add catalog
     *
     * @param \sourcinasia\appBundle\Entity\Catalog $catalog
     *
     * @return Product
     */
    public function addCatalog(\sourcinasia\appBundle\Entity\Catalog $catalog)
    {
        $this->catalogs[] = $catalog;

        return $this;
    }

    /**
     * Remove catalog
     *
     * @param \sourcinasia\appBundle\Entity\Catalog $catalog
     */
    public function removeCatalog(\sourcinasia\appBundle\Entity\Catalog $catalog)
    {
        $this->catalogs->removeElement($catalog);
    }

    /**
     * Get catalogs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCatalogs()
    {
        return $this->catalogs;
    }



}