<?php

namespace sourcinasia\appBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 *  Document
 *  @Vich\Uploadable
 */
class Document {

    const CATALOG_TYPE = 'catalog';
    const QUOTATION_TYPE = 'quotation';
    const CUSTOMER_OFFER_TYPE = 'customer_offer';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $documentFileName;

    /**
     * @var string
     */
    private $title;

    /**
     * @var File
     * @Vich\UploadableField(mapping="document", fileNameProperty="documentFileName")
     */
    private $documentFile;

    /**
     * @var String
     */
    private $documentType;

    /**
     * @var Supplier
     */
    private $supplier;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    public function setDocumentFile(File $document = null)
    {
        $this->documentFile = $document;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($document) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDocumentFileName()
    {
        return $this->documentFileName;
    }

    /**
     * @param string $documentFileName
     */
    public function setDocumentFileName(?string $documentFileName): void
    {
        $this->documentFileName = $documentFileName;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return File
     */
    public function getDocumentFile()
    {
        return $this->documentFile;
    }

    /**
     * @return String
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @param String $documentType
     */
    public function setDocumentType(string $documentType): void
    {
        if (!in_array($documentType, array(self::CATALOG_TYPE, self::QUOTATION_TYPE, self::CUSTOMER_OFFER_TYPE))) {
            throw new \InvalidArgumentException("Invalid Document Type");
        }

        $this->documentType = $documentType;
    }

    /**
     * @return Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public static function getTypes()
    {
        return [self::CATALOG_TYPE,self::QUOTATION_TYPE,self::CUSTOMER_OFFER_TYPE];
    }
}