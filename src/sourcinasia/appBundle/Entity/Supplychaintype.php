<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supplychaintype
 */
class Supplychaintype {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplychaincat
     */
    private $supplychaincat;

    /**
     * @var boolean
     */
    private $validation;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Supplychaintype
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set supplychaincat
     *
     * @param \sourcinasia\appBundle\Entity\Supplychaincat $supplychaincat
     * @return Supplychaintype
     */
    public function setSupplychaincat(\sourcinasia\appBundle\Entity\Supplychaincat $supplychaincat = null) {
        $this->supplychaincat = $supplychaincat;

        return $this;
    }

    /**
     * Get supplychaincat
     *
     * @return \sourcinasia\appBundle\Entity\Supplychaincat 
     */
    public function getSupplychaincat() {
        return $this->supplychaincat;
    }


    /**
     * Set validation
     *
     * @param boolean $validation
     * @return Supplychaintype
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * Get validation
     *
     * @return boolean 
     */
    public function getValidation()
    {
        return $this->validation;
    }
    /**
     * @var integer
     */
    private $step;

    /**
     * @var string
     */
    private $and;

    /**
     * @var string
     */
    private $or;


    /**
     * Set step
     *
     * @param integer $step
     * @return Supplychaintype
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step
     *
     * @return integer 
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set and
     *
     * @param string $and
     * @return Supplychaintype
     */
    public function setAnd($and)
    {
        $this->and = $and;

        return $this;
    }

    /**
     * Get and
     *
     * @return string 
     */
    public function getAnd()
    {
        return $this->and;
    }

    /**
     * Set or
     *
     * @param string $or
     * @return Supplychaintype
     */
    public function setOr($or)
    {
        $this->or = $or;

        return $this;
    }

    /**
     * Get or
     *
     * @return string 
     */
    public function getOr()
    {
        return $this->or;
    }
    /**
     * @var boolean
     */
    private $obligatoire;

    /**
     * @var boolean
     */
    private $facultatif;

    /**
     * @var string
     */
    private $conditions;


    /**
     * Set obligatoire
     *
     * @param boolean $obligatoire
     * @return Supplychaintype
     */
    public function setObligatoire($obligatoire)
    {
        $this->obligatoire = $obligatoire;

        return $this;
    }

    /**
     * Get obligatoire
     *
     * @return boolean 
     */
    public function getObligatoire()
    {
        return $this->obligatoire;
    }

    /**
     * Set facultatif
     *
     * @param boolean $facultatif
     * @return Supplychaintype
     */
    public function setFacultatif($facultatif)
    {
        $this->facultatif = $facultatif;

        return $this;
    }

    /**
     * Get facultatif
     *
     * @return boolean 
     */
    public function getFacultatif()
    {
        return $this->facultatif;
    }

    /**
     * Set conditions
     *
     * @param string $conditions
     * @return Supplychaintype
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;

        return $this;
    }

    /**
     * Get conditions
     *
     * @return string 
     */
    public function getConditions()
    {
        return $this->conditions;
    }
    /**
     * @var boolean
     */
    private $hide;


    /**
     * Set hide
     *
     * @param boolean $hide
     * @return Supplychaintype
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return boolean 
     */
    public function getHide()
    {
        return $this->hide;
    }
   
    /**
     * @var boolean
     */
    private $notupload;


    /**
     * Set notupload
     *
     * @param boolean $notupload
     * @return Supplychaintype
     */
    public function setNotupload($notupload)
    {
        $this->notupload = $notupload;

        return $this;
    }

    /**
     * Get notupload
     *
     * @return boolean 
     */
    public function getNotupload()
    {
        return $this->notupload;
    }
    /**
     * @var boolean
     */
    private $notbtn;


    /**
     * Set notbtn
     *
     * @param boolean $notbtn
     * @return Supplychaintype
     */
    public function setNotbtn($notbtn)
    {
        $this->notbtn = $notbtn;

        return $this;
    }

    /**
     * Get notbtn
     *
     * @return boolean 
     */
    public function getNotbtn()
    {
        return $this->notbtn;
    }
    /**
     * @var boolean
     */
    private $customer;


    /**
     * Set customer
     *
     * @param boolean $customer
     *
     * @return Supplychaintype
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return boolean
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
