<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Dashbord controller.
 *
 */
class DashbordController extends Controller {

    public function indexAction() {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $histories = $this->getDoctrine()->getRepository('appBundle:History')->findBy(array(), array('date' => 'desc'), 100, 0);
            $Useractivity = $this->getDoctrine()->getRepository('appBundle:History')->findBy(array(), array('date' => 'desc'), 100, 0);

            return $this->render('appBundle:Dashbord:index.html.twig', array(
                        '456' => $histories,
                        '123' => $Useractivity
            ));
        }
    }

}
