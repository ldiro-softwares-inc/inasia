<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 */
class Image {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $imagekey;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \sourcinasia\appBundle\Entity\Product
     */
    private $product;

    /**
     * @var string
     */
    private $filename;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Image
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set imagekey
     *
     * @param string $imagekey
     * @return Image
     */
    public function setImagekey($imagekey) {
        $this->imagekey = $imagekey;

        return $this;
    }

    /**
     * Get imagekey
     *
     * @return string 
     */
    public function getImagekey() {
        return $this->imagekey;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Image
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set product
     *
     * @param \sourcinasia\appBundle\Entity\Product $product
     * @return Image
     */
    public function setProduct(\sourcinasia\appBundle\Entity\Product $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \sourcinasia\appBundle\Entity\Product 
     */
    public function getProduct() {
        return $this->product;
    }



    /**
     * Set filename
     *
     * @param string filename
     * @return filename
     */
    public function setFilename($filename) {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /*     * ***** HasLifecycleCallbacks ******** */

    protected function getTmpUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __SOURCINASIA__ . 'htdocs/';
    }

    protected function MyDirectory() {
        return 'products/' . $this->getProduct()->getId() . '/';
    }

    public function getFullImagePath() {
        return null === $this->image ? null : $this->getTmpUploadRootDir() . $this->image;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir() . $this->MyDirectory();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadImage() {
        // the file property can be empty if the field is not required
        if (null === $this->image)
            return;

        if (!empty($this->image) && !empty($this->filename)) {
            $src = $this->getTmpUploadRootDir() . $this->image;
            $dest = $this->getTmpUploadRootDir() . $this->filename;
            copy($src, $dest);
            unlink($src);

            $this->setImage($this->MyDirectory() . md5($this->filename) . '/original.jpg');
            $this->setImageKey(md5($this->filename));
            $this->setCreated(new \DateTime());
        } else {
            $this->filename = date('ymdhis') . $this->image->getClientOriginalName();
            $this->image->move($this->getTmpUploadRootDir(), $this->filename);
            $this->setImage($this->MyDirectory() . md5($this->filename) . '/original.jpg');
            $this->setImageKey(md5($this->filename));
        }
    }

    /**
     * @ORM\PostPersist()
     */
    public function moveImage() {
        if (null === $this->image)
            return;

        $directory = $this->getUploadRootDir() . md5($this->filename) . '/';

        $this->CheckDirectory($this->getTmpUploadRootDir() . 'products/');
        $this->CheckDirectory($this->getUploadRootDir());
        $this->CheckDirectory($directory);

        //Copier l'original
        copy($this->getTmpUploadRootDir() . $this->filename, $directory . 'original.jpg');

        //Applications
        $info = getimagesize($directory . 'original.jpg');
        $img = new \claviska\SimpleImage();
        switch ($info['mime']) {
            case 'image/gif':
                $img->fromFile($directory . 'original.jpg')->bestFit(120, 120)->toFile($directory . 'small.jpg');
                break;
            case 'image/jpeg':
                $img->fromFile($directory . 'original.jpg')->bestFit(120, 120)->toFile($directory . 'small.jpg');
                break;
            case 'image/png':
                $img->fromFile($directory . 'original.jpg')->bestFit(120, 120)->toFile($directory . 'small.jpg');
                break;
            default:
                break;
        }

        unlink($this->getTmpUploadRootDir() . $this->filename);
    }

    /**
     * @ORM\PreRemove()
     */
    public function removeImage() {

        $dir = str_replace('original.jpg', '', $this->image);
        if (is_dir($dir)) {
            if (file_exists($dir . 'original.jpg'))
                unlink($dir . 'original.jpg');
            if (file_exists($dir . 'small.jpg'))
                unlink($dir . 'small.jpg');
            if (file_exists($dir . 'index.html'))
                unlink($dir . 'index.html');
            rmdir($dir);
        }
    }

    private function CheckDirectory($url) {
        if (!is_dir($url)) {
            mkdir($url);
            $fichier = fopen($url . 'index.html', 'w+');
            fputs($fichier, "");
            fclose($fichier);
        }
        return true;
    }

    public function rotateImage() {
        if (null === $this->image)
            return;

        $imagePath = $this->getFullImagePath();
        $smallImagePath = str_replace("original", "small", $imagePath);
        
        $info = getimagesize($this->getFullImagePath());

        $img = new \claviska\SimpleImage();
        switch ($info['mime']) {
            case 'image/gif':
                $img->fromFile($imagePath)->rotate(90)->toFile($imagePath);
                $img->fromFile($smallImagePath)->rotate(90)->toFile($smallImagePath);
                break;
            case 'image/jpeg':
                $img->fromFile($imagePath)->rotate(90)->toFile($imagePath);
                $img->fromFile($smallImagePath)->rotate(90)->toFile($smallImagePath);
                break;
            case 'image/png':
                $img->fromFile($imagePath)->rotate(90)->toFile($imagePath);
                $img->fromFile($smallImagePath)->rotate(90)->toFile($smallImagePath);
                break;
            default:
                break;
        }
    }

}
