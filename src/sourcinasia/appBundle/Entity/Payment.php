<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payment
 */
class Payment
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var \sourcinasia\appBundle\Entity\Paymenttype
     */
    private $type;

    /**
     * @var \sourcinasia\appBundle\Entity\Invoicecustomer
     */
    private $invoicecustomer;

    /**
     * @var \sourcinasia\appBundle\Entity\Invoicesupplier
     */
    private $invoicesupplier;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Payment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Payment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Payment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set type
     *
     * @param \sourcinasia\appBundle\Entity\Paymenttype $type
     * @return Payment
     */
    public function setType(\sourcinasia\appBundle\Entity\Paymenttype $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \sourcinasia\appBundle\Entity\Paymenttype 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set invoicecustomer
     *
     * @param \sourcinasia\appBundle\Entity\Invoicecustomer $invoicecustomer
     * @return Payment
     */
    public function setInvoicecustomer(\sourcinasia\appBundle\Entity\Invoicecustomer $invoicecustomer = null)
    {
        $this->invoicecustomer = $invoicecustomer;

        return $this;
    }

    /**
     * Get invoicecustomer
     *
     * @return \sourcinasia\appBundle\Entity\Invoicecustomer 
     */
    public function getInvoicecustomer()
    {
        return $this->invoicecustomer;
    }

    /**
     * Set invoicesupplier
     *
     * @param \sourcinasia\appBundle\Entity\Invoicesupplier $invoicesupplier
     * @return Payment
     */
    public function setInvoicesupplier(\sourcinasia\appBundle\Entity\Invoicesupplier $invoicesupplier = null)
    {
        $this->invoicesupplier = $invoicesupplier;

        return $this;
    }

    /**
     * Get invoicesupplier
     *
     * @return \sourcinasia\appBundle\Entity\Invoicesupplier 
     */
    public function getInvoicesupplier()
    {
        return $this->invoicesupplier;
    }
}
