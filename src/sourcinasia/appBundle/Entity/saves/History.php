<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * History
 */
class History
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $ico;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplier
     */
    private $supplier;

    /**
     * @var \sourcinasia\appBundle\Entity\Product
     */
    private $product;

    /**
     * @var \sourcinasia\appBundle\Entity\Offer
     */
    private $offer;

    /**
     * @var \sourcinasia\appBundle\Entity\Customer
     */
    private $customer;

    /**
     * @var \sourcinasia\appBundle\Entity\Cadencier
     */
    private $cadencier;

    /**
     * @var \sourcinasia\appBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return History
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set ico
     *
     * @param string $ico
     * @return History
     */
    public function setIco($ico)
    {
        $this->ico = $ico;

        return $this;
    }

    /**
     * Get ico
     *
     * @return string 
     */
    public function getIco()
    {
        return $this->ico;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return History
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return History
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set supplier
     *
     * @param \sourcinasia\appBundle\Entity\Supplier $supplier
     * @return History
     */
    public function setSupplier(\sourcinasia\appBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \sourcinasia\appBundle\Entity\Supplier 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set product
     *
     * @param \sourcinasia\appBundle\Entity\Product $product
     * @return History
     */
    public function setProduct(\sourcinasia\appBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \sourcinasia\appBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set offer
     *
     * @param \sourcinasia\appBundle\Entity\Offer $offer
     * @return History
     */
    public function setOffer(\sourcinasia\appBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get offer
     *
     * @return \sourcinasia\appBundle\Entity\Offer 
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * Set customer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customer
     * @return History
     */
    public function setCustomer(\sourcinasia\appBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \sourcinasia\appBundle\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set cadencier
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadencier
     * @return History
     */
    public function setCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadencier = null)
    {
        $this->cadencier = $cadencier;

        return $this;
    }

    /**
     * Get cadencier
     *
     * @return \sourcinasia\appBundle\Entity\Cadencier 
     */
    public function getCadencier()
    {
        return $this->cadencier;
    }

    /**
     * Set user
     *
     * @param \sourcinasia\appBundle\Entity\User $user
     * @return History
     */
    public function setUser(\sourcinasia\appBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \sourcinasia\appBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
