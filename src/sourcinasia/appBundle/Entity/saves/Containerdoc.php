<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Containerdoc
 */
class Containerdoc {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $validate;

    /**
     * @var string
     */
    private $document;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $validate_date;

    /**
     * @var \sourcinasia\appBundle\Entity\Command
     */
    private $command;

    /**
     * @var \sourcinasia\appBundle\Entity\User
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set validate
     *
     * @param boolean $validate
     * @return Containerdoc
     */
    public function setValidate($validate) {
        $this->validate = $validate;

        return $this;
    }

    /**
     * Get validate
     *
     * @return boolean 
     */
    public function getValidate() {
        return $this->validate;
    }

    /**
     * Set document
     *
     * @param string $document
     * @return Containerdoc
     */
    public function setDocument($document) {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return string 
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Containerdoc
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set validate_date
     *
     * @param \DateTime $validateDate
     * @return Containerdoc
     */
    public function setValidateDate($validateDate) {
        $this->validate_date = $validateDate;

        return $this;
    }

    /**
     * Get validate_date
     *
     * @return \DateTime 
     */
    public function getValidateDate() {
        return $this->validate_date;
    }

    /**
     * Set user
     *
     * @param \sourcinasia\appBundle\Entity\User $user
     * @return Containerdoc
     */
    public function setUser(\sourcinasia\appBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \sourcinasia\appBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }
    
    
    /**
     * Set command
     *
     * @param \sourcinasia\appBundle\Entity\Command $command
     * @return Containerdoc
     */
    public function setCommand(\sourcinasia\appBundle\Entity\Command $command = null) {
        $this->command = $command;

        return $this;
    }

    /**
     * Get command
     *
     * @return \sourcinasia\appBundle\Entity\Command 
     */
    public function getCommand() {
        return $this->command;
    }


    /*     * ***** HasLifecycleCallbacks ******** */

    protected function getUploadTmpDir() {
        return __SOURCINASIA__ . 'tmp';
    }

    protected function getUploadDir() {
        return __SOURCINASIA__ . 'documents/containers';
    }

    /**
     * @ORM\PrePersist
     */
    public function uploadDocument() {

        if (!is_object($this->document))
            return;

        $filename = $this->document->getClientOriginalName();
        $this->document->move($this->getUploadTmpDir(), $filename);
        $source = $this->getUploadTmpDir() . '/' . $filename;

        $this->CheckDirectory($this->getUploadDir() . '/' . ($this->getCommand()->getId()));
        $directory = $this->getCommand()->getId() . '/' . $this->getSupplychaintype()->getTitle();
        $destination = $this->getUploadDir() . '/' . $directory;
        $this->CheckDirectory($destination);

        if (in_array(mime_content_type($source), array('application/pdf', 'image/jpeg', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/msword'))) {
            $filename = '/' . date('ymdhis') . $filename;
            copy($source, $destination . $filename);
            $this->setDocument($directory . $filename);
            $this->setCreated(new \DateTime());
            unlink($source);
        }
    }

    /**
     * @ORM\PreRemove
     */
    public function removeDocument() {
        $file = $this->getUploadDir() . '/' . $this->getDocument();
        if (file_exists($file))
            unlink($file);
    }

    private function CheckDirectory($url) {
        if (!is_dir($url)) {
            mkdir($url);
            $fichier = fopen($url . 'index.html', 'w+');
            fputs($fichier, "");
            fclose($fichier);
        }
        return true;
    }
    
        /**
     * @var string
     */
    private $name;

    /**
     * Set name
     *
     * @param string $name
     * @return Containerdoc
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }
    

    /**
     * @var \sourcinasia\appBundle\Entity\Supplychaintype
     */
    private $supplychaintype;


    /**
     * Set supplychaintype
     *
     * @param \sourcinasia\appBundle\Entity\Supplychaintype $supplychaintype
     * @return Containerdoc
     */
    public function setSupplychaintype(\sourcinasia\appBundle\Entity\Supplychaintype $supplychaintype = null)
    {
        $this->supplychaintype = $supplychaintype;

        return $this;
    }

    /**
     * Get supplychaintype
     *
     * @return \sourcinasia\appBundle\Entity\Supplychaintype 
     */
    public function getSupplychaintype()
    {
        return $this->supplychaintype;
    }
}
