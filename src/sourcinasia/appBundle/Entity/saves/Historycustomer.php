<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historycustomer
 */
class Historycustomer {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var \sourcinasia\appBundle\Entity\Customer
     */
    private $customer; 

    /**
     * @var \sourcinasia\appBundle\Entity\User
     */
    private $user;

    /**
     * @var string
     */
    private $ico;

    /**
     * @var \sourcinasia\appBundle\Entity\Cadencier
     */
    private $cadencier;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Historycustomer
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Historycustomer
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Historycustomer
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set customer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customer
     * @return Historycustomer
     */
    public function setCustomer(\sourcinasia\appBundle\Entity\Customer $customer = null) {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \sourcinasia\appBundle\Entity\Customer 
     */
    public function getCustomer() {
        return $this->customer;
    }

    /**
     * Set user
     *
     * @param \sourcinasia\appBundle\Entity\User $user
     * @return Historycustomer
     */
    public function setUser(\sourcinasia\appBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \sourcinasia\appBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set ico
     *
     * @param string $ico
     * @return Historycustomer
     */
    public function setIco($ico) {
        $this->ico = $ico;

        return $this;
    }

    /**
     * Get ico
     *
     * @return string 
     */
    public function getIco() {
        return $this->ico;
    }

    /**
     * Set cadencier
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadencier
     * @return Historycustomer
     */
    public function setCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadencier = null) {
        $this->cadencier = $cadencier;

        return $this;
    }

    /**
     * Get cadencier
     *
     * @return \sourcinasia\appBundle\Entity\Cadencier 
     */
    public function getCadencier() {
        return $this->cadencier;
    }

}
