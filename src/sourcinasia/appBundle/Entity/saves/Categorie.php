<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 */
class Categorie {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $department;

    /**
     * @var string
     */
    private $shelves;

    /**
     * @var string
     */
    private $familly;

    /**
     * @var string
     */
    private $subfamilly;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var integer
     */
    private $indexation;
    
    /**
     * @var integer
     */
    private $margesourcin;

    /**
     * @var string
     */
    private $restriction;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $products;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return id
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Categorie
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set department
     *
     * @param string $department
     * @return Categorie
     */
    public function setDepartment($department) {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return string 
     */
    public function getDepartment() {
        return $this->department;
    }

    /**
     * Set shelves
     *
     * @param string $shelves
     * @return Categorie
     */
    public function setShelves($shelves) {
        $this->shelves = $shelves;

        return $this;
    }

    /**
     * Get shelves
     *
     * @return string 
     */
    public function getShelves() {
        return $this->shelves;
    }

    /**
     * Set familly
     *
     * @param string $familly
     * @return Categorie
     */
    public function setFamilly($familly) {
        $this->familly = $familly;

        return $this;
    }

    /**
     * Get familly
     *
     * @return string 
     */
    public function getFamilly() {
        return $this->familly;
    }

    /**
     * Set subfamilly
     *
     * @param string $subfamilly
     * @return Categorie
     */
    public function setSubfamilly($subfamilly) {
        $this->subfamilly = $subfamilly;

        return $this;
    }

    /**
     * Get subfamilly
     *
     * @return string 
     */
    public function getSubfamilly() {
        return $this->subfamilly;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Categorie
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Categorie
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Get getIdname()
     *
     * @return string 
     */
    public function getIdname() {
        return $this->id . '-' . $this->name;
    }

    /**
     * Set margesourcin
     *
     * @param integer $margesourcin
     * @return Categorie
     */
    public function setMargesourcin($margesourcin) {
        $this->margesourcin = $margesourcin;

        return $this;
    }

    /**
     * Get margesourcin
     *
     * @return integer 
     */
    public function getMargesourcin() {
        return $this->margesourcin;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add products
     *
     * @param \sourcinasia\appBundle\Entity\Product $products
     * @return Categorie
     */
    public function addProduct(\sourcinasia\appBundle\Entity\Product $products) {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \sourcinasia\appBundle\Entity\Product $products
     */
    public function removeProduct(\sourcinasia\appBundle\Entity\Product $products) {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts() {
        return $this->products;
    }

    /**
     * Set restriction
     *
     * @param string $restriction
     * @return Categorie
     */
    public function setRestriction() {
        
        $this->restriction = $this->getDepartment() . $this->getShelves() . $this->getFamilly();

        return $this;
    }

    /**
     * Get restriction
     *
     * @return string 
     */
    public function getRestriction() {
        return $this->restriction;
    }

    /**
     * Set indexation
     *
     * @param integer $indexation
     * @return Categorie
     */
    public function setIndexation($indexation)
    {
        $this->indexation = $indexation;
    
        return $this;
    }

    /**
     * Get indexation
     *
     * @return integer 
     */
    public function getIndexation()
    {
        return $this->indexation;
    }
}