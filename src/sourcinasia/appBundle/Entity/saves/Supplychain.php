<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supplychain
 */
class Supplychain {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $validate;

    /**
     * @var string
     */
    private $document;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $validate_date;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplychaintype
     */
    private $supplychaintype;

    /**
     * @var \sourcinasia\appBundle\Entity\User
     */
    private $user;

    /**
     * @var \sourcinasia\appBundle\Entity\Cadencier
     */
    private $cadencier;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set validate
     *
     * @param boolean $validate
     * @return Supplychain
     */
    public function setValidate($validate) {
        $this->validate = $validate;

        return $this;
    }

    /**
     * Get validate
     *
     * @return boolean 
     */
    public function getValidate() {
        return $this->validate;
    }

    /**
     * Set document
     *
     * @param string $document
     * @return Supplychain
     */
    public function setDocument($document) {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return string 
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Supplychain
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set validate_date
     *
     * @param \DateTime $validateDate
     * @return Supplychain
     */
    public function setValidateDate($validateDate) {
        $this->validate_date = $validateDate;

        return $this;
    }

    /**
     * Get validate_date
     *
     * @return \DateTime 
     */
    public function getValidateDate() {
        return $this->validate_date;
    }

    /**
     * Set supplychaintype
     *
     * @param \sourcinasia\appBundle\Entity\Supplychaintype $supplychaintype
     * @return Supplychain
     */
    public function setSupplychaintype(\sourcinasia\appBundle\Entity\Supplychaintype $supplychaintype = null) {
        $this->supplychaintype = $supplychaintype;

        return $this;
    }

    /**
     * Get supplychaintype
     *
     * @return \sourcinasia\appBundle\Entity\Supplychaintype 
     */
    public function getSupplychaintype() {
        return $this->supplychaintype;
    }

    /**
     * Set user
     *
     * @param \sourcinasia\appBundle\Entity\User $user
     * @return Supplychain
     */
    public function setUser(\sourcinasia\appBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \sourcinasia\appBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set cadencier
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadencier
     * @return Supplychain
     */
    public function setCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadencier = null) {
        $this->cadencier = $cadencier;

        return $this;
    }

    /**
     * Get cadencier
     *
     * @return \sourcinasia\appBundle\Entity\Cadencier 
     */
    public function getCadencier() {
        return $this->cadencier;
    }

    /*     * ***** HasLifecycleCallbacks ******** */

    protected function getUploadTmpDir() {
        return __SOURCINASIA__ . 'tmp';
    }

    protected function getUploadDir() {
        return __SOURCINASIA__ . 'documents/orders';
    }

    /**
     * @ORM\PrePersist
     */
    public function uploadDocument() {

        if (!is_object($this->document))
            return;

        $this->filename = $this->document->getClientOriginalName();
        $this->document->move($this->getUploadTmpDir(), $this->filename);
        $source = $this->getUploadTmpDir() . '/' . $this->filename;

        $this->CheckDirectory($this->getUploadDir() . '/' . ($this->getCadencier()->getId()));
        $directory = $this->getCadencier()->getId() . '/' . $this->getSupplychaintype()->getTitle();
        $destination = $this->getUploadDir() . '/' . $directory;
        $this->CheckDirectory($destination);

        print_r(mime_content_type($source));

        if (in_array(mime_content_type($source), array('application/pdf', 'image/jpeg', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/msword'))) {
            $filename = '/' . date('ymdhis') . $this->filename;
            copy($source, $destination . $filename);
            $this->setDocument($directory . $filename);
            $this->setCreated(new \DateTime());
            $this->setName($this->filename);
            unlink($source);
        }
    }

    /**
     * @ORM\PreRemove
     */
    public function removeDocument() {
        $file = $this->getUploadDir() . '/' . $this->getDocument();
        if (file_exists($file))
            unlink($file);
    }

    private function CheckDirectory($url) {
        if (!is_dir($url)) {
            mkdir($url);
            $fichier = fopen($url . '/' . 'index.html', 'w+');
            fputs($fichier, "");
            fclose($fichier);
        }
        return true;
    }

    /**
     * @var string
     */
    private $name;

    /**
     * Set name
     *
     * @param string $name
     * @return Supplychain
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

}
