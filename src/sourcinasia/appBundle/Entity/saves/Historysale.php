<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historysale
 */
class Historysale {

  
    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $devis;

    /**
     * @var integer
     */
    private $qty;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $incoterm;

    /**
     * @var string
     */
    private $pol;

    /**
     * @var \sourcinasia\appBundle\Entity\Cadencier
     */
    private $cadencier;

    /**
     * @var \sourcinasia\appBundle\Entity\Customer
     */
    private $customer;

    /**
     * @var \sourcinasia\appBundle\Entity\Product
     */
    private $product;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Historysale
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set devis
     *
     * @param string $devis
     * @return Historysale
     */
    public function setDevis($devis)
    {
        $this->devis = $devis;

        return $this;
    }

    /**
     * Get devis
     *
     * @return string 
     */
    public function getDevis()
    {
        return $this->devis;
    }

    /**
     * Set qty
     *
     * @param integer $qty
     * @return Historysale
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     * @return integer 
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Historysale
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set incoterm
     *
     * @param string $incoterm
     * @return Historysale
     */
    public function setIncoterm($incoterm)
    {
        $this->incoterm = $incoterm;

        return $this;
    }

    /**
     * Get incoterm
     *
     * @return string 
     */
    public function getIncoterm()
    {
        return $this->incoterm;
    }

    /**
     * Set pol
     *
     * @param string $pol
     * @return Historysale
     */
    public function setPol($pol)
    {
        $this->pol = $pol;

        return $this;
    }

    /**
     * Get pol
     *
     * @return string 
     */
    public function getPol()
    {
        return $this->pol;
    }

    /**
     * Set cadencier
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadencier
     * @return Historysale
     */
    public function setCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadencier = null)
    {
        $this->cadencier = $cadencier;

        return $this;
    }

    /**
     * Get cadencier
     *
     * @return \sourcinasia\appBundle\Entity\Cadencier 
     */
    public function getCadencier()
    {
        return $this->cadencier;
    }

    /**
     * Set customer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customer
     * @return Historysale
     */
    public function setCustomer(\sourcinasia\appBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \sourcinasia\appBundle\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set product
     *
     * @param \sourcinasia\appBundle\Entity\Product $product
     * @return Historysale
     */
    public function setProduct(\sourcinasia\appBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \sourcinasia\appBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
}
