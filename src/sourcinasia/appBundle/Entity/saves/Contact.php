<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 */
class Contact {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $mobile;

    /**
     * @var string
     */
    private $fax;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $job;

    /**
     * @var string
     */
    private $qq;

    /**
     * @var string
     */
    private $skype;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplier
     */
    private $supplier;

    /**
     * @var \sourcinasia\appBundle\Entity\Customer
     */
    private $customer;

    /**
     * @var \DateTime
     */
    private $exportdate;

    /**
     * @var integer
     */
    private $exportnbr;

    /**
     * @var integer
     */
    private $exportnbrmax;

    /**
     * @var \sourcinasia\appBundle\Entity\User
     */
    private $user;

    /**
     * @var integer
     */
    private $type;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Contact
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Contact
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return Contact
     */
    public function setMobile($mobile) {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile() {
        return $this->mobile;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Contact
     */
    public function setFax($fax) {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax() {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Contact
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set job
     *
     * @param string $job
     * @return Contact
     */
    public function setJob($job) {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return string 
     */
    public function getJob() {
        return $this->job;
    }

    /**
     * Set qq
     *
     * @param string $qq
     * @return Contact
     */
    public function setQq($qq) {
        $this->qq = $qq;

        return $this;
    }

    /**
     * Get qq
     *
     * @return string 
     */
    public function getQq() {
        return $this->qq;
    }

    /**
     * Set skype
     *
     * @param string $skype
     * @return Contact
     */
    public function setSkype($skype) {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string 
     */
    public function getSkype() {
        return $this->skype;
    }

    /**
     * Set supplier
     *
     * @param \sourcinasia\appBundle\Entity\Supplier $supplier
     * @return Contact
     */
    public function setSupplier(\sourcinasia\appBundle\Entity\Supplier $supplier = null) {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \sourcinasia\appBundle\Entity\Supplier 
     */
    public function getSupplier() {
        return $this->supplier;
    }

    /**
     * Set customer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customer
     * @return Contact
     */
    public function setCustomer(\sourcinasia\appBundle\Entity\Customer $customer = null) {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \sourcinasia\appBundle\Entity\Customer 
     */
    public function getCustomer() {
        return $this->customer;
    }

    /**
     * Set user
     *
     * @param \sourcinasia\appBundle\Entity\User $user
     * @return Contact
     */
    public function setUser(\sourcinasia\appBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \sourcinasia\appBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set exportdate
     *
     * @param \DateTime $exportdate
     * @return Contact
     */
    public function setExportdate($exportdate) {
        $this->exportdate = $exportdate;

        return $this;
    }

    /**
     * Get exportdate
     *
     * @return \DateTime 
     */
    public function getExportdate() {
        return $this->exportdate;
    }

    /**
     * Set exportnbr
     *
     * @param integer $exportnbr
     * @return Contact
     */
    public function setExportnbr($exportnbr) {
        $this->exportnbr = $exportnbr;

        return $this;
    }

    /**
     * Get exportnbr
     *
     * @return integer 
     */
    public function getExportnbr() {
        return $this->exportnbr;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Contact
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set exportnbrmax
     *
     * @param integer $exportnbrmax
     * @return Customer
     */
    public function setExportnbrmax($exportnbrmax) {
        $this->exportnbrmax = $exportnbrmax;

        return $this;
    }

    /**
     * Get exportnbrmax
     *
     * @return integer 
     */
    public function getExportnbrmax() {
        return $this->exportnbrmax;
    }

}
