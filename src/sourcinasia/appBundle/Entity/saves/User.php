<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 */
class User extends BaseUser {

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var \sourcinasia\appBundle\Entity\Contact
     */
    private $contact;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $histories;

    /**
     * @var \sourcinasia\appBundle\Entity\Logo
     */
    private $logo;

    /**
     * @var integer
     */
    private $modeonlogin;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var integer
     */
    private $customermargecoef;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $domainerestriction;

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->histories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set contact
     *
     * @param \sourcinasia\appBundle\Entity\Contact $contact
     * @return User
     */
    public function setContact(\sourcinasia\appBundle\Entity\Contact $contact = null) {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \sourcinasia\appBundle\Entity\Contact 
     */
    public function getContact() {
        return $this->contact;
    }

    /**
     * Set logo
     *
     * @param \sourcinasia\appBundle\Entity\Logo $logo
     * @return User
     */
    public function setLogo(\sourcinasia\appBundle\Entity\Logo $logo = null) {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return \sourcinasia\appBundle\Entity\Logo 
     */
    public function getLogo() {
        return $this->logo;
    }

    /**
     * Set modeonlogin
     *
     * @param integer $modeonlogin
     * @return User
     */
    public function setModeonlogin($modeonlogin) {
        $this->modeonlogin = $modeonlogin;

        return $this;
    }

    /**
     * Get modeonlogin
     *
     * @return integer 
     */
    public function getModeonlogin() {
        return $this->modeonlogin;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return User
     */
    public function setLocale($locale) {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string 
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * Set customermargecoef
     *
     * @param integer $customermargecoef
     * @return User
     */
    public function setCustomermargecoef($customermargecoef) {
        $this->customermargecoef = $customermargecoef;

        return $this;
    }

    /**
     * Get customermargecoef
     *
     * @return integer 
     */
    public function getCustomermargecoef() {
        return $this->customermargecoef;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set domainerestriction
     *
     * @param string $domainerestriction
     * @return User
     */
    public function setDomainerestriction($domainerestriction) {
        $this->domainerestriction = $domainerestriction;

        return $this;
    }

    /**
     * Get domainerestriction
     *
     * @return string 
     */
    public function getDomainerestriction() {
        return $this->domainerestriction;
    }


    /**
     * Add histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     * @return User
     */
    public function addHistory(\sourcinasia\appBundle\Entity\History $histories)
    {
        $this->histories[] = $histories;

        return $this;
    }

    /**
     * Remove histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     */
    public function removeHistory(\sourcinasia\appBundle\Entity\History $histories)
    {
        $this->histories->removeElement($histories);
    }

    /**
     * Get histories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistories()
    {
        return $this->histories;
    }
}
