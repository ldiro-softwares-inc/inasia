<?php

namespace sourcinasia\appBundle\Entity;

use claviska\SimpleImage;
use Doctrine\ORM\Mapping as ORM;

/**
 * Logo
 */
class Logo {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $imagekey;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \sourcinasia\appBundle\Entity\Customer
     */
    private $customer;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Logo
     */
    public function setImage($image) {
        if ($image)
            $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set imagekey
     *
     * @param string $imagekey
     * @return Logo
     */
    public function setImagekey($imagekey) {
        $this->imagekey = $imagekey;

        return $this;
    }

    /**
     * Get imagekey
     *
     * @return string
     */
    public function getImagekey() {
        return $this->imagekey;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Logo
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set customer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customer
     * @return Logo
     */
    public function setCustomer(\sourcinasia\appBundle\Entity\Customer $customer = null) {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \sourcinasia\appBundle\Entity\Customer
     */
    public function getCustomer() {
        return $this->customer;
    }

    /*     * ***** HasLifecycleCallbacks ******** */

    protected function getUploadTmpDir() {
        return __SOURCINASIA__ . 'tmp';
    }

    protected function getUploadDir() {
        return __SOURCINASIA__ . 'htdocs/logos';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadImage() {

        if (empty($this->image))
            return;

        $this->filename = date('ymdhis') . rand(0, 100000);
        $tmpdest = $this->getUploadTmpDir() . '/' . $this->filename;
        $filename = md5(date('ymdhis') . $this->filename) . '.jpeg';
        $dest = $this->getUploadDir() . '/' . $filename;
        $this->image->move($this->getUploadTmpDir(), $this->filename);
        $info = getimagesize($tmpdest);
        $img = new SimpleImage();
        switch ($info['mime']) {
            case 'image/gif':
                $img->fromFile($tmpdest)->bestFit(120, 120)->toFile($dest);
                break;
            case 'image/jpeg':
                $img->fromFile($tmpdest)->bestFit(120, 120)->toFile($dest);
                break;
            case 'image/png':
                $img->fromFile($tmpdest)->bestFit(120, 120)->toFile($dest);
                break;
        }
        $this->setImage('/logos/' . $filename);
        $this->setCreated(new \DateTime());
        unlink($tmpdest);
    }

    /**
     * @ORM\PreRemove()
     */
    public function removeImage() {
        $file = $this->getUploadDir() . $this->getImage();
        if (file_exists($file))
            unlink($file);
    }

}
