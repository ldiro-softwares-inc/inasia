<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Offer
 */
class Offer {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $exwusd;

    /**
     * @var string
     */
    private $fob;

    /**
     * @var string
     */
    private $exw;

    /**
     * @var integer
     */
    private $moq;

    /**
     * @var boolean
     */
    private $deal;

    /**
     * @var string
     */
    private $customermoqs;

    /**
     * @var boolean
     */
    private $mainoffer;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var integer
     */
    private $moqpacking;

    /**
     * @var string
     */
    private $itemnumber;

    /**
     * @var string
     */
    private $moqunit;

    /**
     * @var string
     */
    private $moqpackingunit;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $histories;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplier
     */
    private $supplier;

    /**
     * @var \sourcinasia\appBundle\Entity\Product
     */
    private $product;

    /**
     * @var \sourcinasia\appBundle\Entity\Currency
     */
    private $currency;

    /**
     * @var \sourcinasia\appBundle\Entity\Pol
     */
    private $pol;

    /**
     * @var \DateTime
     */
    private $exwusdmodificationdate;

    /**
     * @var string
     */
    private $fobnegociation;

    /**
     * @var \DateTime
     */
    private $fobmodificationdate;

    /**
     * @var string
     */
    private $exwnegociation;

    /**
     * @var string
     */
    private $customerprices;

    /**
     * @var string
     */
    private $customerhidden;

    /**
     * @var string
     */
    private $archive;

    /**
     * Constructor
     */
    public function __construct() {
        $this->modified = new\dateTime();
        $this->histories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set exwusd
     *
     * @param integer $exwusd
     * @return Offer
     */
    public function setExwusd($exwusd) {
        $this->exwusd = $exwusd;

        return $this;
    }

    /**
     * Get exwusd
     *
     * @return integer 
     */
    public function getExwusd() {
        return $this->exwusd;
    }

    /**
     * Set fob
     *
     * @param string $fob
     * @return Offer
     */
    public function setFob($fob) {
        $this->fob = $fob;

        return $this;
    }

    /**
     * Get fob
     *
     * @return string 
     */
    public function getFob() {
        return $this->fob;
    }

    /**
     * Set exw
     *
     * @param string $exw
     * @return Offer
     */
    public function setExw($exw) {
        $this->exw = $exw;

        return $this;
    }

    /**
     * Get exw
     *
     * @return string 
     */
    public function getExw() {
        return $this->exw;
    }

    /**
     * Set moq
     *
     * @param integer $moq
     * @return Offer
     */
    public function setMoq($moq) {
        $this->moq = $moq;

        return $this;
    }

    /**
     * Get moq
     *
     * @return integer 
     */
    public function getMoq($customerid = 0) {
        if ($this->isCustomerMoq($customerid)) {
            $Customermoqs = $this->getCustomermoqs();
            return $Customermoqs[$customerid . 'm'];
        }

        return $this->moq;
    }

    public function isCustomerMoq($customerid) {
        return ($customerid && array_key_exists($customerid . 'm', $this->getCustomermoqs()));
    }

    /**
     * Set deal
     *
     * @param boolean $deal
     * @return Offer
     */
    public function setDeal($deal) {
        $this->deal = $deal;

        return $this;
    }

    /**
     * Get deal
     *
     * @return boolean 
     */
    public function getDeal() {
        return $this->deal;
    }

    /**
     * Set mainoffer
     *
     * @param boolean $mainoffer
     * @return Offer
     */
    public function setMainoffer($mainoffer) {
        $this->mainoffer = $mainoffer;

        return $this;
    }

    /**
     * Get mainoffer
     *
     * @return boolean 
     */
    public function getMainoffer() {
        return $this->mainoffer;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Offer
     */
    public function setModified($modified) {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified() {
        return $this->modified;
    }

    /**
     * Set moqpacking
     *
     * @param integer $moqpacking
     * @return Offer
     */
    public function setMoqpacking($moqpacking) {
        $this->moqpacking = $moqpacking;

        return $this;
    }

    /**
     * Get moqpacking
     *
     * @return integer 
     */
    public function getMoqpacking() {
        return $this->moqpacking;
    }

    /**
     * Set itemnumber
     *
     * @param string $itemnumber
     * @return Offer
     */
    public function setItemnumber($itemnumber) {
        $this->itemnumber = $itemnumber;

        return $this;
    }

    /**
     * Get itemnumber
     *
     * @return string 
     */
    public function getItemnumber() {
        return $this->itemnumber;
    }

    /**
     * Set moqunit
     *
     * @param string $moqunit
     * @return Offer
     */
    public function setMoqunit($moqunit) {
        $this->moqunit = $moqunit;

        return $this;
    }

    /**
     * Get moqunit
     *
     * @return string 
     */
    public function getMoqunit() {
        return $this->moqunit;
    }

    /**
     * Set moqpackingunit
     *
     * @param string $moqpackingunit
     * @return Offer
     */
    public function setMoqpackingunit($moqpackingunit) {
        $this->moqpackingunit = $moqpackingunit;

        return $this;
    }

    /**
     * Get moqpackingunit
     *
     * @return string 
     */
    public function getMoqpackingunit() {
        return $this->moqpackingunit;
    }

    /**
     * Set supplier
     *
     * @param \sourcinasia\appBundle\Entity\Supplier $supplier
     * @return Offer
     */
    public function setSupplier(\sourcinasia\appBundle\Entity\Supplier $supplier = null) {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \sourcinasia\appBundle\Entity\Supplier 
     */
    public function getSupplier() {
        return $this->supplier;
    }

    /**
     * Set product
     *
     * @param \sourcinasia\appBundle\Entity\Product $product
     * @return Offer
     */
    public function setProduct(\sourcinasia\appBundle\Entity\Product $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \sourcinasia\appBundle\Entity\Product 
     */
    public function getProduct() {
        return $this->product;
    }

    /**
     * Set currency
     *
     * @param \sourcinasia\appBundle\Entity\Currency $currency
     * @return Offer
     */
    public function setCurrency(\sourcinasia\appBundle\Entity\Currency $currency = null) {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \sourcinasia\appBundle\Entity\Currency 
     */
    public function getCurrency() {
        return $this->currency;
    }

    /**
     * Set pol
     *
     * @param \sourcinasia\appBundle\Entity\Pol $pol
     * @return Offer
     */
    public function setPol(\sourcinasia\appBundle\Entity\Pol $pol = null) {
        $this->pol = $pol;

        return $this;
    }

    /**
     * Get pol
     *
     * @return \sourcinasia\appBundle\Entity\Pol 
     */
    public function getPol() {
        return $this->pol;
    }

    public function isCustomerprices($user, $incoterm) {
        return ($user['cutomer_id'] && array_key_exists($user['cutomer_id'] . $incoterm, $this->getCustomerprices()));
    }

    public function GetSaleprice($incoterm, $user, $getCustomerPrice = true, $forceMArge = true) { //$forceMArge � true pour �tre sur que dans les exports cela soit pris en compte
        $isCustomerprices = $this->isCustomerprices($user, $incoterm);

        if ($isCustomerprices && $getCustomerPrice) {
            $Customerprices = $this->getCustomerprices();
            $prixventesourcinasiafix = $Customerprices[$user['cutomer_id'] . $incoterm];
        } else
            $prixventesourcinasiafix = 0;

        if (in_array($incoterm, array('fob', 'exw')))
            $today = new \DateTime();
        switch ($incoterm) {
            case 'fob':
                $price = $this->GetFob();
                $nbj = $this->GetFobModificationdate()->diff($today)->days;
                break;
            case 'exw':
                $price = $this->GetExwusd();
                $nbj = $this->GetExwusdmodificationdate()->diff($today)->days;
                break;
            default:
                die('No incoterm');
        }
        $margesourcin = $this->GetProduct()->GetCategorie()->getMargesourcin();
        $pindex = ($nbj * $this->GetProduct()->GetCategorie()->getIndexation() / 365);
        $customercoef = (int) $user['customer_coef'];

        $prixventesourcinasiafree = $price / (1 - (($margesourcin + $pindex ) / 100));
        $prixventesourcinasiafreewithcustomercoef = $price / (1 - (($margesourcin + $pindex + $customercoef ) / 100));

        $prixventesourcinasia = ( $prixventesourcinasiafix < $prixventesourcinasiafree) ? $prixventesourcinasiafreewithcustomercoef : $prixventesourcinasiafix;

        if (($user['mode'] == 'commercial') || ($user['mode'] == 'presentation')) // meme si pas de prix en presentation
            return sprintf("%.2f", $prixventesourcinasia / (1 - $user['commercial_coef'] / 100));
        elseif ($user['contact_type'] == 4) // il est de toutes facons en mode 4
            return sprintf("%.2f", $prixventesourcinasia / (1 - $user['commercial_coef'] / 100));
        elseif ($user['cutomer_id'] == 0 && $forceMArge)
            return sprintf("%.2f", $prixventesourcinasia / (1 - $user['commercial_coef'] / 100));
        else
            return sprintf("%.2f", $prixventesourcinasia);
    }

    public function GetSourcinasiaprice($incoterm) {
        if (in_array($incoterm, array('fob', 'exw')))
            $today = new \DateTime();
        switch ($incoterm) {
            case 'fob':
                $price = $this->GetFob();
                $nbj = $this->GetFobModificationdate()->diff($today)->days;
                break;
            case 'exw':
                $price = $this->GetExwusd();
                $nbj = $this->GetExwusdmodificationdate()->diff($today)->days;
                break;
            default:
                die('No incoterm');
        }
        $margesourcin = $this->GetProduct()->GetCategorie()->getMargesourcin();
        $pindex = ($nbj * $this->GetProduct()->GetCategorie()->getIndexation() / 365);
        $prixsourcing = $price / (1 - (($margesourcin + $pindex ) / 100));
        return $prixsourcing;
    }

    public function GetSourcingprice($incoterm) {
        if (in_array($incoterm, array('fob', 'exw')))
            switch ($incoterm) {
                case 'fob':
                    $price = $this->GetFob();
                    break;
                case 'exw':
                    $price = $this->GetExwusd();
                    break;
                default:
                    die('No incoterm');
            }
        return $price;
    }

    public function isHidden($customerid) {
        return (in_array('p' . $customerid, $this->getCustomerhidden()));
    }

    /**
     * Set exwusdmodificationdate
     *
     * @param \DateTime $exwusdmodificationdate
     * @return Offer
     */
    public function setExwusdmodificationdate($exwusdmodificationdate) {
        $this->exwusdmodificationdate = $exwusdmodificationdate;

        return $this;
    }

    /**
     * Get exwusdmodificationdate
     *
     * @return \DateTime 
     */
    public function getExwusdmodificationdate() {
        return $this->exwusdmodificationdate;
    }

    /**
     * Set fobnegociation
     *
     * @param string $fobnegociation
     * @return Offer
     */
    public function setFobnegociation($fobnegociation) {
        $this->fobnegociation = $fobnegociation;

        return $this;
    }

    /**
     * Get fobnegociation
     *
     * @return string 
     */
    public function getFobnegociation() {
        return $this->fobnegociation;
    }

    /**
     * Set fobmodificationdate
     *
     * @param \DateTime $fobmodificationdate
     * @return Offer
     */
    public function setFobmodificationdate($fobmodificationdate) {
        $this->fobmodificationdate = $fobmodificationdate;

        return $this;
    }

    /**
     * Get fobmodificationdate
     *
     * @return \DateTime 
     */
    public function getFobmodificationdate() {
        return $this->fobmodificationdate;
    }

    /**
     * Set exwnegociation
     *
     * @param string $exwnegociation
     * @return Offer
     */
    public function setExwnegociation($exwnegociation) {
        $this->exwnegociation = $exwnegociation;

        return $this;
    }

    /**
     * Get exwnegociation
     *
     * @return string 
     */
    public function getExwnegociation() {
        return $this->exwnegociation;
    }

    /**
     * Set customerprices
     *
     * @param string $customerprices
     * @return Offer
     */
    public function setCustomerprices($customerprices) {
        $this->customerprices = json_encode($customerprices);

        return $this;
    }

    /**
     * Get customerprices
     *
     * @return string 
     */
    public function getCustomerprices() {
        if (empty($this->customerprices))
            return array();
        else
            return (array) json_decode($this->customerprices);
    }

    /**
     * Set customermoqs
     *
     * @param string $customermoqs
     * @return Offer
     */
    public function setCustomermoqs($customermoqs) {
        $this->customermoqs = json_encode($customermoqs);

        return $this;
    }

    /**
     * Get customermoqs
     *
     * @return string 
     */
    public function getCustomermoqs() {
        if (empty($this->customermoqs))
            return array();
        else
            return (array) json_decode($this->customermoqs);
    }

    /**
     * Get histories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistories() {
        return $this->histories;
    }

    /**
     * Add histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     * @return Offer
     */
    public function addHistory(\sourcinasia\appBundle\Entity\History $histories) {
        $this->histories[] = $histories;

        return $this;
    }

    /**
     * Remove histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     */
    public function removeHistory(\sourcinasia\appBundle\Entity\History $histories) {
        $this->histories->removeElement($histories);
    }

    /**
     * Set customerhidden
     *
     * @param string $customerhidden
     * @return Offer
     */
    public function setCustomerhidden($customerhidden) {
        $this->customerhidden = json_encode($customerhidden);

        return $this;
    }

    /**
     * Get customerhidden
     *
     * @return string 
     */
    public function getCustomerhidden() {
        return (array) json_decode($this->customerhidden);
    }

    /**
     * Set archive
     *
     * @param string $archive
     * @return Offer
     */
    public function setArchive($archive) {
        $this->archive = json_encode($archive);

        return $this;
    }

    /**
     * Get archive
     *
     * @return string 
     */
    public function getArchive() {
        return (array) json_decode($this->archive, true);
    }

}
