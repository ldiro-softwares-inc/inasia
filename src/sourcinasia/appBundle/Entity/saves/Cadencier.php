<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cadencier
 */
class Cadencier {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $offerslist;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $selection;

    /**
     * @var boolean
     */
    private $locked;

    /**
     * @var boolean
     */
    private $visible;

    /**
     * @var boolean
     */
    private $published;

    /**
     * @var string
     */
    private $gw;

    /**
     * @var string
     */
    private $nw;

    /**
     * @var string
     */
    private $cbm;

    /**
     * @var string
     */
    private $quantpieces;

    /**
     * @var string
     */
    private $quantpackages;

    /**
     * @var string
     */
    private $totalvalue;

    /**
     * @var string
     */
    private $publictoken;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var \DateTime
     */
    private $closepublicdate;

    /**
     * @var integer
     */
    private $state;

    /**
     * @var \DateTime
     */
    private $stepValidation;

    /**
     * @var \DateTime
     */
    private $stepProduction;

    /**
     * @var \DateTime
     */
    private $stepQcend;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $historiescustomers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $useractivities;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $supplychains;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $histories;

    /**
     * @var \sourcinasia\appBundle\Entity\Command
     */
    private $command;

    /**
     * @var \sourcinasia\appBundle\Entity\Customer
     */
    private $customer;

    /**
     * @var \sourcinasia\appBundle\Entity\Pol
     */
    private $pol;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplier
     */
    private $supplier;

    /**
     * @var string
     */
    private $supplierci;

    /**
     * @var string
     */
    private $customerforwarder;

    /**
     * @var \sourcinasia\appBundle\Entity\Incoterm
     */
    private $incoterm;

    /**
     * @var \sourcinasia\appBundle\Entity\Container
     */
    private $container;

    /**
     * @var \sourcinasia\appBundle\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $payments;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $customers;

    /**
     * @var float
     */
    private $totalvaluepi;

    /**
     * @var string
     */
    private $servicedescription;

    /**
     * @var float
     */
    private $servicevalue;

    /**
     * @var string
     */
    private $finalincoterm;

    /**
     * @var float
     */
    private $totalmarge;

    /**
     * @var string
     */
    private $finalcustomer;

    /**
     * @var integer
     */
    private $deleverytime;

    /**
     * @var \sourcinasia\appBundle\Entity\Paymentcondition
     */
    private $supplierpaymenterms;

    /**
     * @var \sourcinasia\appBundle\Entity\Paymentcondition
     */
    private $customerpaymenterms;

    /**
     * Constructor
     */
    public function __construct() {
        $this->useractivities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supplychains = new \Doctrine\Common\Collections\ArrayCollection();
        $this->histories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->payments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->customers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->historiescustomers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Cadencier
     */
    public function setComment($comment) {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     * @return Cadencier
     */
    public function setLocked($locked) {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean 
     */
    public function getLocked() {
        return $this->locked;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Cadencier
     */
    public function setVisible($visible) {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible() {
        return $this->visible;
    }

    /**
     * Set published
     *
     * @param boolean $published
     * @return Cadencier
     */
    public function setPublished($published) {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean 
     */
    public function getPublished() {
        return $this->published;
    }

    /**
     * Set gw
     *
     * @param string $gw
     * @return Cadencier
     */
    public function setGw($gw) {
        $this->gw = $gw;

        return $this;
    }

    /**
     * Get gw
     *
     * @return string 
     */
    public function getGw() {
        return $this->gw;
    }

    /**
     * Set nw
     *
     * @param string $nw
     * @return Cadencier
     */
    public function setNw($nw) {
        $this->nw = $nw;

        return $this;
    }

    /**
     * Get nw
     *
     * @return string 
     */
    public function getNw() {
        return $this->nw;
    }

    /**
     * Set cbm
     *
     * @param string $cbm
     * @return Cadencier
     */
    public function setCbm($cbm) {
        $this->cbm = $cbm;

        return $this;
    }

    /**
     * Get cbm
     *
     * @return string 
     */
    public function getCbm() {
        return $this->cbm;
    }

    /**
     * Set quantpieces
     *
     * @param string $quantpieces
     * @return Cadencier
     */
    public function setQuantpieces($quantpieces) {
        $this->quantpieces = $quantpieces;

        return $this;
    }

    /**
     * Get quantpieces
     *
     * @return string 
     */
    public function getQuantpieces() {
        return $this->quantpieces;
    }

    /**
     * Set quantpackages
     *
     * @param string $quantpackages
     * @return Cadencier
     */
    public function setQuantpackages($quantpackages) {
        $this->quantpackages = $quantpackages;

        return $this;
    }

    /**
     * Get quantpackages
     *
     * @return string 
     */
    public function getQuantpackages() {
        return $this->quantpackages;
    }

    /**
     * Set totalvalue
     *
     * @param string $totalvalue
     * @return Cadencier
     */
    public function setTotalvalue($totalvalue) {
        $this->totalvalue = $totalvalue;

        return $this;
    }

    /**
     * Get totalvalue
     *
     * @return string 
     */
    public function getTotalvalue() {
        return $this->totalvalue;
    }

    /**
     * Set publictoken
     *
     * @param string $publictoken
     * @return Cadencier
     */
    public function setPublictoken($publictoken) {
        $this->publictoken = $publictoken;

        return $this;
    }

    /**
     * Get publictoken
     *
     * @return string 
     */
    public function getPublictoken() {
        return $this->publictoken;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Cadencier
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Cadencier
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set closepublicdate
     *
     * @param \DateTime $closepublicdate
     * @return Cadencier
     */
    public function setClosepublicdate($closepublicdate) {
        $this->closepublicdate = $closepublicdate;

        return $this;
    }

    /**
     * Get closepublicdate
     *
     * @return \DateTime 
     */
    public function getClosepublicdate() {
        return $this->closepublicdate;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return Cadencier
     */
    public function setState($state) {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState() {
        return $this->state;
    }

    /**
     * Set stepValidation
     *
     * @param \DateTime $stepValidation
     * @return Cadencier
     */
    public function setStepValidation($stepValidation) {
        $this->stepValidation = $stepValidation;

        return $this;
    }

    /**
     * Get stepValidation
     *
     * @return \DateTime 
     */
    public function getStepValidation() {
        return $this->stepValidation;
    }

    /**
     * Set stepProduction
     *
     * @param \DateTime $stepProduction
     * @return Cadencier
     */
    public function setStepProduction($stepProduction) {
        $this->stepProduction = $stepProduction;

        return $this;
    }

    /**
     * Get stepProduction
     *
     * @return \DateTime 
     */
    public function getStepProduction() {
        return $this->stepProduction;
    }

    /**
     * Set stepQcend
     *
     * @param \DateTime $stepQcend
     * @return Cadencier
     */
    public function setStepQcend($stepQcend) {
        $this->stepQcend = $stepQcend;

        return $this;
    }

    /**
     * Get stepQcend
     *
     * @return \DateTime 
     */
    public function getStepQcend() {
        return $this->stepQcend;
    }

    /**
     * Add useractivities
     *
     * @param \sourcinasia\appBundle\Entity\Useractivity $useractivities
     * @return Cadencier
     */
    public function addUseractivity(\sourcinasia\appBundle\Entity\Useractivity $useractivities) {
        $this->useractivities[] = $useractivities;

        return $this;
    }

    /**
     * Remove useractivities
     *
     * @param \sourcinasia\appBundle\Entity\Useractivity $useractivities
     */
    public function removeUseractivity(\sourcinasia\appBundle\Entity\Useractivity $useractivities) {
        $this->useractivities->removeElement($useractivities);
    }

    /**
     * Get useractivities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUseractivities() {
        return $this->useractivities;
    }

    /**
     * Add supplychains
     *
     * @param \sourcinasia\appBundle\Entity\Supplychain $supplychains
     * @return Cadencier
     */
    public function addSupplychain(\sourcinasia\appBundle\Entity\Supplychain $supplychains) {
        $this->supplychains[] = $supplychains;

        return $this;
    }

    /**
     * Remove supplychains
     *
     * @param \sourcinasia\appBundle\Entity\Supplychain $supplychains
     */
    public function removeSupplychain(\sourcinasia\appBundle\Entity\Supplychain $supplychains) {
        $this->supplychains->removeElement($supplychains);
    }

    /**
     * Get supplychains
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplychains() {
        return $this->supplychains;
    }

    /**
     * Add histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     * @return Cadencier
     */
    public function addHistory(\sourcinasia\appBundle\Entity\History $histories) {
        $this->histories[] = $histories;

        return $this;
    }

    /**
     * Remove histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     */
    public function removeHistory(\sourcinasia\appBundle\Entity\History $histories) {
        $this->histories->removeElement($histories);
    }

    /**
     * Get histories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistories() {
        return $this->histories;
    }

    /**
     * Set command
     *
     * @param \sourcinasia\appBundle\Entity\Command $command
     * @return Cadencier
     */
    public function setCommand(\sourcinasia\appBundle\Entity\Command $command = null) {
        $this->command = $command;

        return $this;
    }

    /**
     * Get command
     *
     * @return \sourcinasia\appBundle\Entity\Command 
     */
    public function getCommand() {
        return $this->command;
    }

    /**
     * Set customer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customer
     * @return Cadencier
     */
    public function setCustomer(\sourcinasia\appBundle\Entity\Customer $customer = null) {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \sourcinasia\appBundle\Entity\Customer 
     */
    public function getCustomer() {
        return $this->customer;
    }

    /**
     * Set pol
     *
     * @param \sourcinasia\appBundle\Entity\Pol $pol
     * @return Cadencier
     */
    public function setPol(\sourcinasia\appBundle\Entity\Pol $pol = null) {
        $this->pol = $pol;

        return $this;
    }

    /**
     * Get pol
     *
     * @return \sourcinasia\appBundle\Entity\Pol 
     */
    public function getPol() {
        return $this->pol;
    }

    /**
     * Set supplier
     *
     * @param \sourcinasia\appBundle\Entity\Supplier $supplier
     * @return Cadencier
     */
    public function setSupplier(\sourcinasia\appBundle\Entity\Supplier $supplier = null) {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \sourcinasia\appBundle\Entity\Supplier 
     */
    public function getSupplier() {
        return $this->supplier;
    }

    /**
     * Set incoterm
     *
     * @param \sourcinasia\appBundle\Entity\Incoterm $incoterm
     * @return Cadencier
     */
    public function setIncoterm(\sourcinasia\appBundle\Entity\Incoterm $incoterm = null) {
        $this->incoterm = $incoterm;

        return $this;
    }

    /**
     * Get incoterm
     *
     * @return \sourcinasia\appBundle\Entity\Incoterm 
     */
    public function getIncoterm() {
        return $this->incoterm;
    }

    /**
     * Set container
     *
     * @param \sourcinasia\appBundle\Entity\Container $container
     * @return Cadencier
     */
    public function setContainer(\sourcinasia\appBundle\Entity\Container $container = null) {
        $this->container = $container;

        return $this;
    }

    /**
     * Get container
     *
     * @return \sourcinasia\appBundle\Entity\Container 
     */
    public function getContainer() {
        return $this->container;
    }

    /**
     * Set user
     *
     * @param \sourcinasia\appBundle\Entity\User $user
     * @return Cadencier
     */
    public function setUser(\sourcinasia\appBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \sourcinasia\appBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Add payments
     *
     * @param \sourcinasia\appBundle\Entity\Payment $payments
     * @return Cadencier
     */
    public function addPayment(\sourcinasia\appBundle\Entity\Payment $payments) {
        $this->payments[] = $payments;

        return $this;
    }

    /**
     * Remove payments
     *
     * @param \sourcinasia\appBundle\Entity\Payment $payments
     */
    public function removePayment(\sourcinasia\appBundle\Entity\Payment $payments) {
        $this->payments->removeElement($payments);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPayments() {
        return $this->payments;
    }

    /**
     * Add customers
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customers
     * @return Cadencier
     */
    public function addCustomer(\sourcinasia\appBundle\Entity\Customer $customers) {
        $this->customers[] = $customers;

        return $this;
    }

    /**
     * Remove customers
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customers
     */
    public function removeCustomer(\sourcinasia\appBundle\Entity\Customer $customers) {
        $this->customers->removeElement($customers);
    }

    /**
     * Get customers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomers() {
        return $this->customers;
    }

    /**
     * Set offerslist
     *
     * @param string $offerslist
     * @return Cadencier
     */
    public function setOfferslist($offerslist) {
        $this->offerslist = implode(',', $offerslist);

        return $this;
    }

    /**
     * Get offerslist
     *
     * @return string 
     */
    public function getOfferslist() {
        return explode(',', $this->offerslist);
    }

    /**
     * Set selection
     *
     * @param string $selection
     * @return Cadencier
     */
    public function setSelection($selection) {
        $this->selection = json_encode($selection);

        return $this;
    }

    /**
     * Get selection
     *
     * @return string 
     */
    public function getSelection() {
        return json_decode($this->selection, true);
    }

    /**
     * Set totalvaluepi
     *
     * @param float $totalvaluepi
     * @return Cadencier
     */
    public function setTotalvaluepi($totalvaluepi) {
        $this->totalvaluepi = $totalvaluepi;

        return $this;
    }

    /**
     * Get totalvaluepi
     *
     * @return float 
     */
    public function getTotalvaluepi() {
        return $this->totalvaluepi;
    }

    /**
     * Set servicedescription
     *
     * @param string $servicedescription 
     * @return Cadencier
     */
    public function setServicedescription($servicedescription) {
        $this->servicedescription = json_encode($servicedescription);

        return $this;
    }

    /**
     * Get servicedescription
     *
     * @return string 
     */
    public function getServicedescription() {

        return json_decode($this->servicedescription, true);
    }

    /**
     * Set servicevalue
     *
     * @param float $servicevalue
     * @return Cadencier
     */
    public function setServicevalue($servicevalue) {
        $this->servicevalue = $servicevalue;

        return $this;
    }

    /**
     * Get servicevalue
     *
     * @return float 
     */
    public function getServicevalue() {
        return $this->servicevalue;
    }

    /**
     * Set finalincoterm
     *
     * @param string $finalincoterm
     * @return Cadencier
     */
    public function setFinalincoterm($finalincoterm) {
        $this->finalincoterm = $finalincoterm;

        return $this;
    }

    /**
     * Get finalincoterm
     *
     * @return string 
     */
    public function getFinalincoterm() {
        return $this->finalincoterm;
    }

    /**
     * Set totalmarge
     *
     * @param float $totalmarge
     * @return Cadencier
     */
    public function setTotalmarge($totalmarge) {
        $this->totalmarge = $totalmarge;

        return $this;
    }

    /**
     * Get totalmarge
     *
     * @return float 
     */
    public function getTotalmarge() {
        return $this->totalmarge;
    }

    /**
     * Set finalcustomer
     *
     * @param string $finalcustomer
     * @return Cadencier
     */
    public function setFinalcustomer($finalcustomer) {
        $this->finalcustomer = $finalcustomer;

        return $this;
    }

    /**
     * Get finalcustomer
     *
     * @return string 
     */
    public function getFinalcustomer() {
        return $this->finalcustomer;
    }

    /**
     * Add historiescustomers
     *
     * @param \sourcinasia\appBundle\Entity\Historycustomer $historiescustomers
     * @return Cadencier
     */
    public function addHistoriescustomer(\sourcinasia\appBundle\Entity\Historycustomer $historiescustomers) {
        $this->historiescustomers[] = $historiescustomers;

        return $this;
    }

    /**
     * Remove historiescustomers
     *
     * @param \sourcinasia\appBundle\Entity\Historycustomer $historiescustomers
     */
    public function removeHistoriescustomer(\sourcinasia\appBundle\Entity\Historycustomer $historiescustomers) {
        $this->historiescustomers->removeElement($historiescustomers);
    }

    /**
     * Get historiescustomers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoriescustomers() {
        return $this->historiescustomers;
    }

    /**
     * @var string
     */
    private $finalpol;

    /**
     * @var string
     */
    private $finaltva;

    /**
     * @var string
     */
    private $finaldevis;

    /**
     * Set finalpol
     *
     * @param string $finalpol
     * @return Cadencier
     */
    public function setFinalpol($finalpol) {
        $this->finalpol = $finalpol;

        return $this;
    }

    /**
     * Get finalpol
     *
     * @return string 
     */
    public function getFinalpol() {
        return $this->finalpol;
    }

    /**
     * Set finaltva
     *
     * @param string $finaltva
     * @return Cadencier
     */
    public function setFinaltva($finaltva) {
        $this->finaltva = $finaltva;

        return $this;
    }

    /**
     * Get finaltva
     *
     * @return string 
     */
    public function getFinaltva() {
        return $this->finaltva;
    }

    /**
     * Set finaldevis
     *
     * @param string $finaldevis
     * @return Cadencier
     */
    public function setFinaldevis($finaldevis) {
        $this->finaldevis = $finaldevis;

        return $this;
    }

    /**
     * Get finaldevis
     *
     * @return string 
     */
    public function getFinaldevis() {
        return $this->finaldevis;
    }

    /**
     * @var float
     */
    private $margecoef;

    /**
     * Set margecoef
     *
     * @param float $margecoef
     * @return Cadencier
     */
    public function setMargecoef($margecoef) {
        $this->margecoef = $margecoef;

        return $this;
    }

    /**
     * Get margecoef
     *
     * @return float 
     */
    public function getMargecoef() {
        return $this->margecoef;
    }

    /**
     * Set deleverytime
     *
     * @param integer $deleverytime
     * @return Cadencier
     */
    public function setDeleverytime($deleverytime) {
        $this->deleverytime = $deleverytime;

        return $this;
    }

    /**
     * Get deleverytime
     *
     * @return integer 
     */
    public function getDeleverytime() {
        return $this->deleverytime;
    }

    /**
     * Set supplierci
     *
     * @param string $supplierci
     * @return Cadencier
     */
    public function setSupplierci($supplierci) {
        $this->supplierci = $supplierci;

        return $this;
    }

    /**
     * Get supplierci
     *
     * @return string 
     */
    public function getSupplierci() {
        return $this->supplierci;
    }

    /**
     * Set customerforwarder
     *
     * @param string $customerforwarder
     * @return Cadencier
     */
    public function setCustomerforwarder($customerforwarder) {
        $this->customerforwarder = $customerforwarder;

        return $this;
    }

    /**
     * Get customerforwarder
     *
     * @return string 
     */
    public function getCustomerforwarder() {
        return $this->customerforwarder;
    }

    /**
     * @var float
     */
    private $suppliervalue;

    /**
     * Set suppliervalue
     *
     * @param float $suppliervalue
     * @return Cadencier
     */
    public function setSuppliervalue($suppliervalue) {
        $this->suppliervalue = $suppliervalue;

        return $this;
    }

    /**
     * Get suppliervalue
     *
     * @return float 
     */
    public function getSuppliervalue() {
        return $this->suppliervalue;
    }

    /**
     * Set supplierpaymenterms
     *
     * @param \sourcinasia\appBundle\Entity\Paymentcondition $supplierpaymenterms
     * @return Cadencier
     */
    public function setSupplierpaymenterms(\sourcinasia\appBundle\Entity\Paymentcondition $supplierpaymenterms = null) {
        $this->supplierpaymenterms = $supplierpaymenterms;

        return $this;
    }

    /**
     * Get supplierpaymenterms
     *
     * @return \sourcinasia\appBundle\Entity\Paymentcondition 
     */
    public function getSupplierpaymenterms() {
        return $this->supplierpaymenterms;
    }

    /**
     * Set customerpaymenterms
     *
     * @param \sourcinasia\appBundle\Entity\Paymentcondition $customerpaymenterms
     * @return Cadencier
     */
    public function setCustomerpaymenterms(\sourcinasia\appBundle\Entity\Paymentcondition $customerpaymenterms = null) {
        $this->customerpaymenterms = $customerpaymenterms;

        return $this;
    }

    /**
     * Get customerpaymenterms
     *
     * @return \sourcinasia\appBundle\Entity\Paymentcondition 
     */
    public function getCustomerpaymenterms() {
        return $this->customerpaymenterms;
    }

    /**
     * @var string
     */
    private $supplierpi;


    /**
     * Set supplierpi
     *
     * @param string $supplierpi
     * @return Cadencier
     */
    public function setSupplierpi($supplierpi)
    {
        $this->supplierpi = $supplierpi;

        return $this;
    }

    /**
     * Get supplierpi
     *
     * @return string 
     */
    public function getSupplierpi()
    {
        return $this->supplierpi;
    }
    /**
     * @var integer
     */
    private $nbr;


    /**
     * Set nbr
     *
     * @param integer $nbr
     * @return Cadencier
     */
    public function setNbr($nbr)
    {
        $this->nbr = $nbr;

        return $this;
    }

    /**
     * Get nbr
     *
     * @return integer 
     */
    public function getNbr()
    {
        return $this->nbr;
    }
}
