<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supplier
 */
class Supplier {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $website;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var integer
     */
    private $mark;

    /**
     * @var string
     */
    private $note;

    /**
     * @var integer
     */
    private $deleverytime;

    /**
     * @var integer
     */
    private $validityoffer;

    /**
     * @var string
     */
    private $offername;

    /**
     * @var string
     */
    private $infobank;

    /**
     * @var boolean
     */
    private $exportlicence;

    /**
     * @var string
     */
    private $businesslicence;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $offers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $histories;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $addresses;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $contacts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $suppliercomments;

    /**
     * @var \sourcinasia\appBundle\Entity\Paymentcondition
     */
    private $paymenterms;

    /**
     * @var \sourcinasia\appBundle\Entity\Pol
     */
    private $pol;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $customers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $containers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $incoterms;

    /**
     * Constructor
     */
    public function __construct() {
        $this->offers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->histories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->addresses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contacts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->suppliercomments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->customers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->containers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->incoterms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /*
      MOI
     */

    public function getIdname() {
        return $this->id . '-' . $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Supplier
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Supplier
     */
    public function setWebsite($website) {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite() {
        return $this->website;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Supplier
     */
    public function setComments($comments) {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * Set mark
     *
     * @param integer $mark
     * @return Supplier
     */
    public function setMark($mark) {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return integer 
     */
    public function getMark() {
        return $this->mark;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Supplier
     */
    public function setNote($note) {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote() {
        return $this->note;
    }

    /**
     * Set deleverytime
     *
     * @param integer $deleverytime
     * @return Supplier
     */
    public function setDeleverytime($deleverytime) {
        $this->deleverytime = $deleverytime;

        return $this;
    }

    /**
     * Get deleverytime
     *
     * @return integer 
     */
    public function getDeleverytime() {
        return $this->deleverytime;
    }

    /**
     * Set offername
     *
     * @param string $offername
     * @return Supplier
     */
    public function setOffername($offername) {
        $this->offername = $offername;

        return $this;
    }

    /**
     * Get offername
     *
     * @return string 
     */
    public function getOffername() {
        return $this->offername;
    }

    /**
     * Set infobank
     *
     * @param string $infobank
     * @return Supplier
     */
    public function setInfobank($infobank) {
        $this->infobank = $infobank;

        return $this;
    }

    /**
     * Get infobank
     *
     * @return string 
     */
    public function getInfobank() {
        return $this->infobank;
    }

    /**
     * Set exportlicence
     *
     * @param boolean $exportlicence
     * @return Supplier
     */
    public function setExportlicence($exportlicence) {
        $this->exportlicence = $exportlicence;

        return $this;
    }

    /**
     * Get exportlicence
     *
     * @return boolean 
     */
    public function getExportlicence() {
        return $this->exportlicence;
    }

    /**
     * Set businesslicence
     *
     * @param string $businesslicence
     * @return Supplier
     */
    public function setBusinesslicence($businesslicence) {
        $this->businesslicence = $businesslicence;

        return $this;
    }

    /**
     * Get businesslicence
     *
     * @return string 
     */
    public function getBusinesslicence() {
        return $this->businesslicence;
    }

    /**
     * Add offers
     *
     * @param \sourcinasia\appBundle\Entity\Offer $offers
     * @return Supplier
     */
    public function addOffer(\sourcinasia\appBundle\Entity\Offer $offers) {
        $this->offers[] = $offers;

        return $this;
    }

    /**
     * Remove offers
     *
     * @param \sourcinasia\appBundle\Entity\Offer $offers
     */
    public function removeOffer(\sourcinasia\appBundle\Entity\Offer $offers) {
        $this->offers->removeElement($offers);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOffers() {
        return $this->offers;
    }

    /**
     * Add histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     * @return Supplier
     */
    public function addHistorie(\sourcinasia\appBundle\Entity\History $histories) {
        $this->histories[] = $histories;

        return $this;
    }

    /**
     * Remove histories
     *
     * @param \sourcinasia\appBundle\Entity\History $histories
     */
    public function removeHistorie(\sourcinasia\appBundle\Entity\History $histories) {
        $this->histories->removeElement($histories);
    }

    /**
     * Get histories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistories() {
        return $this->histories;
    }

    /**
     * Add addresses
     *
     * @param \sourcinasia\appBundle\Entity\Address $addresses
     * @return Supplier
     */
    public function addAddresse(\sourcinasia\appBundle\Entity\Address $addresses) {
        $this->addresses[] = $addresses;

        return $this;
    }

    /**
     * Remove addresses
     *
     * @param \sourcinasia\appBundle\Entity\Address $addresses
     */
    public function removeAddresse(\sourcinasia\appBundle\Entity\Address $addresses) {
        $this->addresses->removeElement($addresses);
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAddresses() {
        return $this->addresses;
    }

    /**
     * Add contacts
     *
     * @param \sourcinasia\appBundle\Entity\Contact $contacts
     * @return Supplier
     */
    public function addContact(\sourcinasia\appBundle\Entity\Contact $contacts) {
        $this->contacts[] = $contacts;

        return $this;
    }

    /**
     * Remove contacts
     *
     * @param \sourcinasia\appBundle\Entity\Contact $contacts
     */
    public function removeContact(\sourcinasia\appBundle\Entity\Contact $contacts) {
        $this->contacts->removeElement($contacts);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContacts() {
        return $this->contacts;
    }

    /**
     * Add suppliercomments
     *
     * @param \sourcinasia\appBundle\Entity\Suppliercomment $suppliercomments
     * @return Supplier
     */
    public function addSuppliercomment(\sourcinasia\appBundle\Entity\Suppliercomment $suppliercomments) {
        $this->suppliercomments[] = $suppliercomments;

        return $this;
    }

    /**
     * Remove suppliercomments
     *
     * @param \sourcinasia\appBundle\Entity\Suppliercomment $suppliercomments
     */
    public function removeSuppliercomment(\sourcinasia\appBundle\Entity\Suppliercomment $suppliercomments) {
        $this->suppliercomments->removeElement($suppliercomments);
    }

    /**
     * Get suppliercomments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSuppliercomments() {
        return $this->suppliercomments;
    }

    /**
     * Set paymenterms
     *
     * @param \sourcinasia\appBundle\Entity\Paymentcondition $paymenterms
     * @return Supplier
     */
    public function setPaymenterms(\sourcinasia\appBundle\Entity\Paymentcondition $paymenterms = null) {
        $this->paymenterms = $paymenterms;

        return $this;
    }

    /**
     * Get paymenterms
     *
     * @return \sourcinasia\appBundle\Entity\Paymentcondition 
     */
    public function getPaymenterms() {
        return $this->paymenterms;
    }

    /**
     * Set pol
     *
     * @param \sourcinasia\appBundle\Entity\Pol $pol
     * @return Supplier
     */
    public function setPol(\sourcinasia\appBundle\Entity\Pol $pol = null) {
        $this->pol = $pol;

        return $this;
    }

    /**
     * Get pol
     *
     * @return \sourcinasia\appBundle\Entity\Pol 
     */
    public function getPol() {
        return $this->pol;
    }

    /**
     * Add customers
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customers
     * @return Supplier
     */
    public function addCustomer(\sourcinasia\appBundle\Entity\Customer $customers) {
        $this->customers[] = $customers;

        return $this;
    }

    /**
     * Remove customers
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customers
     */
    public function removeCustomer(\sourcinasia\appBundle\Entity\Customer $customers) {
        $this->customers->removeElement($customers);
    }

    /**
     * Get customers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomers() {
        return $this->customers;
    }

    /**
     * Add containers
     *
     * @param \sourcinasia\appBundle\Entity\Container $containers
     * @return Supplier
     */
    public function addContainer(\sourcinasia\appBundle\Entity\Container $containers) {
        $this->containers[] = $containers;

        return $this;
    }

    /**
     * Remove containers
     *
     * @param \sourcinasia\appBundle\Entity\Container $containers
     */
    public function removeContainer(\sourcinasia\appBundle\Entity\Container $containers) {
        $this->containers->removeElement($containers);
    }

    /**
     * Get containers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContainers() {
        return $this->containers;
    }

    /**
     * Add incoterms
     *
     * @param \sourcinasia\appBundle\Entity\Incoterm $incoterms
     * @return Supplier
     */
    public function addIncoterm(\sourcinasia\appBundle\Entity\Incoterm $incoterms) {
        $this->incoterms[] = $incoterms;

        return $this;
    }

    /**
     * Remove incoterms
     *
     * @param \sourcinasia\appBundle\Entity\Incoterm $incoterms
     */
    public function removeIncoterm(\sourcinasia\appBundle\Entity\Incoterm $incoterms) {
        $this->incoterms->removeElement($incoterms);
    }

    /**
     * Get incoterms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIncoterms() {
        return $this->incoterms;
    }

    /**
     * @var string
     */
    private $offernamefr;

    /**
     * Set offernamefr
     *
     * @param string $offernamefr
     * @return Supplier
     */
    public function setOffernamefr($offernamefr) {
        $this->offernamefr = $offernamefr;

        return $this;
    }

    /**
     * Get offernamefr
     *
     * @return string 
     */
    public function getOffernamefr() {
        return $this->offernamefr;
    }

    /**
     * Set validityoffer
     *
     * @param integer $validityoffer
     * @return Supplier
     */
    public function setValidityoffer($validityoffer) {
        $this->validityoffer = $validityoffer;

        return $this;
    }

    /**
     * Get validityoffer
     *
     * @return integer 
     */
    public function getValidityoffer() {
        return $this->validityoffer;
    }

}
