<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payment
 */
class Payment
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $mode;

    /**
     * @var \sourcinasia\appBundle\Entity\Customer
     */
    private $cutomer;

    /**
     * @var \sourcinasia\appBundle\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cadenciers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cadenciers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Payment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set mode
     *
     * @param string $mode
     * @return Payment
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return string 
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set cutomer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $cutomer
     * @return Payment
     */
    public function setCutomer(\sourcinasia\appBundle\Entity\Customer $cutomer = null)
    {
        $this->cutomer = $cutomer;

        return $this;
    }

    /**
     * Get cutomer
     *
     * @return \sourcinasia\appBundle\Entity\Customer 
     */
    public function getCutomer()
    {
        return $this->cutomer;
    }

    /**
     * Set user
     *
     * @param \sourcinasia\appBundle\Entity\User $user
     * @return Payment
     */
    public function setUser(\sourcinasia\appBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \sourcinasia\appBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add cadenciers
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadenciers
     * @return Payment
     */
    public function addCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadenciers)
    {
        $this->cadenciers[] = $cadenciers;

        return $this;
    }

    /**
     * Remove cadenciers
     *
     * @param \sourcinasia\appBundle\Entity\Cadencier $cadenciers
     */
    public function removeCadencier(\sourcinasia\appBundle\Entity\Cadencier $cadenciers)
    {
        $this->cadenciers->removeElement($cadenciers);
    }

    /**
     * Get cadenciers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCadenciers()
    {
        return $this->cadenciers;
    }
}
