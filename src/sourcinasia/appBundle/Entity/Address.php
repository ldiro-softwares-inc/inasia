<?php

namespace sourcinasia\appBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Address
 */
class Address {

    /**
     * @var integer
     */
    private $id;

  
    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $type;

    /**
     * @var \sourcinasia\appBundle\Entity\City
     */
    private $city;

    /**
     * @var \sourcinasia\appBundle\Entity\Supplier
     */
    private $supplier;

    /**
     * @var \sourcinasia\appBundle\Entity\Customer
     */
    private $customer;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Address
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set city
     *
     * @param \sourcinasia\appBundle\Entity\City $city
     * @return Address
     */
    public function setCity(\sourcinasia\appBundle\Entity\City $city = null)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return \sourcinasia\appBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set supplier
     *
     * @param \sourcinasia\appBundle\Entity\Supplier $supplier
     * @return Address
     */
    public function setSupplier(\sourcinasia\appBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;
    
        return $this;
    }

    /**
     * Get supplier
     *
     * @return \sourcinasia\appBundle\Entity\Supplier 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set customer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customer
     * @return Address
     */
    public function setCustomer(\sourcinasia\appBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;
    
        return $this;
    }

    /**
     * Get customer
     *
     * @return \sourcinasia\appBundle\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
