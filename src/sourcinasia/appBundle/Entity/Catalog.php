<?php

namespace sourcinasia\appBundle\Entity;

/**
 * Catalog
 */
class Catalog
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $titleEn;

    /**
     * @var string
     */
    private $titleFr;

    /**
     * @var string
     */
    private $rights;

    /**
     * @var string
     */
    private $catalogType;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $products;

    /**
     * @var boolean
     */
    private $enabled;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
        $this->catalogType = 'member';
        $this->enabled = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titleEn
     *
     * @param string $titleEn
     *
     * @return Catalog
     */
    public function setTitleEn($titleEn)
    {
        $this->titleEn = $titleEn;

        return $this;
    }

    /**
     * Get titleEn
     *
     * @return string
     */
    public function getTitleEn()
    {
        return $this->titleEn;
    }

    /**
     * Set titleFr
     *
     * @param string $titleFr
     *
     * @return Catalog
     */
    public function setTitleFr($titleFr)
    {
        $this->titleFr = $titleFr;

        return $this;
    }

    /**
     * Get titleFr
     *
     * @return string
     */
    public function getTitleFr()
    {
        return $this->titleFr;
    }

    /**
     * Add product
     *
     * @param \sourcinasia\appBundle\Entity\Product $product
     *
     * @return Catalog
     */
    public function addProduct(\sourcinasia\appBundle\Entity\Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \sourcinasia\appBundle\Entity\Product $product
     */
    public function removeProduct(\sourcinasia\appBundle\Entity\Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $customers;


    /**
     * Add customer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customer
     *
     * @return Catalog
     */
    public function addCustomer(\sourcinasia\appBundle\Entity\Customer $customer)
    {
        $this->customers[] = $customer;

        return $this;
    }

    /**
     * Remove customer
     *
     * @param \sourcinasia\appBundle\Entity\Customer $customer
     */
    public function removeCustomer(\sourcinasia\appBundle\Entity\Customer $customer)
    {
        $this->customers->removeElement($customer);
    }

    /**
     * Get customers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * @return string
     */
    public function getRights()
    {
        return $this->rights;
    }

    /**
     * @param string $rights
     *
     * @return Catalog
     */
    public function setRights($rights)
    {
        $this->rights = $rights;

        return $this;
    }

    /**
     * @return string
     */
    public function getCatalogType()
    {
        return $this->catalogType;
    }

    /**
     * @param string $catalogType
     *
     * @return Catalog
     */
    public function setCatalogType($catalogType)
    {
        $this->catalogType = $catalogType;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return $this
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }
}
