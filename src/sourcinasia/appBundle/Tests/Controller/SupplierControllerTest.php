<?php

namespace sourcinasia\appBundle\Tests\Controller;

use sourcinasia\appBundle\Entity\Supplier;
use sourcinasia\appBundle\Tests\ExtendControllerTest;

class SupplierControllerTest extends ExtendControllerTest
{
    function __construct()
    {
        parent::__construct();
        $this->doLogin('olivier', '123456');

        // Clear data to correctly test
        if ($Supplier = $this->em->getRepository('appBundle:Supplier')->findOneBy(['name' => 'test'])) {
            $this->em->remove($Supplier);
            $this->em->flush();
        }

        if ($Supplier = $this->em->getRepository('appBundle:Supplier')->findOneBy(['name' => 'TESTIMPORT'])) {
            $this->em->remove($Supplier);
            $this->em->flush();
        }
    }

    function testExportCsv()
    {
        $this->client->request('GET', $this->route->generate('supplier_export_csv_all'));
        $this->responseMustBe(200);
    }

    function testImportCsv()
    {
        // Display upload form
        $crawler = $this->client->request('GET', $this->route->generate('supplier_import_csv_suppliers'));
        $this->responseMustBe(200);

        // Upload csv file
        $form = $crawler->selectButton('submit-btn')->form();
        $form['form[attachment]']->upload(__SOURCINASIA__ . 'src/sourcinasia/appBundle/Tests/Files/suppliers_export.csv');
        $this->client->submit($form);
        $this->responseMustBe(200);

        // Confirm csv import

        $Supplier = new Supplier();
        $Supplier->setName('test');
        $Supplier->setMark('test');
        $Supplier->setDeleverytime(7);
        $Supplier->setValidityoffer(7);
        $Supplier->setOffername('test');
        $Supplier->setOffernamefr('test');
        $Supplier->setExportlicence('test');
        $this->em->persist($Supplier);
        $this->em->flush();

        $this->client->request('POST', $this->route->generate('supplier_import_csv_suppliers'), ['datas' => '[{"id":' . $Supplier->getId() . ',"name":"TESTIMPORT","website":"www.shuangchao.cn","comments":"","mark":"0","note":"","deleverytime":"45","offername":"Fitness Equipment","infobank":"","productshidden":"","exportlicence":"","businesslicence":"","offernamefr":"\u00c9quipement De Fitness","validityoffer":"30","shopnumber":"123ff","contact_name":"nametest","contact_office_phone":"phonetest","contact_mobile":"test","contact_email":"emailtest","contact_qq":"test","supplier_category":"","cif":"X","ddp":"X","exw":"X","fob":"X","20gp":"X","40gp":"X","40hq":"X","lcl":"X"}]']);
        $this->responseMustBe(302);

        $this->em->refresh($Supplier);
        $this->assertTrue($Supplier->getName() == "TESTIMPORT");

        // TODO
        $this->assertTrue($Supplier->getIncoterms()->count() == 4, $Supplier->getIncoterms()->count());
        $this->assertTrue($Supplier->getContainers()->count() == 4, $Supplier->getContainers()->count());

        $Contact = $Supplier->getContacts()->first();
        $this->assertTrue($Contact->getName() == "nametest");
        $this->assertTrue($Contact->getPhone() == "phonetest");
        $this->assertTrue($Contact->getEmail() == "emailtest");

        $this->em->remove($Supplier);
        $this->em->flush();
    }

}
