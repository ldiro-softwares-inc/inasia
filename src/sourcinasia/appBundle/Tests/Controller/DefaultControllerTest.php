<?php

namespace sourcinasia\appBundle\Tests\Controller;

use sourcinasia\appBundle\Entity\Contact;
use sourcinasia\appBundle\Entity\Customer;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DefaultControllerTest extends WebTestCase
{
    private $client = null;
    private $em = null;
    private $route = null;
    private $idOffer = null;

    function __construct()
    {
        parent::__construct();

        $this->client = static::createClient(array(
            'environment' => 'test',
            'debug' => true,
        ));

        $this->route = $this->client->getContainer()->get('router');

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        // Clear data to correctly test
        if (!$Customer = $this->em->getRepository('appBundle:Customer')->findOneBy(['name' => 'test'])) {
            $Customer = new Customer();
            $Customer->setName('test');
            $Customer->setSiret('siretTest');
            $Customer->setMargecoef(1);
            $Customer->setAddress('adresse test');
            $Customer->setCity('city test');
            $Customer->setNomenclatures('1001001026,1001002001,1001002002,1001002003,1001002004,1001002005,1001002006,1102001001,1102001002,1102001003,1102001004,1102001005,1102001006,1102001007,1102001008,1102001009,1102001010,1102001011,1102001012,1102001013,1102002001,1102002002,1102002003,1102002004,1102002005,1102002006,1102002007,1102002008,1102002009,1102007001,1102007002,1102007003,1102007004,1102007005,1102008001,1102008002,1102009001,1102009002,1304001001,1304001002,1304001003,1304001004,9001001001');
            $Customer->setPc('123');
            $Customer->setDeleverytime(2);
            $Customer->setMainsaler($this->em->getRepository('appBundle:User')->findOneBy(['username' => 'olivier']));
            $Customer->addZone($this->em->getRepository('appBundle:Zone')->findOneBy(['title' => 'FRANCE']));
            $Customer->setActivity($this->em->getRepository('appBundle:Activity')->find(3));
            // $this->em->persist($Customer);
            // $this->em->flush();
        };

        if ($Customer->getContacts()->count() == 0) {
            $Contact = new Contact();
            $Contact->setName('contact test');
            $Contact->setEmail('emailtest@test.fr');
            $Contact->setCustomer($Customer);
            $Contact->setPhone('123');
            $this->em->persist($Contact);
            $this->em->flush();
        } else {
            $Contact = $Customer->getContacts()->first();
        }

        if (!$Contact->getUser()) {
            $userManager = static::$kernel->getContainer()->get('fos_user.user_manager');
            $user = $userManager->createUser();
            $user->setUsername($Contact->getEmail());
            $user->setEmail($Contact->getEmail());
            $user->setPlainPassword('123456');
            $user->setContact($Contact);
            $user->setRoles(array('ROLE_CLIENT', 'ROLE_USER'));
            $user->setEnabled(true);
            $user->setExpiresAt(null);
            $user->setModeonlogin('buy');
            $user->setName($Contact->getName());
            $user->setCustomermargecoef('5');
            $user->setLocale('en');
            $userManager->updateUser($user, true);
        }

        if (!$this->em->getRepository('appBundle:User')->findOneBy(['username' => 'sourcing1@sourcinasia.com'])) {
            $userManager = static::$kernel->getContainer()->get('fos_user.user_manager');
            $user = $userManager->createUser();
            $user->setUsername('sourcing1@sourcinasia.com');
            $user->setEmail('sourcing1@sourcinasia.com');
            $user->setPlainPassword('123456');
            $user->setRoles(array('ROLE_SOURCING', 'ROLE_USER','ROLE_ALLOWIP','ROLE_PRODUCTION'));
            $user->setEnabled(true);
            $user->setExpiresAt(null);
            $user->setCustomermargecoef('5');
            $user->setName('sourcing1@sourcinasia.com');
            $user->setLocale('en');
            $userManager->updateUser($user, true);
        }

    }

    public function testEtapesDeCommandeIndex()
    {
        $this->doLogin('emailtest@test.fr', '123456');

        if (true) {
            for ($i = 0; $i <= 10; $i++) {
                $state = "state" . $i;
                $this->$state();
            }
        }
    }

    //Catalogue - Etape 0 - Sélection en attente de validation par
    private function state0()
    {
        // login client
        $this->doLogin('emailtest@test.fr', '123456');

        // Message client
        $this->TestRoutes(array(
            '/en/chats/new',
        ));
        $crawler = $this->client->request('GET', '/en/chats/new');
        $this->client->submit($crawler->selectButton('chat[submit]')->form(array(
            'chat[title]' => 'test',
            'chat[message]' => 'test',
        )));
        $this->assertTrue($this->client->getResponse()->isRedirect());
        $crawler = $this->client->followRedirect();


        $this->TestRoutes(array(
            '/en/offers/catalog/',
        ));

        /* $crawler = $this->client->request('POST', '/en/offers/savecatalog/',
             array('selectionids' => array('$this->idOffer', '18860')
             )
         );*/

        //tests des exports catalogues
        $this->TestRoutes(array(
            //  '/en/export/' . $this->idCadencier . '/en/export/exportcatalogpdf/,
            // '/en/export/' . $this->idCadencier . '/en/export/exportcatalogxls/'
        ));

    }


    //Validation du cadencier - Etape 1.0 - Demande de proforma client recue
    private function state1()
    {
        $this->idCadencier = false;
        if ($this->idCadencier) {
            $crawler = $this->client->request('GET', '/en/cadenciers/' . $this->idCadencier . '/do');
        } else {

            $crawler = $this->client->request('GET', $this->route->generate('offers_request', ['_locale' => 'en','request[offerIdPushOnTop]'=>0]));

            $ids=$crawler->filter('input[type=checkbox]')->each(function (Crawler $node, $i) {
                return $node->attr('value');
            });

            $this->idOffer= $ids[0];

            shuffle($ids);

            $this->client->request('POST', '/en/cadenciers/display/generate/',
                array('selection' =>
                    array(
                        $this->idOffer => array('o' => $this->idOffer)
                    )
                )
            );
            $this->assertTrue($this->client->getResponse()->isRedirect());
            $crawler = $this->client->followRedirect();
            $this->idCadencier = $crawler->filter('input[name=cadencierid]')->eq(0)->attr('value');
        }

        //tests des exports cadenciers
        $this->TestRoutes(array(
            //  '/en/export/' . $this->idCadencier . '/qs/quotationsheet',
            // '/en/export/' . $this->idCadencier . '/qs/0/0/quotationsheetpdf'
        ));
        //selection articles
        $crawler = $this->client->request('POST', '/en/cadenciers/json/generate/',
            array(
                'cadencierid' => $this->idCadencier,
                'comment' => "aaa",
                'form' => array(
                    'customerpaymenterms' => 1,
                    'containers' => 1,
                    'incoterms' => 2,
                ),
                'selection' =>
                    array(
                        $this->idOffer => array('o' => $this->idOffer, 'q' => 60)
                    )
            )
        );

        //todo erreur 18860 // selection // offerarray différent // bug a corriger

        //confirmation
        $crawler = $this->client->request('POST', '/en/cadenciers/pi/generate/',
            array(
                'cadencierid' => $this->idCadencier,
                'comment' => "bbb",
                'form' => array(
                    'customerpaymenterms' => 1,
                    'containers' => 1,
                    'incoterms' => 2,
                ),
                'selection' =>
                    array(
                        $this->idOffer => array('o' => $this->idOffer, 'q' => 1),
                    )
            )
        );
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->verifierEtatdeCommande(1);
    }

    //Validation du cadencier - Etape 1.1 - Demande de proforma client confirmée
    private function state2()
    {

        //confirmation du commercial
        $this->doLogin('olivier', '123456');
        $this->TestRoutes(array(
            '/fr/offers/catalog/',
            '/fr/cadenciers/',
            '/fr/customers/',
            '/fr/customers/leads/',
            '/fr/orders/',
            '/fr/orders/' . $this->idCadencier . '/show',
            '/fr/cadenciers/' . $this->idCadencier . '/do',
        ));

        $this->TestRoutes(array(
            '/fr/orders/' . $this->idCadencier . '/step01'
        ), 302);

        $this->verifierEtatdeCommande(2);
    }


    //Validation du cadencier - Etape 1.2 - Enregistrer la PI fournisseur
    private function state3()
    {
        // login sourcing
        $this->doLogin('sourcing1@sourcinasia.com', '123456');
        $this->TestRoutes(array(
            '/fr/offers/catalog/',
            '/en/products/',
            '/en/suppliers/',
            '/fr/customers/leads/',
            '/fr/orders/',
            '/fr/orders/' . $this->idCadencier . '/show',
        )); //todo pourquoi c'est l'admin supplier ?


        $this->TestRoutes(array(
            '/en/export/' . $this->idCadencier . '/pifrs/quotationsheet'
        ), 500); //todo export

        $this->verifierEtatdeCommande(3);
    }


    //Validation du cadencier - Etape 1.3 - Validation des prix fournisseur
    private function state4()
    {
        $this->TestRoutes(array(
            '/en/cadenciers/' . $this->idCadencier . '/do/production',
        ));

        $crawler = $this->client->request('POST', '/en/offers/negfrprice/',
            array(
                'idoffer' => $this->idOffer,
                'idcustomer' => 7,
                'incoterm' => "fob",
                'value' => 20,
            )
        );

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->TestRoutes(array(
            '/fr/orders/' . $this->idCadencier . '/step12'
        ), 302);

        $this->verifierEtatdeCommande(4);
    }


    //Validation du cadencier - Etape 1.4 - Editer la proforma client (commercial)
    private function state5()
    {
        // login client
        $this->doLogin('olivier', '123456');
        $this->TestRoutes(array(
            '/fr/orders/' . $this->idCadencier . '/show',
            '/fr/cadenciers/' . $this->idCadencier . '/do/admin',
        ));

        $crawler = $this->client->request('POST', '/fr/offers/confirm/',
            array(
                'idoffer' => $this->idOffer,
                'etat' => 'clear',
                'incoterm' => "fob",
                'cadencier' => $this->idCadencier,
                'prixnegociation' => 20,
            )
        );
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->request('POST', '/fr/cadenciers/json/generate/',
            array(
                'cadencierid' => $this->idCadencier,
                'deleverytime' => 80,
                'supplierpi' => 'sdqsd1',
                'comment' => "bbb",
                'form' => array(
                    'customerpaymenterms' => 1,
                    'containers' => 1,
                    'incoterms' => 2,
                ),
                'selection' =>
                    array(
                        $this->idOffer => array('o' => $this->idOffer, 'q' => 1, 'supplierp' => 12, 'confirm' => 1, 'confirmfieldmessage' => 'ok')
                    )
            )
        );
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->TestRoutes(array(
            '/fr/orders/' . $this->idCadencier . '/step13'
        ), 302);

        $this->verifierEtatdeCommande(5);
    }


    //Validation du cadencier - Etape 1.5 - Confirmer proforma client (Admin)
    private function state6()
    {

    }


    //Validation du cadencier - Etape 2 - Proforma client en attente de signature
    private function state7()
    {
        $this->TestRoutes(array(
            '/fr/cadenciers/' . $this->idCadencier . '/do/genpi',
        ));

        $this->TestRoutes(array(
            //  '/fr/export/' . $this->idCadencier . '/picustomer/quotationsheet',
            // '/fr/export/' . $this->idCadencier . '/pi/0/0/quotationsheetpdf'
        ));

        $this->TestRoutes(array(
            '/fr/orders/' . $this->idCadencier . '/step14'
        ), 302);

        $this->verifierEtatdeCommande(7);
    }

    //Validation du cadencier - Etape 3 - Commande client confirmée - En cours de production
    private function state8()
    {
        // todo Erreur ajouter les sécurités de la pi
        $this->uploadDoc(37);
        $this->verifierEtatdeCommande(8);
    }

    //Validation du cadencier - Etape 5 - Commande arrivée a destination
    private function state9()
    {
        $this->uploadDoc(65);
        $this->verifierEtatdeCommande(9);
    }

    //Validation du cadencier par le commercial - Etape 1.1
    private function state10()
    {
        $this->uploadDoc(67);
        $this->verifierEtatdeCommande(10);
    }


    private function uploadDoc($type, $valider = true)
    {
        copy(__SOURCINASIA__ . 'src/sourcinasia/appBundle/Tests/Files/test.jpg', __SOURCINASIA__ . 'src/sourcinasia/appBundle/Tests/Files/test2.jpg');
        $crawler = $this->client->request('POST', '/fr/supplychain/multiupload/adddoc',
            array(
                'cadencier' => $this->idCadencier,
                'supplychaintype' => $type,
            ), array(
                'file' => new UploadedFile(
                    __SOURCINASIA__ . 'src/sourcinasia/appBundle/Tests/Files/test2.jpg',
                    'photo.jpg',
                    'image/jpeg',
                    123
                )
            ));
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        if ($valider) {
            $this->TestRoutes(array(
                '/fr/supplychain/valid/' . $this->client->getResponse()->getContent() . '/' . $this->idCadencier
            ), 302);
        }
    }


    private function verifierEtatdeCommande($state)
    {
        $crawler = $this->client->request('GET', '/en/orders/request', array(
            'request' => array(
                'steps' => array($state)
            )
        ));
        $this->assertContains($this->idCadencier, $this->client->getResponse()->getContent());

    }

    private function TestRoutes($routes, $test = 200)
    {
        foreach ($routes as $route) {
            $this->client->request('GET', $route);
            if ($test) {
                $this->assertEquals($test, $this->client->getResponse()->getStatusCode());
            } else {
                $this->assertEquals($test, $this->client->getResponse()->getStatusCode());
            }
        }
    }

    public function doLogin($username, $password)
    {
        $this->client->request('GET', '/logout');
        $crawler = $this->client->request('GET', '/login');
        $this->client->submit($crawler->selectButton('_submit')->form(array(
            '_username' => $username,
            '_password' => $password,
        )));

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
    }

}
