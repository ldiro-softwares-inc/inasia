<?php

namespace sourcinasia\appBundle\Tests\Controller;

use sourcinasia\appBundle\Entity\Supplier;
use sourcinasia\appBundle\Tests\ExtendControllerTest;

class OfferControllerTest extends ExtendControllerTest
{
    function __construct()
    {
        parent::__construct();
        $this->doLogin('olivier', '123456');

    }

    function testCatalogListing()
    {
        $this->client->request('GET', $this->route->generate('catalog_v2'));
        $this->responseMustBe(200);

        $this->client->request('GET', $this->route->generate('offers_requests_v2'). '?request[data_limit]=50&request[offset]=0');
        $this->responseMustBe(200);

        $this->client->request('GET', $this->route->generate('product_request_v2', ["id"=>33721]));
        $this->responseMustBe(200);
    }
}