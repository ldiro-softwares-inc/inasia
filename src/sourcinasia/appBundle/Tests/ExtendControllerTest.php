<?php

namespace sourcinasia\appBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ExtendControllerTest extends WebTestCase
{
    protected $client = null;

    protected $route = null;

    protected $em = null;

    /**
     * Base constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->client = static::createClient([
            'environment' => 'test',
            'debug' => true,
        ]);

        $this->route = $this->client->getContainer()->get('router');

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    protected function responseMustBe($code, $params = null)
    {
        if ($params) {
            $this->assertSame($code, $this->client->getResponse()->getStatusCode(), $params);
        } else {
            $this->assertSame($code, $this->client->getResponse()->getStatusCode());
        }
    }

    protected function logout()
    {
        $this->client->request('GET', '/logout');
    }

    protected function getJsonDecodedResponse()
    {
        return json_decode($this->client->getResponse()->getContent(), true);
    }

    protected function doLogin($username, $password)
    {
        $this->client->request('GET', $this->route->generate('fos_user_security_logout'));
        $crawler = $this->client->request('GET', $this->route->generate('fos_user_security_login'));

        $this->client->submit($crawler->selectButton('_submit')->form([
            '_username' => $username,
            '_password' => $password,
        ]));

        $this->assertTrue($this->client->getResponse()->isRedirect());
        $this->client->followRedirect();
    }

}
