<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Cadencier;
use sourcinasia\appBundle\Entity\Customer;
use sourcinasia\appBundle\Entity\Command;
use sourcinasia\appBundle\Entity\History;
use sourcinasia\appBundle\Entity\Historycustomer;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\EventDispatcher\GenericEvent;

class CadencierController extends Controller
{

    /**
     * Actualisation du filtre request
     */
    public function requestsAction(Request $request)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {

            $request = $request->query->get('request');

            if (!is_array($request)) {
                $response = $this->render('appBundle:Error:404.html.twig');
                $response->setStatusCode(404);
                return $response;
            }

            $request = $this->getDoctrine()
                ->getRepository('appBundle:Supplier')
                ->seach(array_filter($request), $this->get('Profils')->get(), false, false, $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'));

            return $this->render('appBundle:Cadencier:list.html.twig', array(
                'suppliers' => $request,
            ));
        } else
            throw $this->createNotFoundException('Url not found');
    }

    /**
     * Afficher tous les cadenciers des usines
     */
    public function indexAction()
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
            $user = $this->get('security.token_storage')->getToken()->getUser();

            return $this->render('appBundle:Cadencier:index.html.twig', array(
                'active' => 'suppliers_cadenciers',
                'session' => $this->get('session'),
                'openCadenciers' => $this->GetOpenedCadencier(),
            ));
        } else
            throw $this->createNotFoundException('Url not found');
    }

    /*
     * Ouverture du cadencier fournisseur
     */

    public function cadenciefromsupplierAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {

            $profil = $this->get('Profils')->get();
            return $this->render('appBundle:Cadencier:cadencier.html.twig', array(
                'openCadenciers' => $this->GetOpenedCadencier(),
                'screen' => 'edit',
                'offers' => array(),
                'myuser' => $profil,
                'supplier' => $this->getDoctrine()->getRepository('appBundle:Supplier')->getSupplier($id, $profil, $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')),
            ));
        }
    }

    /*
     * Afficher un cadenciers � partir de son id
     */

    public function showAction($id, $action)
    {
        $cadencier = $this->GetCadencier($id);

        if (!$cadencier) {
            $this->get('session')->getFlashBag()->add('notice', 'Can not access to cadencier. Please Contact Sourcinasia');
            $cadencier = $this->GetOpenedCadencier();
            if (count($cadencier)) {
                $cadencier = $cadencier[0];
                return $this->redirect($this->generateUrl('cadencier_show', array('id' => $cadencier->getId())));
            } else
                return $this->redirect($this->generateUrl('suppliers_cadenciers'));
        } else {
            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') || ($cadencier->getState() <= 9 and !$cadencier->getLocked())) {

                $pol = ($cadencier->GetPol()) ? $cadencier->GetPol()->GetId() : false;

                $offers = $this->getDoctrine()
                    ->getRepository('appBundle:Offer')
                    ->getOffersFromSupplier($cadencier->getSupplier()->getId(), $this->get('Profils')->get(), $cadencier->getOfferslist(), $pol, array(), $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'));

                if (empty($offers) and $cadencier->getLocked() == false)
                    return $this->forward('appBundle:Cadencier:delete', array('id' => $cadencier->getId()));

                if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') and $cadencier->getCustomer()) {
                    $profil = $this->get('Profils')->setCustomer($cadencier->getCustomer(), true);
                } else
                    $profil = $this->get('Profils')->get();

                if ($action == "edit" and ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') or $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') or $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION'))) {

                } elseif ($action == "genpi" and ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') or $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN'))) {

                } elseif ($action == "admin" and $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {

                } elseif ($action == "production" and $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {

                } else {
                    throw $this->createNotFoundException('Cadencier not found');
                }

                //forcer l'affichage de la marge.
                /* if ($profil['contact_type'] == 4 || ($profil['mode'] == 'commercial' and $cadencier->GetCustomer()->getCommercialtools() and $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) || (!$cadencier->GetCustomer() and $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                  )
                  $profil['commercial_coef'] = $cadencier->getMargecoef();
                 */
                return $this->render('appBundle:Cadencier:cadencier.html.twig', array(
                    'openCadenciers' => ($action == 'edit') ? $this->GetOpenedCadencier() : array(),
                    'cadencier' => $cadencier,
                    'screen' => $action,
                    'offers' => $offers,
                    'myuser2' => $profil,
                    'supplier' => $cadencier->GetSupplier(),
                    'form' => $this->BuildCadencierForm($cadencier->getSupplier(), $offers, $cadencier)->createView()
                ));
            } else {
                $this->get('session')->getFlashBag()->add('warning', 'Cadencier locked');
                return $this->redirect($this->generateUrl('command'));
            }
        }
        return $this->redirect($this->generateUrl('catalog'));
    }

    /**
     * Displays a form to edit an existing Service entity.
     *
     */
    public function editAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
            $cadencier = $this->getDoctrine()->getRepository('appBundle:Cadencier')->findOneById($id);

            if (!$cadencier)
                throw $this->createNotFoundException('Unable to find Cadencier entity.');

            $form = $this->createForm('sourcinasia\appBundle\Form\CadencierType', $cadencier, array(
                'action' => $this->generateUrl("cadencier_update", array('id' => $cadencier->Getid())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));

            $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));


            return $this->render('appBundle:Cadencier:form.html.twig', array(
                'Cadencier' => $cadencier,
                'form' => $form->createView()
            ));
        }
    }

    /**
     * Edits an existing Service entity.
     */
    public function updateAction(Request $request, $id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $cadencier = $this->getDoctrine()->getRepository('appBundle:Cadencier')->findOneById($id);

            if (!$cadencier)
                throw $this->createNotFoundException('Unable to find Cadencier entity.');

            $form = $this->createForm('sourcinasia\appBundle\Form\CadencierType', $cadencier, array(
                'action' => $this->generateUrl("cadencier_update", array('id' => $cadencier->Getid())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ))->handleRequest($request);

            if ($form->isValid()) {
                if (!$cadencier->getlocked() || ($cadencier->getlocked() && $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($cadencier);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
                    return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->getId())));
                }
            }
            $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

            return $this->render('appBundle:Cadencier:form.html.twig', array(
                'Cadencier' => $cadencier,
                'form' => $form->createView()
            ));
        }
    }

    /*
     *  G�n�rer mes cadenciers
     * @deprecated
     */

    public function generateAction(Request $request, $action)
    {

        $result = array(
            'Gw' => 0,
            'Nw' => 0,
            'Cbm' => 0,
            'Quantpieces' => 0,
            'Quantpackages' => 0,
            'Totalvalue' => 0,
            'Containeroccupation' => 0,
            'Container' => array(
                'volume' => '0',
                'name' => '',
            )
        );

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {

            $user = $this->get('security.token_storage')->getToken()->getUser();
            $session = $this->get('session');
            /* $selectionids= $session->get('selectionids');
              if (in_array($offer->getId(), $selectionids))
              unset($selectionids[array_search($offer->getId(), $selectionids)]); */
            $session->set('selection', '');
            $session->set('selectionids', array());
            //initialisation
            $em = $this->getDoctrine()->getManager();
            $documentscount = 0;
            $totalvalue = 0;
            $totalvaluepi = 0;
            $suppliervalue = 0;
            $cbm = 0;
            $document = false;
            //On a besoin de r�cup�rer toutes les offres pour retrouver les suplliers
            $offerselectedsumbited = array();
            $selection = $request->request->get('selection');
            $deleverytime = $request->request->get('deleverytime');
            $visible = $request->request->get('visible');
            $cadencierid = $request->request->get('cadencierid');
            $cadenciercomment = $request->request->get('comment');
            $servicedescription = $request->request->get('servicesdescription');
            $servicesvalue = $request->request->get('servicesvalue');
            $finalincoterm = $request->request->get('finalincoterm');
            $finalcustomer = $request->request->get('finalcustomer');
            $commercial_coef = (float)$request->request->get('commercial_coef');
            $finalpol = $request->request->get('finalpol');
            $finaltva = $request->request->get('finaltva');
            $supplierci = $request->request->get('supplierci');
            $supplierpi = $request->request->get('supplierpi');
            $customerforwarder = $request->request->get('customerforwarder');
            $finaldevise = $request->request->get('finaldevise');
            $profil = $this->get('Profils')->get();

            if (empty($selection))
                $selection = array();

            foreach ($selection as $offer)
                if (array_key_exists('o', $offer)) {
                    if (array_key_exists('q', $offer))
                        $offerselectedsumbited[$offer['o']]['q'] = (int)$offer['q'];
                    else
                        $offerselectedsumbited[$offer['o']]['q'] = false;

                    if (array_key_exists('sales', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')))
                        $offerselectedsumbited[$offer['o']]['sales'] = (float)$offer['sales'];
                    else
                        $offerselectedsumbited[$offer['o']]['sales'] = 0;

                    if (array_key_exists('confirm', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')))
                        $offerselectedsumbited[$offer['o']]['confirm'] = (int)$offer['confirm'];
                    else
                        $offerselectedsumbited[$offer['o']]['confirm'] = "";

                    if (array_key_exists('confirmfieldmessage', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')))
                        $offerselectedsumbited[$offer['o']]['confirmfieldmessage'] = $offer['confirmfieldmessage'];
                    else
                        $offerselectedsumbited[$offer['o']]['confirmfieldmessage'] = '';

                    if (array_key_exists('exclu', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')))
                        $offerselectedsumbited[$offer['o']]['exclu'] = ($offer['exclu']) ? 1 : 0;
                    else
                        $offerselectedsumbited[$offer['o']]['exclu'] = 0;

                    if (array_key_exists('supplierp', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')))
                        $offerselectedsumbited[$offer['o']]['supplierp'] = (float)$offer['supplierp'];
                    else
                        $offerselectedsumbited[$offer['o']]['supplierp'] = 0;

                    if (array_key_exists('confirmPriceInCurrency', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')))
                        $offerselectedsumbited[$offer['o']]['confirmPriceInCurrency'] = $offer['confirmPriceInCurrency'];
                    else
                        $offerselectedsumbited[$offer['o']]['confirmPriceInCurrency'] = 0;
                }
            //Si on a une selection de produit
            if (count($offerselectedsumbited)) {
                //retourner les suppliers et leurs offres
                $suppliers = $em->getRepository('appBundle:Supplier')->getSuppliersFromSelection(array_keys($offerselectedsumbited));

                $form = $request->request->get('form');

                $exw = false;
                if (!empty($form))
                    if (array_key_exists('incoterms', $form))
                        if (!empty($form['incoterms']))
                            $exw = $form['incoterms'] == 1;

                $Supplier_Offers = $em->getRepository('appBundle:Supplier')->getOffersBySupplierFromSelection(array_keys($offerselectedsumbited), $exw);

                //On v�rife que l'utilisateur peut cr��er un cadcier car il est li� � un customer
                if ($this->get('Profils')->GetCustomerid())
                    $customer = $em->getRepository('appBundle:Customer')->find($this->get('Profils')->GetCustomerid());
                elseif (($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) && !$this->get('profils')->GetCustomerid())
                    $customer = false;
                else
                    die('error404');

                //Pour chaque fournisseur trouv� on doit avoir un cadencier...
                foreach ($suppliers as $supplier) {
                    foreach (array_keys($Supplier_Offers[$supplier->getId()]) as $pol) {

                        // On s�l�ctionne les offres du founisseur
                        $offerselected = $em->getRepository('appBundle:Offer')->OfferIn($Supplier_Offers[$supplier->getId()][$pol], $profil);

                        // On retrouve le cadencier
                        if ($cadencierid)
                            $document = $this->GetCadencier($cadencierid);
                        else
                            $document = false;

                        /* {
                          if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                          $document = $em->getRepository('appBundle:Cadencier')->getCommercialCadenciers($user->getId(), $profil['cutomer_id'], $supplier->getId(), $pol);
                          else if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
                          $document = $em->getRepository('appBundle:Cadencier')->getCustomerCadenciers($user->GetId(), $profil['cutomer_id'], $supplier->getId(), $pol);
                          }
                          } */

                        // Si on le retrouve pas on le fabrique
                        if (!$document) {
                            $document = new Cadencier();

                            $document->setCreated(new \DateTime());
                            $document->setDeleverytime($supplier->getDeleverytime());
                            $document->setLocked(0);
                            if ($customer)
                                $document->setCustomer($customer);
                            $document->SetSupplier($supplier);

                            $Containers = $supplier->getContainers();
                            $document->setContainer($Containers[0]);
                            $document->setUser($user);
                            if ($pol) {
                                $thepol = $em->getRepository('appBundle:Pol')->find($pol);
                                if ($thepol) {
                                    $document->setPol($thepol);
                                    $document->setFinalpol($thepol->getTitle());
                                }
                            }

                            if (!$finalpol && $pol) {
                                if (count($offerselected)) {
                                    if ($offerselected[0]->GetFob()) {
                                        $document->setIncoterm($this->getDoctrine()->getRepository('appBundle:Incoterm')->find(2));
                                    } else
// when $finalpol == NULL (from request) and fob == null (in the DB), we end up here.
// Not setting incoterm results in the cadencier not being created, so setIncoterm(???)
                                    /* start bug fix */
                                    if ($offerselected[0]->GetExw()) {
                                        $document->setIncoterm($this->getDoctrine()->getRepository('appBundle:Incoterm')->findOneById('1'));
                                    } else {
                                        die('No incoterm');
                                    }
                                    /* end of fix */
                                } else {
                                    die('No incoterm');
                                }
                            } else {
                                if ($offerselected[0]->GetExw())
                                    $document->setIncoterm($this->getDoctrine()->getRepository('appBundle:Incoterm')->findOneById('1'));
                                else
                                    die('No incoterm');
                            }
                            if ($profil['commercial_coef'] > 0 && $profil['commercial_coef'] < 10)
                                $document->setMargecoef($profil['commercial_coef']);
                        }

                        if (!$document->getNbr())
                            $document->setNbr(1);

                        if (!$document->getCustomerpaymenterms() && $customer)
                            $document->setCustomerpaymenterms($customer->getPaymentcondition());

                        if (!$document->getSupplierpaymenterms() && $supplier)
                            $document->setSupplierpaymenterms($supplier->getPaymenterms());

                        if (!empty($deleverytime) && ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')))
                            $document->setDeleverytime($deleverytime);

                        // Si des donn�es ont �t� modifi�es on le mise � jour du cadencier
                        $form = $request->request->get('form');
                        if (!empty($form)) {
                            if (array_key_exists('containers', $form))
                                if (!empty($form['containers']))
                                    $document->setContainer($em->getRepository('appBundle:Container')->find($form['containers']));
                            if (array_key_exists('incoterms', $form))
                                if (!empty($form['incoterms']))
                                    $document->setIncoterm($em->getRepository('appBundle:Incoterm')->find($form['incoterms']));
                            if (array_key_exists('supplierpaymenterms', $form))
                                if (!empty($form['supplierpaymenterms']))
                                    $document->setSupplierpaymenterms($em->getRepository('appBundle:Paymentcondition')->find($form['supplierpaymenterms']));
                            if (array_key_exists('customerpaymenterms', $form))
                                if (!empty($form['customerpaymenterms']))
                                    $document->setCustomerpaymenterms($em->getRepository('appBundle:Paymentcondition')->find($form['customerpaymenterms']));
                            $selection = $offer = array();
                        } else {
                            $selection = $document->GetSelection();
                            $offer = $document->GetOfferslist();
                        }
//bug fix requested
                        if (!$document->getIncoterm()) {
                            $this->get('session')->getFlashBag()->add('warning', 'Cannot build cadencier because no Incoterm selected for supplier');
                            return $this->redirect($this->generateUrl('catalog'));
                        }

                        $gw = $nw = $cbm = $quantpieces = $quantpackages = $totalvalue = 0;
                        foreach ($offerselected as $offer) {
                            $qty = $offerselectedsumbited[$offer->getId()]['q'];

                            if ($qty === false)
                                $qty = 1;
                            /*
                              if ($document->getCustomer())
                              $qty = $offer->getMoq($document->getCustomer()->GetId());
                              else
                              $qty = $offer->getMoq();
                             */

                            if ($document->getCustomer()) {
                                $test = !$offer->isHidden($document->getCustomer()->GetId());
                            } else {
                                $test = true;
                            }

                            switch (strtolower($document->getIncoterm()->GetTitle())) {
                                case 'fob':
                                    $test2 = $offer->GetFob() > 0;
                                    break;
                                case 'exw':
                                    $test2 = $offer->GetExw() > 0;
                                    break;;
                            }

                            if ($qty > 0 && $test && $test2) {
                                $gw += $qty * $offer->getProduct()->getGwproduct();
                                $nw += $qty * $offer->getProduct()->getNwproduct();
                                $cbm += $qty * $offer->getProduct()->getCbm() / $offer->getProduct()->getPcb();
                                $quantpieces += $qty;
                                $quantpackages += $qty / $offer->getProduct()->getPcb();
                                $price = 0;

                                if ($document->getCustomer()) {
                                    $profil = $this->get('Profils')->setCustomer($document->getCustomer(), true);
                                } else
                                    $profil = $this->get('Profils')->get();

                                switch (strtolower($document->getIncoterm()->GetTitle())) {
                                    case 'fob':
                                        $incotermvalue = 'fob';
                                        break;
                                    case 'exw':
                                        $incotermvalue = 'exw';
                                        break;;
                                }

                                //forcer l'affichage de la marge.
                                if ($profil['contact_type'] == 4 ||
                                    ($profil['mode'] == 'commercial' and $document->GetCustomer()->getCommercialtools() and $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) || (!$document->GetCustomer() and $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                                )
                                    $profil['commercial_coef'] = $document->getMargecoef();

                                $price = $offer->GetSaleprice($incotermvalue, $profil);
                                $theoprice = $offer->GetSaleprice($incotermvalue, $profil, false);
                                $saleprice = $offerselectedsumbited[$offer->getId()]['sales'];

                                $oldselection = (array)$document->getSelection();

                                if (empty($saleprice)) {
                                    if ($commercial_coef && $commercial_coef > 0 && $commercial_coef < 10) {
                                        $saleprice = $price;
                                    } elseif (array_key_exists('o' . $offer->getId(), $oldselection))
                                        if (array_key_exists('finalprice', $oldselection['o' . $offer->getId()]))
                                            if ($document->getState() >= 4)
                                                $saleprice = $oldselection['o' . $offer->getId()]['finalprice'];
                                            else
                                                $saleprice = $price;
                                        else
                                            $saleprice = $price;
                                    else
                                        $saleprice = $price;
                                }

                                if (in_array($document->getState(), array(0, 1, 2, 3, 4)))
                                    $totalvalue += $qty * $price;
                                else
                                    $totalvalue += $qty * $saleprice;

                                $suppliervalue += $qty * $offer->GetSourcingprice($incotermvalue);

                                $totalvaluepi += $qty * $saleprice;
                                $selection['o' . $offer->getId()] = array('offer' => $offer->getId(),
                                    'price' => $price,
                                    'theoprice' => $theoprice,
                                    'qty' => $qty,
                                    'finalprice' => $saleprice,
                                    'supplierp' => 0,
                                    'confirm' => 0,
                                    'confirmfieldmessage' => '',
                                    'confirmPriceInCurrency' => '',
                                    'exclu' => 0);

                                if (is_array($oldselection)) {
                                    if (array_key_exists('o' . $offer->getId(), $oldselection)) {
                                        if (empty($offerselectedsumbited[$offer->getId()]['confirm']) && array_key_exists('confirm', $oldselection['o' . $offer->getId()]))
                                            $selection['o' . $offer->getId()]['confirm'] = $oldselection['o' . $offer->getId()]['confirm'];
                                        else {
                                            if ($offerselectedsumbited[$offer->getId()]['confirm'] == 1 && $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                                                $selection['o' . $offer->getId()]['confirm'] = $offerselectedsumbited[$offer->getId()]['confirm'];
                                            else
                                                $selection['o' . $offer->getId()]['confirm'] = 0;
                                        }

                                        if (empty($offerselectedsumbited[$offer->getId()]['confirmfieldmessage']) && array_key_exists('confirmfieldmessage', $oldselection['o' . $offer->getId()]))
                                            $selection['o' . $offer->getId()]['confirmfieldmessage'] = $oldselection['o' . $offer->getId()]['confirmfieldmessage'];
                                        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                                            $selection['o' . $offer->getId()]['confirmfieldmessage'] = $offerselectedsumbited[$offer->getId()]['confirmfieldmessage'];


                                        if (empty($offerselectedsumbited[$offer->getId()]['exclu']) && array_key_exists('exclu', $oldselection['o' . $offer->getId()]))
                                            $selection['o' . $offer->getId()]['exclu'] = $oldselection['o' . $offer->getId()]['exclu'];
                                        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                                            $selection['o' . $offer->getId()]['exclu'] = $offerselectedsumbited[$offer->getId()]['exclu'];


                                        if (empty($offerselectedsumbited[$offer->getId()]['supplierp']) && array_key_exists('supplierp', $oldselection['o' . $offer->getId()]))
                                            $selection['o' . $offer->getId()]['supplierp'] = $oldselection['o' . $offer->getId()]['supplierp'];
                                        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                                            $selection['o' . $offer->getId()]['supplierp'] = $offerselectedsumbited[$offer->getId()]['supplierp'];


                                        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && $document->getState() == 7 && $oldselection['o' . $offer->getId()]['qty'] != $offerselectedsumbited[$offer->getId()]['q']) {
                                            $document->setState(5);
                                            $this->get('event_dispatcher')->dispatch('sendmail', new GenericEvent($document, array('step' => 'PRODUCTIONCONFIRMPRICES')));
                                        }

                                        if (empty($offerselectedsumbited[$offer->getId()]['confirmPriceInCurrency']) && array_key_exists('confirmPriceInCurrency', $oldselection['o' . $offer->getId()]))
                                            $selection['o' . $offer->getId()]['confirmPriceInCurrency'] = $oldselection['o' . $offer->getId()]['confirmPriceInCurrency'];
                                    } else {
                                        if (in_array($document->getState(), array(3, 4, 5, 6, 7))) {
                                            $document->setState(2);
                                            $razselection = true;
                                            $this->get('event_dispatcher')->dispatch('sendmail', new GenericEvent($document, array('step' => 'SALERCONFIRMSELECTION')));
                                        }
                                    }
                                }
                                $offerlist[] = $offer->getId();
                            }
                        }

                        if (!$finaldevise)
                            $document->setFinaldevis('USD');

                        if (!empty($offerlist)) {
                            $document->SetPublished(true);

                            if ($cadenciercomment)
                                $document->SetComment($cadenciercomment);

                            if ($finalcustomer)
                                $document->setFinalcustomer($finalcustomer);

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                                if ($finalpol)
                                    $document->setFinalpol($finalpol);

                                $document->setFinaltva($finaltva);
                                if ($finaldevise)
                                    $document->setFinaldevis($finaldevise);

                                if ($customerforwarder)
                                    $document->setCustomerforwarder($customerforwarder);
                            }

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
                                if ($supplierci)
                                    $document->setSupplierci($supplierci);
                                if ($supplierpi)
                                    $document->setSupplierpi($supplierpi);
                            }

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
                                if (is_array($servicedescription)) {
                                    $services = array();
                                    foreach ($servicedescription as $key => $service) {
                                        $service = array();
                                        if (array_key_exists($key, $servicedescription))
                                            $service['description'] = $servicedescription[$key];
                                        if (array_key_exists($key, $servicesvalue)) {
                                            $service['value'] = (float)$servicesvalue[$key];
                                            $totalvaluepi = $totalvaluepi + $service['value'];
                                        }
                                        if ($service['description'] || $service['value'])
                                            $services[] = $service;
                                    }
                                    $document->setServicedescription($services);
                                    $document->setServicevalue($document->getTotalvaluepi() - $document->getTotalvalue());
                                    $document->setTotalvaluepi($totalvaluepi);
                                }
                                if ($finalincoterm)
                                    $document->setFinalincoterm($finalincoterm);
                            }

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                                if ($finalincoterm)
                                    $document->setFinalincoterm($finalincoterm);
                            }


                            if ($commercial_coef && $commercial_coef > 0 && $commercial_coef < 10) {
                                if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') and $user->getContact()->getType() == 4)
                                    $document->setMargecoef($commercial_coef);
                                elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                                    $document->setMargecoef($commercial_coef);
                                } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT'))
                                    if ($document->GetCustomer())
                                        if ($document->GetCustomer()->GetActivity()->getId() == 6)
                                            $document->setMargecoef($commercial_coef);
                            }

                            if (!empty($razselection)) {
                                foreach ($selection as $key => $item)
                                    $selection[$key]['confirm'] = 0;
                            }

                            $document->SetDate(new \DateTime());
                            $document->setGw($gw);
                            $document->setNw($nw);
                            $document->setCbm(round($cbm, 3));
                            $document->setQuantpieces(round($quantpieces));
                            $document->setQuantpackages(round($quantpackages));
                            $document->setTotalvalue(round($totalvalue, 2));
                            $document->setSelection($selection);
                            $document->setOfferslist($offerlist);
                            $document->setSuppliervalue(round($suppliervalue, 2));

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                                $document->SetVisible($visible);

                            // On enregistre le cadencier
                            if (!empty($offerlist)) {
                                if ($document->getLocked() == 1) {
                                    $response = new Response();
                                    $response->setContent(json_encode(array()));
                                    $response->headers->set('Content-Type', 'application/json');
                                    return $response;
                                } else {
                                    $em->persist($document);
                                    $em->flush();
                                    $documentscount++;
                                }
                            }

                            if ($document->GetCommand())
                                $cbm = $this->getDoctrine()->getRepository('appBundle:Command')->GetTotalValue($document->GetCommand()->getId());

                            $containeroccupation = $cbm / $document->getContainer()->getVolume() * 100;


                            $result = array(
                                'Gw' => $document->getGw(),
                                'Nw' => $document->getNw(),
                                'Cbm' => $document->getCbm(),
                                'Quantpieces' => $document->getQuantpieces(),
                                'Quantpackages' => $document->getQuantpackages(),
                                'Totalvalue' => $document->getTotalvalue(),
                                'Containeroccupation' => $containeroccupation,
                                'validation' => $this->isValidSelection($document->getSelection()),
                                'Container' => array(
                                    'volume' => $document->getContainer()->getVolume(),
                                    'name' => $document->getContainer()->getTitle(),
                                )
                            );

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                                $result = array_merge($result, array('Totalvaluepi' => $document->getTotalvaluepi()));

                            if ($document->GetCustomer() && $action != "json") {
                                $Historycustomer = new Historycustomer();
                                $Historycustomer->setTitle('Add Cadencier');
                                $Historycustomer->setIco('<i class="icon-book"></i>');
                                $Historycustomer->setDescription('Add Cadencier');
                                $Historycustomer->setDate(new \DateTime());
                                $Historycustomer->setCadencier($document);
                                $Historycustomer->setCustomer($document->getCustomer());
                                $Historycustomer->setUser($user);
                                $em->persist($Historycustomer);
                                $em->flush();
                            }
                        }
                    }
                }
            }

            if ($action == "pi") {
                if ($cadencierid)
                    $document = $this->GetCadencier($cadencierid);

                if (!$document)
                    throw $this->createNotFoundException('Cadencier not found');

                $Command = $document->getCommand();

                if (!$Command) {
                    $Command = new Command();
                    $document->setCommand($Command);

                    if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') and $user->getContact()->getType() != 4 and $user->getContact()->getType() > 0) {
                        $document->setState(0);
                    } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                        $document->setState(2);
                    } else {
                        $document->setState(1);
                    }

                    $Command->setContainer($document->getContainer());
                    $Command->setShipper('-');
                    $Command->setConsignee('-');
                    $Command->setNotify('-');
                    $Command->setFreightpayableat('');
                    $Command->setBookingnumber('-');
                    $Command->setEtd(new \DateTime());
                    $Command->setEta(new \DateTime());
                    $Command->setVessel('-');
                    $Command->setTcnum('-');
                    $Command->setSealnum('-');
                    $Command->setLocked(0);
                    $Command->setModified(new \DateTime());
                    $Command->setCreated(new \DateTime());
                    $em->persist($document);

                    //mailSupplier PI Request;

                    $Historycustomer = new Historycustomer();
                    $Historycustomer->setTitle('ASK PI');
                    $Historycustomer->setDescription('ASK PI');
                    $Historycustomer->setDate(new \DateTime());
                    $Historycustomer->setIco('<i class="icon-book"></i>');
                    $Historycustomer->setCadencier($document);
                    $Historycustomer->setCustomer($document->getCustomer());
                    $Historycustomer->setUser($user);
                    $em->persist($Historycustomer);
                    $em->flush();

                    if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') and $user->getContact()->getType() != 4 and $user->getContact()->getType() > 0) {
                        $this->get('event_dispatcher')->dispatch('sendmail', new GenericEvent($document, array('step' => 'SALERUSERCOMFIRMSELECTION')));
                        $this->get('session')->getFlashBag()->add('notice', 'COMMAND_CREATED');
                        return $this->render('appBundle:Command:confirmationcustomer.html.twig');
                    } else {
                        $this->get('event_dispatcher')->dispatch('sendmail', new GenericEvent($document, array('step' => 'CUSTOMERCONFIRMSELECTION')));
                        $this->get('session')->getFlashBag()->add('notice', 'COMMAND_CREATED');
                        return $this->render('appBundle:Command:confirmation.html.twig');
                    }
                } else {
                    $Command->setModified(new \Datetime());
                    $this->get('session')->getFlashBag()->add('notice', 'COMMAND_UPDATED');
                    return $this->redirect($this->generateUrl('command_show', array('id' => $document->getId())));
                }
            } elseif ($action == "confirm") {

                $document = $this->GetCadencier($cadencierid);
                if (!$document)
                    throw $this->createNotFoundException('Cadencier not found');
                $document->setState(1);
                $document->SetDate(new \DateTime());
                $em->persist($document);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', 'COMMAND_CREATED');
                return $this->render('appBundle:Command:confirmation.html.twig');
            } elseif ($action != "json") {
                if ($documentscount)
                    return $this->redirect($this->generateUrl('cadencier_show', array('id' => $document->getId())));
                else
                    return $this->redirect($this->generateUrl('catalog'));
            }
        }

        $response = new Response();
        $response->setContent(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /*
     *  Generate cadenciers from catalog
     */
    public function generateV2Action(Request $request, $action)
    {
        $result = array(
            'Gw' => 0,
            'Nw' => 0,
            'Cbm' => 0,
            'Quantpieces' => 0,
            'Quantpackages' => 0,
            'Totalvalue' => 0,
            'Containeroccupation' => 0,
            'Container' => array(
                'volume' => '0',
                'name' => '',
            )
        );

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {

            $user = $this->get('security.token_storage')->getToken()->getUser();
            $session = $this->get('session');
            /* $selectionids= $session->get('selectionids');
              if (in_array($offer->getId(), $selectionids))
              unset($selectionids[array_search($offer->getId(), $selectionids)]); */
            $session->set('selectionV2', '');
            $session->set('selectionids', array());
            //initialisation
            $em = $this->getDoctrine()->getManager();
            $documentscount = 0;
            $totalvalue = 0;
            $totalvaluepi = 0;
            $suppliervalue = 0;
            $cbm = 0;
            $document = false;
            //On a besoin de r�cup�rer toutes les offres pour retrouver les suplliers
            $offerselectedsumbited = array();
            $selection = $request->request->get('selection');
            $deleverytime = $request->request->get('deleverytime');
            $visible = $request->request->get('visible');
            $cadencierid = $request->request->get('cadencierid');
            $cadenciercomment = $request->request->get('comment');
            $servicedescription = $request->request->get('servicesdescription');
            $servicesvalue = $request->request->get('servicesvalue');
            $finalincoterm = $request->request->get('finalincoterm');
            $finalcustomer = $request->request->get('finalcustomer');
            $commercial_coef = (float)$request->request->get('commercial_coef');
            $finalpol = $request->request->get('finalpol');
            $finaltva = $request->request->get('finaltva');
            $supplierci = $request->request->get('supplierci');
            $supplierpi = $request->request->get('supplierpi');
            $customerforwarder = $request->request->get('customerforwarder');
            $finaldevise = $request->request->get('finaldevise');
            $profil = $this->get('Profils')->get();

            if (empty($selection))
                $selection = array();

            foreach ($selection as $offer)


            if (array_key_exists('o', $offer)) {
                    if (array_key_exists('q', $offer))
                        $offerselectedsumbited[$offer['o']]['q'] = (int)$offer['q'];
                    else
                        $offerselectedsumbited[$offer['o']]['q'] = false;

                    if (array_key_exists('sales', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')))
                        $offerselectedsumbited[$offer['o']]['sales'] = (float)$offer['sales'];
                    else
                        $offerselectedsumbited[$offer['o']]['sales'] = 0;

                    if (array_key_exists('confirm', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')))
                        $offerselectedsumbited[$offer['o']]['confirm'] = (int)$offer['confirm'];
                    else
                        $offerselectedsumbited[$offer['o']]['confirm'] = "";

                    if (array_key_exists('confirmfieldmessage', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')))
                        $offerselectedsumbited[$offer['o']]['confirmfieldmessage'] = $offer['confirmfieldmessage'];
                    else
                        $offerselectedsumbited[$offer['o']]['confirmfieldmessage'] = '';

                    if (array_key_exists('exclu', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')))
                        $offerselectedsumbited[$offer['o']]['exclu'] = ($offer['exclu']) ? 1 : 0;
                    else
                        $offerselectedsumbited[$offer['o']]['exclu'] = 0;

                    if (array_key_exists('supplierp', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')))
                        $offerselectedsumbited[$offer['o']]['supplierp'] = (float)$offer['supplierp'];
                    else
                        $offerselectedsumbited[$offer['o']]['supplierp'] = 0;

                    if (array_key_exists('confirmPriceInCurrency', $offer) && ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')))
                        $offerselectedsumbited[$offer['o']]['confirmPriceInCurrency'] = $offer['confirmPriceInCurrency'];
                    else
                        $offerselectedsumbited[$offer['o']]['confirmPriceInCurrency'] = 0;
                }
            //Si on a une selection de produit
            if (count($offerselectedsumbited)) {
                //retourner les suppliers et leurs offres
                $suppliers = $em->getRepository('appBundle:Supplier')->getSuppliersFromSelection(array_keys($offerselectedsumbited));


                $form = $request->request->get('form');

                $exw = false;
                if (!empty($form))
                    if (array_key_exists('incoterms', $form))
                        if (!empty($form['incoterms']))
                            $exw = $form['incoterms'] == 1;

                $Supplier_Offers = $em->getRepository('appBundle:Supplier')->getOffersBySupplierFromSelection(array_keys($offerselectedsumbited), $exw);

                //On v�rife que l'utilisateur peut cr��er un cadcier car il est li� � un customer
                if ($this->get('Profils')->GetCustomerid())
                    $customer = $em->getRepository('appBundle:Customer')->find($this->get('Profils')->GetCustomerid());
                elseif (($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) && !$this->get('profils')->GetCustomerid())
                    $customer = false;
                else
                    die('error404');

                //Pour chaque fournisseur trouv� on doit avoir un cadencier...
                foreach ($suppliers as $supplier) {

                    foreach (array_keys($Supplier_Offers[$supplier->getId()]) as $pol) {

                        // On s�l�ctionne les offres du founisseur
                        $offerselected = $em->getRepository('appBundle:Offer')->OfferIn($Supplier_Offers[$supplier->getId()][$pol], $profil);

                        // On retrouve le cadencier
                        if ($cadencierid)
                            $document = $this->GetCadencier($cadencierid);
                        else
                            $document = false;

                        /* {
                          if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                          $document = $em->getRepository('appBundle:Cadencier')->getCommercialCadenciers($user->getId(), $profil['cutomer_id'], $supplier->getId(), $pol);
                          else if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
                          $document = $em->getRepository('appBundle:Cadencier')->getCustomerCadenciers($user->GetId(), $profil['cutomer_id'], $supplier->getId(), $pol);
                          }
                          } */            ;

                        // Si on le retrouve pas on le fabrique
                        if (!$document) {

                            $document = new Cadencier();

                            $document->setCreated(new \DateTime());
                            $document->setDeleverytime($supplier->getDeleverytime());
                            $document->setLocked(0);
                            if ($customer)
                                $document->setCustomer($customer);
                            $document->SetSupplier($supplier);

                            $Containers = $supplier->getContainers();
                            $document->setContainer($Containers[0]);
                            $document->setUser($user);
                            if ($pol) {
                                $thepol = $em->getRepository('appBundle:Pol')->find($pol);
                                if ($thepol) {
                                    $document->setPol($thepol);
                                    $document->setFinalpol($thepol->getTitle());
                                }
                            }

                            if (!$finalpol && $pol) {
                                if (count($offerselected)) {
                                    if ($offerselected[0]->GetFob()) {
                                        $document->setIncoterm($this->getDoctrine()->getRepository('appBundle:Incoterm')->find(2));
                                    } else
// when $finalpol == NULL (from request) and fob == null (in the DB), we end up here.
// Not setting incoterm results in the cadencier not being created, so setIncoterm(???)
                                        /* start bug fix */
                                        if ($offerselected[0]->GetExw()) {
                                            $document->setIncoterm($this->getDoctrine()->getRepository('appBundle:Incoterm')->findOneById('1'));
                                        } else {
                                            die('No incoterm');
                                        }
                                    /* end of fix */
                                } else {
                                    die('No incoterm');
                                }
                            } else {
                                if ($offerselected[0]->GetExw())
                                    $document->setIncoterm($this->getDoctrine()->getRepository('appBundle:Incoterm')->findOneById('1'));
                                else
                                    die('No incoterm');
                            }
                            if ($profil['commercial_coef'] > 0 && $profil['commercial_coef'] < 10)
                                $document->setMargecoef($profil['commercial_coef']);
                        }

                        if (!$document->getNbr())
                            $document->setNbr(1);

                        if (!$document->getCustomerpaymenterms() && $customer)
                            $document->setCustomerpaymenterms($customer->getPaymentcondition());

                        if (!$document->getSupplierpaymenterms() && $supplier)
                            $document->setSupplierpaymenterms($supplier->getPaymenterms());

                        if (!empty($deleverytime) && ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')))
                            $document->setDeleverytime($deleverytime);

                        // Si des donn�es ont �t� modifi�es on le mise � jour du cadencier
                        $form = $request->request->get('form');
                        if (!empty($form)) {
                            if (array_key_exists('containers', $form))
                                if (!empty($form['containers']))
                                    $document->setContainer($em->getRepository('appBundle:Container')->find($form['containers']));
                            if (array_key_exists('incoterms', $form))
                                if (!empty($form['incoterms']))
                                    $document->setIncoterm($em->getRepository('appBundle:Incoterm')->find($form['incoterms']));
                            if (array_key_exists('supplierpaymenterms', $form))
                                if (!empty($form['supplierpaymenterms']))
                                    $document->setSupplierpaymenterms($em->getRepository('appBundle:Paymentcondition')->find($form['supplierpaymenterms']));
                            if (array_key_exists('customerpaymenterms', $form))
                                if (!empty($form['customerpaymenterms']))
                                    $document->setCustomerpaymenterms($em->getRepository('appBundle:Paymentcondition')->find($form['customerpaymenterms']));
                            $selection = $offer = array();
                        } else {
                            $selection = $document->GetSelection();
                            $offer = $document->GetOfferslist();
                        }
//bug fix requested
                        if (!$document->getIncoterm()) {
                            $this->get('session')->getFlashBag()->add('warning', 'Cannot build cadencier because no Incoterm selected for supplier');
                            return $this->redirect($this->generateUrl('catalog_v2'));
                        }

                        $gw = $nw = $cbm = $quantpieces = $quantpackages = $totalvalue = 0;
                        foreach ($offerselected as $offer) {
                            $qty = $offerselectedsumbited[$offer->getId()]['q'];

                            if ($qty === false)
                                $qty = 1;
                            /*
                              if ($document->getCustomer())
                              $qty = $offer->getMoq($document->getCustomer()->GetId());
                              else
                              $qty = $offer->getMoq();
                             */

                            if ($document->getCustomer()) {
                                $test = !$offer->isHidden($document->getCustomer()->GetId());
                            } else {
                                $test = true;
                            }

                            switch (strtolower($document->getIncoterm()->GetTitle())) {
                                case 'fob':
                                    $test2 = $offer->GetFob() > 0;
                                    break;
                                case 'exw':
                                    $test2 = $offer->GetExw() > 0;
                                    break;;
                            }

                            if ($qty > 0 && $test && $test2) {
                                $gw += $qty * $offer->getProduct()->getGwproduct();
                                $nw += $qty * $offer->getProduct()->getNwproduct();
                                $cbm += $qty * $offer->getProduct()->getCbm() / $offer->getProduct()->getPcb();
                                $quantpieces += $qty;
                                $quantpackages += $qty / $offer->getProduct()->getPcb();
                                $price = 0;

                                if ($document->getCustomer()) {
                                    $profil = $this->get('Profils')->setCustomer($document->getCustomer(), true);
                                } else
                                    $profil = $this->get('Profils')->get();

                                switch (strtolower($document->getIncoterm()->GetTitle())) {
                                    case 'fob':
                                        $incotermvalue = 'fob';
                                        break;
                                    case 'exw':
                                        $incotermvalue = 'exw';
                                        break;;
                                }

                                //forcer l'affichage de la marge.
                                if ($profil['contact_type'] == 4 ||
                                    ($profil['mode'] == 'commercial' and $document->GetCustomer()->getCommercialtools() and $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) || (!$document->GetCustomer() and $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                                )
                                    $profil['commercial_coef'] = $document->getMargecoef();

                                $price = $offer->GetSaleprice($incotermvalue, $profil);
                                $theoprice = $offer->GetSaleprice($incotermvalue, $profil, false);
                                $saleprice = $offerselectedsumbited[$offer->getId()]['sales'];

                                $oldselection = (array)$document->getSelection();

                                if (empty($saleprice)) {
                                    if ($commercial_coef && $commercial_coef > 0 && $commercial_coef < 10) {
                                        $saleprice = $price;
                                    } elseif (array_key_exists('o' . $offer->getId(), $oldselection))
                                        if (array_key_exists('finalprice', $oldselection['o' . $offer->getId()]))
                                            if ($document->getState() >= 4)
                                                $saleprice = $oldselection['o' . $offer->getId()]['finalprice'];
                                            else
                                                $saleprice = $price;
                                        else
                                            $saleprice = $price;
                                    else
                                        $saleprice = $price;
                                }

                                if (in_array($document->getState(), array(0, 1, 2, 3, 4)))
                                    $totalvalue += $qty * $price;
                                else
                                    $totalvalue += $qty * $saleprice;

                                $suppliervalue += $qty * $offer->GetSourcingprice($incotermvalue);

                                $totalvaluepi += $qty * $saleprice;
                                $selection['o' . $offer->getId()] = array('offer' => $offer->getId(),
                                    'price' => $price,
                                    'theoprice' => $theoprice,
                                    'qty' => $qty,
                                    'finalprice' => $saleprice,
                                    'supplierp' => 0,
                                    'confirm' => 0,
                                    'confirmfieldmessage' => '',
                                    'confirmPriceInCurrency' => '',
                                    'exclu' => 0);

                                if (is_array($oldselection)) {
                                    if (array_key_exists('o' . $offer->getId(), $oldselection)) {
                                        if (empty($offerselectedsumbited[$offer->getId()]['confirm']) && array_key_exists('confirm', $oldselection['o' . $offer->getId()]))
                                            $selection['o' . $offer->getId()]['confirm'] = $oldselection['o' . $offer->getId()]['confirm'];
                                        else {
                                            if ($offerselectedsumbited[$offer->getId()]['confirm'] == 1 && $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                                                $selection['o' . $offer->getId()]['confirm'] = $offerselectedsumbited[$offer->getId()]['confirm'];
                                            else
                                                $selection['o' . $offer->getId()]['confirm'] = 0;
                                        }

                                        if (empty($offerselectedsumbited[$offer->getId()]['confirmfieldmessage']) && array_key_exists('confirmfieldmessage', $oldselection['o' . $offer->getId()]))
                                            $selection['o' . $offer->getId()]['confirmfieldmessage'] = $oldselection['o' . $offer->getId()]['confirmfieldmessage'];
                                        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                                            $selection['o' . $offer->getId()]['confirmfieldmessage'] = $offerselectedsumbited[$offer->getId()]['confirmfieldmessage'];


                                        if (empty($offerselectedsumbited[$offer->getId()]['exclu']) && array_key_exists('exclu', $oldselection['o' . $offer->getId()]))
                                            $selection['o' . $offer->getId()]['exclu'] = $oldselection['o' . $offer->getId()]['exclu'];
                                        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                                            $selection['o' . $offer->getId()]['exclu'] = $offerselectedsumbited[$offer->getId()]['exclu'];


                                        if (empty($offerselectedsumbited[$offer->getId()]['supplierp']) && array_key_exists('supplierp', $oldselection['o' . $offer->getId()]))
                                            $selection['o' . $offer->getId()]['supplierp'] = $oldselection['o' . $offer->getId()]['supplierp'];
                                        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                                            $selection['o' . $offer->getId()]['supplierp'] = $offerselectedsumbited[$offer->getId()]['supplierp'];


                                        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && $document->getState() == 7 && $oldselection['o' . $offer->getId()]['qty'] != $offerselectedsumbited[$offer->getId()]['q']) {
                                            $document->setState(5);
                                            $this->get('event_dispatcher')->dispatch('sendmail', new GenericEvent($document, array('step' => 'PRODUCTIONCONFIRMPRICES')));
                                        }

                                        if (empty($offerselectedsumbited[$offer->getId()]['confirmPriceInCurrency']) && array_key_exists('confirmPriceInCurrency', $oldselection['o' . $offer->getId()]))
                                            $selection['o' . $offer->getId()]['confirmPriceInCurrency'] = $oldselection['o' . $offer->getId()]['confirmPriceInCurrency'];
                                    } else {
                                        if (in_array($document->getState(), array(3, 4, 5, 6, 7))) {
                                            $document->setState(2);
                                            $razselection = true;
                                            $this->get('event_dispatcher')->dispatch('sendmail', new GenericEvent($document, array('step' => 'SALERCONFIRMSELECTION')));
                                        }
                                    }
                                }
                                $offerlist[] = $offer->getId();
                            }
                        }

                        if (!$finaldevise)
                            $document->setFinaldevis('USD');

                        if (!empty($offerlist)) {
                            $document->SetPublished(true);

                            if ($cadenciercomment)
                                $document->SetComment($cadenciercomment);

                            if ($finalcustomer)
                                $document->setFinalcustomer($finalcustomer);

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                                if ($finalpol)
                                    $document->setFinalpol($finalpol);

                                $document->setFinaltva($finaltva);
                                if ($finaldevise)
                                    $document->setFinaldevis($finaldevise);

                                if ($customerforwarder)
                                    $document->setCustomerforwarder($customerforwarder);
                            }

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
                                if ($supplierci)
                                    $document->setSupplierci($supplierci);
                                if ($supplierpi)
                                    $document->setSupplierpi($supplierpi);
                            }

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
                                if (is_array($servicedescription)) {
                                    $services = array();
                                    foreach ($servicedescription as $key => $service) {
                                        $service = array();
                                        if (array_key_exists($key, $servicedescription))
                                            $service['description'] = $servicedescription[$key];
                                        if (array_key_exists($key, $servicesvalue)) {
                                            $service['value'] = (float)$servicesvalue[$key];
                                            $totalvaluepi = $totalvaluepi + $service['value'];
                                        }
                                        if ($service['description'] || $service['value'])
                                            $services[] = $service;
                                    }
                                    $document->setServicedescription($services);
                                    $document->setServicevalue($document->getTotalvaluepi() - $document->getTotalvalue());
                                    $document->setTotalvaluepi($totalvaluepi);
                                }
                                if ($finalincoterm)
                                    $document->setFinalincoterm($finalincoterm);
                            }

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                                if ($finalincoterm)
                                    $document->setFinalincoterm($finalincoterm);
                            }


                            if ($commercial_coef && $commercial_coef > 0 && $commercial_coef < 10) {
                                if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') and $user->getContact()->getType() == 4)
                                    $document->setMargecoef($commercial_coef);
                                elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                                    $document->setMargecoef($commercial_coef);
                                } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT'))
                                    if ($document->GetCustomer())
                                        if ($document->GetCustomer()->GetActivity()->getId() == 6)
                                            $document->setMargecoef($commercial_coef);
                            }

                            if (!empty($razselection)) {
                                foreach ($selection as $key => $item)
                                    $selection[$key]['confirm'] = 0;
                            }

                            $document->SetDate(new \DateTime());
                            $document->setGw($gw);
                            $document->setNw($nw);
                            $document->setCbm(round($cbm, 3));
                            $document->setQuantpieces(round($quantpieces));
                            $document->setQuantpackages(round($quantpackages));
                            $document->setTotalvalue(round($totalvalue, 2));
                            $document->setSelection($selection);
                            $document->setOfferslist($offerlist);
                            $document->setSuppliervalue(round($suppliervalue, 2));

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                                $document->SetVisible($visible);

                            // On enregistre le cadencier
                            if (!empty($offerlist)) {
                                if ($document->getLocked() == 1) {
                                    $response = new Response();
                                    $response->setContent(json_encode(array()));
                                    $response->headers->set('Content-Type', 'application/json');
                                    return $response;
                                } else {
                                    $em->persist($document);
                                    $em->flush();
                                    $documentscount++;
                                }
                            }

                            if ($document->GetCommand())
                                $cbm = $this->getDoctrine()->getRepository('appBundle:Command')->GetTotalValue($document->GetCommand()->getId());

                            $containeroccupation = $cbm / $document->getContainer()->getVolume() * 100;


                            $result = array(
                                'Gw' => $document->getGw(),
                                'Nw' => $document->getNw(),
                                'Cbm' => $document->getCbm(),
                                'Quantpieces' => $document->getQuantpieces(),
                                'Quantpackages' => $document->getQuantpackages(),
                                'Totalvalue' => $document->getTotalvalue(),
                                'Containeroccupation' => $containeroccupation,
                                'validation' => $this->isValidSelection($document->getSelection()),
                                'Container' => array(
                                    'volume' => $document->getContainer()->getVolume(),
                                    'name' => $document->getContainer()->getTitle(),
                                )
                            );

                            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                                $result = array_merge($result, array('Totalvaluepi' => $document->getTotalvaluepi()));

                            if ($document->GetCustomer() && $action != "json") {
                                $Historycustomer = new Historycustomer();
                                $Historycustomer->setTitle('Add Cadencier');
                                $Historycustomer->setIco('<i class="icon-book"></i>');
                                $Historycustomer->setDescription('Add Cadencier');
                                $Historycustomer->setDate(new \DateTime());
                                $Historycustomer->setCadencier($document);
                                $Historycustomer->setCustomer($document->getCustomer());
                                $Historycustomer->setUser($user);
                                $em->persist($Historycustomer);
                                $em->flush();
                            }
                        }
                    }
                }
            }

            if ($action == "pi") {
                if ($cadencierid)
                    $document = $this->GetCadencier($cadencierid);

                if (!$document)
                    throw $this->createNotFoundException('Cadencier not found');

                $Command = $document->getCommand();

                if (!$Command) {
                    $Command = new Command();
                    $document->setCommand($Command);

                    if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') and $user->getContact()->getType() != 4 and $user->getContact()->getType() > 0) {
                        $document->setState(0);
                    } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                        $document->setState(2);
                    } else {
                        $document->setState(1);
                    }

                    $Command->setContainer($document->getContainer());
                    $Command->setShipper('-');
                    $Command->setConsignee('-');
                    $Command->setNotify('-');
                    $Command->setFreightpayableat('');
                    $Command->setBookingnumber('-');
                    $Command->setEtd(new \DateTime());
                    $Command->setEta(new \DateTime());
                    $Command->setVessel('-');
                    $Command->setTcnum('-');
                    $Command->setSealnum('-');
                    $Command->setLocked(0);
                    $Command->setModified(new \DateTime());
                    $Command->setCreated(new \DateTime());
                    $em->persist($document);

                    //mailSupplier PI Request;

                    $Historycustomer = new Historycustomer();
                    $Historycustomer->setTitle('ASK PI');
                    $Historycustomer->setDescription('ASK PI');
                    $Historycustomer->setDate(new \DateTime());
                    $Historycustomer->setIco('<i class="icon-book"></i>');
                    $Historycustomer->setCadencier($document);
                    $Historycustomer->setCustomer($document->getCustomer());
                    $Historycustomer->setUser($user);
                    $em->persist($Historycustomer);
                    $em->flush();

                    if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') and $user->getContact()->getType() != 4 and $user->getContact()->getType() > 0) {
                        $this->get('event_dispatcher')->dispatch('sendmail', new GenericEvent($document, array('step' => 'SALERUSERCOMFIRMSELECTION')));
                        $this->get('session')->getFlashBag()->add('notice', 'COMMAND_CREATED');
                        return $this->render('appBundle:Command:confirmationcustomer.html.twig');
                    } else {
                        $this->get('event_dispatcher')->dispatch('sendmail', new GenericEvent($document, array('step' => 'CUSTOMERCONFIRMSELECTION')));
                        $this->get('session')->getFlashBag()->add('notice', 'COMMAND_CREATED');
                        return $this->render('appBundle:Command:confirmation.html.twig');
                    }
                } else {
                    $Command->setModified(new \Datetime());
                    $this->get('session')->getFlashBag()->add('notice', 'COMMAND_UPDATED');
                    return $this->redirect($this->generateUrl('command_show', array('id' => $document->getId())));
                }
            } elseif ($action == "confirm") {

                $document = $this->GetCadencier($cadencierid);
                if (!$document)
                    throw $this->createNotFoundException('Cadencier not found');
                $document->setState(1);
                $document->SetDate(new \DateTime());
                $em->persist($document);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', 'COMMAND_CREATED');
                return $this->render('appBundle:Command:confirmation.html.twig');
            } elseif ($action != "json") {

                if ($documentscount)
                    return $this->redirect($this->generateUrl('cadencier_show', array('id' => $document->getId())));
                else
                    return $this->redirect($this->generateUrl('catalog_v2'));

            }

        }
        $response = new Response();
        $response->setContent(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    private function BuildCadencierForm($supplier, $offers, $cadencier = false)
    {

        //Gal�re pour afficher les cadencier et incoterm...
        $containers = $supplier->GetContainers();
        $containersids = array();
        foreach ($containers as $container)
            $containersids[] = $container->getId();

        $incotermsidsids = array();
        foreach ($offers as $offer) {
            if ($offer->GetFob())
                if (!array_key_exists('fob', $incotermsidsids))
                    $incotermsidsids['fob'] = $this->getDoctrine()->getRepository('appBundle:Incoterm')->findOneById('2');
            if ($offer->GetExw())
                if (!array_key_exists('exw', $incotermsidsids))
                    $incotermsidsids['exw'] = $this->getDoctrine()->getRepository('appBundle:Incoterm')->findOneById('1');
        }

        //Cr�ation du formulaire
        $form = $this->createFormBuilder()
            ->add('containers', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Container',
                'choice_label' => 'title',
                'multiple' => false,
                'expanded' => true,
                'attr' => array('class' => 'order'),
                'query_builder' => function (EntityRepository $er) use ($containersids) {
                    return $er->createQueryBuilder('c')
                        ->where('c.id in ( :containersids )')
                        ->setParameters(array('containersids' => $containersids))
                        ->orderBy('c.title', 'DESC');
                }
            ))
            ->add('incoterms', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Incoterm',
                'choice_label' => 'title',
                'multiple' => false,
                'expanded' => true,
                'attr' => array('class' => 'order'),
                'query_builder' => function (EntityRepository $er) use ($incotermsidsids) {
                    return $er->createQueryBuilder('i')
                        ->where('i.id in ( :incotermsidsids )')
                        ->setParameters(array('incotermsidsids' => array_values($incotermsidsids)))
                        ->orderBy('i.title', 'DESC');
                }
            ))
            ->add('supplierpaymenterms', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Paymentcondition',
                'choice_label' => 'title',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.title', 'ASC');
                }))
            ->add('customerpaymenterms', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Paymentcondition',
                'choice_label' => 'title',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.title', 'ASC');
                }))
            ->getForm();

        //d�finition des valeurs par defaut
        if ($cadencier) {
            $form->get('incoterms')->setData($cadencier->getIncoterm());
            $form->get('containers')->setData($cadencier->getContainer());
            $form->get('supplierpaymenterms')->setData($cadencier->getSupplierpaymenterms());
            $form->get('customerpaymenterms')->setData($cadencier->getCustomerpaymenterms());
        } else {
            $Incoterms = $supplier->getIncoterms();
            $form->get('incoterms')->setData($Incoterms[0]);
            $Containers = $supplier->getContainers();
            $form->get('containers')->setData($Containers[0]);
        }

        return $form;
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @deprecated
     */
    public function deleteAction($id)
    {

        $Cadencier = $this->GetCadencier($id);

        if (!$Cadencier)
            return $this->redirect($this->generateUrl('suppliers_cadenciers'));

        if (!$Cadencier->GetCommand()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($Cadencier);
            $em->flush();
        } else
            $this->get('session')->getFlashBag()->add('warning', 'Please cancel your command first');

        $cadencier = $this->GetOpenedCadencier();
        if (count($cadencier)) {
            $cadencier = $cadencier[0];
            return $this->redirect($this->generateUrl('cadencier_show', array('id' => $cadencier->getId())));
        } else
            return $this->redirect($this->generateUrl('suppliers_cadenciers'));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteV2Action($id)
    {
        $Cadencier = $this->GetCadencier($id);

        if (!$Cadencier)
            return $this->redirect($this->generateUrl('catalog_v2'));

        if (!$Cadencier->GetCommand()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($Cadencier);
            $em->flush();
        } else
            $this->get('session')->getFlashBag()->add('warning', 'Please cancel your command first');

        return $this->redirect($this->generateUrl('catalog_v2'));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @deprecated
     */
    public function deleteallAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            $profil = $this->get('Profils')->get();
            $i = 0;

            foreach ($this->GetOpenedCadencier() as $Cadencier) {
                // Miro (2018-08-07) - removed " && $i <= 10 " from if condition
                if ($Cadencier->getState() === null && $Cadencier->getLocked() === false && $Cadencier->GetCommand() === null ) {
                    if (!$Cadencier->GetCommand()) {
                        $em = $this->getDoctrine()->getManager();
                        $em->remove($Cadencier);
                        $em->flush();
//                        $i++;
                    }
                }
            }
            return $this->redirect($this->generateUrl('suppliers_cadenciers'));
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteallV2Action()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            $profil = $this->get('Profils')->get();
            $i = 0;

            foreach ($this->GetOpenedCadencier() as $Cadencier) {
                // Miro (2018-08-07) - removed " && $i <= 10 " from if condition
                if ($Cadencier->getState() === null && $Cadencier->getLocked() === false && $Cadencier->GetCommand() === null ) {
                    if (!$Cadencier->GetCommand()) {
                        $em = $this->getDoctrine()->getManager();
                        $em->remove($Cadencier);
                        $em->flush();
//                        $i++;
                    }
                }
            }
            return $this->redirect($this->generateUrl('catalog_v2'));
        }
    }

    /*
     * Permet de r�cup�rer les cadenciers dans la barre
     * Copie dans cadencier, home, offer controller
     */

    private function GetOpenedCadencier()
    {
        $profil = ($this->get('profils')->get());
        $userid = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $em = $this->getDoctrine()->getManager()->getRepository('appBundle:Cadencier');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
            return $em->getCommercialCadenciers($userid, $profil['cutomer_id']);
        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT'))
            return $em->getCustomerCadenciers($userid, $profil['cutomer_id']);
    }

    /*
     * R�cup�re le cadencier en fonction des droits
     * Delete, show , generate ACTION
     */

    private function GetCadencier($id)
    {
        $profil = $this->get('Profils')->get();

        $em = $this->getDoctrine()->getRepository('appBundle:Cadencier');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
            return $em->getCommercialCadencier($id);
        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION'))
            return $em->getCommercialCadencier($id);
        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && $profil['cutomer_id']) {
            return $em->getCustomerCadencier($this->get('security.token_storage')->getToken()->getUser()->GetId(), $profil['cutomer_id'], $id);
        } else
            return false;
    }

    private function isValidSelection($selections)
    {
        if (empty($selections) && !is_array($selections)) {
            return false;
        } else {
            foreach ($selections as $selection) {
                if (array_key_exists('confirm', $selection)) {
                    if (empty($selection['confirm'])) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        return true;
    }

}
