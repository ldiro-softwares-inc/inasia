<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Ifsnop\Mysqldump as IMysqldump;

class ToolsController extends Controller
{

    var $copies = array();
    var $sql = array();
    var $errors = array();

    public function dumpAction()
    {
        $this->dst = __SOURCINASIA__ . 'saves/';
        //$this->dumpsql();
        $this->copyDir(__SOURCINASIA__ . 'documents/');
        $this->copyDir(__SOURCINASIA__ . 'htdocs/barcodes/');
        $this->copyDir(__SOURCINASIA__ . 'htdocs/logos/');
        $this->copyDir(__SOURCINASIA__ . 'htdocs/products/');
        $this->WriteLog();

        $response = new Response();
        $response->setContent('');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    private function dumpsql()
    {
        try {
            @mkdir($this->dst . 'sql/');
            $dumpSettingsDefault = array(
                'include-tables' => array(),
                'exclude-tables' => array(),
                'compress' => 'Gzip',
                'no-data' => false,
                'add-drop-table' => false,
                'single-transaction' => true,
                'lock-tables' => false,
                'add-locks' => true,
                'extended-insert' => true,
                'disable-keys' => true,
                'where' => '',
                'no-create-info' => false,
                'skip-triggers' => false,
                'add-drop-trigger' => true,
                'hex-blob' => true,
                'databases' => false,
                'add-drop-database' => false,
                'no-autocommit' => true,
                'default-character-set' => 'utf8',
            );
            $dump = new IMysqldump\Mysqldump('sourcinasia', 'root', 'bidule', 'localhost', 'mysql', $dumpSettingsDefault);
            $dump->start($this->dst . 'sql/' . date('d') . '.gzip');
            $this->sql[date('d-m-Y')] = "Dump sql";
        } catch (\Exception $e) {
            $this->errors[] = "Dump SQL Error :" . $e->getMessage();
        }
    }

    private function copyDir($src)
    {
        try {
            if (file_exists($src)) {
                @mkdir($this->dst . 'files/');
                @mkdir($this->dst . 'files/' . basename($src));
                $this->copy($src, $this->dst . 'files/' . basename($src));
            } else
                throw new Exception('Le repertoire n\'existe pas :' . $src);
        } catch (\Exception $e) {
            print $e->getMessage();
            $this->errors[] = "Errors Copy :" . $e->getMessage();
        }
    }

    private function copy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    $this->copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    if (!file_exists($dst . '/' . $file)) {
                        copy($src . '/' . $file, $dst . '/' . $file);
                        $this->copies[] = $src . '/' . $file;
                    } elseif (filemtime($src . '/' . $file) > filemtime($dst . '/' . $file)) {
                        copy($src . '/' . $file, $dst . '/' . $file);
                        $this->copies[] = $src . '/' . $file;
                    }
                }
            }
        }
        closedir($dir);
    }

    private function WriteLog()
    {

        $file = $this->dst . "/history.log";
        if (!file_exists($file))
            file_put_contents($file, "");


        foreach ($this->copies as $log)
            file_put_contents($file, $log . "\r\n" . file_get_contents($file));

        foreach ($this->errors as $log)
            file_put_contents($file, $log . "\r\n" . file_get_contents($file));

        foreach ($this->sql as $date => $log)
            file_put_contents($file, '[' . $date . ']:' . $log . "\r\n" . file_get_contents($file));

        file_put_contents($file, "Copies :" . count($this->copies) . "\r\n" . file_get_contents($file));
        file_put_contents($file, "Erreurs :" . count($this->errors) . "\r\n" . file_get_contents($file));

        file_put_contents($file, "-------------------------------" . date('F j, Y, g:i a') . "-------------------------------" . "\r\n" . file_get_contents($file));

        if (count($this->errors)) {
            $this->get('mailer')->send(\Swift_Message::newInstance()
                ->setSubject('SOURCINASIA Error Sauvegarde')
                ->setFrom('follow@site-internet-reunion.re')
                ->setTo('alert@site-internet-reunion.re')
                ->setBody(implode('<br>', $this->errors)));
        }
    }

}
