<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use sourcinasia\appBundle\Entity\Supplychaincat;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Supplychaincat controller.
 *
 */
class SupplychaincatController extends Controller
{
    /*
    * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
    */

    /**
     * Lists all Supplychaincat entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Supplychaincat:index.html.twig', array(
            'Supplychaincats' => $Supplychaincat = $this->GetSecuritySupplychaincats()
        ));
    }

    /**
     * Finds and displays a Supplychaincat entity.
     */
    public function showAction($id)
    {
        $Supplychaincat = $this->GetSecuritySupplychaincat($id);

        if (!$Supplychaincat) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Supplychaincat:show.html.twig', array(
            'Supplychaincat' => $Supplychaincat,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Supplychaincat entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Supplychaincat:form.html.twig', array(
            'Supplychaincat' => $this->GetSecuritySupplychaincat(),
            'form' => $this->buildForm($this->GetSecuritySupplychaincat())->createView(),
        ));
    }

    /**
     * Creates a new Supplychaincat entity.
     */
    public function createAction(Request $request)
    {
        $Supplychaincat = $this->GetSecuritySupplychaincat();
        $form = $this->buildForm($Supplychaincat)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Supplychaincat);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('supplychaincat'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Supplychaincat:form.html.twig', array(
            'Supplychaincat' => $Supplychaincat,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Supplychaincat entity.
     *
     */
    public function editAction($id)
    {
        $Supplychaincat = $this->GetSecuritySupplychaincat($id);

        if (!$Supplychaincat)
            throw $this->createNotFoundException('Unable to find Supplychaincat entity.');

        return $this->render('appBundle:Supplychaincat:form.html.twig', array(
            'Supplychaincat' => $Supplychaincat,
            'form' => $this->buildForm($Supplychaincat, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Supplychaincat entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Supplychaincat = $this->GetSecuritySupplychaincat($id);

        if (!$Supplychaincat)
            throw $this->createNotFoundException('Unable to find Supplychaincat entity.');

        $form = $this->buildForm($Supplychaincat, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Supplychaincat);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('supplychaincat'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Supplychaincat:form.html.twig', array(
            'Supplychaincat' => $Supplychaincat,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Supplychaincat entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $Supplychaincat = $this->GetSecuritySupplychaincat($id);

        if (!$Supplychaincat)
            throw $this->createNotFoundException('Unable to find Supplychaincat entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Supplychaincat);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('supplychaincat'));
    }

    /**
     * Creates a form to delete a Supplychaincat entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('supplychaincat_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }


    /**
     * Creates a form to create/edit a Supplychaincat entity.
     */
    private function buildForm(Supplychaincat $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\SupplychaincatType', $entity, array(
                'action' => $this->generateUrl("supplychaincat_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\SupplychaincatType', $entity, array(
                'action' => $this->generateUrl("supplychaincat_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Supplychaincat
     */
    private function GetSecuritySupplychaincats()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Supplychaincat')->findAll();
        else
            throw new AccessDeniedException();
    }

    /**
     * Gets Supplychaincat
     */
    private function GetSecuritySupplychaincat($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Supplychaincat')->find($id);
            else
                return new Supplychaincat();
        else
            throw new AccessDeniedException();
    }
}
