<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use sourcinasia\appBundle\Entity\Image;
use sourcinasia\appBundle\Entity\History;

/**
 * Image controller.
 */
class ImageController extends Controller
{

    /**
     * PluUPLOAD
     */
    public function multiuploadAction(Request $request, $id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            return $this->render('appBundle:Image:multiupload.html.twig', array(
                    'entity' => $this->getDoctrine()->getManager()->getRepository('appBundle:Product')->findOneById($id),
                    'supplier' => $request->query->get('supplier')
                )
            );
        }
    }

    /**
     * PluUPLOAD php upload script
     */
    public function multiuploadAddAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $idproduct = ($request->request->get('product')) ? ($request->request->get('product')) : '';
            if ($idproduct) {
                $product = $this->getDoctrine()->getManager()->getRepository('appBundle:Product')->findOneById($idproduct);
                $setmainimage = $product->GetImages()->isEmpty();

                $entity = new Image();
                $entity->setImage($request->files->get('file'));
                $entity->setproduct($product);
                $entity->setcreated(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);

                if ($setmainimage) {
                    $product->setMainimage($entity->getImage());
                    $em->persist($product);
                }

                $History = new History();
                $History->SetTitle('HISTORY_NEW_IMAGE');
                $History->SetDescription('HISTORY_NEW_IMAGE_DESCRIPTION');
                $History->SetIco('<i class="icon-plus-sign"></i>');
                $History->SetDate(new \DateTime());
                $History->SetProduct($product);
                $History->SetUser($this->get('security.token_storage')->getToken()->getUser());
                $em->persist($History);
                $em->flush();

                die('ok');
            }
        }
    }

    /**
     * Deletes a Image entity.
     */
    public function deleteAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('appBundle:Image')->find($id);

            if (!$entity)
                throw $this->createNotFoundException('Unable to find Image entity.');

            $History = new History();
            $History->SetTitle('HISTORY_DELETE_IMAGE');
            $History->SetDescription('HISTORY_DELETE_IMAGE_DESCRIPTION');
            $History->SetIco('<i class="icon-trash"></i>');
            $History->SetDate(new \DateTime());
            $History->SetProduct($entity->GetProduct());
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);
            $em->flush();

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');
            return $this->redirect($this->generateUrl('product_edit', array('id' => $History->GetProduct()->GetId())));
        }
    }

    /**
     * Rotate a Image entity.
     */
    public function rotateAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('appBundle:Image')->find($id);

            if (!$entity)
                throw $this->createNotFoundException('Unable to find Image entity.');

            $entity->rotateImage();
            $em->flush();

            return $this->redirect($this->generateUrl('product_edit', array('id' => $entity->GetProduct()->GetId())));
        }
    }
}
