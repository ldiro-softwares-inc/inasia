<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Payment;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Payment controller.
 *
 */
class PaymentController extends Controller
{
    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Displays a form to create a new Payment entity.
     */
    public function newAction($customer, $supplier)
    {

        $form = $this->buildForm($this->GetSecurityPayment(), "create", $customer, $supplier);

        return $this->render('appBundle:Payment:form.html.twig', array(
            'Payment' => $this->GetSecurityPayment(),
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Payment entity.
     */
    public function createAction(Request $request, $customer, $supplier)
    {
        $Payment = $this->GetSecurityPayment();
        $form = $this->buildForm($Payment, "create", $customer, $supplier)->handleRequest($request);

        if ($customer)
            $invoice = $this->getDoctrine()->getManager()->getRepository('appBundle:Invoicecustomer')->find($customer);
        elseif ($supplier)
            $invoice = $this->getDoctrine()->getManager()->getRepository('appBundle:Invoicesupplier')->find($supplier);
        else
            throw $this->createNotFoundException('Unable to find si entity.');

        if (!$invoice)
            throw $this->createNotFoundException('Unable to find si entity.');

        if ($form->isValid() && ($customer || $supplier)) {
            $em = $this->getDoctrine()->getManager();

            if ($customer && ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')||$this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')))
                $Payment->setInvoicecustomer($invoice);
            elseif ($supplier && $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')||$this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN'))
                $Payment->setInvoicesupplier($invoice);
            else
                throw $this->createNotFoundException('Unable to find si entity.');

            $em->persist($Payment);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('command_show', array('id' => $invoice->getCadencier()->getId())) . '#Payments');
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Payment:form.html.twig', array(
            'Payment' => $Payment,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Payment entity.
     *
     */
    public function editAction($id)
    {
        $Payment = $this->GetSecurityPayment($id);

        if (!$Payment)
            throw $this->createNotFoundException('Unable to find Payment entity.');

        return $this->render('appBundle:Payment:form.html.twig', array(
            'Payment' => $Payment,
            'form' => $this->buildForm($Payment, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Payment entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Payment = $this->GetSecurityPayment($id);

        if (!$Payment)
            throw $this->createNotFoundException('Unable to find Payment entity.');

        $form = $this->buildForm($Payment, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Payment);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');

            if ($Payment->getInvoicecustomer())
                $invoice = $Payment->getInvoicecustomer();
            elseif ($Payment->getInvoicesupplier())
                $invoice = $Payment->getInvoicesupplier();
            else
                throw $this->createNotFoundException('Unable to find Payment entity.');

            return $this->redirect($this->generateUrl('command_show', array('id' => $invoice->getCadencier()->getId())) . '#Payments');
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Payment:form.html.twig', array(
            'Payment' => $Payment,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Payment entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $Payment = $this->GetSecurityPayment($id);

        if (!$Payment)
            throw $this->createNotFoundException('Unable to find Payment entity.');

        if ($Payment->getInvoicecustomer())
            $id = $Payment->getInvoicecustomer()->getCadencier()->getId();
        elseif ($Payment->getInvoicesupplier())
            $id = $Payment->getInvoicesupplier()->getCadencier()->getId();
        else
            throw $this->createNotFoundException('Unable to find Payment entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Payment);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('command_show', array('id' => $id)) . '#Payments');
    }

    /**
     * Creates a form to create/edit a Payment entity.
     */
    private function buildForm(Payment $entity, $task = "create", $customer = 0, $supplier = 0)
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\PaymentType', $entity, array(
                'action' => $this->generateUrl("payment_create", array("customer" => $customer, "supplier" => $supplier)),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\PaymentType', $entity, array(
                'action' => $this->generateUrl("payment_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets Payment
     */
    private function GetSecurityPayment($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
            if ($id) {
                $Payment = $this->getDoctrine()->getManager()->getRepository('appBundle:Payment')->find($id);

                if ($Payment->getInvoicecustomer() && ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')))
                    return $Payment;
                elseif ($Payment->getInvoicesupplier() && ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') ||$this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')))
                    return $Payment;
                else
                    throw new AccessDeniedException();
            } else
                return new Payment();
        else
            throw new AccessDeniedException();
    }

}
