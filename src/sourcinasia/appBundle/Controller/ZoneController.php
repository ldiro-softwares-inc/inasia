<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use sourcinasia\appBundle\Entity\Zone;

/**
 * Zone controller.
 *
 */
class ZoneController extends Controller
{
    /*
    * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
    */

    /**
     * Lists all Zone entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Zone:index.html.twig', array(
            'Zones' => $Zone = $this->GetSecurityZones()
        ));
    }


    /**
     * Displays a form to create a new Zone entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Zone:form.html.twig', array(
            'Zone' => $this->GetSecurityZone(),
            'form' => $this->buildForm($this->GetSecurityZone())->createView(),
        ));
    }

    /**
     * Creates a new Zone entity.
     */
    public function createAction(Request $request)
    {
        $Zone = $this->GetSecurityZone();
        $form = $this->buildForm($Zone)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Zone);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('zone'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Zone:form.html.twig', array(
            'Zone' => $Zone,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Zone entity.
     *
     */
    public function editAction($id)
    {
        $Zone = $this->GetSecurityZone($id);

        if (!$Zone)
            throw $this->createNotFoundException('Unable to find Zone entity.');

        return $this->render('appBundle:Zone:form.html.twig', array(
            'Zone' => $Zone,
            'form' => $this->buildForm($Zone, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Zone entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Zone = $this->GetSecurityZone($id);

        if (!$Zone)
            throw $this->createNotFoundException('Unable to find Zone entity.');

        $form = $this->buildForm($Zone, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Zone);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('zone'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Zone:form.html.twig', array(
            'Zone' => $Zone,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Zone entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {

        $Zone = $this->GetSecurityZone($id);

        if (!$Zone)
            throw $this->createNotFoundException('Unable to find Zone entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Zone);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');


        return $this->redirect($this->generateUrl('zone'));
    }


    /**
     * Creates a form to create/edit a Zone entity.
     */
    private function buildForm(Zone $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\ZoneType', $entity, array(
                'action' => $this->generateUrl("zone_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\ZoneType', $entity, array(
                'action' => $this->generateUrl("zone_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Zone
     */
    private function GetSecurityZones()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Zone')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Zone.');
    }

    /**
     * Gets Zone
     */
    private function GetSecurityZone($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Zone')->find($id);
            else
                return new Zone();
        else
            throw $this->createNotFoundException('Unable to find Zone.');
    }
}
