<?php

namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\Contact;
use sourcinasia\appBundle\Entity\History;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Contact controller.
 *
 */
class ContactController extends Controller
{

    public function unsuscribeAction(Request $request,$token)
    {
        $contact = $this->getDoctrine()->getRepository('appBundle:Contact')->findOneBy(array('token' => $token));
        if ($contact) {
            $em = $this->getDoctrine()->getManager();

            if ($request->query->get('f')) {
                $contact->setUnsubscribe(null);
                $contact->setFrequencenews((int)$request->query->get('f'));
                $em->persist($contact);
                $em->flush();
            } elseif ($request->query->get('unsub')) {
                $contact->setUnsubscribe(new \DateTime());
                if ($request->query->get('unsub-reason'))
                    $contact->setUnsubscribeReason($request->query->get('unsub-reason'));
                $em->persist($contact);
                $em->flush();
            }
        }
        return $this->render('appBundle:Contact:desabonnement.html.twig', array(
            'token' => $token,
            'unsub' => $request->query->get('unsub'),
            'update' => $request->query->get('f')
        ));
    }

    /**
     * Lists all Contact entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Contact:index.html.twig', array(
            'Contacts' => $Contact = $this->GetSecurityContacts()
        ));
    }

    /**
     * Finds and displays a Contact entity.
     */
    public function showAction($id)
    {
        $Contact = $this->GetSecurityContact($id);

        if (!$Contact) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Contact:show.html.twig', array(
            'Contact' => $Contact,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Contact entity.
     */
    public function newAction(Request $request)
    {

        $Contact = $this->GetSecurityContact();
        $form = $this->buildForm($Contact);
        $supplier = (int)$request->query->get('supplier');
        $customer = (int)$request->query->get('customer');

        if ($supplier) {
            $supplier = $this->getDoctrine()->getManager()->getRepository('appBundle:Supplier')->findOneById($supplier);
            $form->get('supplier')->setData($supplier);
            $form->remove('customer');
        } elseif ($customer) {
            $customer = $this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->findOneById($customer);
            $form->get('customer')->setData($customer);
            $form->remove('supplier');
        }

        return $this->render('appBundle:Contact:form.html.twig', array(
            'Contact' => $Contact,
            'supplier' => $supplier,
            'customer' => $customer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Contact entity.
     */
    public function createAction(Request $request)
    {
        $Contact = $this->GetSecurityContact();
        $form = $this->buildForm($Contact)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $Contact->setExportnbrmax(5);
            $Contact->setToken(md5(date('dmYs')));

            $em->persist($Contact);

            $History = new History();
            $History->SetTitle('HISTORY_NEW_CONTACT');
            $History->SetDescription('HISTORY_NEW_CONTACT_DESCRIPTION' . $Contact->GetName());
            $History->SetIco('<i class="icon-plus-sign"></i>');
            $History->SetDate(new \DateTime());
            if ($form->get('supplier')->getData())
                $History->SetSupplier($Contact->GetSupplier());
            if ($form->get('customer')->getData())
                $History->SetCustomer($Contact->GetCustomer());
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');

            if ($form->get('supplier')->getData())
                return $this->redirect($this->generateUrl('supplier_show', array('id' => $Contact->getSupplier()->getId())) . '#contacts');

            if ($form->get('customer')->getData())
                return $this->redirect($this->generateUrl('customer_show', array('id' => $Contact->getCustomer()->getId())) . '#contacts');
            die('backtohome');
        }

        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Contact:form.html.twig', array(
            'Contact' => $Contact,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Contact entity.
     *
     */
    public function editAction($id)
    {

        $Contact = $this->GetSecurityContact($id);

        if (!$Contact)
            throw $this->createNotFoundException('Unable to find Contact entity.');

        $form = $this->buildForm($Contact, 'edit');

        if ($Contact->GetSupplier())
            $form->remove('customer');
        if ($Contact->GetCustomer()) {
            $form->remove('supplier');

            if ($Contact->GetCustomer()->GetActivity()->getId() == 6) {
                $form->remove('type');
                $form->add('type', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                    'choices' => array(
                        'Agent Co(SOURCINASIA)' => '4',
                    ), 'attr' => array('class' => 'chzn-select'),
                    'choices_as_values' => true
                ));
            } elseif ($Contact->GetCustomer()->getCommercialtools()) {
                $form->remove('type');
                $form->add('type', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                    'choices' => array(
                        'Admin' => '0',
                        'Saler (Saler mode with PO)' => '1',
                        'Saler (without PO)' => '2',
                        'Presentation (only prestation mode)' => '3',
                    ), 'attr' => array('class' => 'chzn-select'),
                    'choices_as_values' => true
                ));
            } else {
                $form->remove('type');
                $form->add('type', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                    'choices' => array(
                        'Customer' => '0',
                    ), 'attr' => array('class' => 'chzn-select'),
                    'choices_as_values' => true
                ));
            }
        }

        return $this->render('appBundle:Contact:form.html.twig', array(
            'Contact' => $Contact,
            'form' => $form->createView()
        ));
    }

    /**
     * Edits an existing Contact entity.
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $Contact = $this->GetSecurityContact($id);

        if (!$Contact)
            throw $this->createNotFoundException('Unable to find Contact entity.');

        $form = $this->buildForm($Contact, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($Contact);
            $em->flush();

            // If Conctact is linked to user update user Email
            if ($Contact->getEmail() && $Contact->getUser()) {
                $user = $Contact->getUser();
                $user->setEmail($Contact->getEmail());
                $em->persist($user);
                $em->flush();
            }

            $History = new History();
            $History->SetTitle('Modification  contact ');
            $History->SetDescription('Modification du contact : ' . $Contact->GetName());
            $History->SetIco('<i class="icon-pencil"></i>');
            $History->SetDate(new \DateTime());
            if ($form->get('supplier')->getData())
                $History->SetSupplier($Contact->GetSupplier());
            if ($form->get('customer')->getData())
                $History->SetCustomer($Contact->GetCustomer());
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');

            if ($form->get('supplier')->getData())
                return $this->redirect($this->generateUrl('supplier_show', array('id' => $Contact->getSupplier()->getId())) . '#contacts');

            if ($form->get('customer')->getData())
                return $this->redirect($this->generateUrl('customer_show', array('id' => $Contact->getCustomer()->getId())) . '#contacts');

        }

        return $this->render('appBundle:Contact:form.html.twig', array(
            'Contact' => $Contact,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Contact entity.
     *
     */
    public function deleteAction($id)
    {

        $Contact = $this->GetSecurityContact($id);

        if (!$Contact)
            throw $this->createNotFoundException('Unable to find Contact entity.');

        $idsupplier = 0;
        $idcustomer = 0;

        if ($Contact->GetSupplier())
            $idsupplier = $Contact->GetSupplier()->GetId();
        if ($Contact->GetCustomer())
            $idcustomer = $Contact->GetCustomer()->GetId();

        $em = $this->getDoctrine()->getManager();

        if ($Contact->GetUser()) {
            $user = $Contact->GetUser();
            if ($user->isEnabled()) {
                $this->get('session')->getFlashBag()->add('warning', 'TO DELETE CONTACT PLEASE FIRST REMOVE USER');
            } else {
                $orders = 0;
                foreach ($user->getCadenciers() as $cadencier)
                    if ($cadencier->getCommand())
                        $orders++;

                if ($orders == 0) {
                    $em->remove($user);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');
                } else
                    $this->get('session')->getFlashBag()->add('warning', 'CAN NOT DELETE USER BECAUSE ' . $orders . ' ORDER' . ($orders > 1 ? 'S  ARE' : ' IS') . ' LINKED');
            }
        } else {
            $em->remove($Contact);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');
        }

        if ($idsupplier)
            return $this->redirect($this->generateUrl('supplier_show', array('id' => $idsupplier)) . '#contacts');
        if ($idcustomer)
            return $this->redirect($this->generateUrl('customer_show', array('id' => $idcustomer)) . '#contacts');
        die;
        return $this->redirect($this->generateUrl('home'));
    }

    /**
     * Creates a form to delete a Contact entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contact_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Contact entity.
     */
    private function buildForm(Contact $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\ContactType', $entity, array(
                'action' => $this->generateUrl("contact_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\ContactType', $entity, array(
                'action' => $this->generateUrl("contact_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Contacts
     */
    private function GetSecurityContacts()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Contact')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Contact.');
    }

    /**
     * Gets Suppliers
     */
    private function GetSecurityContact($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Contact')->find($id);
            else
                return new Contact();
        else
            throw $this->createNotFoundException('Unable to find Contact.');
    }

}
