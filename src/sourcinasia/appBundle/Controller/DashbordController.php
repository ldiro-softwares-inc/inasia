<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Dashbord controller.
 *
 */
class DashbordController extends Controller
{

    public function indexAction(Request $request)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {

            if ($request->query->get('start'))
                $start = new \datetime(date('d-m-Y', strtotime($request->query->get('start'))));
            else {
                $start = new \datetime();
                $start->modify('-1 month');
            }
            if ($request->query->get('start'))
                $stop = new \datetime(date('d-m-Y', strtotime($request->query->get('stop'))));
            else
                $stop = new \datetime();

            if ($start > $stop) {
                $start = new \datetime();
                $start->modify('-1 month');
                $stop = new \datetime();
            }

            $histories = $this->getDoctrine()->getRepository('appBundle:History')->findBy(array(), array('date' => 'desc'), 100, 0);
            $Useractivity = $this->getDoctrine()->getRepository('appBundle:Historycustomer')->findBy(array(), array('date' => 'desc'), 100, 0);


            $InvoiceCustomers = array();
            foreach ($this->getDoctrine()->getRepository('appBundle:Invoicecustomer')->findAll() as $InvoicecustomersRequest) {
                $Payments = 0;
                foreach ($InvoicecustomersRequest->getPayments() as $Payment) {
                    $Payments += $Payment->getAmount();
                }

                if ($Payments < $InvoicecustomersRequest->getAmount()) {
                    $InvoiceCustomers[] = $InvoicecustomersRequest;
                }
            }

            return $this->render('appBundle:Dashbord:index.html.twig', array(
                    'start' => $start->format('d-m-Y'),
                    'stop' => $stop->format('d-m-Y'),
                    'InvoiceCustomers' => $InvoiceCustomers


                    /*'graphs' => array(
                        'graphqs' => $this->assemblate($this->getDoctrine()->getRepository('appBundle:Cadencier')->GetQsGraph($start, $stop), $start, $stop),
                        'graphlogin' => $this->assemblate($this->getDoctrine()->getRepository('appBundle:Cadencier')->GetLoginGraph($start, $stop), $start, $stop),
                        'exportedselection' => $this->assemblate($this->getDoctrine()->getRepository('appBundle:Cadencier')->GetexportedselectionGraph($start, $stop), $start, $stop),
                        'graphnewproduct' => $this->assemblate($this->getDoctrine()->getRepository('appBundle:Cadencier')->GetNewProductGraph($start, $stop), $start, $stop),
                        'grapheditproduct' => $this->assemblate($this->getDoctrine()->getRepository('appBundle:Cadencier')->GetNewProductGraph($start, $stop), $start, $stop),
                        'graphpi' => $this->assemblate($this->getDoctrine()->getRepository('appBundle:Cadencier')->GetPiGraph($start, $stop), $start, $stop),
                        'graphcommandes' => $this->assemblate($this->getDoctrine()->getRepository('appBundle:Cadencier')->GetCommandesGraph($start, $stop), $start, $stop))*/)
            );
        }
    }

    public function journalAction()
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $histories = $this->getDoctrine()->getRepository('appBundle:History')->findBy(array(), array('date' => 'desc'), 100, 0);
            $Useractivity = $this->getDoctrine()->getRepository('appBundle:Historycustomer')->findBy(array(), array('date' => 'desc'), 100, 0);

            return $this->render('appBundle:Dashbord:journal.html.twig', array(
                'histories' => $histories,
                'useractivity' => $Useractivity
            ));
        }
    }

    private function assemblate($values, $start, $stop)
    {

        $calendar = array();
        $x = clone $start;
        while ($x <= $stop) {
            $calendar[$x->format('Y-m-d')] = '{"x": "' . $x->format('Y-m-d') . '","y": 0}';
            $x->add(new \DateInterval('P1D'));
        }

        foreach ($values as $value)
            if (array_key_exists($value['date'], $calendar))
                $calendar[$value['date']] = '{"x": "' . $value['date'] . '","y": ' . (int)$value['value'] . '}';

        return $calendar;
    }

}
