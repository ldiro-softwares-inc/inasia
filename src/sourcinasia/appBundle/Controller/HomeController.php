<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{

    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getRepository('appBundle:Offer');
        $DealsList = array();
        $LastList = array();
        $orders = array();

        $profil = $this->get('Profils')->get();
        //if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') &&  !$profil['cutomer_id'])
        //    $profil = $this->get('profils')->initProfil();

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') or $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') or $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $DealsList = $em->DealsList($profil, $request->getLocale());
            $LastList = $em->LastList($profil, $request->getLocale());
        }
        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT'))
            $orders = $this->getDoctrine()->getRepository('appBundle:Command')->LastOrders($profil);
        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') && $profil['cutomer_id'])
            $orders = $this->getDoctrine()->getRepository('appBundle:Command')->LastOrders($profil);
        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
            $orders = $this->getDoctrine()->getRepository('appBundle:Command')->LastOrders($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'));

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $histories = $this->getDoctrine()->getRepository('appBundle:History')->findBy(array(), array('date' => 'desc'), 100, 0);
        } else
            $histories = array();

        if ($profil)
            return $this->render('appBundle:Home:index.html.twig', array(
                'deals' => $DealsList,
                'lasts' => $LastList,
                'orders' => $orders,
                'histories' => $histories,
                'openCadenciers' => $this->GetOpenedCadencier(),
                'active' => 'home'
            ));
    }

    /*
     * Permet de r�cup�rer les cadenciers dans la barre
     * Copie dans cadencier, home, offer controller
     */

    private function GetOpenedCadencier()
    {
        $profil = ($this->get('Profils')->get());
        $userid = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $em = $this->getDoctrine()->getManager()->getRepository('appBundle:Cadencier');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
            return $em->getCommercialCadenciers($userid, $profil['cutomer_id']);
        elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT'))
            return $em->getCustomerCadenciers($userid, $profil['cutomer_id']);
    }

}
