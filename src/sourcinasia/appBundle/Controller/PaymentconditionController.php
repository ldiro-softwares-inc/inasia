<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Paymentcondition;

/**
 * Paymentcondition controller.
 *
 */
class PaymentconditionController extends Controller
{
    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Paymentcondition entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Paymentcondition:index.html.twig', array(
            'Paymentconditions' => $Paymentcondition = $this->GetSecurityPaymentconditions()
        ));
    }

    /**
     * Finds and displays a Paymentcondition entity.
     */
    public function showAction($id)
    {
        $Paymentcondition = $this->GetSecurityPaymentcondition($id);

        if (!$Paymentcondition) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Paymentcondition:show.html.twig', array(
            'Paymentcondition' => $Paymentcondition,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Paymentcondition entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Paymentcondition:form.html.twig', array(
            'Paymentcondition' => $this->GetSecurityPaymentcondition(),
            'form' => $this->buildForm($this->GetSecurityPaymentcondition())->createView(),
        ));
    }

    /**
     * Creates a new Paymentcondition entity.
     */
    public function createAction(Request $request)
    {
        $Paymentcondition = $this->GetSecurityPaymentcondition();
        $form = $this->buildForm($Paymentcondition)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Paymentcondition);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('paymentcondition'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Paymentcondition:form.html.twig', array(
            'Paymentcondition' => $Paymentcondition,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Paymentcondition entity.
     *
     */
    public function editAction($id)
    {
        $Paymentcondition = $this->GetSecurityPaymentcondition($id);

        if (!$Paymentcondition)
            throw $this->createNotFoundException('Unable to find Paymentcondition entity.');

        return $this->render('appBundle:Paymentcondition:form.html.twig', array(
            'Paymentcondition' => $Paymentcondition,
            'form' => $this->buildForm($Paymentcondition, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Paymentcondition entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Paymentcondition = $this->GetSecurityPaymentcondition($id);

        if (!$Paymentcondition)
            throw $this->createNotFoundException('Unable to find Paymentcondition entity.');

        $form = $this->buildForm($Paymentcondition, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Paymentcondition);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('paymentcondition'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Paymentcondition:form.html.twig', array(
            'Paymentcondition' => $Paymentcondition,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Paymentcondition entity.
     *
     */
    public function deleteAction($id)
    {
        $Paymentcondition = $this->GetSecurityPaymentcondition($id);

        if (!$Paymentcondition)
            throw $this->createNotFoundException('Unable to find Paymentcondition entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Paymentcondition);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('paymentcondition'));
    }

    /**
     * Creates a form to delete a Paymentcondition entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('paymentcondition_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Paymentcondition entity.
     */
    private function buildForm(Paymentcondition $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\PaymentconditionType', $entity, array(
                'action' => $this->generateUrl("paymentcondition_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\PaymentconditionType', $entity, array(
                'action' => $this->generateUrl("paymentcondition_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Paymentcondition
     */
    private function GetSecurityPaymentconditions()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Paymentcondition')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Paymentcondition.');
    }

    /**
     * Gets Paymentcondition
     */
    private function GetSecurityPaymentcondition($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Paymentcondition')->find($id);
            else
                return new Paymentcondition();
        else
            throw $this->createNotFoundException('Unable to find Paymentcondition.');
    }

}
