<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Currency;

/**
 * Currency controller.
 *
 */
class CurrencyController extends Controller
{

    /**
     * Lists all Currency entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Currency:index.html.twig', array(
            'Currencys' => $Currency = $this->GetSecurityCurrencies()
        ));
    }

    /**
     * Finds and displays a Currency entity.
     */
    public function showAction($id)
    {
        $Currency = $this->GetSecurityCurrency($id);

        if (!$Currency) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Currency:show.html.twig', array(
            'Currency' => $Currency,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Currency entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Currency:form.html.twig', array(
            'Currency' => $this->GetSecurityCurrency(),
            'form' => $this->buildForm($this->GetSecurityCurrency())->createView(),
        ));
    }

    /**
     * Creates a new Currency entity.
     */
    public function createAction(Request $request)
    {
        $Currency = $this->GetSecurityCurrency();
        $form = $this->buildForm($Currency)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Currency);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'CREATED!');
            return $this->redirect($this->generateUrl('currency'));
        }
        $this->get('session')->getFlashBag()->add('error', 'NOT CREATED!');
        return $this->render('appBundle:Currency:form.html.twig', array(
            'Currency' => $Currency,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Currency entity.
     *
     */
    public function editAction($id)
    {
        $Currency = $this->GetSecurityCurrency($id);

        if (!$Currency)
            throw $this->createNotFoundException('Unable to find Currency entity.');

        return $this->render('appBundle:Currency:form.html.twig', array(
            'Currency' => $Currency,
            'form' => $this->buildForm($Currency, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Currency entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Currency = $this->GetSecurityCurrency($id);

        if (!$Currency)
            throw $this->createNotFoundException('Unable to find Currency entity.');

        $form = $this->buildForm($Currency, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Currency);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'UPDATED!');
            return $this->redirect($this->generateUrl('currency'));
        }
        $this->get('session')->getFlashBag()->add('error', 'NOT UPDATED!');

        return $this->render('appBundle:Currency:form.html.twig', array(
            'Currency' => $Currency,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Currency entity.
     *
     */
    public function deleteAction($id)
    {

        $Currency = $this->GetSecurityCurrency($id);

        if (!$Currency)
            throw $this->createNotFoundException('Unable to find Currency entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Currency);
        $em->flush();

        return $this->redirect($this->generateUrl('currency'));
    }

    /**
     * Creates a form to delete a Currency entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('currency_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Currency entity.
     */
    private function buildForm(Currency $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\CurrencyType', $entity, array(
                'action' => $this->generateUrl("currency_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\CurrencyType', $entity, array(
                'action' => $this->generateUrl("currency_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Currencys
     */
    private function GetSecurityCurrencies()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Currency')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Currency.');
    }

    /**
     * Gets Suppliers
     */
    private function GetSecurityCurrency($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Currency')->find($id);
            else
                return new Currency();
        else
            throw $this->createNotFoundException('Unable to find Currency.');
    }

}
