<?php

namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Categorie controller.
 *
 */
class CategorieController extends Controller
{


    /**
     *  Affiche dans le catalogue les offres lié à une requete
     */
    public function requestsAction(Request $request)
    {

        $request = $request->query->get('request');

        if (!is_array($request)) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        $Categories = $this->getDoctrine()
            ->getRepository('appBundle:Categorie')
            ->seach(array_filter($request), $this->get('Profils')->get());

        $response = new Response();
        //return $response->setContent('<html><head></head><body>test</body></html>'); 

        $session = $this->get('session');
        if (array_key_exists('departementid', $request)) {
            $session->set('departementFilter', $request['departementid']);
        } else {
            $session->remove('departementFilter');
        }

        if (array_key_exists('shelvesid', $request)) {
            $session->set('shelvesFilter', $request['shelvesid']);
        } else {
            $session->remove('shelvesFilter');
        }

        if (array_key_exists('famillyid', $request)) {
            $session->set('famillyFilter', $request['famillyid']);
        } else {
            $session->remove('famillyFilter');
        }

        if (array_key_exists('subfamillyid', $request)) {
            $session->set('subfamillyFilter', $request['subfamillyid']);
        } else {
            $session->remove('subfamillyFilter');
        }

        return $this->render('appBundle:Categorie:list.html.twig', array(
            'Categories' => $Categories,
        ));
    }


    /*
     * Retourne le json 
     */

    public function filtersAction(Request $request, $filter)
    {
        if (!is_array($request->query->get('request'))) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        $content = $this->getDoctrine()
            ->getRepository('appBundle:Categorie')
            ->seach(array_filter($request->query->get('request')), $this->get('Profils')->get(), $filter);

        if ($filter == "nomenclature")
            $content = $this->getDoctrine()
                ->getRepository('appBundle:Categorie')
                ->getCategroies(array_filter($content), $request->getLocale());

        $response = new Response();
        $response->setContent(json_encode($content));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Categorie entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Categorie:index.html.twig', array(
            'session' => $this->get('session'),
            'Categories' => $Categorie = $this->GetSecurityCategories()
        ));
    }

    /**
     * Finds and displays a Categorie entity.
     */
    public function showAction($id)
    {
        $Categorie = $this->GetSecurityCategorie($id);

        if (!$Categorie) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Categorie:show.html.twig', array(
            'Categorie' => $Categorie,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Categorie entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Categorie:form.html.twig', array(
            'Categorie' => $this->GetSecurityCategorie(),
            'form' => $this->buildForm($this->GetSecurityCategorie())->createView(),
        ));
    }

    /**
     * Creates a new Categorie entity.
     */
    public function createAction(Request $request)
    {
        $Categorie = $this->GetSecurityCategorie();
        $form = $this->buildForm($Categorie)->handleRequest($request);


        if ($form->isValid()) {

            switch ($Categorie->getType()) {
                case 'SEC':
                    if (strlen($Categorie->getId()) == 2) {
                        $Categorie->setDepartment($Categorie->getId());
                        $Categorie->setShelves('00');
                        $Categorie->setFamilly('000');
                        $Categorie->setSubfamilly('000');
                    } else {
                        $this->get('session')->getFlashBag()->add('warning', 'Key error');
                        return $this->render('appBundle:Categorie:form.html.twig', array(
                            'Categorie' => $Categorie,
                            'form' => $form->createView()));
                    }
                    break;
                case 'RAY':
                    if (strlen($Categorie->getId()) == 4) {
                        $Categorie->setDepartment(substr($Categorie->getId(), 0, 2));
                        $Categorie->setShelves(substr($Categorie->getId(), 2, 2));
                        $Categorie->setFamilly('000');
                        $Categorie->setSubfamilly('000');
                    } else {
                        $this->get('session')->getFlashBag()->add('warning', 'Key error');
                        return $this->render('appBundle:Categorie:form.html.twig', array(
                            'Categorie' => $Categorie,
                            'form' => $form->createView()));
                    }
                    break;
                case 'FAM':
                    if (strlen($Categorie->getId()) == 7) {
                        $Categorie->setDepartment(substr($Categorie->getId(), 0, 2));
                        $Categorie->setShelves(substr($Categorie->getId(), 2, 2));
                        $Categorie->setFamilly(substr($Categorie->getId(), 4, 3));
                        $Categorie->setSubfamilly('000');
                    } else {
                        $this->get('session')->getFlashBag()->add('warning', 'Key error');
                        return $this->render('appBundle:Categorie:form.html.twig', array(
                            'Categorie' => $Categorie,
                            'form' => $form->createView()));
                    }
                    break;
                case 'SSF':
                    if (strlen($Categorie->getId()) == 10) {
                        $Categorie->setDepartment(substr($Categorie->getId(), 0, 2));
                        $Categorie->setShelves(substr($Categorie->getId(), 2, 2));
                        $Categorie->setFamilly(substr($Categorie->getId(), 4, 3));
                        $Categorie->setSubfamilly(substr($Categorie->getId(), 7, 3));
                        $Categorie->SetId($Categorie->getId());
                    } else {
                        $this->get('session')->getFlashBag()->add('notice', 'Key error');
                        return $this->render('appBundle:Categorie:form.html.twig', array(
                            'Categorie' => $Categorie,
                            'form' => $form->createView()));
                    }
                    break;
            }

            $Categorie->SetId($Categorie->getDepartment() . $Categorie->getShelves() . $Categorie->getFamilly() . $Categorie->getSubfamilly());
            $Categorie->setRestriction();
            $em = $this->getDoctrine()->getManager();
            $em->persist($Categorie);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('categorie'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Categorie:form.html.twig', array(
            'Categorie' => $Categorie,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Categorie entity.
     *
     */
    public function editAction($id)
    {
        $Categorie = $this->GetSecurityCategorie($id);

        if (!$Categorie)
            throw $this->createNotFoundException('Unable to find Categorie entity.');

        return $this->render('appBundle:Categorie:form.html.twig', array(
            'Categorie' => $Categorie,
            'form' => $this->buildForm($Categorie, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Categorie entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Categorie = $this->GetSecurityCategorie($id);

        if (!$Categorie)
            throw $this->createNotFoundException('Unable to find Categorie entity.');

        $form = $this->buildForm($Categorie, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $Categorie->setDepartment(substr($Categorie->getId(), 0, 2));
            $Categorie->setShelves(substr($Categorie->getId(), 2, 2));
            $Categorie->setFamilly(substr($Categorie->getId(), 4, 3));
            $Categorie->setSubfamilly(substr($Categorie->getId(), 7, 3));
            $Categorie->setRestriction();
            if ($Categorie->getSubfamilly() == "000")
                $Categorie->setMargesourcin('0');

            $em = $this->getDoctrine()->getManager();
            $em->persist($Categorie);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('categorie'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Categorie:form.html.twig', array(
            'Categorie' => $Categorie,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Categorie entity.
     *
     */
    public function deleteAction($id)
    {

        $Categorie = $this->GetSecurityCategorie($id);

        if (!$Categorie)
            throw $this->createNotFoundException('Unable to find Categorie entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Categorie);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('categorie'));
    }

    /**
     * Creates a form to delete a Categorie entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categorie_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Categorie entity.
     */
    private function buildForm(Categorie $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\CategorieType', $entity, array(
                'action' => $this->generateUrl("categorie_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\CategorieType', $entity, array(
                'action' => $this->generateUrl("categorie_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Categorie
     */
    private function GetSecurityCategories()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Categorie')->getCategorielist();
        else
            throw $this->createNotFoundException('Unable to find Categorie.');
    }

    /**
     * Gets Categorie
     */
    private function GetSecurityCategorie($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Categorie')->find($id);
            else
                return new Categorie();
        else
            throw $this->createNotFoundException('Unable to find Categorie.');
    }

}
