<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Pol;
/**
 * Pol controller.
 *
 */
class PolController extends Controller
{

    /**
     * Lists all Pol entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Pol:index.html.twig', array(
            'Pols' => $Pol = $this->GetSecurityPols()
        ));
    }

    /**
     * Finds and displays a Pol entity.
     */
    public function showAction($id)
    {
        $Pol = $this->GetSecurityPol($id);

        if (!$Pol) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Pol:show.html.twig', array(
            'Pol' => $Pol,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Pol entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Pol:form.html.twig', array(
            'Pol' => $this->GetSecurityPol(),
            'form' => $this->buildForm($this->GetSecurityPol())->createView(),
        ));
    }

    /**
     * Creates a new Pol entity.
     */
    public function createAction(Request $request)
    {
        $Pol = $this->GetSecurityPol();
        $form = $this->buildForm($Pol)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Pol);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'CREATED!');
            return $this->redirect($this->generateUrl('pol'));
        }
        $this->get('session')->getFlashBag()->add('error', 'NOT CREATED!');
        return $this->render('appBundle:Pol:form.html.twig', array(
            'Pol' => $Pol,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Pol entity.
     *
     */
    public function editAction($id)
    {
        $Pol = $this->GetSecurityPol($id);

        if (!$Pol)
            throw $this->createNotFoundException('Unable to find Pol entity.');

        return $this->render('appBundle:Pol:form.html.twig', array(
            'Pol' => $Pol,
            'form' => $this->buildForm($Pol, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Pol entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Pol = $this->GetSecurityPol($id);

        if (!$Pol)
            throw $this->createNotFoundException('Unable to find Pol entity.');

        $form = $this->buildForm($Pol, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Pol);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'UPDATED!');
            return $this->redirect($this->generateUrl('pol'));
        }
        $this->get('session')->getFlashBag()->add('error', 'NOT UPDATED!');

        return $this->render('appBundle:Pol:form.html.twig', array(
            'Pol' => $Pol,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Pol entity.
     *
     */
    public function deleteAction($id)
    {

        $Pol = $this->GetSecurityPol($id);

        if (!$Pol)
            throw $this->createNotFoundException('Unable to find Pol entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Pol);
        $em->flush();

        return $this->redirect($this->generateUrl('pol'));
    }

    /**
     * Creates a form to delete a Pol entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pol_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Pol entity.
     */
    private function buildForm(Pol $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\PolType', $entity, array(
                'action' => $this->generateUrl("pol_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\PolType', $entity, array(
                'action' => $this->generateUrl("pol_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Pols
     */
    private function GetSecurityPols()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADDPOL'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Pol')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Pol.');
    }

    /**
     * Gets Suppliers
     */
    private function GetSecurityPol($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADDPOL'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Pol')->find($id);
            else
                return new Pol();
        else
            throw $this->createNotFoundException('Unable to find Pol.');
    }

}
