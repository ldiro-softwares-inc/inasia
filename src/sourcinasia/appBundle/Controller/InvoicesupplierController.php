<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Invoicesupplier;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Invoicesupplier controller.
 *
 */
class InvoicesupplierController extends Controller
{
    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Displays a form to edit an existing Invoicesupplier entity.
     *
     */
    public function editAction($id)
    {
        $Invoicesupplier = $this->GetSecurityInvoicesupplier($id);

        if (!$Invoicesupplier)
            throw $this->createNotFoundException('Unable to find Invoicesupplier entity.');

        return $this->render('appBundle:Invoicesupplier:form.html.twig', array(
            'Invoicesupplier' => $Invoicesupplier,
            'form' => $this->buildForm($Invoicesupplier, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Invoicesupplier entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Invoicesupplier = $this->GetSecurityInvoicesupplier($id);

        if (!$Invoicesupplier)
            throw $this->createNotFoundException('Unable to find Invoicesupplier entity.');

        $form = $this->buildForm($Invoicesupplier, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Invoicesupplier);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('command_show', array('id' => $Invoicesupplier->getCadencier()->getId())) . '#Payments');
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Invoicesupplier:form.html.twig', array(
            'Invoicesupplier' => $Invoicesupplier,
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a form to create/edit a Invoicesupplier entity.
     */
    private function buildForm(Invoicesupplier $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\InvoicesupplierType', $entity, array(
                'action' => $this->generateUrl("invoicesupplier_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\InvoicesupplierType', $entity, array(
                'action' => $this->generateUrl("invoicesupplier_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets Invoicesupplier
     */
    private function GetSecurityInvoicesupplier($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Invoicesupplier')->find($id);
            else
                return new Invoicesupplier();
        else
            throw new AccessDeniedException();
    }

}
