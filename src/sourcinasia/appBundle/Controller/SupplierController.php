<?php

namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\Document;
use sourcinasia\appBundle\Entity\History;
use sourcinasia\appBundle\Entity\Image;
use sourcinasia\appBundle\Entity\Offer;
use sourcinasia\appBundle\Entity\Product;
use sourcinasia\appBundle\Entity\Supplier;
use sourcinasia\appBundle\Services\Read\ReadCsv;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Supplier controller.
 *
 */
class SupplierController extends Controller
{
    /**
     * PluUPLOAD
     */
    public function importimagesformAction($supplier)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            return $this->render('appBundle:Image:multiuploadimages.html.twig', array(
                    'Supplier' => $this->getDoctrine()->getManager()->getRepository('appBundle:Supplier')->findOneById($supplier)
                )
            );
        }
    }

    /**
     * PluUPLOAD php upload script
     */
    public function importimagesfileAction(Request $request, supplier $supplier)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $name = explode('-', str_replace(['.jpg', '.jpeg', '.png', '.gif'], '', $request->files->get('file')->getClientOriginalName()));
            if (count($name) > 1) {
                $last = end($name);
                $lastkey = key($name);
                if (strlen($name[$lastkey]) == 1 && (int)$name[$lastkey]) {
                    unset($name[$lastkey]);
                }
            }

            if ($offer = $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->findOneBy(['supplier' => $supplier->getId(), 'itemnumber' => implode('-', $name)])) {
                $setmainimage = $offer->getProduct()->GetImages()->isEmpty();
                $Image = new Image();
                $Image->setImage($request->files->get('file'));
                $Image->setproduct($offer->getProduct());
                $Image->setcreated(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($Image);
                if ($setmainimage) {
                    $offer->getProduct()->setMainimage($Image->getImage());
                    $em->persist($offer->getProduct());
                }
                $em->flush();

                $History = new History();
                $History->SetTitle('HISTORY_NEW_IMAGE');
                $History->SetDescription('HISTORY_NEW_IMAGE_DESCRIPTION');
                $History->SetIco('<i class="icon-plus-sign"></i>');
                $History->SetDate(new \DateTime());
                $History->SetProduct($offer->getProduct());
                $History->SetUser($this->get('security.token_storage')->getToken()->getUser());
                $em->persist($History);
                $em->flush();

                print 1;
                die;
            }
            throw $this->createNotFoundException('No offer ' . implode('-', $name));
        }
        throw $this->createNotFoundException('Denied .');
    }


    /*
     * Products imporation
     */

    public function importAction(Request $request, $supplier)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            ini_set('max_execution_time', 480);
            ini_set('memory_limit', '600M');
            $entity = $this->getDoctrine()->getManager()->getRepository('appBundle:Supplier')->findOneById($supplier);
            $post = ($request->getMethod() == 'POST');
            $form = $this->createFormBuilder()
                ->add('startline', 'Symfony\Component\Form\Extension\Core\Type\IntegerType', array(
                    'data' => '1'))
                ->add('attachment', 'Symfony\Component\Form\Extension\Core\Type\FileType')
                ->getForm();
            if ($post) { //upload step
                $form->handleRequest($request);
                $datas = $request->request->get('datas');
                $em = $this->getDoctrine()->getManager();
                $importxls = $this->get('Importxls');
                $importxls->SetPol($this->getDoctrine()->getManager()->getRepository('appBundle:Pol')->getArrayDatas());
                $importxls->setunits($this->getDoctrine()->getManager()->getRepository('appBundle:Unit')->getArrayDatas());
                $importxls->setcurrencies($this->getDoctrine()->getManager()->getRepository('appBundle:Currency')->getArrayDatas());
                if (!$datas) { //file is posted
                    if ($form->isValid()) {
                        $importxls->importRequest($form, $request->request->get('form'), ($entity->getPol()) ? $entity->getPol()->getTitle() : array(), $em->getRepository('appBundle:Categorie')->Allids());
                        if ($importxls->isallowExt()) { //move to tmp + send  (datas + images + check supplieritemnumer stored) into json
                            $importxls->moveTmpDir();
                            $datas = $importxls->ExctactRows();
                            $images = $importxls->ExctactImages();
                            $updateoffer = $this->getDoctrine()
                                ->getRepository('appBundle:Offer')
                                ->checksupplieritemnumber($entity->GetId(), $datas['supplieritemnumber']);

                            $datas = '[' . json_encode($datas['datas']) . ']';
                            $updateoffer = '[' . json_encode($updateoffer) . ']';
                            //$this->get('session')->getFlashBag()->add('notice', 'Fichier correctement lu');
                        } else {
                            $this->get('session')->getFlashBag()->add('warning', 'Format de fichier errone : ' . $importxls->ext . '. Merci de fournir un xlsx.');
                            $post = false;
                        }
                    }
                } else {
                    $importxls->getJsonPostedDatas($datas, $request->request->get('images'));
                    $create = 0;
                    $update = 0;
                    foreach ($importxls->datas as $line => $data) {
                        $offer = $this->getDoctrine()
                            ->getRepository('appBundle:Offer')
                            ->findOneBy(array('itemnumber' => $data->supplier_item_number, 'supplier' => $entity->GetId()));

                        $oldexw = "";
                        $oldfob = "";
                        if (empty($offer)) {
                            $offer = new Offer();
                            /* Imported file doesn't have this field:  Undefined property: stdClass::$sourcinasia_n */
                            /*
                                                        $product = $this->getDoctrine()
                                                            ->getRepository('appBundle:Product')
                                                            ->findOneBy(array('id' => $data->sourcinasia_n));

                                                        if (!$product) */
                            $product = new Product();

                            $updatecount = false;
                        } else {
                            $oldexw = $offer->getExw();
                            $oldcurrency = $offer->getCurrency()->getTitle();
                            $oldexwdate = $offer->getExwusdmodificationdate();
                            $oldmoq = $offer->getMoq();
                            $oldfob = $offer->getFob();
                            $oldfobdate = $offer->getFobmodificationdate();
                            $product = $offer->GetProduct();
                            $updatecount = true;
                        }

                        $archive = $offer->getArchive();

                        list($product, $offer) = $importxls->PrepareEntites($data, $product, $offer, $entity, $this->getDoctrine());

                        if ($oldexw != $offer->getExwusd()) {
                            if (empty($archive["exw"]) && !empty($oldexwdate)) {
                                $archive["exw"][$oldexwdate->format('d-m-Y')]['p'] = $oldexw;
                                $archive["exw"][$oldexwdate->format('d-m-Y')]['d'] = $oldcurrency;
                                $archive["exw"][$oldexwdate->format('d-m-Y')]['m'] = $oldmoq;
                            }
                            $archive["exw"][date('d-m-Y')]['p'] = $offer->getExw();
                            $archive["exw"][date('d-m-Y')]['d'] = $offer->getCurrency()->getTitle();
                            $archive["exw"][date('d-m-Y')]['m'] = $offer->getMoq();
                        }

                        if ($oldfob != $offer->getFob()) {
                            if (empty($archive["fob"]) && !empty($oldfobdate)) {
                                $archive["fob"][$oldfobdate->format('d-m-Y')]['p'] = $oldfob;
                                $archive["fob"][$oldfobdate->format('d-m-Y')]['d'] = "USD";
                                $archive["fob"][$oldfobdate->format('d-m-Y')]['m'] = $oldmoq;
                            }
                            $archive["fob"][date('d-m-Y')]['p'] = $offer->getFob();
                            $archive["fob"][date('d-m-Y')]['d'] = "USD";
                            $archive["fob"][date('d-m-Y')]['m'] = $offer->getMoq();
                        }

                        $offer->setArchive($archive);

                        $em->persist($product);
                        $em->persist($offer);


                        print ($product->getCategorie()->getId()) ? 'yes' : 'no';
                        print (($offer->getMoq() && $offer->getMoqunit()) || ($offer->getMoqpacking() && $offer->getMoqpackingunit())) ? 'yes' : 'no';
                        print (($offer->getFob()) || ($offer->getExw() && $offer->getCurrency()->getid())) ? 'yes' : 'no';

                        if ($product->getCategorie()->getId() &&
                            (($offer->getMoq() && $offer->getMoqunit()) || ($offer->getMoqpacking() && $offer->getMoqpackingunit())) &&
                            (($offer->getFob()) || ($offer->getExw() && $offer->getCurrency()->getid()))
                        ):

                            $em->flush();

                            if ($updatecount == false) {
                                if (is_array($importxls->images)) {
                                    if (array_key_exists($line + 1, $importxls->images)) {
                                        foreach ($importxls->images[$line + 1] as $img) {
                                            if ($img->url && $img->filename) {
                                                $Image = new Image();
                                                $Image->setImage($img->url);
                                                $Image->setFilename($img->filename);
                                                $Image->setcreated(new \DateTime());
                                                $Image->setProduct($product);
                                                $em->persist($Image);
                                            }
                                        }
                                    } else if (array_key_exists(-1, $importxls->images)) {
                                        foreach ($importxls->images[-1] as $key => $img) {
                                            if ($key == $line) {
                                                if ($img->url && $img->filename) {
                                                    $Image = new Image();
                                                    $Image->setImage($img->url);
                                                    $Image->setFilename($img->filename);
                                                    $Image->setcreated(new \DateTime());
                                                    $Image->setProduct($product);
                                                    $em->persist($Image);
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (!empty($Image))
                                $product->setMainimage($Image->getImage());

                            $History = new History();
                            $History->SetTitle('HISTORY_NEW_PRODUCT');
                            $History->SetDescription('HISTORY_NEW_PRODUCT_DESCRIPTION' . $entity->getName());
                            $History->SetIco('<i class="icon-plus-sign"></i>');
                            $History->SetDate(new \DateTime());
                            $History->SetSupplier($entity);
                            $History->SetProduct($product);
                            $History->SetOffer($offer);
                            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
                            $em->persist($History);
                            $em->flush();
                            if ($updatecount)
                                $update++;
                            else
                                $create++;
                        endif;
                    }
                    $this->get('session')->getFlashBag()->add('notice', (($create) ? $create . ' products created' : '') . ' ' . (($update) ? $update . ' offers updated ' : ''));
                    return $this->redirect($this->generateUrl('supplier_show', array('id' => $entity->getId())) . '#offers');
                }
            }

            return $this->render('appBundle:Supplier:import.html.twig', array(
                'form' => $form->createView(),
                'datas' => (empty($datas) ? '' : $datas),
                'images' => (empty($images) ? '' : $images),
                'updateoffer' => (empty($updateoffer) ? '' : $updateoffer),
                'file' => (empty($filename) ? '' : $filename),
                'post' => $post,
                'supplier' => $entity
            ));
        }
    }

    public function extractAction(Request $request, $supplier)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $entity = $this->getDoctrine()->getManager()->getRepository('appBundle:Supplier')->findOneById($supplier);
            $post = ($request->getMethod() == 'POST');
            $form = $this->createFormBuilder()
                ->add('attachment', 'file')
                ->getForm();
            if ($post) { //upload step
                $form->handleRequest($request);

                $importxls = $this->get('Importxls');
                if ($form->isSubmitted() && $form->isValid()) {
                    $importxls->importsimpleRequest($form, $request->request->get('form'));
                    if ($importxls->isallowExt()) { //move to tmp + send  (datas + images + check supplieritemnumer stored) into json
                        $importxls->moveTmpDir();

                        $createZip = $this->get('createZip');
                        $createZip->addDirectory('/');

                        foreach ($importxls->ExctactImages(false) as $key => $product) {
                            foreach ($product as $image) {
                                $createZip->addFile(file_get_contents(__SOURCINASIA__ . 'htdocs/' . $image['url']), $image['filename']);
                            }
                        }
                        $createZip->output();
                    } else {
                        $this->get('session')->getFlashBag()->add('warning', 'Format de fichier errone. Merci de fournir un xlsx.');
                        $post = false;
                    }
                }
            }

            return $this->render('appBundle:Supplier:extract.html.twig', array(
                'form' => $form->createView(),
                'datas' => (empty($datas) ? '' : $datas),
                'images' => (empty($images) ? '' : $images),
                'updateoffer' => (empty($updateoffer) ? '' : $updateoffer),
                'file' => (empty($filename) ? '' : $filename),
                'post' => $post,
                'supplier' => $entity
            ));
        }
    }

    /*
     * Retourne les suppliers filtrer
     */

    public function requestsAction(Request $request)
    {

        $request = $request->query->get('request');

        if (!is_array($request)) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        $session = $this->get('session');
        if (array_key_exists('departementid', $request)) {
            $session->set('departementFilter', $request['departementid']);
        } else {
            $session->remove('departementFilter');
        }

        if (array_key_exists('shelvesid', $request)) {
            $session->set('shelvesFilter', $request['shelvesid']);
        } else {
            $session->remove('shelvesFilter');
        }

        if (array_key_exists('famillyid', $request)) {
            $session->set('famillyFilter', $request['famillyid']);
        } else {
            $session->remove('famillyFilter');
        }

        if (array_key_exists('subfamillyid', $request)) {
            $session->set('subfamillyFilter', $request['subfamillyid']);
        } else {
            $session->remove('subfamillyFilter');
        }

        $request = $this->getDoctrine()
            ->getRepository('appBundle:Supplier')
            ->seach(array_filter($request), $this->get('Profils')->get(), false, ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')));
        /* $response = new Response();
          return $response->setContent('<html><head></head><body></body></html>'); */

        return $this->render('appBundle:Supplier:list.html.twig', array(
            'entities' => $request
        ));
    }

    /*
     * Retour pour les filtres
     */

    public function filtersAction(Request $request, $filter)
    {
        if (!is_array($request->query->get('request'))) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        $content = $this->getDoctrine()
            ->getRepository('appBundle:Supplier')
            ->seach(array_filter($request->query->get('request')), $this->get('Profils')->get(), $filter, ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')));

        if ($filter == "nomenclature")
            $content = $this->getDoctrine()
                ->getRepository('appBundle:Categorie')
                ->getCategroies(array_filter($content), $request->getLocale());

        $response = new Response();
        $response->setContent(json_encode($content));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function myselectionAction(Request $request)
    {
        $resquest = $request->request;
        $task = $resquest->get('task');
        $idsupplier = $resquest->get('supplier');
        $offerselectedsumbited = array();
        foreach ($resquest->get('selection') as $offer)
            if (array_key_exists('o', $offer))
                if (array_key_exists('q', $offer))
                    $offerselectedsumbited[$offer['o']] = (int)$offer['q'];
                else
                    $offerselectedsumbited[$offer['o']] = 1;

        switch ($task) {
            case 'delete':
                return $this->forward('appBundle:Offer:DeleteSelection', array('idsupplier' => $idsupplier, 'selection' => $offerselectedsumbited));
                break;
            case 'export':
                return $this->forward('appBundle:Export:exportSourcinSelection', array('idsupplier' => $idsupplier, 'selection' => $offerselectedsumbited));
                break;
            case 'deals':
                return $this->forward('appBundle:Offer:deals', array('idsupplier' => $idsupplier, 'selection' => $offerselectedsumbited));
                break;
        }

        return $this->redirect($this->generateUrl('supplier_show', array('id' => $supplierid)) . '#offers');
    }

    /*
     *  CRUD SYMFONY //////////////////////////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Supplier entities.
     */
    public function indexAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') or $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION'))
            return $this->render('appBundle:Supplier:index.html.twig', array('session' => $this->get('session')));
        else
            throw $this->createNotFoundException('Unable to find Supplier entity.');
    }

    /**
     * Finds and displays a Supplier entity.
     */
    public function showAction(Request $request, $id)
    {
        ini_set('memory_limit', '300M');

        $Supplier = $this->GetSecuritySupplier($id);

        if (!$Supplier) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }
        $filterparams = $request->query->get('request');
        $filterparams['supplierid'] = $Supplier->getId();

        $profil = ($this->get('Profils')->get());
        if ($profil['cutomer_id']) {
            $Customer = $this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->find($profil['cutomer_id']);
            $Catalogs = $Customer->getCatalogs();

        } else {
            $Catalogs = $this->getDoctrine()->getManager()->getRepository('appBundle:Catalog')->findAll();
        }

        $documents_quotations = $this->getDoctrine()->getManager()->getRepository(Document::class)->GetSupplierDocumentsOfType($Supplier, Document::QUOTATION_TYPE);
        $documents_catalogs = $this->getDoctrine()->getManager()->getRepository(Document::class)->GetSupplierDocumentsOfType($Supplier, Document::CATALOG_TYPE);
        $documents_customer_offers = $this->getDoctrine()->getManager()->getRepository(Document::class)->GetSupplierDocumentsOfType($Supplier, Document::CUSTOMER_OFFER_TYPE);

        $document_can_list = [
            Document::CATALOG_TYPE => false,
            Document::QUOTATION_TYPE => false,
            Document::CUSTOMER_OFFER_TYPE => false
        ];
        $document_can_add = [
            Document::CATALOG_TYPE => false,
            Document::QUOTATION_TYPE => false,
            Document::CUSTOMER_OFFER_TYPE => false
        ];
        $document_can_remove = [
            Document::CATALOG_TYPE => false,
            Document::QUOTATION_TYPE => false,
            Document::CUSTOMER_OFFER_TYPE => false
        ];

        if($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $document_can_list[Document::CUSTOMER_OFFER_TYPE] = true;
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
            $document_can_list[Document::CATALOG_TYPE] = true;
            $document_can_list[Document::QUOTATION_TYPE] = true;
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $document_can_list[Document::CATALOG_TYPE] = true;
            $document_can_add[Document::CATALOG_TYPE] = true;
            $document_can_list[Document::QUOTATION_TYPE] = true;
            $document_can_add[Document::QUOTATION_TYPE] = true;
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
            $document_can_list[Document::CATALOG_TYPE] = true;
            $document_can_add[Document::CATALOG_TYPE] = true;
            $document_can_remove[Document::CATALOG_TYPE] = true;
            $document_can_list[Document::QUOTATION_TYPE] = true;
            $document_can_add[Document::QUOTATION_TYPE] = true;
            $document_can_remove[Document::QUOTATION_TYPE] = true;
            $document_can_list[Document::CUSTOMER_OFFER_TYPE] = true;
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSOURCING')) {
            $document_can_list[Document::CATALOG_TYPE] = true;
            $document_can_add[Document::CATALOG_TYPE] = true;
            $document_can_remove[Document::CATALOG_TYPE] = true;
            $document_can_list[Document::QUOTATION_TYPE] = true;
            $document_can_add[Document::QUOTATION_TYPE] = true;
            $document_can_remove[Document::QUOTATION_TYPE] = true;
        }

        if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $document_can_list[Document::CATALOG_TYPE] = true;
            $document_can_add[Document::CATALOG_TYPE] = true;
            $document_can_remove[Document::CATALOG_TYPE] = true;
            $document_can_list[Document::QUOTATION_TYPE] = true;
            $document_can_add[Document::QUOTATION_TYPE] = true;
            $document_can_remove[Document::QUOTATION_TYPE] = true;
            $document_can_list[Document::CUSTOMER_OFFER_TYPE] = true;
            $document_can_add[Document::CUSTOMER_OFFER_TYPE] = true;
            $document_can_remove[Document::CUSTOMER_OFFER_TYPE] = true;
        }




        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                $document_can_add = [
                    Document::CATALOG_TYPE => true,
                    Document::QUOTATION_TYPE => true,
                    Document::CUSTOMER_OFFER_TYPE => true
                ];
                $document_can_remove = [
                    Document::CATALOG_TYPE => true,
                    Document::QUOTATION_TYPE => true,
                    Document::CUSTOMER_OFFER_TYPE => true
                ];
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSOURCING') ||
            $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
            $document_can_add[Document::CATALOG_TYPE] = true;
            $document_can_add[Document::QUOTATION_TYPE] = true;
            $document_can_remove[Document::CATALOG_TYPE] = true;
            $document_can_remove[Document::QUOTATION_TYPE] = true;
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $document_can_add[Document::CATALOG_TYPE] = true;
            $document_can_add[Document::QUOTATION_TYPE] = true;
        }


        return $this->render('appBundle:Supplier:show.html.twig', array(
            'Supplier' => $Supplier,
            'Offers' => $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->getOffersFromSupplierSourcing($Supplier->getId(), array(),$filterparams),
            'form' => $this->createFormBuilder()
                ->setAction($this->generateUrl('supplier_actions', array('id' => $Supplier->GetId())))
                ->setMethod('POST')
                ->getForm()->createView(),
            'delete_form' => $this->DeleteForm($id)->createView(),
            'catalogs' => $Catalogs,
            'session' => $this->get('session'),
            'documents_quotations' => $documents_quotations,
            'documents_catalogs' => $documents_catalogs,
            'documents_customer_offers' => $documents_customer_offers,
            'document_can_add' => $document_can_add,
            'document_can_remove' => $document_can_remove,
            'document_can_list' => $document_can_list
        ));
    }

    /**
     * Renders a filtered view of products of a single supplier
     */
    public function showproductsAction(Request $request, $id)
    {

        ini_set('memory_limit', '300M');

        $Supplier = $this->GetSecuritySupplier($id);

        if (!$Supplier) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }
        $filterparams = $request->query->get('request');
        $filterparams['supplierid'] = $Supplier->getId();

        return $this->render('appBundle:Offer:productsList.html.twig', array(
            'Supplier' => $Supplier,
            'Offers' => $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->getOffersFromSupplierSourcing($Supplier->getId(), array(),$filterparams),
            'form' => $this->createFormBuilder()
                ->setAction($this->generateUrl('supplier_actions', array('id' => $Supplier->GetId())))
                ->setMethod('POST')
                ->getForm()->createView(),
            'delete_form' => $this->DeleteForm($id)->createView(),
            'session' => $this->get('session')
        ));

    }


    /**
     * Displays a form to create a new Supplier entity.
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $Nomenclature = $em->getRepository('appBundle:Categorie')->getJsonData();

        return $this->render('appBundle:Supplier:form.html.twig', array(
            'Supplier' => $this->GetSecuritySupplier(),
            'form' => $this->buildForm($this->GetSecuritySupplier())->createView(),
            'Nomenclatures' => $Nomenclature,
        ));
    }

    /**
     * Creates a new Supplier entity.
     */
    public function createAction(Request $request)
    {
        $Supplier = $this->GetSecuritySupplier();
        $form = $this->buildForm($Supplier)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $Supplier = $em->getRepository('appBundle:Supplier')->refreshCategories($Supplier);
            $em->persist($Supplier);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('supplier_show', array('id' => $Supplier->getId())));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Supplier:form.html.twig', array(
            'Supplier' => $Supplier,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Supplier entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $Supplier = $this->GetSecuritySupplier($id);

        if (!$Supplier)
            throw $this->createNotFoundException('Unable to find Supplier entity.');

        $Nomenclature = $em->getRepository('appBundle:Categorie')->getJsonData($Supplier->getNomenclatures());

        return $this->render('appBundle:Supplier:form.html.twig', array(
            'Supplier' => $Supplier,
            'Nomenclatures' => $Nomenclature,
            'form' => $this->buildForm($Supplier, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Supplier entity.
     */
    public function updateAction(Request $request, $id)
    {

        $Supplier = $this->GetSecuritySupplier($id);

        if (!$Supplier)
            throw $this->createNotFoundException('Unable to find Supplier entity.');

        $form = $this->buildForm($Supplier, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $Supplier = $em->getRepository('appBundle:Supplier')->refreshCategories($Supplier);
            $em->persist($Supplier);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('supplier_show', array('id' => $id)));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Supplier:form.html.twig', array(
            'Supplier' => $Supplier,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Supplier entity.
     *
     */
    public function deleteAction($id)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();

            $Supplier = $this->GetSecuritySupplier($id);

            if (!$Supplier)
                throw $this->createNotFoundException('Unable to find Supplier entity.');

            $test = true;
            foreach ($Supplier->getCadenciers() as $cadencier) {
                if ($cadencier->getCommand())
                    $test = false;
            }

            if ($test) {
                $em->remove($Supplier);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');
            } else
                $this->get('session')->getFlashBag()->add('notice', 'Cannot delete this supplier. Orders are linked');
        }

        return $this->redirect($this->generateUrl('supplier'));
    }

    /**
     * Creates a form to delete a Supplier entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('supplier_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Supplier entity.
     */
    private function buildForm(Supplier $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\SupplierType', $entity, array(
                'action' => $this->generateUrl("supplier_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\SupplierType', $entity, array(
                'action' => $this->generateUrl("supplier_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    private function GetSecuritySupplier($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') or $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Supplier')->find($id);
            else
                return new Supplier();
        else
            throw $this->createNotFoundException('Unable to find Supplier.');
    }

    /*
     * Hide all products of the given supplier, param: supplier id
     */
    public function hideproductsAction(Request $request, $id)
    {
        $supplier = $this->GetSecuritySupplier($id);

        if (!$supplier) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }
//
//        $supplier->setProductshidden(true);
        // also?
        // 1. find all products of the supplier
        // 2. setHidden for each

        $products = $this->getDoctrine()->getRepository('appBundle:Product')
            ->seach(array('supplierid'=>$id), $this->get('Profils')->get(), false, ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')));
        $em = $this->getDoctrine()->getManager();
        $hiddenids = array();
        foreach ($products as $product) {
            if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
                if ($prod = $this->getDoctrine()->getRepository('appBundle:Product')->find($product['id'])) {
//                    $state = $prod->getHidden() ? null : 1;
//                    $prod->setHidden($state);
                    $prod->setHidden(1); // always hide, ignore previous state
                    $em->persist($prod);
                    $em->flush();
                    array_push($hiddenids, $product['id']);
                } else {
                    $this->get('session')->getFlashBag()->add('notice', 'Product '.$product['id'].' not found.');
                }
            } else {
                $this->get('session')->getFlashBag()->add('notice', 'Unauthorized action.');
            }
        }

        $em->persist($supplier);
        $em->flush();
        $this->get('session')->getFlashBag()->add('notice', 'Newly hidden products: '. implode(",",$hiddenids));
        return $this->redirect($this->generateUrl('supplier'));
    }

    /*
     * Generate csv file extraction of supplier table with first-line column names
     */
    public function exportCsvAllAction() {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->get('ExportCsv')->exportAllSuppliers();
        }
    }

    /*
     * Suppliers importation
     */
    public function importCsvSuppliersAction(Request $request) {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            ini_set('max_execution_time', 480);
            ini_set('memory_limit', '600M');
            $post = ($request->getMethod() == 'POST');
            $form = $this->createFormBuilder()
                ->add('attachment', 'Symfony\Component\Form\Extension\Core\Type\FileType')
                ->getForm();
            if ($post) { //upload step
                $form->handleRequest($request);
                $datas = $request->request->get('datas');
                if (!$datas) { //file is posted
                    $fileExt = $form['attachment']->getData()->getClientOriginalExtension();
                    if ($form->isValid() && $fileExt == 'csv') {
                        /** @var $readCsv ReadCsv */
                        $readCsv = $this->get('ReadCsv');
                        $datas = $readCsv->read($form['attachment']->getData());
                    } else {
                        $this->get('session')->getFlashBag()->add('warning', 'Format de fichier erronée : ' . $fileExt . '. Merci de fournir un csv.');
                        $post = false;
                    }
                } else {
                    $importCsv = $this->get('ImportCsv');
                    $createdAndUpdated = $importCsv->importSuppliers($datas);
                    $totalRows = $createdAndUpdated[0] + $createdAndUpdated[1];
                    $this->get('session')->getFlashBag()->add('notice',
                        $totalRows.' fournisseur'.($totalRows > 1 ? 's' : '').' bien importé'.($totalRows > 1 ? 's' : '').' ('.$createdAndUpdated[0].' créé'.($createdAndUpdated[0] > 1 ? 's' : '').' et '.$createdAndUpdated[1].' mise'.($createdAndUpdated[1] > 1 ? 's' : '').' à jour).'
                    );
                    return $this->redirect($this->generateUrl('supplier'));
                }
            }

            return $this->render('appBundle:Supplier:import_multiple_suppliers.html.twig', array(
                'form' => $form->createView(),
                'datas' => (empty($datas) ? '' : $datas),
                'post' => $post
            ));
        }
    }
}

