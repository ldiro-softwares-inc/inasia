<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Invoicecustomer;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Invoicecustomer controller.
 *
 */
class InvoicecustomerController extends Controller
{
    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Displays a form to edit an existing Invoicecustomer entity.
     *
     */
    public function createAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $cadencier = $this->getDoctrine()->getManager()->getRepository('appBundle:Cadencier')->find($id);

            if (!$cadencier)
                throw $this->createNotFoundException('Unable to find Invoicecustomer entity.');

            $Invoicecustomer = $this->GetSecurityInvoicecustomer(0);

            $Invoicecustomer->setCadencier($cadencier);

            $em = $this->getDoctrine()->getManager();

            $query = $em->createQuery('SELECT MAX(s.id) FROM appBundle:Invoicecustomer s');

            $form = $this->buildForm($Invoicecustomer, 'create');
            $form->get('chrono')->setData($query->getSingleScalarResult() + 1);

            return $this->render('appBundle:Invoicecustomer:form.html.twig', array(
                'form' => $form->createView()
            ));
        }
    }

    public function editAction($id)
    {
        $Invoicecustomer = $this->GetSecurityInvoicecustomer($id);

        if (!$Invoicecustomer)
            throw $this->createNotFoundException('Unable to find Invoicecustomer entity.');

        return $this->render('appBundle:Invoicecustomer:form.html.twig', array(
            'number' => $Invoicecustomer->getNumber(),
            'form' => $this->buildForm($Invoicecustomer, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Invoicecustomer entity.
     */
    public function addAction(Request $request)
    {
        $Invoicecustomer = $this->GetSecurityInvoicecustomer(0);

        if (!$Invoicecustomer)
            throw $this->createNotFoundException('Unable to find Invoicecustomer entity.');

        $form = $this->buildForm($Invoicecustomer, 'create')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Invoicecustomer);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('command_show', array('id' => $Invoicecustomer->getCadencier()->getId())) . '#Payments');
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Invoicecustomer:form.html.twig', array(
            'Invoicecustomer' => $Invoicecustomer,
            'form' => $form->createView()
        ));
    }

    /**
     * Edits an existing Invoicecustomer entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Invoicecustomer = $this->GetSecurityInvoicecustomer($id);

        if (!$Invoicecustomer)
            throw $this->createNotFoundException('Unable to find Invoicecustomer entity.');

        $form = $this->buildForm($Invoicecustomer, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Invoicecustomer);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('command_show', array('id' => $Invoicecustomer->getCadencier()->getId())) . '#Payments');
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Invoicecustomer:form.html.twig', array(
            'Invoicecustomer' => $Invoicecustomer,
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $Invoicecustomer = $this->GetSecurityInvoicecustomer($id);
            if ($Invoicecustomer->getPayments()->count()) {
                $this->get('session')->getFlashBag()->add('warning', 'Please remove payments first');
                return $this->redirect($this->generateUrl('command_show', array('id' => $Invoicecustomer->getCadencier()->getId())) . '#Payments');
            } else {
                $id = $Invoicecustomer->getCadencier()->getId();
                $em = $this->getDoctrine()->getManager();
                $em->remove($Invoicecustomer);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');
                return $this->redirect($this->generateUrl('command_show', array('id' => $id)) . '#Payments');
            }
        }
    }


    /**
     * Creates a form to create/edit a Invoicecustomer entity.
     */
    private function buildForm(Invoicecustomer $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\InvoicecustomerType', $entity, array(
                'action' => $this->generateUrl("invoicecustomer_add"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\InvoicecustomerType', $entity, array(
                'action' => $this->generateUrl("invoicecustomer_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets Invoicecustomer
     */
    private function GetSecurityInvoicecustomer($id = null)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Invoicecustomer')->find($id);
            else
                return new Invoicecustomer();
        else
            throw new AccessDeniedException();
    }

}
