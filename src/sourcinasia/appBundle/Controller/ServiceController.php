<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use sourcinasia\appBundle\Entity\Service;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Service controller.
 *
 */
class ServiceController extends Controller
{
    /*
    * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
    */

    /**
     * Lists all Service entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Service:index.html.twig', array(
            'Services' => $Service = $this->GetSecurityServices()
        ));
    }

    /**
     * Finds and displays a Service entity.
     */
    public function showAction($id)
    {
        $Service = $this->GetSecurityService($id);

        if (!$Service) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Service:show.html.twig', array(
            'Service' => $Service,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Service entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Service:form.html.twig', array(
            'Service' => $this->GetSecurityService(),
            'form' => $this->buildForm($this->GetSecurityService())->createView(),
        ));
    }

    /**
     * Creates a new Service entity.
     */
    public function createAction(Request $request)
    {
        $Service = $this->GetSecurityService();
        $form = $this->buildForm($Service)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Service);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('service'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Service:form.html.twig', array(
            'Service' => $Service,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Service entity.
     *
     */
    public function editAction($id)
    {
        $Service = $this->GetSecurityService($id);

        if (!$Service)
            throw $this->createNotFoundException('Unable to find Service entity.');

        return $this->render('appBundle:Service:form.html.twig', array(
            'Service' => $Service,
            'form' => $this->buildForm($Service, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Service entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Service = $this->GetSecurityService($id);

        if (!$Service)
            throw $this->createNotFoundException('Unable to find Service entity.');

        $form = $this->buildForm($Service, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Service);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('service'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Service:form.html.twig', array(
            'Service' => $Service,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Service entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $Service = $this->GetSecurityService($id);

        if (!$Service)
            throw $this->createNotFoundException('Unable to find Service entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Service);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('service'));
    }

    /**
     * Creates a form to delete a Service entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('service_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }


    /**
     * Creates a form to create/edit a Service entity.
     */
    private function buildForm(Service $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\ServiceType', $entity, array(
                'action' => $this->generateUrl("service_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\ServiceType', $entity, array(
                'action' => $this->generateUrl("service_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Service
     */
    private function GetSecurityServices()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Service')->findAll();
        else
            throw new AccessDeniedException();
    }

    /**
     * Gets Service
     */
    private function GetSecurityService($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Service')->find($id);
            else
                return new Service();
        else
            throw new AccessDeniedException();
    }
}
