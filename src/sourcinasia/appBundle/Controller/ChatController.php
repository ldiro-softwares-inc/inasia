<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Chat;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Chat controller.
 *
 */
class ChatController extends Controller
{

    /**
     * Displays a form to create a new Chat entity.
     */
    public function newAction()
    {
        $Chat = $this->GetSecurityChat();
        return $this->render('appBundle:Chat:form.html.twig', array(
            'Chat' => $Chat,
            'form' => $this->buildForm($Chat)->createView(),
        ));
    }

    /**
     * Finds and displays a Product entity.
     */
    public function showAction($id)
    {


        return $this->render('appBundle:Chat:show.html.twig');
    }


    /**
     * Creates a new Chat entity.
     */
    public function createAction(Request $request)
    {
        $Chat = $this->GetSecurityChat();
        $form = $this->buildForm($Chat)->handleRequest($request);
        if ($form->isValid()) {
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $Chat->setDate(new \Datetime());
            $Chat->setUser($user);

            if (method_exists($user, 'getContact'))
                if (method_exists($user->getContact(), 'getCustomer'))
                    if ($user->getContact()->getCustomer())
                        $Chat->setCustomer($user->getContact()->getCustomer());

            $Chat->setUser($this->get('security.token_storage')->getToken()->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($Chat);
            $em->flush();
            $this->get('event_dispatcher')->dispatch('sendmessage', new GenericEvent($Chat));

            return $this->redirect($this->generateUrl('chat_show', array('id' => $Chat->getId())));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Chat:form.html.twig', array(
            'Chat' => $Chat,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create/edit a Chat entity.
     */
    private function buildForm(Chat $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\ChatType', $entity, array(
                'action' => $this->generateUrl("chat_create"),
                'method' => 'POST',
                'attr' => array('class' => 'message-form')
            ));

        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "SEND MESSAGE"));
    }

    /**
     * Gets Chat
     */
    private function GetSecurityChat($id = null)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER'))
            return new Chat();
        else
            throw $this->createNotFoundException('Unable to find Chat.');
    }

}
