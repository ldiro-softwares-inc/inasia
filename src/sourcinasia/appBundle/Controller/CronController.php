<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class CronController extends Controller
{

    public function validationAction()
    {
        if ($Supplychains = $this->getDoctrine()->getRepository('appBundle:Supplychain')->findBy(array('validate' => null))) {
            $message = \Swift_Message::newInstance()
                ->setSubject($this->get('translator')->trans('MARKET - Waiting confirmation'))
                ->setFrom('order@inasia-corp.com')
                ->setTo(array('follow@site-internet-reunion.re','f.aubert@inasia-corp.com'))
                ->setBody(
                    $this->renderView(
                        'appBundle:Emails:waitingdocuments.html.twig',
                        array('Supplychains' => $Supplychains)
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);
        }

        $response = new Response();
        $response->setContent(1);
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }
}
