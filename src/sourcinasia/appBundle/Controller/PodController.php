<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use sourcinasia\appBundle\Entity\Pod;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Pod controller.
 *
 */
class PodController extends Controller
{
    /*
    * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
    */

    /**
     * Lists all Pod entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Pod:index.html.twig', array(
            'Pods' => $Pod = $this->GetSecurityPods()
        ));
    }

    /**
     * Finds and displays a Pod entity.
     */
    public function showAction($id)
    {
        $Pod = $this->GetSecurityPod($id);

        if (!$Pod) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Pod:show.html.twig', array(
            'Pod' => $Pod,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Pod entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Pod:form.html.twig', array(
            'Pod' => $this->GetSecurityPod(),
            'form' => $this->buildForm($this->GetSecurityPod())->createView(),
        ));
    }

    /**
     * Creates a new Pod entity.
     */
    public function createAction(Request $request)
    {
        $Pod = $this->GetSecurityPod();
        $form = $this->buildForm($Pod)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Pod);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('pod'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Pod:form.html.twig', array(
            'Pod' => $Pod,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Pod entity.
     *
     */
    public function editAction($id)
    {
        $Pod = $this->GetSecurityPod($id);

        if (!$Pod)
            throw $this->createNotFoundException('Unable to find Pod entity.');

        return $this->render('appBundle:Pod:form.html.twig', array(
            'Pod' => $Pod,
            'form' => $this->buildForm($Pod, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Pod entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Pod = $this->GetSecurityPod($id);

        if (!$Pod)
            throw $this->createNotFoundException('Unable to find Pod entity.');

        $form = $this->buildForm($Pod, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Pod);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('pod'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Pod:form.html.twig', array(
            'Pod' => $Pod,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Pod entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $Pod = $this->GetSecurityPod($id);

        if (!$Pod)
            throw $this->createNotFoundException('Unable to find Pod entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Pod);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('pod'));
    }

    /**
     * Creates a form to delete a Pod entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pod_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }


    /**
     * Creates a form to create/edit a Pod entity.
     */
    private function buildForm(Pod $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\PodType', $entity, array(
                'action' => $this->generateUrl("pod_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\PodType', $entity, array(
                'action' => $this->generateUrl("pod_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'submit', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Pod
     */
    private function GetSecurityPods()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Pod')->findAll();
        else
            throw new AccessDeniedException();
    }

    /**
     * Gets Pod
     */
    private function GetSecurityPod($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Pod')->find($id);
            else
                return new Pod();
        else
            throw new AccessDeniedException();
    }
}
