<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use sourcinasia\appBundle\Entity\Norme;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Norme controller.
 *
 */
class NormeController extends Controller
{
    /*
    * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
    */

    /**
     * Lists all Norme entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Norme:index.html.twig', array(
            'Normes' => $Norme = $this->GetSecurityNormes()
        ));
    }

    /**
     * Finds and displays a Norme entity.
     */
    public function showAction($id)
    {
        $Norme = $this->GetSecurityNorme($id);

        if (!$Norme) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Norme:show.html.twig', array(
            'Norme' => $Norme,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Norme entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Norme:form.html.twig', array(
            'Norme' => $this->GetSecurityNorme(),
            'form' => $this->buildForm($this->GetSecurityNorme())->createView(),
        ));
    }

    /**
     * Creates a new Norme entity.
     */
    public function createAction(Request $request)
    {
        $Norme = $this->GetSecurityNorme();
        $form = $this->buildForm($Norme)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Norme);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('norme'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Norme:form.html.twig', array(
            'Norme' => $Norme,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Norme entity.
     *
     */
    public function editAction($id)
    {
        $Norme = $this->GetSecurityNorme($id);

        if (!$Norme)
            throw $this->createNotFoundException('Unable to find Norme entity.');

        return $this->render('appBundle:Norme:form.html.twig', array(
            'Norme' => $Norme,
            'form' => $this->buildForm($Norme, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Norme entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Norme = $this->GetSecurityNorme($id);

        if (!$Norme)
            throw $this->createNotFoundException('Unable to find Norme entity.');

        $form = $this->buildForm($Norme, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Norme);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('norme'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Norme:form.html.twig', array(
            'Norme' => $Norme,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Norme entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $Norme = $this->GetSecurityNorme($id);

        if (!$Norme)
            throw $this->createNotFoundException('Unable to find Norme entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Norme);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('norme'));
    }

    /**
     * Creates a form to delete a Norme entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('norme_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }


    /**
     * Creates a form to create/edit a Norme entity.
     */
    private function buildForm(Norme $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\NormeType', $entity, array(
                'action' => $this->generateUrl("norme_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\NormeType', $entity, array(
                'action' => $this->generateUrl("norme_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Norme
     */
    private function GetSecurityNormes()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Norme')->findAll();
        else
            throw new AccessDeniedException();
    }

    /**
     * Gets Norme
     */
    private function GetSecurityNorme($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Norme')->find($id);
            else
                return new Norme();
        else
            throw new AccessDeniedException();
    }
}
