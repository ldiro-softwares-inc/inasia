<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\City;

/**
 * City controller.
 *
 */
class CityController extends Controller
{
    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all City entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:City:index.html.twig', array(
            'Citys' => $City = $this->GetSecurityCitys()
        ));
    }

    /**
     * Finds and displays a City entity.
     */
    public function showAction($id)
    {
        $City = $this->GetSecurityCity($id);

        if (!$City) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:City:show.html.twig', array(
            'City' => $City,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new City entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:City:form.html.twig', array(
            'City' => $this->GetSecurityCity(),
            'form' => $this->buildForm($this->GetSecurityCity())->createView(),
        ));
    }

    /**
     * Creates a new City entity.
     */
    public function createAction(Request $request)
    {
        $City = $this->GetSecurityCity();
        $form = $this->buildForm($City)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($City);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('city'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:City:form.html.twig', array(
            'City' => $City,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing City entity.
     *
     */
    public function editAction($id)
    {
        $City = $this->GetSecurityCity($id);

        if (!$City)
            throw $this->createNotFoundException('Unable to find City entity.');

        return $this->render('appBundle:City:form.html.twig', array(
            'City' => $City,
            'form' => $this->buildForm($City, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing City entity.
     */
    public function updateAction(Request $request, $id)
    {
        $City = $this->GetSecurityCity($id);

        if (!$City)
            throw $this->createNotFoundException('Unable to find City entity.');

        $form = $this->buildForm($City, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($City);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('city'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:City:form.html.twig', array(
            'City' => $City,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a City entity.
     *
     */
    public function deleteAction($id)
    {
        $City = $this->GetSecurityCity($id);

        if (!$City)
            throw $this->createNotFoundException('Unable to find City entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($City);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('city'));
    }

    /**
     * Creates a form to delete a City entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('city_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a City entity.
     */
    private function buildForm(City $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\CityType', $entity, array(
                'action' => $this->generateUrl("city_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\CityType', $entity, array(
                'action' => $this->generateUrl("city_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all City
     */
    private function GetSecurityCitys()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:City')->findAll();
        else
            throw $this->createNotFoundException('Unable to find City.');
    }

    /**
     * Gets City
     */
    private function GetSecurityCity($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:City')->find($id);
            else
                return new City();
        else
            throw $this->createNotFoundException('Unable to find City.');
    }

}
