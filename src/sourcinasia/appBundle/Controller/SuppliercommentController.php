<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use sourcinasia\appBundle\Entity\Suppliercomment;
use sourcinasia\appBundle\Entity\History;

/**
 * Suppliercomment controller.
 *
 */
class SuppliercommentController extends Controller
{
    /*
    * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
    */


    /**
     * Displays a form to create a new Suppliercomment entity.
     */
    public function newAction($idsupplier)
    {

        $Suppliercomment = $this->GetSecuritySuppliercomment();
        $form = $this->buildForm($Suppliercomment);

        $supplier = $this->getDoctrine()->getManager()->getRepository('appBundle:Supplier')->findOneById($idsupplier);

        $form->get('supplier')->setData($supplier);

        return $this->render('appBundle:Suppliercomment:form.html.twig', array(
            'Suppliercomment' => $Suppliercomment,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Suppliercomment entity.
     */
    public function createAction(Request $request)
    {
        $Suppliercomment = $this->GetSecuritySuppliercomment();
        $form = $this->buildForm($Suppliercomment)->handleRequest($request);
        if ($form->isValid()) {
            $Suppliercomment->setDate(new \datetime());
            $Suppliercomment->setUser($this->get('security.token_storage')->getToken()->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($Suppliercomment);

            $History = new History();
            $History->SetTitle('HISTORY_NEW_SUPPLIERCOMMENT');
            $History->SetDescription('HISTORY_NEW_SUPPLIERCOMMENT_DESCRIPTION' . $Suppliercomment->getTitle());
            $History->SetIco('<i class="icon-plus-sign"></i>');
            $History->SetDate(new \DateTime());
            $History->SetSupplier($Suppliercomment->getSupplier());
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);


            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('supplier_show', array('id' => $Suppliercomment->getSupplier()->getId())) . '#suppliercomments');
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Suppliercomment:form.html.twig', array(
            'Suppliercomment' => $Suppliercomment,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Suppliercomment entity.
     *
     */
    public function editAction($idsupplier)
    {
        $Suppliercomment = $this->GetSecuritySuppliercomment($idsupplier);

        if (!$Suppliercomment)
            throw $this->createNotFoundException('Unable to find Suppliercomment entity.');

        return $this->render('appBundle:Suppliercomment:form.html.twig', array(
            'Suppliercomment' => $Suppliercomment,
            'form' => $this->buildForm($Suppliercomment, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Suppliercomment entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Suppliercomment = $this->GetSecuritySuppliercomment($id);

        if (!$Suppliercomment)
            throw $this->createNotFoundException('Unable to find Suppliercomment entity.');

        $form = $this->buildForm($Suppliercomment, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $Suppliercomment->setDate(new \datetime());
            $Suppliercomment->setUser($this->get('security.token_storage')->getToken()->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($Suppliercomment);

            $History = new History();
            $History->SetTitle('HISTORY_EDIT_SUPPLIERCOMMENT');
            $History->SetDescription('HISTORY_EDIT_SUPPLIERCOMMENT_DESCRIPTION' . $Suppliercomment->getTitle());
            $History->SetIco('<i class="icon-pencil"></i>');
            $History->SetDate(new \DateTime());
            $History->SetSupplier($Suppliercomment->getSupplier());
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);


            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('supplier_show', array('id' => $Suppliercomment->getSupplier()->getId())) . '#suppliercomments');
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Suppliercomment:form.html.twig', array(
            'Suppliercomment' => $Suppliercomment,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Suppliercomment entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            throw $this->createNotFoundException('Unable to find Suppliercomment entity.');

        $Suppliercomment = $this->GetSecuritySuppliercomment($id);
        if (!$Suppliercomment)
            throw $this->createNotFoundException('Unable to find Suppliercomment entity.');

        $idsupplier = $Suppliercomment->getSupplier()->GetId();

        $em = $this->getDoctrine()->getManager();
        $em->remove($Suppliercomment);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('supplier_show', array('id' => $idsupplier)) . '#suppliercomments');

    }


    /**
     * Creates a form to create/edit a Suppliercomment entity.
     */
    private function buildForm(Suppliercomment $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\SuppliercommentType', $entity, array(
                'action' => $this->generateUrl("suppliercomment_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\SuppliercommentType', $entity, array(
                'action' => $this->generateUrl("suppliercomment_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Suppliercomment
     */
    private function GetSecuritySuppliercomments()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Suppliercomment')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Suppliercomment.');
    }

    /**
     * Gets Suppliercomment
     */
    private function GetSecuritySuppliercomment($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Suppliercomment')->find($id);
            else
                return new Suppliercomment();
        else
            throw $this->createNotFoundException('Unable to find Suppliercomment.');
    }
}
