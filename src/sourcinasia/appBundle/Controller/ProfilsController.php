<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Address controller.
 *
 */
class ProfilsController extends Controller
{

    public function CommercialAction(Request $request)
    {
        $this->get('Profils')->setMode('commercial');
        $this->get('Profils')->setCommercialcoef($request->request->get('p'));
        return $this->forward('appBundle:Offer:index');
    }

    public function PresentationAction()
    {
        $this->get('Profils')->setMode('presentation');
        return $this->forward('appBundle:Offer:index');
    }

    public function buyAction()
    {
        $this->get('Profils')->setMode('buy');
        return $this->forward('appBundle:Offer:index');
    }

    public function logoutAction()
    {
        $this->get('Profils')->logout();
        return $this->forward('appBundle:Customer:customerslist');
    }

    public function backtoorderAction($id)
    {
        $this->get('Profils')->logout();
        return $this->forward('appBundle:Command:show', array('id' => $id));
    }

    public function CustomerAction(Request $request, $id)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
            $Customer = $this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->find($id);
        else
            throw $this->createNotFoundException('Unable to find Customer.');

        $this->get('Profils')->setCustomer($Customer);

        $session = $this->get('session');
        $session->set('selection', '');
        $session->set('hideRef', false);
        $session->set('selectionids', array());

        return $this->forward('appBundle:Offer:index');
    }

    public function CadencierAction(Request $request, $id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $Command = $this->getDoctrine()->getManager()->getRepository('appBundle:Command')->find($id);
            if ($Command->GetCustomer())
                $Customer = $Command->GetCustomer();
            else
                throw $this->createNotFoundException('Unable to find Customer.');
        } else
            throw $this->createNotFoundException('Unable to find Customer.');

        $this->get('Profils')->setCustomer($Customer);

        $session = $this->get('session');
        $session->set('selection', '');
        $session->set('selectionids', array());

        return $this->redirect($this->generateUrl('cadencier_show', array('id' => $Command->getCadencier()->GetId())));
    }

    public function CadencierfAction(Request $request, $id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
            $Command = $this->getDoctrine()->getManager()->getRepository('appBundle:Command')->find($id);
            if ($Command->GetCustomer())
                $Customer = $Command->GetCustomer();
            else
                throw $this->createNotFoundException('Unable to find Customer.');
        } else
            throw $this->createNotFoundException('Unable to find Customer.');

        $this->get('Profils')->setCustomer($Customer);

        $session = $this->get('session');
        $session->set('selection', '');
        $session->set('selectionids', array());

        return $this->redirect($this->generateUrl('cadencier_fred', array('id' => $Command->getCadencier()->GetId())));
    }

    public function editAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            $user = $this->getUser();

            if ($user) {

                $profil = $request->request->get('profil');
                $userprofil = $this->get('Profils')->get();

                //profil catalogue ne peut rien changer
                if ($userprofil['contact_type'] != 3) {
                    if (!empty($profil['modeonlogin']))
                        if ($profil['modeonlogin'] && $user->GetModeonlogin() != $profil['modeonlogin'])
                            if (in_array($profil['modeonlogin'], array('buy', 'commercial', 'presentation')))
                                $user->setModeonlogin($profil['modeonlogin']);

                    if ($profil['language'] && $user->GetLocale() != $profil['language'])
                        if (in_array($profil['language'], array('en', 'fr')))
                            $user->SetLocale($profil['language']);

                    if ($profil['password'] && strlen($profil['password']) > 5)
                        $user->setPlainPassword($profil['password']);

                    //contact
                    if (!empty($profil['phone']))
                        if ($profil['phone'] && $user->GetContact()->getPhone() != $profil['phone'])
                            $user->GetContact()->SetPhone($profil['phone']);
                    if (!empty($profil['mobile']))
                        if ($profil['mobile'] && $user->GetContact()->getMobile() != $profil['mobile'])
                            $user->GetContact()->SetMobile($profil['mobile']);
                    if (!empty($profil['fax']))
                        if ($profil['fax'] && $user->GetContact()->getFax() != $profil['fax'])
                            $user->GetContact()->SetFax($profil['fax']);
                    if (!empty($profil['job']))
                        if ($profil['job'] && $user->GetContact()->getJob() != $profil['job'])
                            $user->GetContact()->SetJob($profil['job']);
                    if (!empty($profil['qq']))
                        if ($profil['qq'] && $user->GetContact()->getQq() != $profil['qq'])
                            $user->GetContact()->SetQq($profil['qq']);
                    if (!empty($profil['skype']))
                        if ($profil['skype'] && $user->GetContact()->getSkype() != $profil['skype'])
                            $user->GetContact()->SetSkype($profil['skype']);
                    if (!empty($profil['name']))
                        if ($profil['name'] && $user->GetContact()->getName() != $profil['name'])
                            $user->GetContact()->SetName($profil['name']);

                    if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
                        if (array_key_exists('customerpurcentage', $profil)) {
                            if ($profil['customerpurcentage'] && (int)$profil['customerpurcentage'] > 0 && $userprofil['contact_type'] == 0) {
                                if ($user->GetContact()->getCustomer()->GetCustomermargecoef() != $profil['customerpurcentage'])
                                    $user->GetContact()->GetCustomer()->Setcustomermargecoef($profil['customerpurcentage']);
                            }
                        }
                    }

                    if (!empty($profil['purcentage']))
                        if ($profil['purcentage'] && (int)$profil['purcentage'] > 0 && $user->GetCustomermargecoef() != $profil['purcentage']) {
                            switch ($userprofil['contact_type']) {
                                case 0 :
                                    $user->SetCustomerMargecoef((int)$profil['purcentage']);
                                    break;
                                case 4 :
                                    if ((int)$profil['purcentage'] > 10) {
                                        $profil['purcentage'] = 10;
                                        $this->get('session')->getFlashBag()->add('warning', "ERREUR_MARGE_MAX");
                                    }

                                    $user->SetCustomerMargecoef((int)$profil['purcentage']);
                                    break;
                                default :
                                    $user->SetCustomerMargecoef(0);
                                    break;
                            }
                        }


                    if (filter_var($profil['email'], FILTER_VALIDATE_EMAIL) && ($user->getEmail() != $profil['email'])) {
                        if (!$this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneByEmail($profil['email'])) {
                            $user->setEmail($profil['email']);
                            $user->GetContact()->SetEmail($profil['email']);
                        } else
                            $this->get('session')->getFlashBag()->add('warning', "APPLICATION_EMAILALREADYEXIST");
                    }

                    $userManager = $this->container->get('fos_user.user_manager');
                    $userManager->updateUser($user, true);

                    $this->get('session')->getFlashBag()->add('notice', 'APPLICATION_PARAMETERS8UPDATED');
                }
            }
        }

        return $this->redirect($this->generateUrl('routing'));
    }

}
