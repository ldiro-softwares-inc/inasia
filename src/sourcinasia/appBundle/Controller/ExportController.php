<?php

namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\Cadencier;
use sourcinasia\appBundle\Entity\Command;
use sourcinasia\appBundle\Entity\Customer;
use sourcinasia\appBundle\Entity\Historycustomer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;

class ExportController extends Controller
{

    public function exportCadencierAction($doc, $id, $type)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            switch ($doc) {
                case 'catalog':
                    $profil = $this->get('Profils')->get();
                    if (is_array($profil) && $profil['cutomer_id']) {
                        if (!$Selection = $this->get('Session')->get('selectionids')) {
                            $this->get('session')->getFlashBag()->add('notice', 'No product selected');
                            return $this->redirect($this->generateUrl('home'));
                        } else {
                            return $this->get('export')->catalog($type, 'Catalog', $this->getDoctrine()
                                ->getRepository('appBundle:Offer')
                                ->getProductsCatalog($Selection, $profil),
                                $this->getDoctrine()
                                    ->getRepository('appBundle:Customer')
                                    ->findOneById($profil['cutomer_id']), $profil);
                        }
                    } else {
                        $this->get('session')->getFlashBag()->add('notice', 'Please connect to customer before');
                        return $this->redirect($this->generateUrl('home'));
                    }
                    break;
                case 'qs':
                    if (!$cadencier = $this->GetCadencier($id)) {
                        throw $this->createNotFoundException('Unable to find Cadencier');
                    }
                    // limitation download custumer
                    if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && !$this->userLimitDownload($cadencier)) {
                        return $this->redirect($this->generateUrl('home'));
                    }
                    if ($type == "pifrs") {
                        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
                            $mail = array();
                            if ($cadencier->getState() == 2) {
                                $cadencier->setState(3);
                                $supplier = $cadencier->getSupplier();

                                $this->get('event_dispatcher')->dispatch(
                                    'sendmail', new GenericEvent($cadencier, array('step' => 'SENDPITOSUPPLIER'))
                                );

                                $this->get('session')->getFlashBag()->add('warning', 'STEP12');

                                $em = $this->getDoctrine()->getManager();
                                $em->persist($cadencier);
                                $em->flush();

                                $mail['output'] = __SOURCINASIA__ . 'tmp/' . $supplier->getId() . '.xlsx';
                                $mail['to'] = 'o.sabban@inasia-corp.com';
                                $mail['subject'] = $this->get('translator')->trans(
                                    'MAIL_CONFIRMATION_SELECTION_SUPPLIER_TITLE'
                                );
                                $mail['message'] = "to: " . $this->get('translator')->trans(
                                        'MAIL_CONFIRMATION_SELECTION_SUPPLIER'
                                    );
                            }
                        }
                    }

                    $profil = $this->GetProfil($cadencier);
                    return $this->get('export')->qs($type, 'Quotationsheet', $cadencier, $this->getDoctrine()
                        ->getManager()
                        ->getRepository('appBundle:Offer')
                        ->getOffersFromSupplierClient($cadencier->GetSupplier()->GetId(), $profil, $cadencier->getOfferslist()),
                        $profil);
                    break;
                case 'po': // Purchase order: (supplier PI request,'pifrs')
                    if (!$cadencier = $this->GetCadencier($id)) {
                        throw $this->createNotFoundException('Unable to find Cadencier');
                    }
                    // limitation download custumer
                    if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && !$this->userLimitDownload($cadencier)) {
                        return $this->redirect($this->generateUrl('home'));
                    }
                    if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
                        $mail = array();
                        if ($cadencier->getState() == 2) {
                            $cadencier->setState(3);
                            $supplier = $cadencier->getSupplier();

                            $this->get('event_dispatcher')->dispatch(
                                'sendmail', new GenericEvent($cadencier, array('step' => 'SENDPITOSUPPLIER'))
                            );

                            $this->get('session')->getFlashBag()->add('warning', 'STEP12');

                            $em = $this->getDoctrine()->getManager();
                            $em->persist($cadencier);
                            $em->flush();

                            $mail['output'] = __SOURCINASIA__ . 'tmp/' . $supplier->getId() . '.xlsx';
                            $mail['to'] = 'o.sabban@inasia-corp.com';
                            $mail['subject'] = $this->get('translator')->trans(
                                'MAIL_CONFIRMATION_SELECTION_SUPPLIER_TITLE'
                            );
                            $mail['message'] = "to: " . $this->get('translator')->trans(
                                    'MAIL_CONFIRMATION_SELECTION_SUPPLIER'
                                );
                        }
                    }
                    $profil = $this->GetProfil($cadencier);
                    // type = 'pifrs'
                    return $this->get('export')->po($type, 'PurchaseOrder', $cadencier, $this->getDoctrine()
                        ->getManager()
                        ->getRepository('appBundle:Offer')
                        ->getOffersFromSupplierClient($cadencier->GetSupplier()->GetId(), $profil, $cadencier->getOfferslist()),
                        $profil);

                    break; // end po request
                case 'pi':
                    if (!$this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                        throw new AccessDeniedException();
                    }
                    if (!$cadencier = $this->GetCadencier($id)) {
                        throw $this->createNotFoundException('Unable to find Cadencier');
                    }

                    $profil = $this->GetProfil($cadencier);
                    // this takes forever when there are lots of products on the invoice:
                    return $this->get('export')->pi($type, 'Proformat Invoice', $cadencier, $this->getDoctrine()
                        ->getManager()
                        ->getRepository('appBundle:Offer')
                        ->getOffersFromSupplierClient($cadencier->GetSupplier()->GetId(), $profil, $cadencier->getOfferslist()),
                        $profil);
                    break;
                case 'piservice':
                    if (!$this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                        throw new AccessDeniedException();
                    }
                    if (!$Invoice = $this->getDoctrine()->getManager()->getRepository('appBundle:Invoicecustomer')->find($id)) {
                        throw $this->createNotFoundException('Unable to find Cadencier');
                    }

                    return $this->get('export')->piService($type, 'Proformat Invoice Service', $Invoice);
                    break;
                case 'ci':
                    if (!$this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                        throw new AccessDeniedException();
                    }
                    $invoice = $this->getDoctrine()->getManager()->getRepository('appBundle:Invoicecustomer')->find($id);
                    if (!$cadencier = $invoice->getCadencier()) {
                        throw $this->createNotFoundException('Unable to find Cadencier');
                    }
                    $cadencier = $invoice->getCadencier();
                    $profil = $this->GetProfil($cadencier);
                    return $this->get('export')->ci($type, 'Commercial Invoice', $invoice, $this->getDoctrine()
                        ->getManager()
                        ->getRepository('appBundle:Offer')
                        ->getOffersFromSupplierClient($cadencier->GetSupplier()->GetId(), $profil, $cadencier->getOfferslist()),
                        $profil);
                    break;
                case 'ciservice':
                    if (!$this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                        throw new AccessDeniedException();
                    }
                    if (!$Invoice = $this->getDoctrine()->getManager()->getRepository('appBundle:Invoicecustomer')->find($id)) {
                        throw $this->createNotFoundException('Unable to find Cadencier');
                    }

                    return $this->get('export')->ciService($type, 'Commercial Invoice Service', $Invoice);
                    break;
            }
        }
        throw new AccessDeniedException();
    }


    /*
     *  SOURCING - Export selection
     */

    public function exportSourcinSelectionAction($idsupplier, $selection = array())
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_EXPORT')) {
            ini_set('max_execution_time', 240);
            ini_set('memory_limit', '300M');
            if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
                $supplier = $this->getDoctrine()
                    ->getRepository('appBundle:Supplier')
                    ->findOneById($idsupplier);

                $offers = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('appBundle:Offer')
                    ->getOffersFromSupplierSourcing($supplier->getId(), array_keys($selection));

                $export = $this->get('PHPExcel');

                $export->load()
                    ->setSheetName('Supplier' . $supplier->getId())
                    ->exportcadencierRender($offers)
                    ->output($supplier->getId());
            }
        }
    }

    private function userLimitDownload($cadencier = null)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && !$this->get('security.token_storage')->getToken()->getUser()->getExpireDate()) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $contact = $this->get('security.token_storage')->getToken()->getUser()->GetContact();
            $Historycustomer = new Historycustomer();
            $em = $this->getDoctrine()->getManager();
            $exportnbr = (int)$contact->getExportnbr();

            if ($contact->getExportdate() == "") {
                $contact->setExportdate(new \dateTime());
            }

            if (((($exportnbr < $contact->getExportnbrmax()) && $contact->getExportdate()->format('Y-m-d') == date(
                        'Y-m-d'
                    )) || $contact->getExportdate()->format('Y-m-d') != date('Y-m-d'))
            ) {
                $Historycustomer->setTitle('EXPORT_XLS_CADENCIER');
                $Historycustomer->setDescription('EXPORT_XLS_CADENCIER');
                $Historycustomer->setIco('<i class="icon-download-alt"></i>');
                $Historycustomer->setDate(new \DateTime());
                $Historycustomer->setUser($user);
                $Historycustomer->setCustomer($user->getContact()->getCustomer());
                if ($cadencier) {
                    $Historycustomer->setCadencier($cadencier);
                }
                $em->persist($Historycustomer);
                if ($contact->getExportdate()->format('Y-m-d') != date('Y-m-d')) {
                    $contact->setExportnbr(1);
                } else {
                    $contact->setExportnbr($exportnbr + 1);
                }
                $contact->setExportdate(new \DateTime());
                $em->persist($contact);
                $em->flush();

                return true;
            } else {

                $Historycustomer->setTitle('EXPORT_TOO_MUCH_DOWNLOAD');
                $Historycustomer->setDescription('EXPORT_TOO_MUCH_DOWNLOAD');
                $Historycustomer->setIco('<i class="icon-warning-sign"></i>');
                $Historycustomer->setDate(new \DateTime());
                $Historycustomer->setUser($user);
                $Historycustomer->setCustomer($user->getContact()->getCustomer());
                $Historycustomer->setCadencier($cadencier);
                $em->persist($Historycustomer);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'notice', 'Too much download today ! Please contact sourcinasia.'
                );

                return false;
            }
        } else {
            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')
            ) {
                return true;
            }
        }
    }

    /*
     * Export Excel Commercial + Production
     */

    public function exportquotationsheetAction($id, $type = "qs")
    {
        ini_set('max_execution_time', 240);
        ini_set('memory_limit', '300M');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted(
                'ROLE_COMMERCIAL'
            ) || $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && !$this->get('security.token_storage')->getToken()->getUser()->getExpireDate())
        ) {

            if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
                if (!$this->get('security.token_storage')->getToken()->getUser()->getContact()->GetCustomer()->getXlsexport()
                ) {
                    throw $this->createNotFoundException('Unable to find Offer entity.');
                }
            }

            $cadencier = $this->GetCadencier($id);

            if (!$cadencier) {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }

            $profil = $this->GetProfil($cadencier);
            $supplier = $cadencier->GetSupplier();

            $offers = $this->getDoctrine()
                ->getManager()
                ->getRepository('appBundle:Offer')
                ->getOffersFromSupplierClient($supplier->GetId(), $profil, $cadencier->getOfferslist());

            if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && !$this->userLimitDownload($cadencier)) {
                return $this->redirect($this->generateUrl('home'));
                die;
            }
            $export = $this->get('PHPExcel');

            if ($type == "pifrs") {
                if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
                    $mail = array();
                    if ($cadencier->getState() == 2) {
                        $cadencier->setState(3);

                        $this->get('event_dispatcher')->dispatch(
                            'sendmail', new GenericEvent($cadencier, array('step' => 'SENDPITOSUPPLIER'))
                        );

                        $this->get('session')->getFlashBag()->add('warning', 'STEP12');

                        $em = $this->getDoctrine()->getManager();
                        $em->persist($cadencier);
                        $em->flush();

                        $mail['output'] = __SOURCINASIA__ . 'tmp/' . $supplier->getId() . '.xlsx';
                        $mail['to'] = 'o.sabban@inasia-corp.com';
                        $mail['subject'] = $this->get('translator')->trans(
                            'MAIL_CONFIRMATION_SELECTION_SUPPLIER_TITLE'
                        );
                        $mail['message'] = "to: " . $this->get('translator')->trans(
                                'MAIL_CONFIRMATION_SELECTION_SUPPLIER'
                            );
                    }

                    $fichier = $export->load('QUOTATIONSHEETFRS.xlsx')
                        ->setSheetName('Supplier' . $supplier->getId())
                        ->displaySupplierheader($cadencier)
                        ->setInitline(18)
                        ->exportproductionupdatepriceRender($offers, $cadencier)
                        ->output($supplier->getId());
                }
            } elseif ($type == "picustomer") {
                if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                    /* if ($cadencier->getState() == 1 || $cadencier->getState() == 2) {
                      $cadencier->setState(3);

                      $em = $this->getDoctrine()->getManager();
                      $em->persist($cadencier);
                      $em->flush();
                      } */
                    $export->load('PI.xlsx')
                        ->setSheetName('Supplier' . $supplier->getId())
                        ->displayPiElement($cadencier, $this->get('security.token_storage')->getToken()->getUser())
                        ->setInitline(18)
                        ->exportQuotationPiCustomerRender(
                            $offers, $cadencier, $profil, $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'), true
                        )
                        ->output($supplier->getId());
                }
            } elseif ($type == "pl") {
                if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')
                ) {
                    if ($cadencier->getState() >= 8) {

                        if (!$cadencier->getLoadingdate()) {
                            $this->get('session')->getFlashBag()->add('warning', 'No loading date');

                            return $this->redirect(
                                $this->generateUrl('command_show', array('id' => $cadencier->GetId()))
                            );
                        }

                        $export->load('PL.xlsx')
                            ->setSheetName('Supplier' . $supplier->getId())
                            ->displayPlElement(
                                $cadencier->GetCommand(), $this->get('security.token_storage')->getToken()->getUser()
                            )
                            ->setInitline(18)
                            ->exportQuotationPlCustomerRender(
                                $cadencier->GetCommand(), $profil, $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'), true
                            )
                            ->output($supplier->getId());
                    }
                }
            } else {
                $user = $this->get('security.token_storage')->getToken()->getUser();
                if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
                    if ($user->getContact()->getCustomer()->getCommercialtools() and $profil['mode'] == "commercial") {
                        $export->load('QUOTATIONSHEET_CUSTOMER.xlsx')
                            ->setSheetName('Supplier' . $supplier->getId())
                            ->displaySelectionheadercustomer(
                                $cadencier, $this->get('security.token_storage')->getToken()->getUser()
                            )
                            ->setInitline(18)
                            ->exportQuotationCustomerRender($offers, $cadencier, $profil, false, true)
                            ->output($supplier->getId());
                        die;
                    } elseif ($this->get('security.token_storage')->getToken()->getUser()->getContact()->getType() == "4") {
                        $export->load('QUOTATIONSHEET.xlsx')
                            ->setSheetName('Supplier' . $supplier->getId())
                            ->displaySelectionheadercustomer(
                                $cadencier, $this->get('security.token_storage')->getToken()->getUser()
                            )
                            ->setInitline(18)
                            ->exportQuotationCustomerRender(
                                $offers, $cadencier, $profil, $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'), true
                            )
                            ->output($supplier->getId());
                    }
                }

                $export->load('QUOTATIONSHEET_1.xlsx')
                    ->setSheetName('Supplier' . $supplier->getId())
                    ->displaySelectionheader($cadencier, $this->get('security.token_storage')->getToken()->getUser())
                    ->setInitline(18)
                    ->exportQuotationCustomerRender(
                        $offers, $cadencier, $profil, $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'), true
                    )
                    ->output($supplier->getId());
                die;
            }
        }
    }

    public function exportciAction($id)
    {
        ini_set('max_execution_time', 240);
        ini_set('memory_limit', '300M');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted(
                'ROLE_ADMINSUPPLYCHAIN'
            )
        ) {

            $ci = $this->getDoctrine()
                ->getRepository('appBundle:Invoicecustomer')
                ->findOneById($id);

            if (!$ci) {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }

            $cadencier = $ci->getCadencier();
            $profil = $this->GetProfil($cadencier);


            if (!$cadencier) {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }

            if (!$cadencier->getStepProduction() instanceof \datetime) {
                $this->get('session')->getFlashBag()->add('warning', 'Ask admin to check Production date');

                return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->GetId())));
            }

            if (!$ci->getService()) {
                if ($cadencier->GetCommand()->GetTcnum() == "-" || !$cadencier->GetCommand()->GetTcnum()) {
                    $this->get('session')->getFlashBag()->add('warning', 'No container number');

                    return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->GetId())));
                }
                if ($cadencier->GetCommand()->GetSealnum() == "-" || !$cadencier->GetCommand()->GetSealnum()) {
                    $this->get('session')->getFlashBag()->add('warning', 'No seal number');

                    return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->GetId())));
                }

                if ($cadencier->GetCommand()->GetEta() < $cadencier->GetCommand()->GetEtd()) {
                    $this->get('session')->getFlashBag()->add('warning', 'Error: ETA>ETD');

                    return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->GetId())));
                }

                if (!$cadencier->getLoadingdate()) {
                    $this->get('session')->getFlashBag()->add('warning', 'No loading date');

                    return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->GetId())));
                }
            }

            $supplier = $cadencier->GetSupplier();

            $offers = $this->getDoctrine()
                ->getManager()
                ->getRepository('appBundle:Offer')
                ->getOffersFromSupplierClient($supplier->GetId(), $profil, $cadencier->getOfferslist());

            $export = $this->get('PHPExcel');

            if ($cadencier->getState() >= 8) {
                $export->load('CI.xlsx')
                    ->setSheetName('Supplier' . $supplier->getId())
                    ->displayCiElement($ci, $this->get('security.token_storage')->getToken()->getUser())
                    ->setInitline(18)
                    ->exportQuotationCiCustomerRender($offers, $ci)
                    ->output($ci->getNumber());
            }

            die;
        }
    }

    public function exportshippingmarkAction($id)
    {
        ini_set('max_execution_time', 240);
        ini_set('memory_limit', '300M');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted(
                'ROLE_ADMINSUPPLYCHAIN'
            )
        ) {

            $cadencier = $this->GetCadencier($id);

            $profil = $this->GetProfil($cadencier);

            if (!$cadencier) {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }

            $supplier = $cadencier->GetSupplier();

            $offers = $this->getDoctrine()
                ->getManager()
                ->getRepository('appBundle:Offer')
                ->getOffersFromSupplierClient($supplier->GetId(), $profil, $cadencier->getOfferslist());

            $export = $this->get('PHPExcel');

            if ($cadencier->getState() >= 8) {
                $export->load()
                    ->setSheetName('EXPORT EXCEL SHIPPING MARK' . $cadencier->getId())
                    ->setInitline(1)
                    ->exportshippingmarkRender($offers, $cadencier->getId(), $cadencier->getCustomer()->getName())
                    ->output('EXPORT EXCEL SHIPPING MARK' . $cadencier->getId());
            }

            die;
        }
    }

    public function exportquotationsheetpdfAction($id, $type = "qs", $ci = 0, $service = 0)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || ($this->get('security.authorization_checker')->isGranted(
                    'ROLE_CLIENT'
                ) && !$this->get('security.token_storage')->getToken()->getUser()->getExpireDate()) || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')
        ) {
            ini_set('max_execution_time', 480);
            ini_set('memory_limit', '300M');

            $cadencier = $this->GetCadencier($id);

            if (!$this->userLimitDownload($cadencier)) {
                return $this->redirect($this->generateUrl('home'));
                die;
            }

            if (!$cadencier) {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }

            $profil = $this->GetProfil($cadencier);

            $supplier = $cadencier->GetSupplier();

            $offers = $this->getDoctrine()
                ->getManager()
                ->getRepository('appBundle:Offer')
                ->getOffersFromSupplierClient($supplier->GetId(), $profil, $cadencier->getOfferslist());

            if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
                $user = $this->get('security.token_storage')->getToken()->getUser();
                $Historycustomer = new Historycustomer();
                $Historycustomer->setTitle('EXPORT_PDF_CADENCIER');
                $Historycustomer->setDescription('EXPORT_PDF_CADENCIER');
                $Historycustomer->setIco('<i class="icon-download-alt"></i>');
                $Historycustomer->setDate(new \DateTime());
                $Historycustomer->setUser($user);
                $Historycustomer->setCustomer($user->getContact()->getCustomer());
                $Historycustomer->setCadencier($cadencier);
                $em = $this->getDoctrine()->getManager();
                $em->persist($Historycustomer);
                $em->flush();
            }

            $user = $this->get('security.token_storage')->getToken()->getUser();
            $pdf = $this->get('PdfGen');

            $datas = array();
            $datas['{{qsenumber}}'] = "";
            $datas['{{customername}}'] = "";
            $datas['{{customeraddress}}'] = "";
            $datas['{{customerzip}}'] = "";
            $datas['{{customercity}}'] = "";
            $datas['{{customercountry}}'] = "";
            $datas['{{refsupplier}}'] = "";
            $datas['{{incorterm}}'] = "";
            $datas['{{validityoffer}}'] = "";
            $datas['{{paymentterm}}'] = "";
            $datas['{{deliverytime}}'] = "";
            $datas['{{commercialname}}'] = "";
            $datas['{{commercialemail}}'] = "";

            $datas['{{TITLE_REFERENCE_SUPPLIER}}'] = $this->get('translator')->trans('TITLE_REFERENCE_SUPPLIER');
            $datas['{{TITLE_INCOTERM}}'] = $this->get('translator')->trans('TITLE_INCOTERM');
            $datas['{{TITLE_DELECERVYTIME}}'] = $this->get('translator')->trans('TITLE_DELECERVYTIME');
            $datas['{{TITLE_VALIDITYOFFER}}'] = $this->get('translator')->trans('TITLE_VALIDITYOFFER');
            $datas['{{TITLE_PAYMENTTERM}}'] = $this->get('translator')->trans('TITLE_PAYMENTTERM');
            $datas['{{SELECTION_BOTTOM_WARINING}}'] = $this->get('translator')->trans('SELECTION_BOTTOM_WARINING');
            $datas['{{OF_PAGE}} '] = $this->get('translator')->trans('OF_PAGE');
            $datas['{{TITLE_TO}} '] = $this->get('translator')->trans('TITLE_TO');
            $datas['{{TITLE_SALER}} '] = $this->get('translator')->trans('TITLE_SALER');
            $datas['{{TITLE_ITEM_NO}}'] = $this->get('translator')->trans('TITLE_ITEM_NO');
            $datas['{{TITLE_DESCRIPTION}}'] = $this->get('translator')->trans('TITLE_DESCRIPTION');
            $datas['{{TITLE_PHOTO}}'] = $this->get('translator')->trans('TITLE_PHOTO');
            $datas['{{TITLE_PRODUCT_SIZE_CM}}'] = $this->get('translator')->trans('TITLE_PRODUCT_SIZE_CM');
            $datas['{{TITLE_PACKING_SIZE_CM}}'] = $this->get('translator')->trans('TITLE_PACKING_SIZE_CM');
            $datas['{{TITLE_COLOR}}'] = $this->get('translator')->trans('TITLE_COLOR');
            $datas['{{TITLE_PACKING}}'] = $this->get('translator')->trans('TITLE_PACKING');
            $datas['{{TITLE_PCB}}'] = $this->get('translator')->trans('TITLE_PCB');
            $datas['{{TITLE_CBM_PACKING}}'] = $this->get('translator')->trans('TITLE_CBM_PACKING');
            $datas['{{TITLE_UNIT_PRICE}}'] = $this->get('translator')->trans('TITLE_UNIT_PRICE');
            $datas['{{TITLE_ORDER_QUANTITY_PDF}}'] = $this->get('translator')->trans('TITLE_ORDER_QUANTITY_PDF');
            $datas['{{TITLE_TOTAL_CBM}}'] = $this->get('translator')->trans('TITLE_TOTAL_CBM');
            $datas['{{TITLE_TOTAL_USD}}'] = $this->get('translator')->trans('TITLE_TOTAL_USD');
            $datas['{{BANK_INFORMATION}}'] = $this->get('translator')->trans('BANK_INFORMATION');
            $datas['{{TITLE_SIGNATURE}}'] = $this->get('translator')->trans('TITLE_SIGNATURE');
            $datas['{{TITLE_ITEM_NO}}'] = $this->get('translator')->trans('TITLE_ITEM_NO');
            $datas['{{TITLE_DESCRIPTION}}'] = $this->get('translator')->trans('TITLE_DESCRIPTION');
            $datas['{{TITLE_PRODUCT_SIZE_CM}}'] = $this->get('translator')->trans('TITLE_PRODUCT_SIZE_CM');
            $datas['{{TITLE_PACKING_SIZE_CM}}'] = $this->get('translator')->trans('TITLE_PACKING_SIZE_CM');
            $datas['{{TITLE_CBM}}'] = $this->get('translator')->trans('TITLE_CBM_PACKING');
            $datas['{{TITLE_PCB}}'] = $this->get('translator')->trans('TITLE_PCB');
            $datas['{{TITLE_QTY}}'] = $this->get('translator')->trans('TITLE_QTY');
            $datas['{{TITLE_CBM_PACKING}}'] = $this->get('translator')->trans('TITLE_CBM_PACKING');
            $datas['{{TITLE_UNIT_PRICE}}'] = $this->get('translator')->trans('TITLE_UNIT_PRICE');
            $datas['{{ENTITY_OFFER_MOQ}}'] = $this->get('translator')->trans('ENTITY_OFFER_MOQ');

            $datas['{{TITLE_BARCODE}}'] = $this->get('translator')->trans('TITLE_BARCODE');
            $datas['{{TITLE_MATERIEL}}'] = $this->get('translator')->trans('TITLE_MATERIEL');
            $datas['{{TITLE_ORDER_QUANTITYPCS}}'] = $this->get('translator')->trans('TITLE_ORDER_QUANTITYPCS');
            $datas['{{TITLE_ORDER_PACKAGECTNS}}'] = $this->get('translator')->trans('TITLE_ORDER_PACKAGECTNS');
            $datas['{{TITLE_NETWEIGHT}}'] = $this->get('translator')->trans('TITLE_NETWEIGHT');
            $datas['{{TITLE_GROSSWEIGHT}}'] = $this->get('translator')->trans('TITLE_GROSSWEIGHT');
            $datas['{{TITLE_TOTALNETWEIGHT}}'] = $this->get('translator')->trans('TITLE_TOTALNETWEIGHT');
            $datas['{{TITLE_TOTALGROSSWEIGHT}}'] = $this->get('translator')->trans('TITLE_TOTALGROSSWEIGHT');
            if ($cadencier->getStepValidation() instanceof \datetime) {
                $datas['{{date}}'] = $cadencier->getStepValidation()->format('d-m-Y');
            } else {
                $datas['{{date}}'] = date('d-m-Y');
            }

            $template = 'quotationsheet.html';

            if (false && $cadencier->getCustomer() and $cadencier->getCustomer()->GetActivity()->getId() == 6) {
                $datas['{{commercialname}}'] = $user->getName() . ' ' . $user->getPhone();
                $datas['{{commercialemail}}'] = $user->GetEmail();
                $datas['{{customername}}'] = $cadencier->getFinalcustomer();
            } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
                if ($user->getContact()->getCustomer()->getCommercialtools() and $profil['mode'] == "commercial") {
                    $template = 'quotationsheetcustomer.html';
                    $datas['{{customer}}'] = $cadencier->getFinalcustomer();
                    $datas['{{commercialname}}'] = $user->getName() . ' ' . $user->getPhone();
                    $datas['{{commercialemail}}'] = $user->GetEmail();
                    $datas['{{logocustomer}}'] = $cadencier->GetCustomer()->GetLogo()->GetImage();
                    $datas['{{customername}}'] = $cadencier->GetCustomer()->GetName();
                    $datas['{{customeraddress}}'] = $cadencier->GetCustomer()->GetAddress();
                    $datas['{{customercity}}'] = $cadencier->GetCustomer()->GetCity();
                    $datas['{{customerzip}}'] = $cadencier->GetCustomer()->GetPc();
                } else {
                    $template = 'quotationsheet.html';
                    $datas['{{customername}}'] = $cadencier->GetCustomer()->GetName();
                    $datas['{{customeraddress}}'] = $cadencier->GetCustomer()->GetAddress();
                    $datas['{{customercity}}'] = $cadencier->GetCustomer()->GetCity();
                    $datas['{{customerzip}}'] = $cadencier->GetCustomer()->GetPc();
                    $datas['{{customercountry}}'] = $cadencier->GetCustomer()->GetCountry()->GetTitle();
                    $datas['{{paymentterm}}'] = $cadencier->getCustomerpaymenterms()->GetTitle();
                }
            } elseif ($cadencier->GetCustomer()) {
                $template = 'quotationsheet.html';
                $datas['{{customername}}'] = $cadencier->GetCustomer()->GetName();
                $datas['{{customeraddress}}'] = $cadencier->GetCustomer()->GetAddress();
                $datas['{{customercity}}'] = $cadencier->GetCustomer()->GetCity();
                $datas['{{customerzip}}'] = $cadencier->GetCustomer()->GetPc();
                $datas['{{customercountry}}'] = $cadencier->GetCustomer()->GetCountry()->GetTitle();
                $datas['{{paymentterm}}'] = $cadencier->getCustomerpaymenterms() ? $cadencier->getCustomerpaymenterms()->GetTitle() : '';


                if ($cadencier->GetCustomer()->getExportsalername() && $cadencier->GetCustomer()->getExportsaleremail()
                ) {
                    $commercialname = $cadencier->GetCustomer()->getExportsalername();
                    $commercialemail = $cadencier->GetCustomer()->getExportsaleremail();
                } else {
                    $commercial = $cadencier->GetCustomer()->GetMainsaler();
                    if ($commercial) {
                        $commercialname = $commercial->GetName() . ' ' . $commercial->getPhone();
                        $commercialemail = $commercial->GetEmail();
                    }
                }
                $datas['{{commercialname}}'] = $commercialname;
                $datas['{{commercialemail}}'] = $commercialemail;
            } else {
                if ($cadencier->getFinalcustomer()) {
                    $datas['{{customername}}'] = $cadencier->getFinalcustomer();
                    $datas['{{commercialname}}'] = $user->getName() . ' ' . $user->getPhone();
                    $datas['{{commercialemail}}'] = $user->GetEmail();
                }
            }

            if ($cadencier->getSupplier()) {
                $datas['{{refsupplier}}'] = $cadencier->getSupplier()->GetId();
                $datas['{{validityoffer}}'] = $cadencier->getSupplier()->GetValidityoffer() . $this->get(
                        'translator'
                    )->trans('TITLE_DAYS');
                $datas['{{deliverytime}}'] = (int)($cadencier->GetDeleverytime() + 10) . $this->get('translator')->trans(
                        'TITLE_DAYS'
                    );
            }

            $datas['{{customercomments}}'] = ($cadencier->GetComment()) ? '<br/><table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse;" cellpadding="8">
             <thead>
                <tr><td>' . nl2br($cadencier->GetComment()) . '</td></tr>
            </tbody>
        </table>' : '';

            $datas['{{number}}'] = sprintf("%06d", $cadencier->getId());

            $selections = $cadencier->getSelection();

            $allowacces = ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') or $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') or ($this->get('security.authorization_checker')->isGranted(
                        'ROLE_COMMERCIAL'
                    ) and $cadencier->getState() >= 5) or ($this->get('security.authorization_checker')->isGranted(
                        'ROLE_CLIENT'
                    ) and $cadencier->getState() == 7 and ($user->getContact()->getType() == "4" or $user->getContact()->getType() == "0")));

            if ($type == 'pi' && $allowacces) {
                $datas['{{TITLE_UNIT_PRICE}}'] = 'UnitPrice<br/>' . ' ' . $cadencier->getFinaldevis();
                $datas['{{TITLE_TOTAL}}'] = 'Total' . ' ' . $cadencier->getFinaldevis();
                $datas['{{rows}}'] = "";

                $total = 0;
                $totalqty = 0;
                $totauxcbm = 0;

                $datas['{{incorterm}}'] = $cadencier->getFinalincoterm() . '  ' . $cadencier->getFinalpol();


                foreach ($offers as $offer) {
                    if (array_key_exists('o' . $offer->GetId(), $selections)) {
                        $selection = $selections['o' . $offer->GetId()];
                        $image = '<img style="height:65px" src="https://market.inasia-corp.com/' . $offer->getProduct()->getMainimage() . '"/>';
                        $totalcmb = $selection['qty'] * $offer->getProduct()->GetCbm() / $offer->getProduct()->GetPcb();
                        $datas['{{rows}}'] .= ' <tr>
                    <td align="center" >' . $offer->getProduct()->GetId() . '</td>';
                        if ($this->get('request')->getLocale() == "fr") {
                            $datas['{{rows}}'] .= '<td >' . $offer->getProduct()->GetFrenchdescription() . '</td>';
                        } else {
                            $datas['{{rows}}'] .= '<td >' . $offer->getProduct()->GetDescription() . '</td>';
                        }
                        $datas['{{rows}}'] .= '<td align="center" >' . $image . '</td>
                    <td align="right" >' . $offer->getProduct()->getProductsize() . '</td>
                    <td align="right" >' . $offer->getProduct()->getColoris() . '</td>
                    <td align="right" >' . $offer->getProduct()->getPacking() . '</td>
                    <td align="right">' . $offer->getProduct()->GetPcb() . '</td>
                    <td align="right" >' . number_format($offer->getProduct()->GetCbm(), 3, ',', '') . '</td>
                    <td align="right">' . number_format($selection['finalprice'], 2, ',', '') . '$</td>
                    <td align="right" > ' . $selection['qty'] . $offer->getMoqunit()->getTitle() . '</td>
                    <td align="right" >' . number_format($totalcmb, 3, ',', '') . '</td>
                    <td align="right">' . number_format(
                                $selection['qty'] * $selection['finalprice'], 2, ',', ''
                            ) . '$</td>
                </tr>';
                    }
                    $totalqty += $selection['qty'];
                    $total += $selection['qty'] * $selection['finalprice'];
                    $totauxcbm += $totalcmb;
                }
                $totaltxt = ((float)$cadencier->GetFinaltva()) ? "TOTAL HT" : " TOTAL ";

                if ($cadencier->GetServicedescription()) {
                    foreach ($cadencier->GetServicedescription() as $service) {
                        if (array_key_exists('description', $service) && array_key_exists('value', $service)) {
                            $datas['{{rows}}'] .= '<tr><td>SERVICES</td><td colspan="7">' . $service['description'] . '</td><td style="text-align:center;">' . number_format(
                                    $service['value'], 2, ',', ''
                                ) . '$</td><td style="text-align:center;">1</td><td></td><td style="text-align:center;">' . number_format(
                                    $service['value'], 2, ',', ''
                                ) . '$</td></tr>';
                            $total += $service['value'];
                        }
                    }
                }

                $datas['{{rows}}'] .= '<tr><td colspan="9" style="border:0px solid #fff;"></td><td colspan="2" style="text-align:center;"><b>' . $totaltxt . '</td><td colspan="1" style="text-align:center;"><b>' . number_format(
                        $total, 2, ',', ''
                    ) . '$</b></td></tr>';


                if ((float)$cadencier->GetFinaltva()) {
                    $datas['{{rows}}'] .= '<tr><td colspan="9" style="border:0px solid #fff;"></td><td colspan="2" style="text-align:center;"><b>TVA ' . (float)$cadencier->GetFinaltva() . '%</td><td colspan="1" style="text-align:center;"><b>' . number_format(
                            $total * (float)$cadencier->GetFinaltva() / 100, 2, ',', ''
                        ) . '$</b></td></tr>';
                    $datas['{{rows}}'] .= '<tr><td colspan="9" style="border:0px solid #fff;"></td><td colspan="2" style="text-align:center;"><b>TOTAL TTC</td><td colspan="1" style="text-align:center;"><b>' . number_format(
                            $total + $total * (float)$cadencier->GetFinaltva() / 100, 2, ',', ''
                        ) . '$</b></td></tr>';
                }
                if ($type == "pi") {
                    $pdf->load('pi.html')->replace($datas)->set(array('WatermarkText' => ''))->output(
                        'pi-' . $cadencier->getId() . '-' . date('d-m-Y') . '.pdf'
                    );
                }
            } elseif ($type == 'ci' && $allowacces && $cadencier->getState() >= 8) {

                if (!$cadencier->getStepProduction() instanceof \datetime) {
                    $this->get('session')->getFlashBag()->add('warning', 'Ask admin to check Production date');

                    return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->GetId())));
                }


                $ci = $this->getDoctrine()
                    ->getRepository('appBundle:Invoicecustomer')
                    ->findOneById($ci);

                $datas['{{dateci}}'] = $ci->getDate()->format('d-m-Y');

                $datas['{{number}}'] = $ci->getNumber();

                if (!$ci->getService()) {
                    if ($cadencier->GetCommand()->GetTcnum() == "-" || !$cadencier->GetCommand()->GetTcnum()) {
                        $this->get('session')->getFlashBag()->add('warning', 'No container number');

                        return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->GetId())));
                    }

                    if ($cadencier->GetCommand()->GetSealnum() == "-" || !$cadencier->GetCommand()->GetSealnum()) {
                        $this->get('session')->getFlashBag()->add('warning', 'No seal number');

                        return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->GetId())));
                    }

                    if ($cadencier->GetCommand()->GetEta() < $cadencier->GetCommand()->GetEtd()) {
                        $this->get('session')->getFlashBag()->add('warning', 'Error: ETA>ETD');

                        return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->GetId())));
                    }

                    if (!$cadencier->getLoadingdate()) {
                        $this->get('session')->getFlashBag()->add('warning', 'No loading date');

                        return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->GetId())));
                    }
                }


                $datas['{{order}}'] = $cadencier->getId();

                $datas['{{TITLE_UNIT_PRICE}}'] = 'UnitPrice' . ' ' . $cadencier->getFinaldevis();
                $datas['{{TITLE_TOTAL}}'] = 'Total' . ' ' . $cadencier->getFinaldevis();
                $datas['{{incorterm}}'] = $cadencier->getFinalincoterm() . '  ' . $cadencier->getFinalpol();
                $datas['{{containernumber}}'] = $cadencier->GetCommand()->GetTcnum();
                $datas['{{seal}}'] = $cadencier->GetCommand()->GetSealnum();
                $datas['{{etd/eta}}'] = $cadencier->GetCommand()->GetEtd()->format('d-m-Y') . '/' . $cadencier->GetCommand()->GetEta()->format('d-m-Y');

                $datas['{{rows}}'] = "";
                $total = 0;
                $totalqty = 0;
                $totauxcbm = 0;

                if ($service == 0) {
                    $template = 'ci.html';

                    $datas['{{origin}}'] = false;
                    if (count($cadencier->getSupplier()->getAddresses())) {
                        foreach ($cadencier->getSupplier()->getAddresses() as $address) {
                            if ($address->getType() == "Office" && $address->getCity() && $address->getCity()->getProvince() && $address->getCity()->getProvince()->getCountry()) {
                                $datas['{{origin}}'] = $address->getCity()->getProvince()->getCountry()->getTitle();
                            }
                        }
                    }

                    if (!$datas['{{origin}}']) {
                        $datas['{{origin}}'] = "CHINA";
                    }


                    foreach ($offers as $offer) {
                        if (array_key_exists('o' . $offer->GetId(), $selections)) {
                            $selection = $selections['o' . $offer->GetId()];
                            if (!$offer->GetProduct()->GetCodebar()) {
                                $this->get('session')->getFlashBag()->add('warning', 'Please check barcode');

                                return $this->redirect(
                                    $this->generateUrl('command_show', array('id' => $cadencier->GetId()))
                                );
                            }
                            //    $image = '<img style="width:115px" src="/barcodes/' . $offer->GetProduct()->GetCodebar()->GetCodebar() . '.png"/>';
                            $totalcmb = $selection['qty'] * $offer->getProduct()->GetCbm() / $offer->getProduct()->GetPcb();
                            $datas['{{rows}}'] .= ' <tr>
                    <td align="center" >' . $offer->getProduct()->GetId() . '</td>';
                            $datas['{{rows}}'] .= '<td width="16%">' . $offer->getProduct()->GetDescription() . '</td>';
                            $datas['{{rows}}'] .= '<td align="center" >' . $offer->GetProduct()->GetCodebar()->GetCodebar() . '</td>
                    <td align="right" >' . $offer->getProduct()->getMaterial() . '</td>
                    <td align="right" >' . $offer->getProduct()->getProductsize() . '</td>
                    <td align="right" >' . number_format($totalcmb, 3, ',', '') . '</td>
                    <td align="right"> ' . $selection['qty'] . $offer->getMoqunit()->getTitle() . '</td>
                    <td align="right" >' . number_format($selection['finalprice'], 2, ',', '') . '$</td>
                    <td align="right" >' . number_format(
                                    $selection['qty'] * $selection['finalprice'], 2, ',', ''
                                ) . '$</td>
                </tr>';
                        }
                        $totalqty += $selection['qty'];
                        $total += $selection['qty'] * $selection['finalprice'];
                        $totauxcbm += $totalcmb;
                    }
                } else {
                    $template = 'ci_services.html';
                    if ($cadencier->GetServicedescription()) {
                        foreach ($cadencier->GetServicedescription() as $service) {
                            if (array_key_exists('description', $service) && array_key_exists('value', $service)) {
                                $datas['{{rows}}'] .= '<tr><td>SERVICES</td><td colspan="6">' . $service['description'] . '</td><td style="text-align:center;
">1</td><td style="text-align:center;
">' . number_format($service['value'], 2, ',', '') . '</td><td style="text-align:center;
">' . number_format($service['value'], 2, ',', '') . '</td></tr>';
                                $total += $service['value'];
                            }
                        }
                    }
                }

                $totaltxt = ((float)$cadencier->GetFinaltva()) ? "TOTAL HT" : " TOTAL ";
                $datas['{{rows}}'] .= '<tr><td colspan="' . ($service == 0 ? 6 : 7) . '" style="border:0px solid #fff;"></td><td colspan="2" style="text-align:center;"><b>' . $totaltxt . '</td><td colspan="1" style="text-align:center;"><b>' . number_format(
                        $total, 2, ',', ''
                    ) . '$</b></td></tr>';
                if ((float)$cadencier->GetFinaltva()) {
                    $total = number_format($total + $total * (float)$cadencier->GetFinaltva() / 100, 2, ',', '');
                    $datas['{{rows}}'] .= '<tr><td colspan="6" style="border:0px solid #fff;"></td><td colspan="2" style="text-align:center;"><b>TVA ' . (float)$cadencier->GetFinaltva() . '%</td><td colspan="1" style="text-align:center;"><b>' . number_format(
                            $total * (float)$cadencier->GetFinaltva() / 100, 2, ',', ''
                        ) . '$</b></td></tr>';
                    $datas['{{rows}}'] .= '<tr><td colspan="6" style="border:0px solid #fff;"></td><td colspan="2" style="text-align:center;"><b>TOTAL TTC</td><td colspan="1" style="text-align:center;"><b>' . $total . '$</b></td></tr>';
                }

                $total = explode(',', number_format($total, 2, ',', ''));

                if (array_key_exists(0, $total)) {
                    $total1 = $this->Get('Getwords')->num_words($total[0]);
                }
                if (array_key_exists(1, $total)) {
                    $total2 = " AND CENTS " . $this->Get('Getwords')->num_words($total[1]);
                } else {
                    $total2 = "";
                }

                $datas['{{totalin}}'] = "SAY ONLY TOTAL USD " . $total1 . $total2;

                if ($type == "ci") {
                    $pdf->load($template)->replace($datas)->set(array('WatermarkText' => ''))->output(
                        'ci-' . $cadencier->getId() . '-' . date('d-m-Y') . '.pdf'
                    );
                }
            } else {
                if ($cadencier->GetIncoterm()) {
                    $datas['{{incorterm}}'] = $cadencier->GetIncoterm()->GetTitle();
                    if ($cadencier->GetPol() && $cadencier->GetIncoterm()->GetId() == 2) {
                        $datas['{{incorterm}}'] .= ' ' . $cadencier->GetPol()->getTitle();
                    }
                }

                $datas['{{rows}}'] = "";
                foreach ($offers as $offer) {
                    if (array_key_exists('o' . $offer->GetId(), $selections)) {
                        $selection = $selections['o' . $offer->GetId()];
                        $datas['{{rows}}'] .= ' <tr>
                    <td align="center" width="7%">' . $offer->getProduct()->GetId() . '</td>';
                        $datas['{{rows}}'] .= '<td ><img  style="height:65px" src="https://market.inasia-corp.com/' . str_replace(
                                'original.jpg', 'small.jpg', $offer->getProduct()->getMainimage()
                            ) . '" alt=""/></td>';
                        $datas['{{rows}}'] .= '<td>' . $offer->getProduct()->GetDescription() . '</td>';
                        $datas['{{rows}}'] .= '<td width="10%" align="center">' . $offer->getProduct()->GetProductsize() . '</td>';
                        $datas['{{rows}}'] .= '<td width="10%" align="center">' . $offer->getMoq() . ' ' . $offer->getMoqunit()->getTitle() . '</td>
                    <td align="right" width="10%">' . $offer->getSaleprice(
                                strtolower($cadencier->GetIncoterm()->GetTitle()), $profil
                            ) . ' USD</td>';
                        $datas['{{rows}}'] .= '</tr>';
                    }
                }
                $pdf->load($template)->replace($datas)->set(array('WatermarkText' => ''))->output(
                    'quotationsheet-' . date('d-m-Y') . '.pdf'
                );
            }
        }
    }

    public function exportcadencierpicturesAction($id)
    {
        ini_set('max_execution_time', 240);
        ini_set('memory_limit', '900M');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || ($this->get('security.authorization_checker')->isGranted(
                    'ROLE_CLIENT'
                ) && !$this->get('security.token_storage')->getToken()->getUser()->getExpireDate())
        ) {

            /* if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT'))
              if (!$this->get('security.token_storage')->getToken()->getUser()->getContact()->GetCustomer()->getXlsexport())
              throw $this->createNotFoundException('Unable to find Offer entity.'); */
            $cadencier = $this->GetCadencier($id);

            if (!$cadencier) {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }

            $profil = $this->GetProfil($cadencier);

            $supplier = $cadencier->GetSupplier();

            $offers = $this->getDoctrine()
                ->getManager()
                ->getRepository('appBundle:Offer')
                ->getOffersFromSupplierClient($supplier->GetId(), $profil, $cadencier->getOfferslist());

            $createZip = $this->get('createZip');
            $images = 0;
            $createZip->addDirectory('/');
            foreach ($offers as $offer) {
                $directory = $offer->GetProduct()->GetId();
                $createZip->addDirectory($directory);
                $i = 1;
                foreach ($offer->getProduct()->getImages() as $key => $image) {
                    $createZip->addFile(
                        file_get_contents(__SOURCINASIA__ . 'htdocs/' . $image->getImage()), $directory . '/' . $offer->GetProduct()->GetId() . '-' . $i++ . '.jpeg'
                    );
                    $images++;
                }
            }

            if ($images > 0) {
                $createZip->output();
            }
        }
    }

    public function adminstatsAction($start, $stop)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {

            $start = new \datetime($start);
            $stop = new \datetime($stop);

            if ($start > $stop) {
                $start = new \datetime();
                $stop = new \datetime();
                $stop->modify('-1 month');
            }

            $InvoiceCustomers = array();
            $em = $this->getDoctrine()->getManager();
            foreach ($em->createQuery('SELECT Invoicecustomer, customer, cadencier  FROM appBundle:Invoicecustomer Invoicecustomer LEFT JOIN Invoicecustomer.cadencier cadencier LEFT JOIN cadencier.customer customer WHERE Invoicecustomer.date BETWEEN :start AND :stop ORDER BY Invoicecustomer.date DESC')->setParameters(array('start' => $start, 'stop' => $stop))->getResult() as $InvoicecustomersRequest) {
                $Payments = 0;
                foreach ($InvoicecustomersRequest->getPayments() as $Payment) {
                    $Payments += $Payment->getAmount();
                }
                if ($Payments < $InvoicecustomersRequest->getAmount()) {
                    $InvoiceCustomers[] = array('Order' => $InvoicecustomersRequest->getCadencier()->getId(),
                        'Ci Number' => '#' . $InvoicecustomersRequest->getNumber(),
                        'Date' => $InvoicecustomersRequest->getDate()->format('d-m-Y'),
                        'Customer' => $InvoicecustomersRequest->getCadencier()->getCustomer()->getName(),
                        'Amount Ci' => $InvoicecustomersRequest->getAmount(),
                        'Total Payment' => $Payments,
                        'Balance' => $InvoicecustomersRequest->getAmount() - $Payments);
                }
            }

            $InvoiceSupliers = array();
            foreach ($em->createQuery('SELECT Invoicesupplier, supplier, cadencier  FROM appBundle:Invoicesupplier Invoicesupplier LEFT JOIN Invoicesupplier.cadencier cadencier LEFT JOIN cadencier.supplier supplier WHERE Invoicesupplier.date BETWEEN :start AND :stop ORDER BY Invoicesupplier.date DESC')->setParameters(array('start' => $start, 'stop' => $stop))->getResult() as $InvoicesuppliersRequest) {
                $Payments = 0;
                foreach ($InvoicesuppliersRequest->getPayments() as $Payment) {
                    $Payments += $Payment->getAmount();
                }
                if ($Payments < $InvoicesuppliersRequest->getAmount()) {
                    $InvoiceSupliers[] = array('Order' => $InvoicesuppliersRequest->getCadencier()->getId(),
                        'Ci Number' => $InvoicesuppliersRequest->getCadencier()->getSupplierci(),
                        'Date' => $InvoicesuppliersRequest->getDate()->format('d-m-Y'),
                        'Supplier' => ($InvoicesuppliersRequest->getSupplier() ? $InvoicesuppliersRequest->getSupplier()->getName() : '-'),
                        'Amount Ci' => $InvoicesuppliersRequest->getAmount(),
                        'Total Payment' => $Payments,
                        'Balance' => $InvoicesuppliersRequest->getAmount() - $Payments);
                }
            }

            $export = $this->get('PHPExcel')->load()
                ->addSheet(0, 'Supplier payments')
                ->arrayToXls($InvoiceSupliers)
                ->addSheet(1, 'Customer payments')
                ->arrayToXls($InvoiceCustomers)
                ->removeSheet(2)
                ->output($start->format('d-m-Y') . '-' . $stop->format('d-m-Y'));
        }
    }


    private function GetCadencier($id)
    {
        $profil = $this->get('profils')->get();

        $em = $this->getDoctrine()->getRepository('appBundle:Cadencier');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            return $em->getCommercialCadencier($id);
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
            return $em->getCommercialCadencier($id);
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && $profil['cutomer_id']) {
            return $em->getCustomerCadencier(
                $this->get('security.token_storage')->getToken()->getUser()->GetId(), $profil['cutomer_id'], $id
            );
        } else {
            return false;
        }
    }

    /*
     * Export fixprice
     */

    public function exportfixpriceAction($id)
    {
        ini_set('max_execution_time', 240);
        ini_set('memory_limit', '300M');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
            $offers = $this->getDoctrine()
                ->getManager()
                ->getRepository('appBundle:Offer')
                ->getFixedPrice($id);

            $export = $this->get('PHPExcel');

            $export->load()
                ->setSheetName(date('Y-m-d'))
                ->setInitline(1)
                ->exportfixedpriceRender($offers, $id)
                ->output(date('Y-m-d') . '-' . $id);
        }
    }

    private function GetProfil($cadencier)
    {

        $profil = $this->get('profils')->get();

        if ($cadencier) {
            if ($cadencier->getCustomer()) {
                $profil['cutomer_name'] = $cadencier->getCustomer()->GetName();
                $profil['cutomer_id'] = $cadencier->getCustomer()->Getid();
                $profil['commercialtools'] = $cadencier->getCustomer()->GetCommercialtools();
                $profil['customer_nomenclatures'] = explode(',', $cadencier->getCustomer()->GetNomenclatures());
                $profil['customer_supplierrestriction'] = $this->implodeEntities(
                    $cadencier->getCustomer()->getSuppliersrestriction()
                );
                if ($cadencier->getCustomer()->Getlogo()) {
                    $profil['customer_logo'] = $cadencier->getCustomer()->Getlogo()->getImage();
                } else {
                    $profil['customer_logo'] = '/bundles/app/images/logoh.png';
                }
                $profil['customer_zone'] = $this->implodeEntities($cadencier->getCustomer()->getZones());
                $profil['customer_coef'] = $cadencier->getCustomer()->GetMargecoef();
                $profil['contact_type'] = $cadencier->getCustomer()->GetActivity()->getId() == 6 ? 4 : 0;
                //$profil['commercial_coef'] =$cadencier->getCustomer()->GetMargecoef();
            } //else {
                //  $profil['commercial_coef'] =$cadencier->getCustomer()->GetMargecoef();
          //  }
        }

        return $profil;
    }

    /*
     * Service: profil, ExportController
     */

    private function implodeEntities($entites)
    {
        $ids = array();
        foreach ($entites as $entite) {
            $ids[$entite->getId()] = $entite->getId();
        }

        return $ids;
    }

    public function catalogpdfAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            if (!$this->userLimitDownload()) {
                return $this->redirect($this->generateUrl('home'));
                die;
            }

            $pdf = $this->get('PdfGen');
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $session = $this->get('session');
            $datas = $this->getPdfTranslator();

            $offers = array();
            $nomenclatures = array();
            $exw = $fob = false;

            $datas['{{date}}'] = date('d-m-Y');
            $datas['{{number}}'] = date('hms');

            $profil = $this->get('Profils')->get();
            $customer = $this->getDoctrine()
                ->getRepository('appBundle:Customer')
                ->findOneById($profil['cutomer_id']);

            if ($customer) {
                // Customer
                $datas['{{customername}}'] = $customer->GetName();
                $datas['{{customeraddress}}'] = $customer->GetAddress();
                $datas['{{customercity}}'] = $customer->GetCity();
                $datas['{{customerzip}}'] = $customer->GetPc();
                $datas['{{customercountry}}'] = $customer->GetCountry()->GetTitle();

                // Commercial
                $datas['{{commercialname}}'] = $user->getName() . ' ' . $user->getPhone();
                $datas['{{commercialemail}}'] = $user->GetEmail();

                $offers = $this->getDoctrine()
                    ->getRepository('appBundle:Offer')
                    ->getProductsCatalog($session->get('selectionids'), $this->get('profils')->get());

                foreach ($offers as $offer) {
                    $exw = $exw || $offer->getSaleprice(strtolower('exw'), $profil) != '0.00';
                    $fob = $fob || $offer->getSaleprice(strtolower('fob'), $profil) != '0.00';
                }

                $template = 'catalog.html';
            } else {

                // Commercial
                $datas['{{commercialname}}'] = $user->getName() . ' ' . $user->getPhone();
                $datas['{{commercialemail}}'] = $user->GetEmail();


                $offers = $this->getDoctrine()
                    ->getRepository('appBundle:Offer')
                    ->getProductsCatalog($session->get('selectionids'), $this->get('Profils')->get());

                foreach ($offers as $offer) {
                    $exw = $exw || $offer->getSaleprice(strtolower('exw'), $profil) != '0.00';
                    $fob = $fob || $offer->getSaleprice(strtolower('fob'), $profil) != '0.00';
                }

                $template = 'catalogAnonymous.html';
            }

            $line = array();
            foreach ($offers as $offer) {
                $vals = array();
                $vals[] = '<td>' . $offer->getProduct()->GetId() . '</td>';
                $vals[] = '<td><img  style="height:65px" src="https://market.inasia-corp.com/' . str_replace(
                        'original.jpg', 'small.jpg', $offer->getProduct()->getMainimage()
                    ) . '" alt=""/></td>';
                $vals[] = '<td>' . (($user->getLocale() == "fr") ? $offer->getProduct()->GetFrenchdescription() : $offer->getProduct()->GetDescription()) . '</td>';
                $vals[] = '<td style="text-align:center">' . $offer->getMoq() . '</td>';
                if ($exw) {
                    $vals[] = '<td style="text-align:center">' . ($offer->getSaleprice(strtolower('exw'), $profil, true) > 0 ? $offer->getSaleprice(strtolower('exw'), $profil, true) : '-') . '</td>';
                }
                if ($fob) {
                    $vals[] = '<td style="text-align:center">' . ($offer->getSaleprice(strtolower('fob'), $profil, true) > 0 ? $offer->getSaleprice(strtolower('fob'), $profil, true) : '-') . '</td>';
                }
                $line[] = '<tr>' . implode('', $vals) . '</tr>';
            }

            $datas['heads'] = '<tr>
        <td width="7%">' . $this->get('translator')->trans('TITLE_ITEM_NO') . '</td>
        <td>' . $this->get('translator')->trans('ENTITY_USER_PICTURE') . '</td>
        <td >' . $this->get('translator')->trans('TITLE_DESCRIPTION') . '</td>
        <td width="7%">' . $this->get('translator')->trans('ENTITY_OFFER_MOQ') . '</td>' .
                ($exw ? '<td width="7%">EXW USD</td>' : '') .
                ($fob ? '<td width="7%">FOB USD</td>' : '') .
                '</tr>';
            $datas['{{rows}}'] = implode('', $line);

            $pdf->load($template)->replace($datas)->set(array('WatermarkText' => ''))->output(
                'catalog-' . '-' . date('d-m-Y') . '.pdf'
            );
        }
    }

    public function catalogxlsAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            if (!$this->userLimitDownload()) {
                return $this->redirect($this->generateUrl('home'));
                die;
            }

            $user = $this->get('security.token_storage')->getToken()->getUser();
            $session = $this->get('session');
            $export = $this->get('PHPExcel');

            $offers = array();
            $nomenclatures = array();
            $exw = $fob = false;

            $profil = $this->get('Profils')->get();
            $customer = $this->getDoctrine()
                ->getRepository('appBundle:Customer')
                ->findOneById($profil['cutomer_id']);

            if ($customer) {
                if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
                    $offers = $this->getDoctrine()
                        ->getRepository('appBundle:Offer')
                        ->getProductsCatalog($session->get('selectionids'), $this->get('Profils')->get());
                } else {
                    $offers = $this->getDoctrine()
                        ->getRepository('appBundle:Offer')
                        ->getAllProductsCatalog($session->get('selectionids'), $this->get('Profils')->get());
                }
                foreach ($offers as $offer) {
                    $exw = $exw || $offer->getSaleprice(strtolower('exw'), $profil) != '0.00';
                    $fob = $fob || $offer->getSaleprice(strtolower('fob'), $profil) != '0.00';
                }

                $template = 'CATALOG.xlsx';
            } else {
                $offers = $this->getDoctrine()
                    ->getRepository('appBundle:Offer')
                    ->getProductsCatalog($session->get('selectionids'), $this->get('Profils')->get());

                foreach ($offers as $offer) {
                    $exw = $exw || $offer->getSaleprice(strtolower('exw'), $profil) != '0.00';
                    $fob = $fob || $offer->getSaleprice(strtolower('fob'), $profil) != '0.00';
                }
                $template = 'CATALOG.xlsx';
            }

            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                $access = "ROLE_ADMIN";
            } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
                $access = "ROLE_SOURCING";
            } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                $access = "ROLE_COMMERCIAL";
            } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
                $access = "ROLE_USER";
            } else {
                $access = false;
            }

            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') || $this->get('security.authorization_checker')->isGranted(
                    'ROLE_SOURCING'
                ) || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')
            ) {
                $Profil = $this->get('profils')->get();
                if (array_key_exists('cutomer_id', $Profil) and $Profil['cutomer_id'] and !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                    $export = $export->load($template)
                        ->setSheetName('QUOTATIONSHEET' . date('Y-m-d'))
                        ->displayCatalogHeader($user, ($customer ? $customer : false))
                        ->setInitline(18)
                        ->exportCatalogCustomer($offers, $fob, $exw, $access, $this->get('Profils')->get())
                        ->output('QUOTATIONSHEET' . date('Y-m-d'));
                } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                    $export = $export->load($template)
                        ->setSheetName('QUOTATIONSHEET' . date('Y-m-d'))
                        ->displayCatalogHeader($user, ($customer ? $customer : false))
                        ->setInitline(18)
                        ->exportCatalog($offers, $fob, $exw, $access, $Profil)
                        ->output('QUOTATIONSHEET' . date('Y-m-d'));
                } else {
                    $this->get('session')->getFlashBag()->add(
                        'notice', 'Merci de vous connecter en tant que client pour réaliser un export xls'
                    );
                    return $this->redirect($this->generateUrl('home'));
                }
            } else {
                $export = $export->load($template)
                    ->setSheetName('QUOTATIONSHEET' . date('Y-m-d'))
                    ->displayCatalogHeader($user, ($customer ? $customer : false))
                    ->setInitline(18)
                    ->exportCatalogCustomer($offers, $fob, $exw, $access, $this->get('Profils')->get())
                    ->output('QUOTATIONSHEET' . date('Y-m-d'));
            }
        }
        return null;
    }

    public
    function cataloghtmlAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $session = $this->get('session');
            foreach ($session->get('selectionids') as $offer) {
                $offer = $this->getDoctrine()
                    ->getRepository('appBundle:Offer')
                    ->getProductCatalog($offer, $this->get('Profils')->get());
                if ($offer) {
                    $datas = array();
                }

                $template = dirname(__FILE__) . "/templates/" . $template;
                if (file_exists($template)) {
                    $this->html = file_get_contents($template);
                }
            }
        }
    }

    private
    function getPdfTranslator()
    {
        $datas = array();
        $datas['{{qsenumber}}'] = "";
        $datas['{{customername}}'] = "";
        $datas['{{customeraddress}}'] = "";
        $datas['{{customerzip}}'] = "";
        $datas['{{customercity}}'] = "";
        $datas['{{customercountry}}'] = "";
        $datas['{{refsupplier}}'] = "";
        $datas['{{incorterm}}'] = "";
        $datas['{{validityoffer}}'] = "";
        $datas['{{paymentterm}}'] = "";
        $datas['{{deliverytime}}'] = "";
        $datas['{{commercialname}}'] = "";
        $datas['{{commercialemail}}'] = "";
        $datas['{{customercomments}}'] = "";
        $datas['{{TITLE_REFERENCE_SUPPLIER}}'] = $this->get('translator')->trans('TITLE_REFERENCE_SUPPLIER');
        $datas['{{ENTITY_OFFER_MOQ}}'] = $this->get('translator')->trans('ENTITY_OFFER_MOQ');
        $datas['{{TITLE_PICTURE}}'] = $this->get('translator')->trans('ENTITY_USER_PICTURE');
        $datas['{{TITLE_INCOTERM}}'] = $this->get('translator')->trans('TITLE_INCOTERM');
        $datas['{{TITLE_DELECERVYTIME}}'] = $this->get('translator')->trans('TITLE_DELECERVYTIME');
        $datas['{{TITLE_VALIDITYOFFER}}'] = $this->get('translator')->trans('TITLE_VALIDITYOFFER');
        $datas['{{TITLE_PAYMENTTERM}}'] = $this->get('translator')->trans('TITLE_PAYMENTTERM');
        $datas['{{SELECTION_BOTTOM_WARINING}}'] = $this->get('translator')->trans('SELECTION_BOTTOM_WARINING');
        $datas['{{OF_PAGE}} '] = $this->get('translator')->trans('OF_PAGE');
        $datas['{{TITLE_TO}} '] = $this->get('translator')->trans('TITLE_TO');
        $datas['{{TITLE_SALER}} '] = $this->get('translator')->trans('TITLE_SALER');
        $datas['{{TITLE_ITEM_NO}}'] = $this->get('translator')->trans('TITLE_ITEM_NO');
        $datas['{{TITLE_DESCRIPTION}}'] = $this->get('translator')->trans('TITLE_DESCRIPTION');
        $datas['{{TITLE_PHOTO}}'] = $this->get('translator')->trans('TITLE_PHOTO');
        $datas['{{TITLE_PRODUCT_SIZE_CM}}'] = $this->get('translator')->trans('TITLE_PRODUCT_SIZE_CM');
        $datas['{{TITLE_PACKING_SIZE_CM}}'] = $this->get('translator')->trans('TITLE_PACKING_SIZE_CM');
        $datas['{{TITLE_COLOR}}'] = $this->get('translator')->trans('TITLE_COLOR');
        $datas['{{TITLE_PACKING}}'] = $this->get('translator')->trans('TITLE_PACKING');
        $datas['{{TITLE_PCB}}'] = $this->get('translator')->trans('TITLE_PCB');
        $datas['{{TITLE_CBM_PACKING}}'] = $this->get('translator')->trans('TITLE_CBM_PACKING');
        $datas['{{TITLE_UNIT_PRICE}}'] = $this->get('translator')->trans('TITLE_UNIT_PRICE');
        $datas['{{TITLE_ORDER_QUANTITY_PDF}}'] = $this->get('translator')->trans('TITLE_ORDER_QUANTITY_PDF');
        $datas['{{TITLE_TOTAL_CBM}}'] = $this->get('translator')->trans('TITLE_TOTAL_CBM');
        $datas['{{TITLE_TOTAL_USD}}'] = $this->get('translator')->trans('TITLE_TOTAL_USD');
        $datas['{{BANK_INFORMATION}}'] = $this->get('translator')->trans('BANK_INFORMATION');
        $datas['{{TITLE_SIGNATURE}}'] = $this->get('translator')->trans('TITLE_SIGNATURE');
        $datas['{{TITLE_ITEM_NO}}'] = $this->get('translator')->trans('TITLE_ITEM_NO');
        $datas['{{TITLE_DESCRIPTION}}'] = $this->get('translator')->trans('TITLE_DESCRIPTION');
        $datas['{{TITLE_PRODUCT_SIZE_CM}}'] = $this->get('translator')->trans('TITLE_PRODUCT_SIZE_CM');
        $datas['{{TITLE_PACKING_SIZE_CM}}'] = $this->get('translator')->trans('TITLE_PACKING_SIZE_CM');
        $datas['{{TITLE_CBM}}'] = $this->get('translator')->trans('TITLE_CBM_PACKING');
        $datas['{{TITLE_PCB}}'] = $this->get('translator')->trans('TITLE_PCB');
        $datas['{{TITLE_QTY}}'] = $this->get('translator')->trans('TITLE_QTY');
        $datas['{{TITLE_CBM_PACKING}}'] = $this->get('translator')->trans('TITLE_CBM_PACKING');
        $datas['{{TITLE_UNIT_PRICE}}'] = $this->get('translator')->trans('TITLE_UNIT_PRICE');
        $datas['{{TITLE_BARCODE}}'] = $this->get('translator')->trans('TITLE_BARCODE');
        $datas['{{TITLE_MATERIEL}}'] = $this->get('translator')->trans('TITLE_MATERIEL');
        $datas['{{TITLE_ORDER_QUANTITYPCS}}'] = $this->get('translator')->trans('TITLE_ORDER_QUANTITYPCS');
        $datas['{{TITLE_ORDER_PACKAGECTNS}}'] = $this->get('translator')->trans('TITLE_ORDER_PACKAGECTNS');
        $datas['{{TITLE_NETWEIGHT}}'] = $this->get('translator')->trans('TITLE_NETWEIGHT');
        $datas['{{TITLE_GROSSWEIGHT}}'] = $this->get('translator')->trans('TITLE_GROSSWEIGHT');
        $datas['{{TITLE_TOTALNETWEIGHT}}'] = $this->get('translator')->trans('TITLE_TOTALNETWEIGHT');
        $datas['{{TITLE_TOTALGROSSWEIGHT}}'] = $this->get('translator')->trans('TITLE_TOTALGROSSWEIGHT');

        return $datas;
    }

}
