<?php

namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * User controller.
 *
 */
class UserController extends Controller
{
    /*
     * Give to contact access to application
     */

    public function RegeneratepasswordAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $contact = $em->getRepository('appBundle:Contact')->find($id);
            if ($contact->getEmail()) {
                $user = $contact->getUser();
                if ($user) {
                    $password = rand(100000, 1000000);
                    $user->setRoles(array('ROLE_CLIENT', 'ROLE_USER'));
                    $user->setEnabled(true);
                    $user->setPlainPassword($password);
                    $user->setToken($password * 1981);
                    $userManager = $this->container->get('fos_user.user_manager');
                    $userManager->updateUser($user, true);
                    $this->get('session')->getFlashBag()->add('notice', 'Password regenerate');
                    $this->sendmail($user, 'MAIL_TITLE_REGENERATEPASSWORD', 'MAIL_MESSAGE_REGENERATEPASSWORD', $password);
                }
            } else
                $this->get('session')->getFlashBag()->add('warning', 'Contact must have a email address to open access');

            return $this->redirect($this->generateUrl('customer_show', array('id' => $contact->GetCustomer()->GetId())) . '#contacts');
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $this->get('session')->getFlashBag()->add('warning', 'Forbidden');
            return $this->redirect($this->generateUrl('catalog'));
        }
    }

    /*
     * Give to contact access to application
     */

    public function GivecontactacessAction($id, $demo = 0)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')||$this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $em = $this->getDoctrine()->getManager();
            $contact = $em->getRepository('appBundle:Contact')->find($id);
            if ($contact->getEmail()) {
                $user = $em->getRepository('appBundle:User')->findOneByContact($id);
                if (empty($user)) {
                    if (!$this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneByEmail($contact->getEmail())) {
                        $login = str_replace(' ', '-', $contact->getCustomer()->getName() . $contact->getId());
                        $password = rand(100000, 1000000);
                        $userManager = $this->container->get('fos_user.user_manager');
                        $userAdmin = $userManager->createUser();
                        $userAdmin->setUsername($contact->getEmail());
                        $userAdmin->setEmail($contact->getEmail());
                        $userAdmin->setPlainPassword($password);
                        $userAdmin->setContact($contact);
                        $userAdmin->setRoles(array('ROLE_CLIENT', 'ROLE_USER'));
                        $userAdmin->setEnabled(true);
                        if ($demo) {
                            $limit = new \datetime();
                            $limit->modify("+1 week");
                            $userAdmin->setExpired(false);
                            $userAdmin->setExpiresAt($limit);
                        } else
                            $userAdmin->setExpiresAt(null);
                        $userAdmin->setLocale($contact->getCustomer()->getLocale());
                        $userAdmin->setModeonlogin('buy');
                        $userAdmin->setName($contact->getName());
                        $userAdmin->setToken($password * 1981);
                        $userAdmin->setCustomermargecoef('5');
                        $userManager->updateUser($userAdmin, true);
                        $this->get('session')->getFlashBag()->add('notice', 'Utilisateur correctement enregistre');

                        $this->sendmail($userAdmin, 'MAIL_TITLE_NEWACCESS', 'MAIL_MESSAGE_NEWACCESS', $password);
                    } else
                        $this->get('session')->getFlashBag()->add('warning', "APPLICATION_EMAILALREADYEXIST");
                } else {
                    //$password = rand(100000, 1000000);
                    $user->setRoles(array('ROLE_CLIENT', 'ROLE_USER'));
                    $user->setEnabled(true);
                    if ($demo) {
                        $limit = new \datetime();
                        $limit->modify("+1 week");
                        $user->setExpired(false);
                        $user->setExpiresAt($limit);
                    } else
                        $user->setExpiresAt(null);
                    $userManager = $this->container->get('fos_user.user_manager');
                    $userManager->updateUser($user, true);
                    $subject = 'INASIA - REGENERATE PASSWORD USER';
                    $this->get('session')->getFlashBag()->add('notice', 'Password regenerate');

                    $this->sendmail($user, 'MAIL_TITLE_NEWACCESS', 'MAIL_MESSAGE_OPEN', $user->getToken() / 1981);
                }
            } else
                $this->get('session')->getFlashBag()->add('warning', 'Contact must have a email adress to open access');
            return $this->redirect($this->generateUrl('customer_show', array('id' => $contact->GetCustomer()->GetId())) . '#contacts');
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $this->get('session')->getFlashBag()->add('warning', 'Forbidden');
            return $this->redirect($this->generateUrl('catalog'));
        }
    }

    /*
     * Remove access to contact
     */

    public function BlockcontactacessAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')||$this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $em = $this->getDoctrine()->getManager();
            $contact = $em->getRepository('appBundle:Contact')->find($id);
            $user = $contact->getUser();

            if (!$user)
                throw $this->createNotFoundException('Unable to find User.');

            $user->setEnabled(false);
            $em->persist($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add('warning', 'User blocked');
            return $this->redirect($this->generateUrl('customer_show', array('id' => $contact->GetCustomer()->GetId())) . '#contacts');
        }
    }

    /*
     * Remove access to contact
     */

    public function RemovecontactacessAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $contact = $em->getRepository('appBundle:Contact')->find($id);
            $user = $contact->getUser();

            if (!$user)
                throw $this->createNotFoundException('Unable to find User.');

            $em->remove($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add('warning', 'User removed');
            return $this->redirect($this->generateUrl('customer_show', array('id' => $contact->GetCustomer()->GetId())) . '#contacts');
        }
    }

    /*
     *  CRUD SYMFONY //////////////////////////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all User entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:User:index.html.twig', array(
            'Users' => $User = $this->GetSecurityUsers()
        ));
    }

    /**
     * Displays a form to create a new User entity.
     */
    public function newAction()
    {
        $User = $this->GetSecurityUser();

        return $this->render('appBundle:User:form.html.twig', array(
            'User' => $User,
            'form' => $this->buildForm($User)->createView(),
        ));
    }

    /**
     * Creates a new User entity.
     */
    public function createAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $User = $this->GetSecurityUser();
            $form = $this->buildForm($User)->handleRequest($request);

            $em = $this->getDoctrine()->getManager();

            $user_exist = $em->getRepository('appBundle:User')->findByUsername($form->get('username')->getData());
            if (empty($user_exist)) {
                if ($form->isValid()) {
                    $userManager = $this->container->get('fos_user.user_manager');
                    $userAdmin = $userManager->createUser();
                    $userAdmin->setName($form->get('name')->getData());
                    $userAdmin->setUsername(str_replace(' ', '-', $form->get('username')->getData()));
                    $userAdmin->setEmail($form->get('email')->getData());
                    $userAdmin->setPlainPassword($form->get('password')->getData());
                    $userAdmin->setRoles($form->get('roles')->getData());
                    $userAdmin->setEnabled(true);
                    $userAdmin->setLocale('en');
                    $userAdmin->setCustomermargecoef('5');
                    $userManager->updateUser($userAdmin, true);
                    $this->get('session')->getFlashBag()->add('notice', 'FLASH_USER_CREATED');
                } else {
                    $this->get('session')->getFlashBag()->add('notice', 'This email already exist.');
                    return $this->render('appBundle:User:form.html.twig', array(
                        'User' => $User,
                        'form' => $this->buildForm($User)->createView(),
                    ));
                }
                return $this->redirect($this->generateUrl('users'));
            } else {
                $this->get('session')->getFlashBag()->add('notice', 'FLASH_USER_ALREADYEXISTS');
            }

            return $this->render('appBundle:User:form.html.twig', array(
                'User' => $User,
                'form' => $form->createView(),
            ));
        }
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     */
    public function editAction($id)
    {
        $User = $this->GetSecurityUser($id);

        if (!$User)
            throw $this->createNotFoundException('Unable to find User entity.');

        $form = $this->buildForm($User, 'edit');

        return $this->render('appBundle:User:form.html.twig', array(
            'User' => $User,
            'form' => $form->createView()
        ));
    }

    /**
     * Edits an existing User entity.
     */
    public function updateAction(Request $request, $id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $User = $this->GetSecurityUser($id);

            if (!$User)
                throw $this->createNotFoundException('Unable to find User entity.');

            $oldpassword = $User->getPAssword();
            $email = $User->getEmail();
            $login = $User->getUsername();
            //$oldimage = $User->getImage();
            $form = $this->buildForm($User, 'edit')->handleRequest($request);
            $test = true;
            if ($email != $form->get('email')->getData())
                $test = count($em->getRepository('appBundle:User')->findByEmail($form->get('email')->getData())) == 0;

            if ($login != $form->get('username')->getData())
                $test = count($em->getRepository('appBundle:User')->findByUsername($form->get('username')->getData())) == 0;

            if ($test) {
                if ($form->isValid()) {
                    if ($form->get('password')->getData())
                        $User->setPlainPassword($form->get('password')->getData());
                    else
                        $User->setPassword($oldpassword);

                    $em->flush();

                    $this->get('session')->getFlashBag()->add('notice', 'FLASH_USER_UPDATE');

                    return $this->redirect($this->generateUrl('users'));
                } else {
                    $this->get('session')->getFlashBag()->add('warning', 'FLASH_USER_FORM_ERROR');
                }
            } else
                $this->get('session')->getFlashBag()->add('warning', 'FLASH_USER_ALREADYEXISTS');

            return $this->render('appBundle:User:form.html.twig', array(
                'User' => $User,
                'form' => $form->createView()
            ));
        }
    }

    /**
     * Deletes a User entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('user_delete', array('id' => $id)))
                ->setMethod('DELETE')
                ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
                ->getForm()
                ->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $User = $this->GetSecurityUser($id);

                if (!$User)
                    throw $this->createNotFoundException('Unable to find User entity.');

                $orders = 0;
                foreach ($User->getCadenciers() as $cadencier)
                    if ($cadencier->getCommand())
                        $orders++;

                if ($orders == 0) {
                    $em->remove($User);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');
                } else
                    $this->get('session')->getFlashBag()->add('warning', 'CAN NOT DELETE USER BECAUSE ' . $orders . ' ORDER' . ($orders > 1 ? 'S  ARE' : ' IS') . ' LINKED');
            }

            return $this->redirect($this->generateUrl('users'));
        }
    }

    public function confirmationAction($id)
    {
        $User = $this->GetSecurityUser($id);

        if (!$User)
            throw $this->createNotFoundException('Unable to find Product entity.');

        return $this->render('appBundle:User:delete_confirm.html.twig', array(
            'entity' => $User,
            'delete_form' => $this->createFormBuilder()
                ->setAction($this->generateUrl('user_delete', array('id' => $id)))
                ->setMethod('DELETE')
                ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
                ->getForm()->createView(),
        ));
    }

    /**
     * Creates a form to create/edit a User entity.
     */
    private function buildForm(User $entity, $task = "create")
    {
        if ($task == "create") {
            $form = $this->createForm('sourcinasia\appBundle\Form\UserType', $entity, array(
                'action' => $this->generateUrl("user_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
            $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
            return $form;
        } else {
            $form = $this->createForm('sourcinasia\appBundle\Form\UserType', $entity, array(
                'action' => $this->generateUrl("user_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
            $form->add('password', 'password', array('required' => false));
            $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Update'));
            $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));

            return $form;
        }
    }

    /**
     * Gets all Users
     */
    private function GetSecurityUsers()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:User')->findByContact(NULL);
        else
            throw $this->createNotFoundException('Unable to find User.');
    }

    /**
     * Gets Suppliers
     */
    private function GetSecurityUser($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:User')->find($id);
            else
                return new User();
        else
            throw $this->createNotFoundException('Unable to find User.');
    }

    private function sendmail($ContactUSer, $title, $message, $password = "")
    {
        $this->get('event_dispatcher')->dispatch(
            'sendemailuseraccess',
            new GenericEvent('sendemailuseraccess', array('ContactUser' => $ContactUSer,
                'User' => $this->get('security.token_storage')->getToken()->getUser(),
                'Password' => $password,
                'TITLE' => $title,
                'MESSAGE' => $message

            ))
        );
    }

}
