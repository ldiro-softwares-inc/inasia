<?php

namespace sourcinasia\appBundle\Controller;

use Doctrine\ORM\EntityRepository;
use sourcinasia\appBundle\Entity\History;
use sourcinasia\appBundle\Entity\Historycustomer;
use sourcinasia\appBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Product controller.
 */
class ProductController extends Controller
{
    public function publicAction($id)
    {
        $response = new Response();
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            if ($Product = $this->getDoctrine()->getRepository('appBundle:Product')->find($id)) {
                if (!$Product->getHidden()) {
                    $em = $this->getDoctrine()->getManager();
                    $state = $Product->getPublic() ? null : 1;
                    $Product->setPublic($state);
                    $em->persist($Product);
                    $em->flush();
                    return $response->setContent(json_encode($state));
                }
            } else {
                return $response->setContent(json_encode(1));
            }
        } else {
            return $response->setContent(json_encode(1));
        }
        return  $response->setContent(json_encode(0));
    }

    public function hiddenAction($id)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            if ($Product = $this->getDoctrine()->getRepository('appBundle:Product')->find($id)) {
                $em = $this->getDoctrine()->getManager();
                $state = $Product->getHidden() ? null : 1;
                $Product->setHidden($state);
                $em->persist($Product);
                $em->flush();
                return $response->setContent(json_encode($state));
            } else {
                return $response->setContent(json_encode(1));
            }
        } else {
            return $response->setContent(json_encode(1));
        }
        $response->setContent(json_encode(0));
        return $response;
    }

    public function hideselectionAction($prodids='none', $flip=false)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $returnstate = 1; // 1 = hiding successful
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            if ($prodids != 'none') {
                $prodarray = str_getcsv($prodids);
                foreach ($prodarray as $id) {
                    if ($Product = $this->getDoctrine()->getRepository('appBundle:Product')->find($id)) {
                        $em = $this->getDoctrine()->getManager();
                        if ($flip) {
                            $Product->setHidden($Product->getHidden() ? 0 : 1); // flip previous state
                        } else if ($flip == 'unhide') {
                            $Product->setHidden(0); // always unhide
                        } else {
                            $Product->setHidden(1); // always hide, regardless of previous state
                        }
                        $em->persist($Product);
                        $em->flush();
                    } else {
                        $returnstate=0;
                    }
                }
            }
        } else {
            $returnstate=0;
        }

        $response->setContent(json_encode($returnstate));
        return $response;
    }

    /**
     * Retourne une fiche produit et ses offres
     * @deprecated with version 2
     * @param Request $request
     * @return Response
     */
    public function requestAction(Request $request)
    {
        $offer = $this->getDoctrine()
            ->getRepository('appBundle:Offer')
            ->getProductCatalog($request->query->get('id'), $this->get('Profils')->get());

        if (!$offer)
            throw $this->createNotFoundException('Unable to find Product entity.');

        $product = $offer->getProduct();

        $product->setHits((int)$product->getHits() + 1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();

        return $this->render('appBundle:Product:item_offer.html.twig', array(
            'Product' => $product,
            'catalog' => true,
            'offers' => $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->getOffersProduct($product->GetId(), $this->get('Profils')->get())
        ));
    }

    /**
     * Retourne une fiche produit et ses offres
     * @param Request $request
     * @return Response
     */
    public function requestV2Action(Request $request, $id)
    {
        $offer = $this->getDoctrine()
            ->getRepository('appBundle:Offer')
            ->getProductCatalog($id, $this->get('Profils')->get());

        if (!$offer)
            throw $this->createNotFoundException('Unable to find Product entity.');

        $product = $offer->getProduct();

        $product->setHits((int)$product->getHits() + 1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();

        return $this->render('appBundle:Product:item_offer_v2.html.twig', array(
            'product' => $product,
            'catalog' => true,
            'offers' => $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->getOffersProduct($product->GetId(), $this->get('Profils')->get())
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function extractpicturesAction(Request $request, $id)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            $this->userLimitDownload();

            $session = $this->get('session');
            $createZip = $this->get('createZip');
            $createZip->addDirectory('/');
            $images = 0;

            if ($id) {
                $offer = $this->getDoctrine()
                    ->getRepository('appBundle:Offer')
                    ->getProductCatalog($id, $this->get('Profils')->get());

                if ($offer) {
                    $directory = $offer->GetSupplier()->GetId();
                    $createZip->addDirectory($directory);
                    $i = 1;
                    foreach ($offer->getProduct()->getImages() as $key => $image) {
                        if (file_exists(__SOURCINASIA__ . 'htdocs/' . $image->getImage()))
                            $createZip->addFile(file_get_contents(__SOURCINASIA__ . '/htdocs/' . $image->getImage()), $directory . '/' . $offer->GetProduct()->GetId() . '-' . $i++ . '.jpeg');
                        $images++;
                    }
                }
            } else {
                foreach ($session->get('selectionids') as $offer) {
                    $offer = $this->getDoctrine()
                        ->getRepository('appBundle:Offer')
                        ->getProductCatalog($offer, $this->get('Profils')->get());

                    if ($offer) {
                        $directory = $offer->GetSupplier()->GetId();
                        $createZip->addDirectory($directory);
                        $i = 1;
                        foreach ($offer->getProduct()->getImages() as $key => $image) {
                            if (file_exists(__SOURCINASIA__ . 'htdocs/' . $image->getImage()))
                                $createZip->addFile(file_get_contents(__SOURCINASIA__ . 'htdocs/' . $image->getImage()), $directory . '/' . $offer->GetProduct()->GetId() . '-' . $i++ . '.jpeg');
                            $images++;
                        }
                    }
                }
            }
            if ($images > 0)
                $createZip->output();
        }
        return $this->redirect($this->generateUrl('catalog'));
    }

    /*
     * Retourne la liste des produits en fonction des cirt�res de recherche 
     */

    public function requestsAction(Request $request)
    {
        $request = $request->query->get('request');

        if (!is_array($request)) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        // Permet de mettre en session le supplier categorie filter
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $session = $this->get('session');
            if (array_key_exists('containerSize', $request)) {
                $session->set('containerSize', $request['containerSize']);
            } else {
                $session->remove('containerSize');
            }
        }

        $request = $this->getDoctrine()
            ->getRepository('appBundle:Product')
            ->seach(array_filter($request));

        return $this->render('appBundle:Product:list.html.twig', array(
            'entities' => $request
        ));
    }

    /*
     * Retourne le json des nomenclatures pour un filtre produit
     */

    public function filtersAction(Request $request, $filter)
    {
        if (!is_array($request->query->get('request'))) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        $content = $this->getDoctrine()
            ->getRepository('appBundle:Offer')
            ->seach(array_filter($request->query->get('request')), true);

        $content = $this->getDoctrine()
            ->getRepository('appBundle:Categorie')
            ->getCategroies(array_filter($filter), $request->getLocale()); //todo

        if ($filter == "nomenclature")
            $content = $this->getDoctrine()
                ->getRepository('appBundle:Categorie')
                ->getCategroies(array_filter($content), $request->getLocale());

        $response = new Response();
        $response->setContent(json_encode($content));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /*
     * Retourne le json des nomenclatures pour un filtre produit
     */

    public function gencodeAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
            $product = $this->GetSecurityProduct($id);

            if (!$product)
                throw $this->createNotFoundException('Unable to find GENCODE entity.');

            $gencode = $this->get('Gencode');
            $codebar = $gencode->GenerateCodeBar($product);

            if (is_array($codebar))
                $this->get('session')->getFlashBag()->add('warning', 'Cannot build gencode : ' . implode(', ', $codebar));
            else
                $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');


            return $this->redirect($this->generateUrl('product_show', array('id' => $id)));
        }
        return null;
    }

    /*
     *  CRUD SYMFONY //////////////////////////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Product entities.
     */
    public function indexAction()
    {
        // On va récupérer la liste des suppliers categories pour afficher le filtre
        $suppliersCategories = $this->getDoctrine()->getManager()->getRepository('appBundle:Suppliercategory')->findBy(array(), array('title' => 'ASC'));

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            return $this->render('appBundle:Product:index.html.twig', array(
                'active'              => 'product',
                'session'             => $this->get('session'),
                'suppliersCategories' => $suppliersCategories
            ));
        else
            throw $this->createNotFoundException('Unable to find Supplier entity.');
    }

    /**
     * Finds and displays a Product entity.
     */
    public function showAction($id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            throw $this->createNotFoundException('Unable to find Supplier entity.');


        $Product = $this->getDoctrine()->getManager()->getRepository('appBundle:Product')->find($id);

        if (!$Product) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Product:show.html.twig', array(
            'Product' => $Product,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Product entity.
     */
    public function newAction(Request $request)
    {

        $productid = $request->query->get('product');

        if ($productid) {
            $product = clone $this->GetSecurityProduct($productid);
            $product = $product->setMainimage(null);
            foreach ($product->getImages() as $image)
                $product->removeImage($image);
        } else {
            $product = $this->GetSecurityProduct();
        }


        return $this->render('appBundle:Product:form.html.twig', array(
            'Product' => $product,
            'supplierid' => $request->query->get('supplier'),
            'n' => '',
            'form' => $this->buildForm($product)->createView(),
        ));
    }

    /**
     * Creates a new Product entity.
     */
    public function createAction(Request $request)
    {
        $Product = $this->GetSecurityProduct();

        $supplier = $request->request->get('supplier');

        $form = $this->buildForm($Product)->handleRequest($request);
        if ($form->isValid()) {

            $Product = $this->UpdateExclusivity($Product);

            $em = $this->getDoctrine()->getManager();
            $em->persist($Product);

            $History = new History();
            $History->SetTitle('HISTORY_NEW_PRODUCT');
            $History->SetIco('<i class="icon-plus-sign"></i>');
            $History->SetDescription('HISTORY_NEW_PRODUCT_DESCRIPTION' . $Product->getName());
            $History->SetDate(new \DateTime());
            $History->SetProduct($Product);
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');

            // return $this->redirect($this->generateUrl('offer_new', array('product' => $Product->getId(), 'supplier' => $supplier)));

            if ($supplier)
                return $this->redirect($this->generateUrl('multiupload_images', array('id' => $Product->getId(), 'supplier' => $supplier)));
            else
                return $this->redirect($this->generateUrl('product_show', array('id' => $entity->getId())));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');

        return $this->render('appBundle:Product:form.html.twig', array(
            'Product' => $this->GetSecurityProduct(),
            'supplierid' => $request->query->get('supplier'),
            'n' => '',
            'form' => $this->buildForm($this->GetSecurityProduct())->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        $Product = $this->GetSecurityProduct($id);

        if (!$Product)
            throw $this->createNotFoundException('Unable to find Product entity.');

        $form = $this->buildForm($Product, 'edit');

        $form->add('customers', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
            'class' => 'appBundle:Customer',
            'choice_label' => 'name',
            'multiple' => true,
            'expanded' => false,
            'required' => false,
            'attr' => array('class' => 'chzn-select'),
            'query_builder' => function (EntityRepository $er) use ($Product) {
                return $er->createQueryBuilder('c')->Andwhere('c.nomenclatures like :catergories_acces')->setParameters(array('catergories_acces' => "%" . $Product->getCategorie()->getId() . "%"))->orderBy('c.name', 'ASC');
            }
        ));

        return $this->render('appBundle:Product:form.html.twig', array(
            'Product' => $Product,
            'supplierid' => $request->query->get('supplier'),
            'n' => $request->query->get('n'),
            'form' => $form->createView()
        ));
    }

    /**
     * Edits an existing Product entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Product = $this->GetSecurityProduct($id);

        if (!$Product)
            throw $this->createNotFoundException('Unable to find Product entity.');

        $form = $this->buildForm($Product, 'edit')->handleRequest($request);

        if ($form->isValid()) {

            $Product = $this->UpdateExclusivity($Product);

            $em = $this->getDoctrine()->getManager();
            $em->persist($Product);
            $History = new History();
            $History->SetTitle('HISTORY_UPDATE_PRODUCT');
            $History->SetIco('<i class="icon-pencil"></i>');
            $History->SetDescription('HISTORY_UPDATE_PRODUCT_DESCRIPTION' . $Product->getName());
            $History->SetDate(new \DateTime());
            $History->SetProduct($Product);
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');


            $n = $request->request->get('n');
//redirect back to the list @Supplier/consult/offer $this->redirect($this->generateUrl('supplier_show').'#offers')
            if ($request->request->get('supplier'))
                return $this->redirect($this->generateUrl('supplier_show', array('id' => $request->request->get('supplier'))).'#offers');
            else
                if ($n == 'addmainpicture')
                return $this->redirect($this->generateUrl('product_edit', array('id' => $id, 'n' => $Product->getCategorie()->getId())));

            elseif ($n)
                return $this->redirect($this->generateUrl('catalog', array('n' => $n)));
            else
                return $this->redirect($this->generateUrl('product_show', array('id' => $id)));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Product:form.html.twig', array(
            'Product' => $Product,
            'n' => '',
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Product entity.
     *
     */
    public function deleteAction($id)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {

            $Product = $this->GetSecurityProduct($id);

            if (!$Product)
                throw $this->createNotFoundException('Unable to find Product entity.');

            $produitEstCommande = $this->getDoctrine()->getRepository('appBundle:Historysale')->findBy(array('product' => $Product));
            $em = $this->getDoctrine()->getManager();
            $OffersList = array();
            foreach ($em->createQuery('SELECT c FROM appBundle:Cadencier c WHERE c.command IS NOT NULL AND c.state>=1')->getResult() as $cadencier) {
                $OffersList = array_merge($OffersList, $cadencier->getOfferslist());
            }
            array_unique($OffersList);
            $test = true;
            foreach ($Product->getOffers() as $Offer) {
                $test = $test && !in_array($Offer->getId(), $OffersList);
            }
            if (!$produitEstCommande && $test) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($Product);
                $em->flush();

                $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');
            } else {
                $this->get('session')->getFlashBag()->add('notice', 'Impossible. This product belong to processing orders.');
            }

        }

        return $this->redirect($this->generateUrl('product'));
    }

    /**
     * Creates a form to delete a Product entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('product_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Product entity.
     */
    private function buildForm(Product $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\ProductType', $entity, array(
                'action' => $this->generateUrl("product_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\ProductType', $entity, array(
                'action' => $this->generateUrl("product_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    private function GetSecurityProduct($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Product')->find($id);
            else
                return new Product();
        else
            throw $this->createNotFoundException('Unable to find Product.');
    }

    private function UpdateExclusivity($Product)
    {

        $exclusivities = array();
        foreach ($Product->getCustomers() as $customer) {
            foreach ($customer->getZones() as $zone) {
                $exlu = '[' . $zone->GetId() . ':' . $customer->GetId() . ']';
                if (!in_array($exlu, $exclusivities))
                    $exclusivities[] = $exlu;
            }
        }

        $Product->setExclusivity($exclusivities);
        return $Product;
    }

    /*
     * copie de export...
     */
    private function userLimitDownload($cadencier = null)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $contact = $this->get('security.token_storage')->getToken()->getUser()->GetContact();
            $Historycustomer = new Historycustomer();
            $em = $this->getDoctrine()->getManager();
            $exportnbr = (int)$contact->getExportnbr();

            if ($contact->getExportdate() == "") {
                $contact->setExportdate(new \dateTime());
            }

            if (((($exportnbr < $contact->getExportnbrmax()) && $contact->getExportdate()->format('Y-m-d') == date(
                        'Y-m-d'
                    )) || $contact->getExportdate()->format('Y-m-d') != date('Y-m-d'))
            ) {
                $Historycustomer->setTitle('EXPORT_XLS_CADENCIER');
                $Historycustomer->setDescription('EXPORT_XLS_CADENCIER');
                $Historycustomer->setIco('<i class="icon-download-alt"></i>');
                $Historycustomer->setDate(new \DateTime());
                $Historycustomer->setUser($user);
                $Historycustomer->setCustomer($user->getContact()->getCustomer());
                if ($cadencier)
                    $Historycustomer->setCadencier($cadencier);
                $em->persist($Historycustomer);
                if ($contact->getExportdate()->format('Y-m-d') != date('Y-m-d')) {
                    $contact->setExportnbr(1);
                } else {
                    $contact->setExportnbr($exportnbr + 1);
                }
                $contact->setExportdate(new \DateTime());
                $em->persist($contact);
                $em->flush();
                return true;
            } else {

                $Historycustomer->setTitle('EXPORT_TOO_MUCH_DOWNLOAD');
                $Historycustomer->setDescription('EXPORT_TOO_MUCH_DOWNLOAD');
                $Historycustomer->setIco('<i class="icon-warning-sign"></i>');
                $Historycustomer->setDate(new \DateTime());
                $Historycustomer->setUser($user);
                $Historycustomer->setCustomer($user->getContact()->getCustomer());
                $Historycustomer->setCadencier($cadencier);
                $em->persist($Historycustomer);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Too much download today ! Please contact sourcinasia.'
                );

                return false;
            }
        } else if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            return true;
        }
    }
}
