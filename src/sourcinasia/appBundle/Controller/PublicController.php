<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class PublicController extends Controller
{

    public function indexAction()
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $locale = $user->GetLocale();
            $profil = $this->get('Profils')->get();
            if ($user->GetContact()->GetCustomer()->GetCommercialtools())
                $profil['mode'] = $user->getModeonlogin();
            else
                $profil['mode'] = 'buy';

            $this->get('session')->set('profil', $profil);

            return $this->redirect($this->generateUrl('home', array('_locale' => $locale)));
        } else {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $profil = $this->get('Profils')->get();
            $profil['mode'] = '';
            $this->get('session')->set('profil', $profil);
            return $this->redirect($this->generateUrl('home', array('_locale' => $user->GetLocale())));
        }

        return $this->redirect($this->generateUrl('home'));
    }

}
