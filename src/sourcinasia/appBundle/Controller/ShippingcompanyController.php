<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Shippingcompany;

/**
 * Shippingcompany controller.
 *
 */
class ShippingcompanyController extends Controller
{

    /**
     * Lists all Shippingcompany entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Shippingcompany:index.html.twig', array(
            'Shippingcompanys' => $Shippingcompany = $this->GetSecurityShippingcompanies()
        ));
    }

    /**
     * Finds and displays a Shippingcompany entity.
     */
    public function showAction($id)
    {
        $Shippingcompany = $this->GetSecurityShippingcompany($id);

        if (!$Shippingcompany) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Shippingcompany:show.html.twig', array(
            'Shippingcompany' => $Shippingcompany,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Shippingcompany entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Shippingcompany:form.html.twig', array(
            'Shippingcompany' => $this->GetSecurityShippingcompany(),
            'form' => $this->buildForm($this->GetSecurityShippingcompany())->createView(),
        ));
    }

    /**
     * Creates a new Shippingcompany entity.
     */
    public function createAction(Request $request)
    {
        $Shippingcompany = $this->GetSecurityShippingcompany();
        $form = $this->buildForm($Shippingcompany)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Shippingcompany);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'CREATED!');
            return $this->redirect($this->generateUrl('shippingcompany'));
        }
        $this->get('session')->getFlashBag()->add('error', 'NOT CREATED!');
        return $this->render('appBundle:Shippingcompany:form.html.twig', array(
            'Shippingcompany' => $Shippingcompany,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Shippingcompany entity.
     *
     */
    public function editAction($id)
    {
        $Shippingcompany = $this->GetSecurityShippingcompany($id);

        if (!$Shippingcompany)
            throw $this->createNotFoundException('Unable to find Shippingcompany entity.');

        return $this->render('appBundle:Shippingcompany:form.html.twig', array(
            'Shippingcompany' => $Shippingcompany,
            'form' => $this->buildForm($Shippingcompany, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Shippingcompany entity.
     */
    public function updateAction(Request $request, $id)
    {

        $Shippingcompany = $this->GetSecurityShippingcompany($id);

        if (!$Shippingcompany)
            throw $this->createNotFoundException('Unable to find Shippingcompany entity.');

        $form = $this->buildForm($Shippingcompany, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Shippingcompany);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'UPDATED!');
            return $this->redirect($this->generateUrl('shippingcompany'));
        }
        $this->get('session')->getFlashBag()->add('error', 'NOT UPDATED!');

        return $this->render('appBundle:Shippingcompany:form.html.twig', array(
            'Shippingcompany' => $Shippingcompany,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Shippingcompany entity.
     *
     */
    public function deleteAction($id)
    {
        $Shippingcompany = $this->GetSecurityShippingcompany($id);

        if (!$Shippingcompany)
            throw $this->createNotFoundException('Unable to find Shippingcompany entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Shippingcompany);
        $em->flush();

        return $this->redirect($this->generateUrl('shippingcompany'));
    }

    /**
     * Creates a form to delete a Shippingcompany entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('shippingcompany_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Shippingcompany entity.
     */
    private function buildForm(Shippingcompany $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\ShippingcompanyType', $entity, array(
                'action' => $this->generateUrl("shippingcompany_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\ShippingcompanyType', $entity, array(
                'action' => $this->generateUrl("shippingcompany_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Shippingcompanys
     */
    private function GetSecurityShippingcompanies()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Shippingcompany')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Shippingcompany.');
    }

    /**
     * Gets Suppliers
     */
    private function GetSecurityShippingcompany($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Shippingcompany')->find($id);
            else
                return new Shippingcompany();
        else
            throw $this->createNotFoundException('Unable to find Shippingcompany.');
    }

}
