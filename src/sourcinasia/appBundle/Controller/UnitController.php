<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Unit;

/**
 * Unit controller.
 *
 */
class UnitController extends Controller
{

    /**
     * Lists all Unit entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Unit:index.html.twig', array(
            'Units' => $this->GetSecurityUnits()
        ));
    }

    /**
     * Finds and displays a Unit entity.
     */
    public function showAction($id)
    {
        $Unit = $this->GetSecurityUnit($id);

        if (!$Unit) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Unit:show.html.twig', array(
            'Unit' => $Unit,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Unit entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Unit:form.html.twig', array(
            'Unit' => $this->GetSecurityUnit(),
            'form' => $this->buildForm($this->GetSecurityUnit())->createView(),
        ));
    }

    /**
     * Creates a new Unit entity.
     */
    public function createAction(Request $request)
    {
        $Unit = $this->GetSecurityUnit();
        $form = $this->buildForm($Unit)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Unit);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'CREATED!');
            return $this->redirect($this->generateUrl('unit'));
        }
        $this->get('session')->getFlashBag()->add('error', 'NOT CREATED!');
        return $this->render('appBundle:Unit:form.html.twig', array(
            'Unit' => $Unit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Unit entity.
     *
     */
    public function editAction($id)
    {
        $Unit = $this->GetSecurityUnit($id);

        if (!$Unit)
            throw $this->createNotFoundException('Unable to find Unit entity.');

        return $this->render('appBundle:Unit:form.html.twig', array(
            'Unit' => $Unit,
            'form' => $this->buildForm($Unit, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Unit entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Unit = $this->GetSecurityUnit($id);

        if (!$Unit)
            throw $this->createNotFoundException('Unable to find Unit entity.');

        $form = $this->buildForm($Unit, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Unit);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'UPDATED!');
            return $this->redirect($this->generateUrl('unit'));
        }
        $this->get('session')->getFlashBag()->add('error', 'NOT UPDATED!');

        return $this->render('appBundle:Unit:form.html.twig', array(
            'Unit' => $Unit,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Unit entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->DeleteForm($id)->handleRequest($request);

        if ($form->isValid()) {
            $Unit = $this->GetSecurityUnit($id);

            if (!$Unit)
                throw $this->createNotFoundException('Unable to find Unit entity.');

            $em = $this->getDoctrine()->getManager();
            $em->remove($Unit);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('unit'));
    }

    /**
     * Creates a form to delete a Unit entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('unit_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Unit entity.
     */
    private function buildForm(Unit $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\UnitType', $entity, array(
                'action' => $this->generateUrl("unit_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\UnitType', $entity, array(
                'action' => $this->generateUrl("unit_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Units
     */
    private function GetSecurityUnits()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Unit')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Unit.');
    }

    /**
     * Gets Suppliers
     */
    private function GetSecurityUnit($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Unit')->find($id);
            else
                return new Unit();
        else
            throw $this->createNotFoundException('Unable to find Unit.');
    }

}
