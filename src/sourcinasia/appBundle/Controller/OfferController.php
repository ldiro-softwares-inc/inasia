<?php

namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\History;
use sourcinasia\appBundle\Entity\Offer;
use sourcinasia\appBundle\Entity\Product;
use sourcinasia\appBundle\Entity\Suppliercategory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Offer controller.
 *
 */
class OfferController extends Controller
{

    /**
     * Lists all Offer entities.
     * @deprecated
     */
    public function indexAction(Request $request)
    {
        /*ini_set('max_execution_time', 480);
        $Containers = $this->getDoctrine()
            ->getRepository('appBundle:Container')->findAll();
        $em = $this->getDoctrine()->getManager();
        foreach ($this->getDoctrine()
                     ->getRepository('appBundle:Supplier')->findAll() as $Supplier) {
            foreach ($Containers as $Container) {
                if (!$Supplier->getContainers()->contains($Container)) {
                    print $Supplier->getId() . '<br/>';
                    $Supplier->addContainer($Container);
                    $em->persist($Supplier);
                    $em->flush();

                }
            }
        }*/
        $session = $this->get('session');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            if (!$session->has('hideRef')) {
                $session->set('hideRef', 2); // was 3 until 28/8/18
            }
        }

        if ($request->query->get('catalog')) {
            if ($Catalog = $this->getDoctrine()->getManager()->getRepository('appBundle:Catalog')->find($request->query->get('catalog'))) {
                $session->set('hideRef', $Catalog->getId());
            }
        }

        $profil = ($this->get('Profils')->get());
        if ($profil['cutomer_id']) {
            $Customer = $this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->find($profil['cutomer_id']);
            $Catalogs = $Customer->getCatalogs();
        } else {
            $Catalogs = $this->getDoctrine()->getManager()->getRepository('appBundle:Catalog')->findBy(array('enabled' => true));
        }

        // On va récupérer la liste des suppliers categories pour afficher le filtre
        $suppliersCategories = $this->getDoctrine()->getManager()->getRepository('appBundle:Suppliercategory')->findBy(array(), array('title' => 'ASC'));

        return $this->render(
            'appBundle:Offer:index.html.twig',
            array(
                'openCadenciers' => $this->GetOpenedCadencier(),
                'nomenclature' => (int)$request->query->get('n'),
                'offerIdPushOnTop' => (int)$request->query->get('o'),
                'selection' => $session->get('selection'),
                'catalogs' => $Catalogs,
                'session' => $this->get('session'),
                'active' => 'catalog',
                'suppliersCategories' => $suppliersCategories
            )
        );
    }

    /**
     * @version 2.0
     * Lists all Offer entities with new Layout
     */
    public function indexV2Action(Request $request)
    {
        $session = $this->get('session');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            if (!$session->has('hideRef')) {
                $session->set('hideRef', 2); // was 3 until 28/8/18
            }
        }

        if ($request->query->has('l') && $request->query->has('n') ){
            $session->remove('departementFilter');
            $session->remove('famillyFilter');
            $session->remove('shelvesFilter');
            $session->remove('subfamillyFilter');
            $session->remove('hideRef');
            $session->remove('pol'); // A verifier
        }

        if ($request->query->get('catalog')) {
            if ($Catalog = $this->getDoctrine()->getManager()->getRepository('appBundle:Catalog')->find($request->query->get('catalog'))) {
                $session->set('hideRef', $Catalog->getId());
            }
        }

        $profil = ($this->get('Profils')->get());
        if ($profil['cutomer_id']) {
            $Customer = $this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->find($profil['cutomer_id']);
            $Catalogs = $Customer->getCatalogs();
        } else {
            $Catalogs = $this->getDoctrine()->getManager()->getRepository('appBundle:Catalog')->findBy(array('enabled' => true));
        }

        // On va récupérer la liste des suppliers categories pour afficher le filtre
        $suppliersCategories = $this->getDoctrine()->getManager()->getRepository('appBundle:Suppliercategory')->findBy(array(), array('title' => 'ASC'));
        return $this->render(
            'appBundle:Offer:index_v2.html.twig',
            array(
                'openCadenciers' => $this->GetOpenedCadencier(),
                'categorieLevel' => $request->query->get('l'),
                'categorieSelected' => $request->query->get('n'),
                'offerIdPushOnTop' => (int)$request->query->get('o'),
                'selection' => $session->get('selectionV2'),
                'catalogs' => $Catalogs,
                'session' => $this->get('session'),
                'pagination'=> 84,
                'active' => 'catalogV2',
                'suppliersCategories' => $suppliersCategories,
                'topBarMenuCategories' => $this->getDoctrine()->getRepository('sourcinasia\appBundle\Entity\Categorie')->getArrayByLevel($profil['contact_locale'])
            )
        );
    }


    /**
     * Affiche dans le catalogue les offres li� � une requete
     *
     * @param Request $request
     * @return Response
     * @deprecated
     */
    public function requestsAction(Request $request)
    {
        $request = $request->query->get('request');
        $offerIdPushOnTop = (array_key_exists('offerIdPushOnTop', $request)) ? $request['offerIdPushOnTop'] : '';

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            if (array_key_exists('commercial_coef', $request)) {
                unset($request['commercial_coef']);
            }

            /*$session = $request->getSession();
            if (!array_key_exists('departementid', $request) && $session->get('departementFilter')) {
                $request['departementid'] = $session->get('departementFilter');
            }

            if (!array_key_exists('shelvesid', $request) && $session->get('shelvesFilter')) {
                $request['shelvesid'] = $session->get('shelvesFilter');
            }
            if (!array_key_exists('famillyid', $request) && $session->get('famillyFilter')) {
                $request['famillyid'] = $session->get('famillyFilter');
            }

            if (!array_key_exists('subfamillyid', $request) && $session->get('subfamillyFilter')) {
                $request['subfamillyid'] = $session->get('subfamillyFilter');
            }*/


            if (!is_array($request)) {
                $response = $this->render('appBundle:Error:404.html.twig');
                $response->setStatusCode(404);

                return $response;
            }

            $Offers = $this->getDoctrine()
                ->getRepository('appBundle:Offer')
                ->seach(
                    array_filter($request),
                    $this->get('Profils')->get(),
                    false,
                    ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                );

            if (array_key_exists($offerIdPushOnTop, $Offers)) {
                $offerPushOnTop = $Offers[$offerIdPushOnTop];
                unset($Offers[$offerIdPushOnTop]);
                array_unshift($Offers, $offerPushOnTop);
            }

            return $this->render(
                'appBundle:Offer:offer_catalog.html.twig',
                array(
                    'Offers' => $Offers
                )
            );
        }
    }

    /**
     *  Affiche dans le catalogue les offres li� � une requete
     *
     * @param Request $request
     * @return Response
     */
    public function requests2Action(Request $request)
    {

        $request = $request->query->get('request');
        $profil = $this->get('Profils')->get();

        if (!is_array($request)) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);

            return $response;
        }


        $em = $this->getDoctrine()->getRepository('appBundle:Cadencier');

        /*
         * Besoin de savoir s'il y a un cadencier, pour forcer l'affichage des prix
         */
        if (array_key_exists('commercial_coef', $request)) {
            $profil['commercial_coef'] = $request['commercial_coef'];
            unset($request['commercial_coef']);
        }

        if (array_key_exists('cadencierid', $request)) {
            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') && !$this->get('Profils')->GetCustomerid()
            ) {
                $cadencier = $em->FindOneBy(array('id' => $request['cadencierid']));
            } elseif (($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) && $this->get('profils')->GetCustomerid()
            ) {
                $cadencier = $em->getCustomerCadencier($this->get('Profils')->GetCustomerid(), $request['cadencierid']);
            } else {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }
            $spplierid = $cadencier->getSupplier()->getId();
            $pol = $cadencier->getPol()->getId();
        } elseif (array_key_exists('supplierid', $request)) {
            $spplierid = $request['supplierid'];
            $cadencier = array();
            $pol = false;
        } else {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $requests = $this->getDoctrine()
            ->getRepository('appBundle:Offer')
            ->seach(
                array_filter($request),
                $this->get('Profils')->get(),
                false,
                ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')),
                true
            );

        $selection = array();
        foreach ($requests as $req) {
            $selection[] = $req['id'];
        }

        if (empty($selection)) {
            die('no result');
        }

        $offers = $this->getDoctrine()
            ->getRepository('appBundle:Offer')
            ->getOffersFromSupplier(
                $spplierid,
                $this->get('Profils')->get(),
                $selection,
                $pol,
                $request,
                $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')
            );

        return $this->render(
            'appBundle:Cadencier:productlist.html.twig',
            array(
                'offers' => $offers,
                'myuser' => $profil,
                'cadencier' => $cadencier,
                'checked' => false,
            )
        );
    }

    /**
     *  Affiche dans le catalogue les offres une requete
     *
     * @param Request $request
     * @return Response
     * @version 2.0
     */
    public function requestsNewAction(Request $request)
    {
        $request = $request->query->get('request');

        $offerIdPushOnTop = (array_key_exists('offerIdPushOnTop', $request)) ? $request['offerIdPushOnTop'] : '';

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            if (array_key_exists('commercial_coef', $request)) {
                unset($request['commercial_coef']);
            }

            if (!is_array($request)) {
                $response = $this->render('appBundle:Error:404.html.twig');
                $response->setStatusCode(404);

                return $response;
            }

            $Offers = $this->getDoctrine()
                ->getRepository('appBundle:Offer')
                ->seachV2(
                    array_filter($request),
                    $this->get('Profils')->get(),
                    false,
                    ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')),false,false
                );

            if (array_key_exists($offerIdPushOnTop, $Offers)) {
                $offerPushOnTop = $Offers[$offerIdPushOnTop];
                unset($Offers[$offerIdPushOnTop]);
                array_unshift($Offers, $offerPushOnTop);
            }

            return $this->render('appBundle:Offer:offer_catalog_v2.html.twig', array(
                'Offers' => $Offers,
                'pagination'=> 84,
            ));
        }
    }

    /**
     *  Compte le nombre offre dans la categorie affiche
     *
     * @param Request $request
     * @return Response
     * @deprecated
     */
    public function countAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $request = $request->query->get('request');
            if (array_key_exists('commercial_coef', $request)) {
                unset($request['commercial_coef']);
            }
            if (!is_array($request)) {
                $response = $this->render('appBundle:Error:404.html.twig');
                $response->setStatusCode(404);

                return $response;
            }

            $content = count(
                $this->getDoctrine()
                    ->getRepository('appBundle:Offer')
                    ->seach(array_filter($request), $this->get('Profils')->get(), 'count', $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            );

            $response = new Response();
            $response->setContent(json_encode($content));
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }
    }

    /**
     *  Compte le nombre offre dans la categorie affiche
     * @param Request $request
     * @return Response
     */
    public function countV2Action(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $request = $request->query->get('request');
            if (array_key_exists('commercial_coef', $request)) {
                unset($request['commercial_coef']);
            }
            if (!is_array($request)) {
                $response = $this->render('appBundle:Error:404.html.twig');
                $response->setStatusCode(404);

                return $response;
            }

            $content = count(
                $this->getDoctrine()
                    ->getRepository('appBundle:Offer')
                    ->seachV2(array_filter($request), $this->get('Profils')->get(), 'count', $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            );

            $response = new Response();
            $response->setContent(json_encode($content));
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }
    }

    /*
     * Retourne le json pour la barre de filtre
     * @deprecated
     * @param Request $request
     * @param $filter
     * @return Response
     */
    public function filtersAction(Request $request, $filter)
    {
        $locale = $request->getLocale();
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $request = $request->query->get('request');
            if (array_key_exists('commercial_coef', $request)) {
                unset($request['commercial_coef']);
            }
            if (!is_array($request)) {
                $response = $this->render('appBundle:Error:404.html.twig');
                $response->setStatusCode(404);

                return $response;
            }

            $content = $this->getDoctrine()
                ->getRepository('appBundle:Offer')
                ->seach(
                    array_filter($request),
                    $this->get('Profils')->get(),
                    $filter,
                    ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                );

            if ($filter == "nomenclature") {
                $content = $this->getDoctrine()
                    ->getRepository('appBundle:Categorie')
                    ->getCategroies(array_filter($content), $locale);
            }


            if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
                $session = $this->get('session');
                if (array_key_exists('productid', $request)) {
                    $session->set('productFilter', $request['productid']);
                } else {
                    $session->remove('productFilter');
                }

                if (array_key_exists('category', $request)) { //category = nomenclature
                    $session->set('categoryFilter', $request['category']);
                } else {
                    $session->remove('categoryFilter');
                }

                if (array_key_exists('supplierid', $request)) {
                    $session->set('supplierFilter', $request['supplierid']);
                } else {
                    $session->remove('supplierFilter');
                }

                if (array_key_exists('departementid', $request)) {
                    $session->set('departementFilter', $request['departementid']);
                } else {
                    $session->remove('departementFilter');
                }

                if (array_key_exists('shelvesid', $request)) {
                    $session->set('shelvesFilter', $request['shelvesid']);
                } else {
                    $session->remove('shelvesFilter');
                }

                if (array_key_exists('famillyid', $request)) {
                    $session->set('famillyFilter', $request['famillyid']);
                } else {
                    $session->remove('famillyFilter');
                }

                if (array_key_exists('subfamillyid', $request)) {
                    $session->set('subfamillyFilter', $request['subfamillyid']);
                } else {
                    $session->remove('subfamillyFilter');
                }

                if (array_key_exists('pol', $request)) {
                    $session->set('pol', $request['pol']);
                } else {
                    $session->remove('pol');
                }

                if (array_key_exists('minprice', $request)) {
                    $session->set('minprice', $request['minprice']);
                } else {
                    $session->remove('minprice');
                }

                if (array_key_exists('maxprice', $request)) {
                    $session->set('maxprice', $request['maxprice']);
                } else {
                    $session->remove('maxprice');
                }

                if (array_key_exists('minmoq', $request)) {
                    $session->set('minmoq', $request['minmoq']);
                } else {
                    $session->remove('minmoq');
                }

                if (array_key_exists('maxmoq', $request)) {
                    $session->set('maxmoq', $request['maxmoq']);
                } else {
                    $session->remove('maxmoq');
                }

                if (array_key_exists('ce', $request)) {
                    $session->set('ce', $request['ce']);
                } else {
                    $session->remove('ce');
                }

                if (array_key_exists('pol', $request)) {
                    $session->set('pol', $request['pol']);
                } else {
                    $session->remove('pol');
                }

                if (array_key_exists('order', $request)) {
                    $session->set('order', $request['order']);
                } else {
                    $session->remove('order');
                }

                if (array_key_exists('hideRef', $request)) {
                    $session->set('hideRef', $request['hideRef']);
                } else {
                    $session->remove('hideRef');
                }

                if (array_key_exists('supplierCategoryFilter', $request)) {
                    $session->set('supplierCategoryFilter', $request['supplierCategoryFilter']);
                } else {
                    $session->remove('supplierCategoryFilter');
                }

            }

            $response = new Response();
            $response->setContent(json_encode($content));
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }
    }

    /*
    * Retourne le json pour la barre de filtre
    * @param Request $request
    * @param $filter
    * @return Response
    */
    public function filtersv2Action(Request $request, $filter)
    {
        $locale = $request->getLocale();
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $request = $request->query->get('request');
            if (array_key_exists('commercial_coef', $request)) {
                unset($request['commercial_coef']);
            }
            if (!is_array($request)) {
                $response = $this->render('appBundle:Error:404.html.twig');
                $response->setStatusCode(404);

                return $response;
            }

            $content = $this->getDoctrine()
                ->getRepository('appBundle:Offer')
                ->seachV2(
                    array_filter($request),
                    $this->get('Profils')->get(),
                    $filter,
                    ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
                );

            if ($filter == "nomenclature") {
                $content = $this->getDoctrine()
                    ->getRepository('appBundle:Categorie')
                    ->getCategroies(array_filter($content), $locale);
            }


            if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
                $session = $this->get('session');
                if (array_key_exists('productid', $request)) {
                    $session->set('productFilter', $request['productid']);
                } else {
                    $session->remove('productFilter');
                }

                if (array_key_exists('category', $request)) { //category = nomenclature
                    $session->set('categoryFilter', $request['category']);
                } else {
                    $session->remove('categoryFilter');
                }

                if (array_key_exists('supplierid', $request)) {
                    $session->set('supplierFilter', $request['supplierid']);
                } else {
                    $session->remove('supplierFilter');
                }

                if (array_key_exists('departementid', $request)) {
                    $session->set('departementFilter', $request['departementid']);
                } else {
                    $session->remove('departementFilter');
                }

                if (array_key_exists('shelvesid', $request)) {
                    $session->set('shelvesFilter', $request['shelvesid']);
                } else {
                    $session->remove('shelvesFilter');
                }

                if (array_key_exists('famillyid', $request)) {
                    $session->set('famillyFilter', $request['famillyid']);
                } else {
                    $session->remove('famillyFilter');
                }

                if (array_key_exists('subfamillyid', $request)) {
                    $session->set('subfamillyFilter', $request['subfamillyid']);
                } else {
                    $session->remove('subfamillyFilter');
                }

                if (array_key_exists('pol', $request)) {
                    $session->set('pol', $request['pol']);
                } else {
                    $session->remove('pol');
                }

                if (array_key_exists('minprice', $request)) {
                    $session->set('minprice', $request['minprice']);
                } else {
                    $session->remove('minprice');
                }

                if (array_key_exists('maxprice', $request)) {
                    $session->set('maxprice', $request['maxprice']);
                } else {
                    $session->remove('maxprice');
                }

                if (array_key_exists('minmoq', $request)) {
                    $session->set('minmoq', $request['minmoq']);
                } else {
                    $session->remove('minmoq');
                }

                if (array_key_exists('maxmoq', $request)) {
                    $session->set('maxmoq', $request['maxmoq']);
                } else {
                    $session->remove('maxmoq');
                }

                if (array_key_exists('ce', $request)) {
                    $session->set('ce', $request['ce']);
                } else {
                    $session->remove('ce');
                }

                if (array_key_exists('pol', $request)) {
                    $session->set('pol', $request['pol']);
                } else {
                    $session->remove('pol');
                }

                if (array_key_exists('order', $request)) {
                    $session->set('order', $request['order']);
                } else {
                    $session->remove('order');
                }

                if (array_key_exists('hideRef', $request)) {
                    $session->set('hideRef', $request['hideRef']);
                } else {
                  //  $session->remove('hideRef');
                }

                if (array_key_exists('supplierCategoryFilter', $request)) {
                    $session->set('supplierCategoryFilter', $request['supplierCategoryFilter']);
                } else {
                    $session->remove('supplierCategoryFilter');
                }

            }

            $response = new Response();
            $response->setContent(json_encode($content));
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }
    }

    /**
     * Switch to deals
     * SupplierController:selectionAction
     * @param $idsupplier
     * @param array $selection
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function dealsAction($idsupplier, $selection = array())
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $i = 0;
            foreach ($selection as $offerid => $offer) {
                if ($this->switchdealAction($offerid, false)) {
                    $i++;
                }
            }

            $this->get('session')->getFlashBag()->add('notice', $i . 'FLASH_UPDATED');

            return $this->redirect($this->generateUrl('supplier_show', array('id' => $idsupplier)) . '#offers');
        }
    }

    /**
     * @param $id
     * @param bool $redirect
     * @return bool|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function switchdealAction($id, $redirect = true)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            if ($id) {
                $Offer = $this->GetSecurityOffer($id);
                if (!$Offer) {
                    throw $this->createNotFoundException('Unable to find Offer entity.');
                }
                $Offer->setDeal($Offer->getDeal() ? 0 : 1);
                $em = $this->getDoctrine()->getManager();
                $em->persist($Offer);
                $em->flush();
                if ($redirect) {
                    $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATED');

                    return $this->redirect(
                        $this->generateUrl('supplier_show', array('id' => $Offer->GetSupplier()->GetId())) . '#offers'
                    );
                } else {
                    return true;
                }
            }
            if ($redirect) {
                return $this->redirect($this->generateUrl('supplier'));
            } else {
                return false;
            }
        }
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function switchmainofferAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $offer = $this->GetSecurityOffer($id);
            if (!$offer) {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }
            $productid = (int)$offer->GetProduct()->GetId();
            $em = $this->getDoctrine()->getManager();
            $offers = $em->getRepository('appBundle:Offer')->findBy(array('product' => $productid));
            foreach ($offers as $offer) {
                if ($offer->getId() == $id) {
                    $offer->setMainoffer(true);
                } else {
                    $offer->setMainoffer(false);
                }
                $em->persist($offer);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('product_show', array('id' => $productid)) . '#suppliers');
    }

    /**
     * Reseigne un prix
     * @param Request $request
     * @return Response
     */
    public function fixpriceAction(Request $request)
    {
        $idoffer = $request->request->get('idoffer');
        $idcustomer = $request->request->get('idcustomer');
        $incoterm = $request->request->get('incoterm');
        $value = (float)$request->request->get('value');
        $commercial_coef = (float)$request->request->get('commercial_coef');
        $idcadencier = $request->request->get('cadencier');

        $type = "fix";
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            if ($idoffer && $idcustomer && in_array($incoterm, array('exw', 'fob'))) {
                $offer = $this->GetSecurityOffer($idoffer);

                if (!$offer) {
                    throw $this->createNotFoundException('Unable to find Offer entity.');
                }

                $customer = $this->getDoctrine()->getRepository('appBundle:Customer')->find($idcustomer);

                if (!$customer) {
                    throw $this->createNotFoundException('Unable to find Offer entity.');
                }

                $Customerprices = $offer->getCustomerprices();

                $price = $offer->GetSaleprice(
                    $incoterm,
                    array(
                        'customer_coef' => $customer->getMargecoef(),
                        'cutomer_id' => $customer->getId(),
                        'mode' => 'buy',
                        'contact_type' => $customer->GetActivity()->getId() == 6 ? 4 : 0,
                        'commercial_coef' => $commercial_coef
                    ),
                    false
                );

                if ($value && $value > $offer->GetSupplierPrice($incoterm)) {
                    $Customerprices[$idcustomer . $incoterm] = $value;
                    $price = $value;
                } else {
                    if (array_key_exists($idcustomer . $incoterm, $Customerprices)) {
                        unset($Customerprices[$idcustomer . $incoterm]);
                    }
                    $type = "free";
                }

                $offer->setCustomerprices($Customerprices);
                $em = $this->getDoctrine()->getManager();
                $em->persist($offer);
                $em->flush();

                if ($idcadencier) {
                    $cadencier = $em->getRepository('appBundle:Cadencier')->find($idcadencier);
                    if ($cadencier) {
                        $selection = $cadencier->getSelection();
                        if (array_key_exists("o" . $offer->getId(), $selection)) {
                            $selection["o" . $offer->getId()]['finalprice'] = $value;
                            $cadencier->setSelection($selection);
                            $em->persist($cadencier);
                            $em->flush();
                        }
                    }
                }

                $response = new Response();
                $response->setContent(
                    json_encode(array('result' => number_format($price, 2, '.', ''), 'type' => $type))
                );
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
        }
    }

    /**
     * Retour un prix
     * @param Request $request
     * @return Response
     */
    public function getpriceAction(Request $request)
    {
        $idoffer = $request->request->get('idoffer');
        $idcustomer = $request->request->get('idcustomer');
        $incoterm = $request->request->get('incoterm');
        $commercial_coef = (float)$request->request->get('commercial_coef');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
            if ($idoffer && $idcustomer && in_array($incoterm, array('exw', 'fob'))) {
                $profil = $this->get('Profils')->get();
                $offer = $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->Getoffer(
                    $idoffer,
                    $profil
                );
                if (!$offer) {
                    throw $this->createNotFoundException('Unable to find Offer entity.');
                }

                $customer = $this->get('security.token_storage')->getToken()->getUser()->getContact()->GetCustomer();
                if (!$customer) {
                    throw $this->createNotFoundException('Unable to find Offer entity.');
                }
            }
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            if ($idoffer && in_array($incoterm, array('exw', 'fob'))) {
                $offer = $this->GetSecurityOffer($idoffer);
                if (!$offer) {
                    throw $this->createNotFoundException('Unable to find Offer entity.');
                }
                if ($idcustomer) {
                    $customer = $this->getDoctrine()->getRepository('appBundle:Customer')->find($idcustomer);
                    if (!$customer) {
                        throw $this->createNotFoundException('Unable to find Offer entity.');
                    }
                    $profil = array(
                        'customer_coef' => $customer->getMargecoef(),
                        'cutomer_id' => $customer->getId(),
                        'mode' => 'buy',
                        'contact_type' => $customer->GetActivity()->getId() == 6 ? 4 : 0,
                        'commercial_coef' => $commercial_coef
                    );
                } else {
                    $profil = array(
                        'customer_coef' => 0,
                        'cutomer_id' => '',
                        'mode' => '',
                        'contact_type' => 0,
                        'commercial_coef' => $commercial_coef
                    );
                }
            }
        } else {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $price = $offer->GetSaleprice($incoterm, $profil);

        $response = new Response();
        $response->setContent(json_encode(array('result' => number_format($price, 2, '.', ''))));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Renseigne un MOQ
     * @param Request $request
     * @return Response
     */
    public function fixmoqAction(Request $request)
    {
        $idoffer = $request->request->get('idoffer');
        $idcustomer = $request->request->get('idcustomer');
        $incoterm = "m";
        $type = "fix";
        $value = (int)$request->request->get('value');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            if ($idoffer && $idcustomer) {
                $offer = $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->find($idoffer);

                if (!$offer) {
                    throw $this->createNotFoundException('Unable to find Offer entity.');
                }

                $Customermoqs = $offer->getCustomermoqs();

                $moq = $offer->getMoq();

                if ($value && $value >= 1) {
                    $Customermoqs[$idcustomer . $incoterm] = $value;
                    $moq = $value;
                } else {
                    if (array_key_exists($idcustomer . $incoterm, $Customermoqs)) {
                        unset($Customermoqs[$idcustomer . $incoterm]);
                    }
                    $type = "free";
                }
                $offer->setCustomermoqs($Customermoqs);
                $em = $this->getDoctrine()->getManager();
                $em->persist($offer);
                $em->flush();


                $response = new Response();
                $response->setContent(json_encode(array('result' => $moq, 'type' => $type)));
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
        }
    }

    /**
     * Confirme une offre
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function confirmofferAction(Request $request)
    {
        $idoffer = $request->request->get('idoffer');
        $action = $request->request->get('etat');
        $incoterm = $request->request->get('incoterm');
        $idcadencier = $request->request->get('cadencier');
        $prixnegociation = $request->request->get('prixnegociation'); //prixachatnegoarchive

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            if ($idoffer && $prixnegociation) {

                $offer = $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->find($idoffer);

                if ($action == 'archive' && (in_array($incoterm, array('fob', 'exw')))) {
                    $archive = $offer->getArchive();
                    if ($incoterm == "fob") {
                        if (empty($archive["fob"])) {
                            $archive["fob"][$offer->getFobmodificationdate()->format('d-m-Y')]['p'] = $offer->getFob();
                            $archive["fob"][$offer->getFobmodificationdate()->format('d-m-Y')]['d'] = "USD";
                            $archive["fob"][$offer->getFobmodificationdate()->format('d-m-Y')]['m'] = $offer->getMoq() . $offer->getMoqunit()->getTitle();
                        }

                        $archive["fob"][date('d-m-Y')]['p'] = $prixnegociation;
                        $archive["fob"][date('d-m-Y')]['d'] = "USD";
                        $archive["fob"][date('d-m-Y')]['m'] = $offer->getMoq() . $offer->getMoqunit()->getTitle();
                        $offer->setFob($prixnegociation);
                        $offer->setfobmodificationdate(new \DateTime());
                        $offer->setFobnegociation($prixnegociation);
                        $offer->setArchive($archive);
                    } elseif ($incoterm == "exw") {

                        if (empty($archive["exw"])) {
                            $archive["exw"][$offer->getExwusdmodificationdate()->format('d-m-Y')]['p'] = $offer->getExw();
                            $archive["exw"][$offer->getExwusdmodificationdate()->format(
                                'd-m-Y'
                            )]['d'] = $offer->getCurrency()->getTitle();
                            $archive["exw"][$offer->getExwusdmodificationdate()->format('d-m-Y')]['m'] = $offer->getMoq() . $offer->getMoqunit()->getTitle();
                        }
                        $archive["exw"][date('d-m-Y')]['p'] = $prixnegociation;
                        $archive["exw"][date('d-m-Y')]['d'] = $offer->getCurrency()->getTitle();
                        $archive["exw"][date('d-m-Y')]['m'] = $offer->getMoq() . $offer->getMoqunit()->getTitle();
                        $offer->setExwusd($prixnegociation * $offer->getCurrency()->getUsd());
                        $offer->setExw($prixnegociation);
                        $offer->setExwusdmodificationdate(new \DateTime());
                        $offer->setExwnegociation($prixnegociation);
                        $offer->setArchive($archive);
                    }
                }

                $offer->setModified(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($offer);
                $em->flush();

                if ($idcadencier) {
                    $cadencier = $em->getRepository('appBundle:Cadencier')->find($idcadencier);
                    if ($cadencier) {
                        $selection = $cadencier->getSelection();
                        if (array_key_exists("o" . $offer->getId(), $selection)) {
                            $selection["o" . $offer->getId()]['confirmPriceInCurrency'] = $prixnegociation;
                            $cadencier->setSelection($selection);
                            $em->persist($cadencier);
                            $em->flush();
                        }
                    }
                }

                $response = new Response();
                $response->setContent(json_encode(array('result' => $offer->getId())));
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
        } else {
            throw $this->createNotFoundException('Unable to confirm.');
        }
    }

    /*
     * Enregistre le prix n�gociation lors de la saisie des prix
     * A verifier, il semblerait que cette fonction ne devrait pas archiver...
     */

    public function negfrpriceAction(Request $request)
    {
        $idoffer = $request->request->get('idoffer');
        $idcustomer = $request->request->get('idcustomer');
        $idcadencier = $request->request->get('idcadencier');
        $incoterm = $request->request->get('incoterm');
        $action = $request->request->get('action');
        $type = "ok";
        $value = (float)$request->request->get('value');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
            if ($idoffer && $idcustomer) {
                $offer = $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->find($idoffer);
                if (!$offer) {
                    throw $this->createNotFoundException('Unable to find Offer entity.');
                }
                if (in_array($incoterm, array('exw', 'fob'))) {
                    if ($value) {
                        if ($incoterm == "exw") {
                            $offer->setExwnegociation($value);
                        } elseif ($incoterm == "fob") {
                            $offer->setFobnegociation($value);
                        }
                    } elseif ($action == "confirm" && $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                        $archive = $offer->getArchive();
                        if ($incoterm == "exw" && $offer->getExwnegociation()) {
                            $offer->setExwusd($offer->getExwnegociation());
                            $archive['exw'][date('Y-m-d')][] = $offer->getExwnegociation();
                        } elseif ($incoterm == "fob" && $offer->getFobnegociation()) {
                            $offer->setFob($offer->getFobnegociation());
                            $archive['fob'][date('Y-m-d')][] = $offer->getFobnegociation();
                        } else {
                            die('erreur');
                        }
                        $offer->setArchive($offer->getArchive());
                    }

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($offer);
                    $em->flush();

                    if ($idcadencier) {
                        $cadencier = $em->getRepository('appBundle:Cadencier')->find($idcadencier);
                        if ($cadencier) {
                            $selection = $cadencier->getSelection();
                            if (array_key_exists("o" . $offer->getId(), $selection)) {
                                $selection["o" . $offer->getId()]['confirm'] = 0;
                                $cadencier->setSelection($selection);
                                $em->persist($cadencier);
                                $em->flush();
                            }
                        }
                    }

                    $response = new Response();
                    $response->setContent(json_encode(array('result' => $value, 'type' => $type)));
                    $response->headers->set('Content-Type', 'application/json');

                    return $response;
                }
            }
        }
    }

    public function publishedofferAction(Request $request)
    {
        $idoffer = $request->request->get('idoffer');
        $idcustomer = $request->request->get('idcustomer');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            if ($idoffer && $idcustomer) {
                $offer = $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->find($idoffer);
                if (!$offer) {
                    throw $this->createNotFoundException('Unable to find Offer entity.');
                }

                $Customerhidden = $offer->getCustomerhidden();
                if (in_array('p' . $idcustomer, $Customerhidden)) {
                    unset($Customerhidden[array_search('p' . $idcustomer, $Customerhidden)]);
                    $visible = 1;
                } else {
                    $Customerhidden[] = 'p' . $idcustomer;
                    $visible = 0;
                }
                $offer->setCustomerhidden($Customerhidden);

                $em = $this->getDoctrine()->getManager();
                $em->persist($offer);
                $em->flush();

                $response = new Response();
                $response->setContent(json_encode($visible));
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
        }
    }

    /**
     * @param Request $request
     * @return Response
     * @deprecated
     */
    public function savecatalogAction(Request $request)
    {
        $selectionCatalog = $request->request->get('selection');
        $selectionCatalogids = $request->request->get('selectionids');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $session = $this->get('session');
            $session->set('selection', $selectionCatalog);
            $session->set('selectionids', $selectionCatalogids);
        }

        $response = new Response();
        $response->setContent(json_encode(array('result' => 1)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    /**
     * @param Request $request
     * @return Response
     */
    public function savecatalogV2Action(Request $request)
    {
        $selectionCatalog = $request->request->get('selection');
        $selectionCatalogids = $request->request->get('selectionids');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $session = $this->get('session');
            $session->set('selectionV2', $selectionCatalog);
            $session->set('selectionids', $selectionCatalogids);
        }

        $response = new Response();
        $response->setContent(json_encode(array('result' => 1)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Lists all Offer entities.
     */
    public function mobileAction(Request $request)
    {
        $session = $this->get('session');
        return $this->render(
            'appBundle:Offer:mobile.html.twig',
            array(
                'openCadenciers' => array(),
                'nomenclature' => (int)$request->query->get('n'),
                'selection' => $session->get('selection'),
                'active' => 'catalog'
            )
        );
    }

    /**
     * Finds and displays a Offer entity.
     */
    public
    function showAction($id)
    {
        $Offer = $this->GetSecurityOffer($id);

        if (!$Offer) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);

            return $response;
        }


        return $this->render(
            'appBundle:Offer:show.html.twig',
            array(
                'Offer' => $Offer,
                'delete_form' => $this->DeleteForm($id)->createView()
            )
        );
    }

    /**
     * Displays a form to create a new Offer entity.
     */
    public
    function newAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $supplierid = (int)$request->query->get('supplier');
            $productid = (int)$request->query->get('product');

            $supplier = $this->getDoctrine()->getManager()->getRepository('appBundle:Supplier')->findOneById(
                $supplierid
            );

            $form = $this->buildForm($this->GetSecurityOffer(), $productid);
            $form->get('supplier')->setData($supplier);
            if ($supplier) {
                if ($supplier->GetPol()) {
                    $form->get('pol')->setData($supplier->GetPol());
                }
            }

            return $this->render(
                'appBundle:Offer:form.html.twig',
                array(
                    'Offer' => $this->GetSecurityOffer(),
                    'n' => '',
                    'form' => $form->createView(),
                    'return' => $request->query->get('return'),
                    'hiddensupplier' => $supplierid
                )
            );
        }
    }

    /*     * ar
     * Creates a new Offer entity.
     */

    public
    function createAction(Request $request, Product $product)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {

            $em = $this->getDoctrine()->getManager();
            $supplier = $request->request->get('sourcinasia_appbundle_offer');

            /*
             * On ne peut pas avoir 2 offres d'un fournisseur sur le meme produit
             */
            $offer = $em->getRepository('appBundle:Offer')->findOneBy(
                array(
                    'product' => $product->getId(),
                    'supplier' => (int)$supplier['supplier']
                )
            );


            if (!empty($offer)) {
                $Offer = $this->GetSecurityOffer($offer->GetId());
                $history_title = "HISTORY_UPDATE_OFFER";
                $ico = '<i class="icon-pencil"></i>';
                $notice_title = "FLASH_UPDATE";
            } else {
                $Offer = $this->GetSecurityOffer();
                $history_title = "HISTORY_NEW_OFFER";
                $ico = '<i class="icon-plus-sign"></i>';
                $notice_title = "FLASH_CREATED";
                $nbroffer = count($em->getRepository('appBundle:Offer')->findBy(array('product' => $product->getId())));
                if ($nbroffer >= 1) {
                    $Offer->setMainoffer(false);
                } else {
                    $Offer->setMainoffer(true);
                }
            }

            $form = $this->buildForm($Offer, $product->getId())->handleRequest($request);
            if ($form->isValid()) {
                $Offer->setProduct($product);
                $Offer->setModified(new \DateTime());
                $Offer->setExwusdmodificationdate(new \DateTime());
                $Offer->setFobmodificationdate(new \DateTime());
                $Offer->setExwusd($Offer->getExw() * $Offer->getCurrency()->getUsd());
                $Offer->setMoqunit($Offer->getMoqunit());
                $Offer->setMoqpackingunit($Offer->getMoqpackingunit());
                /* if (!$Offer->getMoqpacking())
                  $Offer->setMoqpacking($Offer->getMoq() / $Offer->getProduct()->GetPcb()); */
                $em->persist($Offer);
                $em->flush();
                $History = new History();
                $History->SetTitle($history_title);
                $History->SetDescription(
                    $history_title . ' Product : ' . $Offer->GetProduct()->getName() . ' Supplier : ' . $Offer->GetSupplier()->getName()
                );
                $History->SetIco($ico);
                $History->SetDate(new \DateTime());
                $History->SetOffer($Offer);
                $History->SetSupplier($Offer->GetSupplier());
                $History->SetProduct($Offer->GetProduct());
                $History->SetUser($this->get('security.token_storage')->getToken()->getUser());
                $em->persist($History);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
                if ($request->request->get('return') == "product") {
                    return $this->redirect(
                        $this->generateUrl('product_show', array('id' => $Offer->GetProduct()->GetId())) . '#suppliers'
                    );
                } else {
                    return $this->redirect(
                        $this->generateUrl('supplier_show', array('id' => $Offer->GetSupplier()->GetId())) . '#offers'
                    );
                }
            }
            $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');

            return $this->render(
                'appBundle:Offer:form.html.twig',
                array(
                    'Offer' => $Offer,
                    'form' => $form->createView(),
                )
            );
        }
    }

    /**
     * Displays a form to edit an existing Offer entity.
     *
     */
    public
    function editAction(Request $request, $id)
    {

        ini_set('memory_limit', '300M');

        $Offer = $this->GetSecurityOffer($id);
        if (!$Offer) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $form = $this->buildForm($Offer);

        $form->remove('supplier');

        return $this->render(
            'appBundle:Offer:form.html.twig',
            array(
                'Offer' => $Offer,
                'product' => $Offer->GetProduct(),
                'form' => $form->createView(),
                'return' => $request->query->get('return'),
                'n' => $request->query->get('n'),
                'hiddensupplier' => $Offer->GetSupplier()->Getid()
            )
        );
    }

    /**
     * Edits an existing Offer entity.
     */
    public
    function updateAction(Request $request, $id)
    {

        ini_set('memory_limit', '300M');

        $Offer = $this->GetSecurityOffer($id);
        if (!$Offer) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $supplier = $Offer->getSupplier();
        $oldexw = $Offer->getExwusd();
        $oldcurrency = ($Offer->getCurrency()) ? $Offer->getCurrency()->getTitle() : "USD";
        $oldmoq = $Offer->getMoq();
        $oldexwdate = $Offer->getExwusdmodificationdate();
        $oldfob = $Offer->getFob();
        $oldfobdate = $Offer->getFobmodificationdate();
        $archive = $Offer->getArchive();

        $form = $this->buildForm($Offer)->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $Offer->setSupplier($supplier);
            $Offer->setModified(new \DateTime());
            $Offer->setExwusd($Offer->getExw() * $Offer->getCurrency()->getUsd());
            $Offer->setExwusdmodificationdate(new \DateTime());
            $Offer->setFobmodificationdate(new \DateTime());
            $Offer->setMoqunit($Offer->getMoqunit());
            $Offer->setMoqpackingunit($Offer->getMoqpackingunit());

            if ($Offer->getExwusd() && $Offer->getFob() == 0) {
                $this->get('session')->getFlashBag()->add('warning', 'To define POL please register FOB price');
                $Offer->setPol(null);
            }

            if ($oldexw != $Offer->getExwusd()) {
                if (empty($archive["exw"])) {
                    $archive["exw"][date('d-m-Y')]['p'] = $oldexw;
                    $archive["exw"][date('d-m-Y')]['d'] = $oldcurrency;
                    $archive["exw"][date('d-m-Y')]['m'] = $oldmoq;
                }
                $archive["exw"][date('d-m-Y')]['p'] = $Offer->getExw();
                $archive["exw"][date('d-m-Y')]['d'] = $Offer->getCurrency()->getTitle();
                $archive["exw"][date('d-m-Y')]['m'] = $Offer->getMoq();
            }

            if ($oldfob != $Offer->getFob()) {
                if (empty($archive["fob"]) && !empty($oldfobdate)) {
                    $archive["fob"][$oldfobdate->format('d-m-Y')]['p'] = $oldfob;
                    $archive["fob"][$oldfobdate->format('d-m-Y')]['d'] = "USD";
                    $archive["fob"][$oldfobdate->format('d-m-Y')]['m'] = $oldmoq;
                }
                $archive["fob"][date('d-m-Y')]['p'] = $Offer->getFob();
                $archive["fob"][date('d-m-Y')]['d'] = "USD";
                $archive["fob"][date('d-m-Y')]['m'] = $Offer->getMoq();
            }

            $Offer->setArchive($archive);

            /* if (!$Offer->getMoqpacking())
              $Offer->setMoqpacking($Offer->getMoq() * $Offer->getProduct()->GetPcb()); */

            $em->flush();
            $History = new History();
            $History->SetTitle("HISTORY_UPDATE_OFFER");
            $History->SetDescription(
                "HISTORY_NEW_OFFER_DESCRIPTION" . $Offer->GetProduct()->getName() . ' Supplier : ' . $Offer->GetSupplier()->getName()
            );
            $History->SetDate(new \DateTime());
            $History->SetIco('<i class="icon-pencil"></i>');
            $History->SetOffer($Offer);
            $History->SetSupplier($Offer->GetSupplier());
            $History->SetProduct($Offer->GetProduct());
            $History->SetUser(
                $this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById(
                    $this->get('security.token_storage')->getToken()->getUser()->getId()
                )
            );
            $em->persist($History);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            if ($request->request->get('n')) {
                return $this->redirect(
                    $this->generateUrl('catalog', array('n' => $Offer->GetProduct()->getCategorie()->GetId()))
                );
            } elseif ($request->request->get('return') == "product") {
                return $this->redirect(
                    $this->generateUrl('product_show', array('id' => $Offer->GetProduct()->GetId())) . '#suppliers'
                );
            } else {
                return $this->redirect(
                    $this->generateUrl('supplier_show', array('id' => $Offer->GetSupplier()->GetId())) . '#offers'
                );
            }
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render(
            'appBundle:Offer:form.html.twig',
            array(
                'Offer' => $Offer,
                'form' => $form->createView()
            )
        );
    }

    /**
     * Deletes a Offer entity.
     *
     */
    public
    function deleteAction($id)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_DELETE')) {

            $Offer = $this->GetSecurityOffer($id);

            if (!$Offer) {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }

            $productid = $Offer->GetProduct()->GetId();
            $supplierid = $Offer->GetSupplier()->GetId();
            $em = $this->getDoctrine()->getManager();
            $em->remove($Offer);
            $em->flush();

            $product = $this->getDoctrine()->getManager()->getRepository('appBundle:Product')->find($productid);
            if ($product->GetOffers()->count() == 0) {
                $em->remove($product);
                $em->flush();
            }

            $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

            return $this->redirect($this->generateUrl('supplier_show', array('id' => $supplierid)) . '#offers');
        } else {
            return $this->redirect($this->generateUrl('catalog'));
        }
    }

    public
    function DeleteSelectionAction($idsupplier, $selection = array())
    {
        ini_set('max_execution_time', 240);
        ini_set('memory_limit', '300M');
        $i = 0;
        foreach ($selection as $offerid => $offer) {
            $Offer = $this->GetSecurityOffer($offerid);

            if (!$Offer) {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }

            $productid = $Offer->GetProduct()->GetId();

            if ($Offer->getSupplier()->GetId() == $idsupplier) {
                $i++;
                $em = $this->getDoctrine()->getManager();
                $em->remove($Offer);
                $em->flush();

                $product = $this->getDoctrine()->getManager()->getRepository('appBundle:Product')->find($productid);
                if ($product->GetOffers()->count() == 0) {
                    $em->remove($product);
                    $em->flush();
                }
            }
        }

        $this->get('session')->getFlashBag()->add('notice', $i . 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('supplier_show', array('id' => $idsupplier)) . '#offers');
    }

    /**
     * Creates a form to delete a Offer entity by id.
     */
    private
    function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('offer_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Offer entity.
     */
    private
    function buildForm(Offer $entity, $product = "")
    {
        if ($product) {
            $form = $this->createForm(
                'sourcinasia\appBundle\Form\OfferType',
                $entity,
                array(
                    'action' => $this->generateUrl("offer_create", array('product' => $product)),
                    'method' => 'POST',
                    'attr' => array('class' => 'fill-up')
                )
            );
        } else {
            $form = $this->createForm(
                'sourcinasia\appBundle\Form\OfferType',
                $entity,
                array(
                    'action' => $this->generateUrl("offer_update", array('id' => $entity->GetID())),
                    'method' => 'POST',
                    'attr' => array('class' => 'fill-up')
                )
            );
        }

        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    private function GetSecurityOffer($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            if ($id) {
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->find($id);
            } else {
                return new Offer();
            }
        }
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            if ($id) {
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->find($id);
            } else {
                return new Offer();
            }
        } else {
            throw $this->createNotFoundException('Unable to find Offer.');
        }
    }

    /*
     * Permet de r�cup�rer les cadenciers dans la barre
     * Copie dans cadencier, home, offer controller
     */
    private
    function GetOpenedCadencier()
    {
        $profil = ($this->get('Profils')->get());
        $userid = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $em = $this->getDoctrine()->getManager()->getRepository('appBundle:Cadencier');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            return $em->getCommercialCadenciers($userid, $profil['cutomer_id']);
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
            return $em->getCustomerCadenciers($userid, $profil['cutomer_id']);
        }
    }

}
