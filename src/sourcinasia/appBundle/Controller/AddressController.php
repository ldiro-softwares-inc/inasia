<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Address;
use sourcinasia\appBundle\Entity\History;

/**
 * Address controller.
 *
 */
class AddressController extends Controller
{

    /**
     * Lists all Address entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Address:index.html.twig', array(
            'Addresss' => $this->GetSecurityAddress()
        ));
    }

    /**
     * Finds and displays a Address entity.
     */
    public function showAction($id)
    {
        $Address = $this->GetSecurityAddress($id);

        if (!$Address) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Address:show.html.twig', array(
            'Address' => $Address,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Address entity.
     */
    public function newAction(Request $request)
    {
        $Address = $this->GetSecurityAddress();

        $form = $this->buildForm($Address);

        $supplier = (int)$request->query->get('supplier');
        $customer = (int)$request->query->get('customer');

        if ($supplier) {
            $supplier = $this->getDoctrine()->getManager()->getRepository('appBundle:Supplier')->findOneById($supplier);
            $form->get('supplier')->setData($supplier);
            $form->remove('customer');
        } elseif ($customer) {
            $customer = $this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->findOneById($customer);
            $form->get('customer')->setData($customer);
            $form->remove('supplier');
        }

        return $this->render('appBundle:Address:form.html.twig', array(
            'Address' => $Address,
            'supplier' => $supplier,
            'customer' => $customer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Address entity.
     */
    public function createAction(Request $request)
    {
        $Address = $this->GetSecurityAddress();
        $form = $this->buildForm($Address)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Address);
            $em->flush();

            $History = new History();
            $History->SetTitle('HISTORY_NEW_ADDRESS');
            $History->SetDescription('HISTORY_NEW_ADDRESS_DESCRIPTION');
            $History->SetIco('<i class="icon-plus-sign"></i>');
            $History->SetDate(new \DateTime());
            $History->SetSupplier($Address->GetSupplier());
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');

            if ($Address->GetSupplier())
                return $this->redirect($this->generateUrl('supplier_show', array('id' => $Address->GetSupplier()->GetId())) . '#address');
            if ($Address->GetCustomer())
                return $this->redirect($this->generateUrl('customer_show', array('id' => $Address->GetCustomer()->GetId())) . '#address');
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Address:form.html.twig', array(
            'Address' => $Address,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Address entity.
     *
     */
    public function editAction($id)
    {

        $Address = $this->GetSecurityAddress($id);

        if (!$Address)
            throw $this->createNotFoundException('Unable to find Address entity.');

        $form = $this->buildForm($Address, 'edit');

        $supplier = $Address->GetSupplier();
        if ($supplier)
            $form->remove('customer');

        $customer = $Address->GetCustomer();
        if ($customer) {
            $form->remove('supplier');
            $form->remove('type');
        }

        return $this->render('appBundle:Address:form.html.twig', array(
            'Address' => $Address,
            'supplier' => $supplier,
            'customer' => $customer,
            'form' => $form->createView()
        ));
    }

    /**
     * Edits an existing Address entity.
     */
    public function updateAction(Request $request, $id)
    {

        $Address = $this->GetSecurityAddress($id);

        if (!$Address)
            throw $this->createNotFoundException('Unable to find Address entity.');

        $form = $this->buildForm($Address, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Address);
            $em->flush();
            $History = new History();
            $History->SetTitle('HISTORY_UPDATE_ADDRESS');
            $History->SetDescription('HISTORY_UPDATE_ADDRESS_DESCRIPTION');
            $History->SetIco('<i class="icon-penciln"></i>');
            $History->SetDate(new \DateTime());
            $History->SetSupplier($Address->GetSupplier());
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('supplier_show', array('id' => $Address->GetSupplier()->GetId())) . '#address');
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Address:form.html.twig', array(
            'Address' => $Address,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Address entity.
     *
     */
    public function deleteAction($id)
    {

        $Address = $this->GetSecurityAddress($id);

        if (!$Address)
            throw $this->createNotFoundException('Unable to find Address entity.');

        $supplierid = $Address->GetSupplier()->GetId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($Address);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('supplier_show', array('id' => $supplierid)) . '#address');
    }

    /**
     * Creates a form to delete a Address entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('address_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Address entity.
     */
    private function buildForm(Address $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\AddressType', $entity, array(
                'action' => $this->generateUrl("address_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\AddressType', $entity, array(
                'action' => $this->generateUrl("address_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Addresss
     */
    private function GetSecurityAddresses()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Address')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Address.');
    }

    /**
     * Gets Suppliers
     */
    private function GetSecurityAddress($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Address')->find($id);
            else
                return new Address();
        else
            throw $this->createNotFoundException('Unable to find Address.');
    }

}
