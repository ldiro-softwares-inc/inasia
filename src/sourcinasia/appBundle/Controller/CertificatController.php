<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use sourcinasia\appBundle\Entity\Certificat;


/**
 * Certificat controller.
 *
 */
class CertificatController extends Controller
{
    /*
    * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
    */

    /**
     * Lists all Certificat entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Certificat:index.html.twig', array(
            'Certificats' => $Certificat = $this->GetSecurityCertificats()
        ));
    }

    /**
     * Finds and displays a Certificat entity.
     */
    public function showAction($id)
    {
        $Certificat = $this->GetSecurityCertificat($id);

        if (!$Certificat) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Certificat:show.html.twig', array(
            'Certificat' => $Certificat,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Certificat entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Certificat:form.html.twig', array(
            'Certificat' => $this->GetSecurityCertificat(),
            'form' => $this->buildForm($this->GetSecurityCertificat())->createView(),
        ));
    }

    /**
     * Creates a new Certificat entity.
     */
    public function createAction(Request $request)
    {
        $Certificat = $this->GetSecurityCertificat();
        $form = $this->buildForm($Certificat)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Certificat);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('certificat'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Certificat:form.html.twig', array(
            'Certificat' => $Certificat,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Certificat entity.
     *
     */
    public function editAction($id)
    {
        $Certificat = $this->GetSecurityCertificat($id);

        if (!$Certificat)
            throw $this->createNotFoundException('Unable to find Certificat entity.');

        return $this->render('appBundle:Certificat:form.html.twig', array(
            'Certificat' => $Certificat,
            'form' => $this->buildForm($Certificat, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Certificat entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Certificat = $this->GetSecurityCertificat($id);

        if (!$Certificat)
            throw $this->createNotFoundException('Unable to find Certificat entity.');

        $form = $this->buildForm($Certificat, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Certificat);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('certificat'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Certificat:form.html.twig', array(
            'Certificat' => $Certificat,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Certificat entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $Certificat = $this->GetSecurityCertificat($id);

        if (!$Certificat)
            throw $this->createNotFoundException('Unable to find Certificat entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Certificat);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('certificat'));
    }

    /**
     * Creates a form to delete a Certificat entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('certificat_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }


    /**
     * Creates a form to create/edit a Certificat entity.
     */
    private function buildForm(Certificat $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\CertificatType', $entity, array(
                'action' => $this->generateUrl("certificat_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\CertificatType', $entity, array(
                'action' => $this->generateUrl("certificat_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Certificat
     */
    private function GetSecurityCertificats()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Certificat')->findAll();
        else
            throw new AccessDeniedException();
    }

    /**
     * Gets Certificat
     */
    private function GetSecurityCertificat($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Certificat')->find($id);
            else
                return new Certificat();
        else
            throw new AccessDeniedException();
    }
}
