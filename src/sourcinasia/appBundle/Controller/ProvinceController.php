<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Province;

/**
 * Province controller.
 *
 */
class ProvinceController extends Controller
{
    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Province entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Province:index.html.twig', array(
            'Provinces' => $Province = $this->GetSecurityProvinces()
        ));
    }

    /**
     * Finds and displays a Province entity.
     */
    public function showAction($id)
    {
        $Province = $this->GetSecurityProvince($id);

        if (!$Province) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Province:show.html.twig', array(
            'Province' => $Province,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Province entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Province:form.html.twig', array(
            'Province' => $this->GetSecurityProvince(),
            'form' => $this->buildForm($this->GetSecurityProvince())->createView(),
        ));
    }

    /**
     * Creates a new Province entity.
     */
    public function createAction(Request $request)
    {
        $Province = $this->GetSecurityProvince();
        $form = $this->buildForm($Province)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Province);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('province'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Province:form.html.twig', array(
            'Province' => $Province,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Province entity.
     *
     */
    public function editAction($id)
    {
        $Province = $this->GetSecurityProvince($id);

        if (!$Province)
            throw $this->createNotFoundException('Unable to find Province entity.');

        return $this->render('appBundle:Province:form.html.twig', array(
            'Province' => $Province,
            'form' => $this->buildForm($Province, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Province entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Province = $this->GetSecurityProvince($id);

        if (!$Province)
            throw $this->createNotFoundException('Unable to find Province entity.');

        $form = $this->buildForm($Province, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Province);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('province'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Province:form.html.twig', array(
            'Province' => $Province,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Province entity.
     *
     */
    public function deleteAction($id)
    {
        $Province = $this->GetSecurityProvince($id);

        if (!$Province)
            throw $this->createNotFoundException('Unable to find Province entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Province);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('province'));
    }

    /**
     * Creates a form to delete a Province entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('province_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Province entity.
     */
    private function buildForm(Province $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\ProvinceType', $entity, array(
                'action' => $this->generateUrl("province_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\ProvinceType', $entity, array(
                'action' => $this->generateUrl("province_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Province
     */
    private function GetSecurityProvinces()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Province')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Province.');
    }

    /**
     * Gets Province
     */
    private function GetSecurityProvince($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Province')->find($id);
            else
                return new Province();
        else
            throw $this->createNotFoundException('Unable to find Province.');
    }

}
