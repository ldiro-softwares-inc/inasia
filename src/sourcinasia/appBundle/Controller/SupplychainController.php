<?php

namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\History;
use sourcinasia\appBundle\Entity\Historysale;
use sourcinasia\appBundle\Entity\Invoicesupplier;
use sourcinasia\appBundle\Entity\Supplychain;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Supplychain controller.
 */
class SupplychainController extends Controller
{

    /**
     * PluUPLOAD
     */
    public function multiuploadAction(Request $request, $id)
    {

        $doc = $request->query->get('doc');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
            $form = $this->createFormBuilder()
                ->add('supplychaintype', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Supplychaintype',
                    'choice_label' => 'title',
                    'attr' => array('class' => 'chzn-select')
                ))->getForm();

            $form->get('supplychaintype')->setData($this->getDoctrine()->getRepository('appBundle:Supplychaintype')->findOneById($doc));
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $form = $this->createFormBuilder()->getForm();
        }

        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            return $this->render('appBundle:Supplychain:multiupload.html.twig', array(
                    'form' => $form->createView(),
                    'cadencier' => $this->getDoctrine()->getManager()->getRepository('appBundle:Cadencier')->findOneById($id)
                )
            );
        }
    }

    /**
     * PluUPLOAD php upload script
     */
    public function multiuploadAddAction(Request $request)
    {
        ini_set('max_execution_time', 2000);
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $idcadencier = ($request->request->get('cadencier')) ? ($request->request->get('cadencier')) : '';

            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                $idsupplychaintype = 37;
            elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN'))
                $idsupplychaintype = ($request->request->get('supplychaintype')) ? ($request->request->get('supplychaintype')) : '';

            $cadencier = $this->getDoctrine()->getManager()->getRepository('appBundle:Cadencier')->findOneById((int)$idcadencier);
            $supplychaintype = $this->getDoctrine()->getManager()->getRepository('appBundle:Supplychaintype')->findOneById((int)$idsupplychaintype);


            if ($cadencier) {
                if ($supplychaintype) {
                    if (
                        ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') && $supplychaintype->GetId() == 37 && !$cadencier->getlocked()) ||
                        ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') && !$cadencier->getlocked()) ||
                        ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') && $cadencier->getlocked() && $supplychaintype->GetId() == 67) ||
                        ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) ||
                        ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                    ) {
                        $em = $this->getDoctrine()->getManager();
                        $Supplychain = new Supplychain();
                        $Supplychain->setDocument($request->files->get('file'));
                        $Supplychain->setSupplychaintype($supplychaintype);
                        $Supplychain->setUser( $this->get('security.token_storage')->getToken()->getUser());
                        switch ($supplychaintype->getSupplychaincat()->getType()) {
                            case 0:
                                $Supplychain->setCadencier($cadencier);
                                break;
                            case 1:
                                $Supplychain->setContainer($cadencier->getCommand());
                                $Supplychain->setCadencier($cadencier);
                                break;
                            case 2:
                                $Supplychain->setCadencier($cadencier);
                                break;
                            default:
                                throw $this->createNotFoundException('Type impossible : ' . $supplychaintype->getSupplychaincat()->getType());
                                break;
                        }

                        $Supplychain->setcreated(new \DateTime());
                        $em->persist($Supplychain);

                        if ($Supplychain->getDocument())
                            $em->flush();
                        else
                            throw $this->createNotFoundException('Suplychain Multiupload erreur - Transfere ?');

                        if (!$supplychaintype->GetValidation())
                            $this->validAction($Supplychain->getId(), $cadencier->getId(), 0);
                        else {
                            $this->get('event_dispatcher')->dispatch('sendmail', new GenericEvent($cadencier, array('step' => 'VALIDATION', 'Supplychain' => $Supplychain)));
                        }

                        if ($Supplychain->getSupplychaintype()->GetId() == 37) {
                            $this->get('session')->getFlashBag()->add('notice', 'PI CUSTOMER uploaded. Waiting for admin confirmation');
                        }
                        $response = new Response();
                        $response->setContent($Supplychain->getId());
                        return $response;
                    } else {
                        if ($cadencier->getlocked())
                            throw $this->createNotFoundException('User ' . $this->get('security.token_storage')->getToken()->getUser()->getName() . ' - Cadencier locked : ' . $cadencier->getId());
                        else
                            throw $this->createNotFoundException('User ' . $this->get('security.token_storage')->getToken()->getUser()->getName() . ' - Cadencier  : ' . $cadencier->getId());
                    }
                } else
                    throw $this->createNotFoundException('No supplychaintype found : ' . $supplychaintype);
            } else
                throw $this->createNotFoundException('No cadencier found : ' . $idcadencier);
        }
        throw $this->createNotFoundException('Cannot upload');
    }

    /**
     * Deletes a Supplychain entity.
     */
    public function deleteAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('appBundle:Supplychain')->find($id);

            if (!$entity)
                throw $this->createNotFoundException('Unable to find Supplychain entity.');

            /* $ics = $entity->getInvoicecustomer();
              if (count($ics)) {
              foreach ($ics as $ic) {
              if (count($ic->getPayments())) {
              $this->get('session')->getFlashBag()->add('warning', 'Cannot delete. Payments recored');
              }
              }
              } */

            $is = $entity->getInvoicesupplier();
            if ($is) {
                if (count($is->getPayments())) {
                    $this->get('session')->getFlashBag()->add('warning', 'Cannot delete. Payments recored');
                    return $this->redirect($this->generateUrl('command_show', array('id' => $is->getCadencier()->getId())) . '#Supplychains');
                }
            }

            $id = $entity->getCadencier()->getId();

            if (
                ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') && $entity->getSupplychaintype()->GetId() == 37 && $entity->getValidate() == false) ||
                ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') && $entity->getValidate() == false) ||
                ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') && !$entity->getCadencier()->getlocked()) ||
                ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            ) {
                $em->remove($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');
            }

            return $this->redirect($this->generateUrl('command_show', array('id' => $id)) . '#Supplychains');
        }
    }

    private function operations($offers, $cadencier)
    {
        $i = 0;
        $j = 0;
        $selection = $cadencier->getSelection();
        $em = $this->getDoctrine()->getManager();

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
            foreach ($offers as $offer) {
                $product = $offer->getProduct();
                $archive = $product->getArchive();
                if (empty($archive))
                    $archive = array();

                $historysale = $this->getDoctrine()->getRepository('appBundle:Historysale')->findOneBy(array('product' => $product, 'customer' => $cadencier->getCustomer()));
                if (!$historysale) {
                    $historysale = new Historysale();
                    $historysale->setDate(new \datetime());
                    $historysale->setPrice($selection['o' . $offer->getId()]['finalprice']);
                    $historysale->setDevis($cadencier->getFinaldevis());
                    $historysale->setQty($selection['o' . $offer->getId()]['qty']);
                    $historysale->setIncoterm($cadencier->getFinalincoterm());
                    $historysale->setPol($cadencier->getFinalpol());
                    $historysale->setCadencier($cadencier);
                    $historysale->setProduct($product);
                    $historysale->setCustomer($cadencier->getCustomer());
                    $em->persist($historysale);
                    $em->flush();
                }

                if (!$product->getCodebar()) {
                    $gencode = $this->get('Gencode');
                    if ($gencode->GenerateCodeBar($product))
                        $j++;
                }

                if (array_key_exists('exclu', $selection['o' . $offer->getId()])) {
                    if ($selection['o' . $offer->getId()]['exclu']) {
                        $product->addCustomer($cadencier->getCustomer());
                        $product = $this->UpdateExclusivity($product);
                        $i++;
                    }
                }

                $cadencier->getCustomer()->getId();
                $offer->getId();
                $historysale->getId();

                $archive["c" . $cadencier->getCustomer()->getId()][date('d-m-Y')] = array('q' => $selection['o' . $offer->getId()]['qty'], 'p' => $selection['o' . $offer->getId()]['finalprice'], 'devise' => $cadencier->getFinaldevis(), 'h' => $historysale->getId());
                $product->setArchive($archive);

                $em->persist($product);
                $em->flush();
            }
        }

        return array($j, $i);
    }

    private function UpdateExclusivity($Product)
    {

        $exclusivities = array();
        foreach ($Product->getCustomers() as $customer) {
            foreach ($customer->getZones() as $zone) {
                $exlu = '[' . $zone->GetId() . ':' . $customer->GetId() . ']';
                if (!in_array($exlu, $exclusivities))
                    $exclusivities[] = $exlu;
            }
        }

        $Product->setExclusivity($exclusivities);
        return $Product;
    }

    private function checkOrder($cadencier, $offers)
    {

        /* $cadencierexist = $this->getDoctrine()->getRepository('appBundle:Supplychain')->findByCadencier(array('cadencier' => $cadencier->getId(), 'supplychaintype' => 37));

          if (count($cadencierexist) > 1) {
          $this->get('session')->getFlashBag()->add('warning', 'PI is already uploaded');
          return false;
          }
         */
        $selection = $cadencier->getSelection();
        foreach ($offers as $offer) {
            if (array_key_exists('o' . $offer->getId(), $selection)) {
                if (!array_key_exists('qty', $selection['o' . $offer->getId()])) {
                    $this->get('session')->getFlashBag()->add('warning', 'QTY IS MISSING - PLEASE EDIT ORDER');
                    return false;
                } else {
                    if (!(int)$selection['o' . $offer->getId()]['qty']) {
                        $this->get('session')->getFlashBag()->add('warning', 'QTY IS EMPTY - PLEASE EDIT ORDER');
                        return false;
                    }
                }
                if (!array_key_exists('finalprice', $selection['o' . $offer->getId()])) {
                    $this->get('session')->getFlashBag()->add('warning', 'QTY IS MISSING - PLEASE EDIT ORDER');
                    return false;
                } else {
                    if (!(float)$selection['o' . $offer->getId()]['finalprice']) {
                        $this->get('session')->getFlashBag()->add('warning', 'PRICE IS EMPTY - PLEASE EDIT ORDER');
                        return false;
                    }
                }
            } else {
                $this->get('session')->getFlashBag()->add('warning', 'OFFER MISSING - PLEASE EDIT ORDER');
                return false;
            }
        }

        if (!$cadencier->getFinaldevis()) {
            $this->get('session')->getFlashBag()->add('warning', 'NO CURRENCY DEFINE - PLEASE EDIT ORDER');
            return false;
        }

        /* if (!$cadencier->getFinalpol()) {
          $this->get('session')->getFlashBag()->add('warning', 'NO POL DEFINE - PLEASE EDIT ORDER');
          return false;
          } */

        return true;
    }

    public function validAction($id, $idcadencier, $redirect)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
            $em = $this->getDoctrine()->getManager();
            $Supplychain = $em->getRepository('appBundle:Supplychain')->find($id);

            if (!$Supplychain)
                throw $this->createNotFoundException('Unable to find Supplychain entity.');

            if ($Supplychain->getValidateadmin() == 1 && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                throw $this->createNotFoundException('Unable to find Supplychain entity.');

            $cadencier = $Supplychain->getCadencier();
            $container = $Supplychain->getContainer();

            if ($cadencier) {

                if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') && $Supplychain->getSupplychaintype()->GetId() == 37) { //PI CLIENT 
                    $offers = $this->getDoctrine()
                        ->getRepository('appBundle:Offer')
                        ->getOffersFromSupplierSourcing($cadencier->getSupplier()->getId(), $cadencier->getOfferslist());

                    if ($this->checkOrder($cadencier, $offers) && $cadencier->getLocked() == 0) {
                        $cadencier->setState(8);
                        $cadencier->setStepProduction(new \datetime());


                        $em = $this->getDoctrine()->getManager();
                        $em->persist($cadencier);
                        $em->flush();

                        list($nbr_codebar, $nbr_exclu) = $this->operations($offers, $cadencier);
                        $this->get('session')->getFlashBag()->add('notice', $nbr_exclu . ' Exclusivit' . ($nbr_exclu >= 2 ? 'ies' : 'y') . ' generated - ' . $nbr_codebar . ' Barcode' . ($nbr_codebar >= 2 ? 's' : '') . ' generated');
                        $this->get('event_dispatcher')->dispatch('sendmail', new GenericEvent($cadencier, array('step' => 'CUSTOMERCONFIRMPI')));
                        $this->get('session')->getFlashBag()->add('notice', 'STEP3');
                        $Supplychain->setValidate(true);
                        $Supplychain->setValidateDate(new \Datetime());


                        $em->persist($Supplychain);

                        /* $invoice = new Invoicecustomer();
                          $invoice->setDate($cadencier->getstepProduction());
                          $tva = ($cadencier->getFinaltva()) ? $cadencier->getTotalvalue() * ($cadencier->getFinaltva() / 100) : 0;
                          $invoice->setAmount($cadencier->getTotalvalue() + $tva);
                          $invoice->setCustomer($cadencier->getCustomer());
                          $invoice->setCadencier($cadencier);
                          $invoice->setDocument($Supplychain);
                          $em->persist($invoice); */
                        /*
                          if (count($cadencier->getServicedescription())) {
                          $invoice2 = new Invoicecustomer();
                          $invoice2->setDate($cadencier->getstepProduction());

                          $totalservice = 0;
                          foreach ($cadencier->getservicedescription() as $service) {
                          $totalservice+=$service['value'];
                          }

                          $tva = ($cadencier->getFinaltva()) ? $totalservice * ($cadencier->getFinaltva() / 100) : 0;
                          $invoice2->setAmount($totalservice + $tva);
                          $invoice2->setCustomer($cadencier->getCustomer());
                          $invoice2->setCadencier($cadencier);
                          $invoice2->setDocument($Supplychain);
                          $invoice2->setService(true);
                          $em->persist($invoice2);
                          }; */

                        $em->flush();
                    } elseif ($this->checkOrder($cadencier, $offers) && $cadencier->getLocked() == 1) {
                        $Supplychain->setValidate(true);
                        $Supplychain->setValidateDate(new \Datetime());
                        $em->persist($Supplychain);
                        $em->flush();
                        $this->get('session')->getFlashBag()->add('notice', 'VALIDATE');
                        /* $invoice = new Invoicecustomer();
                          $invoice->setDate($cadencier->getstepProduction());
                          $invoice->setAmount(0);
                          $invoice->setCustomer($cadencier->getCustomer());
                          $invoice->setCadencier($cadencier);
                          $invoice->setDocument($Supplychain);
                          $em->persist($invoice);
                          $em->flush(); */
                    } else {
                        $this->get('session')->getFlashBag()->add('warning', ' Order has a error. Please contact Admin to validate.');
                    }
                    if ($redirect)
                        return $this->redirect($this->generateUrl('command_show', array('id' => $Supplychain->getCadencier()->getId())) . '#Supplychains');
                } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') && $Supplychain->getSupplychaintype()->GetId() == 73) { //PI CLIENT SERVICE
                    $Supplychain->setValidate(true);
                    $Supplychain->setValidateDate(new \Datetime());
                    $em->persist($Supplychain);

                    /* $invoice2 = new Invoicecustomer();
                      $invoice2->setDate(new \datetime());
                      $invoice2->setAmount(0);
                      $invoice2->setCustomer($cadencier->getCustomer());
                      $invoice2->setCadencier($cadencier);
                      $invoice2->setDocument($Supplychain);
                      $invoice2->setService(true);
                      $em->persist($invoice2); */

                    $em->flush();
                    $this->get('session')->getFlashBag()->add('notice', 'VALIDATE');

                    if ($redirect)
                        return $this->redirect($this->generateUrl('command_show', array('id' => $Supplychain->getCadencier()->getId())) . '#Supplychains');
                } elseif ($Supplychain->getSupplychaintype()->GetId() == 69) { //PI SUPPLIER
                    $Supplychain->setValidate(true);
                    $Supplychain->setValidateDate(new \Datetime());
                    $em->persist($Supplychain);

                    $invoice = new Invoicesupplier();
                    $invoice->setDate(new \datetime());
                    $invoice->setAmount($cadencier->getSuppliervalue());
                    $invoice->setSupplier($cadencier->getSupplier());
                    $invoice->setCadencier($cadencier);
                    $invoice->setDocument($Supplychain);
                    $em->persist($invoice);


                    $em->flush();
                    $this->get('session')->getFlashBag()->add('notice', 'VALIDATE');

                    if ($redirect)
                        return $this->redirect($this->generateUrl('command_show', array('id' => $Supplychain->getCadencier()->getId())) . '#Supplychains');
                } elseif ($Supplychain->getSupplychaintype()->GetId() == 65 || $Supplychain->getSupplychaintype()->GetId() == 66) {
                    $cadencier->setState(9);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($cadencier);
                    $em->flush();

                    $this->get('event_dispatcher')->dispatch('sendmail', new GenericEvent($cadencier, array('step' => 'LOADERORDER')));
                    $this->get('session')->getFlashBag()->add('notice', 'STEP4');

                    $Supplychain->setValidate(true);
                    $Supplychain->setValidateDate(new \Datetime());
                    $em->persist($Supplychain);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('notice', 'VALIDATE');

                    if ($redirect)
                        return $this->redirect($this->generateUrl('command_show', array('id' => $Supplychain->getCadencier()->getId())) . '#Supplychains');
                } elseif ($Supplychain->getSupplychaintype()->GetId() == 67) { //delevery note
                    $cadencier->setState(10);
                    $cadencier->setlocked(1);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($cadencier);
                    $em->flush();

                    $Supplychain->setValidate(true);
                    $Supplychain->setValidateDate(new \Datetime());
                    $em->persist($Supplychain);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('notice', 'VALIDATE');

                    if ($redirect)
                        return $this->redirect($this->generateUrl('command_show', array('id' => $Supplychain->getCadencier()->getId())) . '#Supplychains');
                } elseif ($Supplychain->getSupplychaintype()->GetId() == 68) { //delevery note
                    $cadencier->setlocked(1);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($cadencier);
                    $em->flush();
                    $Supplychain->setValidate(true);
                    $Supplychain->setValidateDate(new \Datetime());
                    $em->persist($Supplychain);
                    $em->flush();
                    if ($redirect)
                        return $this->redirect($this->generateUrl('command_show', array('id' => $Supplychain->getCadencier()->getId())) . '#Supplychains');
                } else {
                    $Supplychain->setValidate(true);
                    $Supplychain->setValidateDate(new \Datetime());
                    $em->persist($Supplychain);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('notice', 'VALIDATE');
                }
                if ($redirect)
                    return $this->redirect($this->generateUrl('command_show', array('id' => $Supplychain->getCadencier()->getId())) . '#Supplychains');
            } elseif ($container) {
                $cadencier = $em->getRepository('appBundle:Cadencier')->find($idcadencier);
                $Supplychain->setValidate(true);
                $Supplychain->setValidateDate(new \Datetime());
                $em->persist($Supplychain);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', 'VALIDATE');

                if ($redirect)
                    return $this->redirect($this->generateUrl('command_show', array('id' => $idcadencier)) . '#Logistic');
            }
        }
    }

    public function readAction($id)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {

            $em = $this->getDoctrine()->getManager();
            $Supplychain = $em->getRepository('appBundle:Supplychain')->find($id);

            if (!$Supplychain)
                throw $this->createNotFoundException('Unable to find Supplychain entity..');

            if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
                if (in_array($Supplychain->getSupplychaintype()->getSupplychaincat()->getId(), array(7, 11)) && $this->get('security.token_storage')->getToken()->getUser()->getContact() && $this->get('security.token_storage')->getToken()->getUser()->getContact()->getCustomer()) {
                    if ($Supplychain->getCadencier()->getCustomer->getId() == $this->get('security.token_storage')->getToken()->getUser()->getContact()->getCustomer()->getId()) {

                    } else {
                        throw new AccessDeniedException();
                    }
                } else {
                    throw new AccessDeniedException();
                }
            }


            $access = true;

            if ($Supplychain->getSupplychaintype()->GetId() == 40 && ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')))
                $access = false;

            if ($access && ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION'))) {
                if ($Supplychain->getCadencier())
                    $filename = __SOURCINASIA__ . 'documents/orders/' . $Supplychain->getDocument();
                elseif ($Supplychain->getContainer())
                    $filename = __SOURCINASIA__ . 'documents/containers/' . $Supplychain->getDocument();
                else
                    throw new AccessDeniedException();
            } else
                throw new AccessDeniedException();

            if (file_exists($filename)) {
                switch (mime_content_type($filename)) {
                    case 'image/jpeg':
                        header("Content-Disposition: inline; filename=image.jpeg");
                        header("Content-type: image/jpeg");
                        header('Cache-Control: private, max-age=0, must-revalidate');
                        header('Pragma: public');
                        break;
                    case 'application/pdf':
                        header("Content-Disposition: inline; filename=document.pdf");
                        header("Content-type: application/pdf");
                        header('Cache-Control: private, max-age=0, must-revalidate');
                        header('Pragma: public');
                        break;
                    case 'application/vnd.ms-excel':
                        header("Content-Disposition: inline; filename=document.xls");
                        header("Content-type: application/vnd.ms-excel");
                        header('Cache-Control: private, max-age=0, must-revalidate');
                        header('Pragma: public');
                        break;
                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                        header("Content-Disposition: inline; filename=document.xlsx");
                        header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                        header('Cache-Control: private, max-age=0, must-revalidate');
                        header('Pragma: public');
                        break;
                    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                        header("Content-Disposition: inline; filename=document.docx");
                        header("Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                        header('Cache-Control: private, max-age=0, must-revalidate');
                        header('Pragma: public');
                        break;
                    case 'application/msword':
                        header("Content-Disposition: inline; filename=document.doc");
                        header("Content-type: application/msword");
                        header('Cache-Control: private, max-age=0, must-revalidate');
                        header('Pragma: public');
                        break;
                    default:
                        throw $this->createNotFoundException('Unable to find Supplychain entity..');
                        break;
                }
                echo file_get_contents($filename);

                //todo ouverture fichier au lieu dl
                die;
            } else
                throw $this->createNotFoundException('Unable to find Supplychain file.');
        }
    }

    public
    function commercialDocsAction($idsupplychain, $idcadencier)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            $cadencier = $this->getDoctrine()->getManager()->getRepository('appBundle:Cadencier')->findOneById($idcadencier);
            if (!$cadencier)
                throw $this->createNotFoundException('Unable to find Supplychain entity..');

            $em = $this->getDoctrine()->getManager();
            $Supplychain = $em->getRepository('appBundle:Supplychain')->getCustomerDocument($idsupplychain, $cadencier, $this->get('security.token_storage')->getToken()->getUser()->getContact()->GetCustomer());

            if (!$Supplychain)
                throw $this->createNotFoundException('Unable to find Supplychain entity..');

            if (in_array($Supplychain->GetSupplychaintype()->getId(), array(37, 38)) && $this->get('security.token_storage')->getToken()->getUser()->getContact()->GetCustomer() == $Supplychain->GetCadencier()->GetCustomer()) {
                $filename = __SOURCINASIA__ . 'documents/orders/' . $Supplychain->getDocument();
                if (file_exists($filename)) {
                    switch (mime_content_type($filename)) {
                        case 'image/jpeg':
                            header("Content-Disposition: inline; filename=image.jpeg");
                            header("Content-type: image/jpeg");
                            header('Cache-Control: private, max-age=0, must-revalidate');
                            header('Pragma: public');
                            break;
                        case 'application/pdf':
                            header("Content-Disposition: inline; filename=document.pdf");
                            header("Content-type: application/pdf");
                            header('Cache-Control: private, max-age=0, must-revalidate');
                            header('Pragma: public');
                            break;
                        case 'application/vnd.ms-excel':
                            header("Content-Disposition: inline; filename=document.xls");
                            header("Content-type: application/vnd.ms-excel");
                            header('Cache-Control: private, max-age=0, must-revalidate');
                            header('Pragma: public');
                            break;
                        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                            header("Content-Disposition: inline; filename=document.xlsx");
                            header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                            header('Cache-Control: private, max-age=0, must-revalidate');
                            header('Pragma: public');
                            break;
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            header("Content-Disposition: inline; filename=document.docx");
                            header("Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                            header('Cache-Control: private, max-age=0, must-revalidate');
                            header('Pragma: public');
                            break;
                        case 'application/msword':
                            header("Content-Disposition: inline; filename=document.doc");
                            header("Content-type: application/msword");
                            header('Cache-Control: private, max-age=0, must-revalidate');
                            header('Pragma: public');
                            break;
                        default:
                            throw $this->createNotFoundException('Unable to find Supplychain entity..');
                            break;
                    }
                    echo file_get_contents($filename);
                    die;
                } else
                    throw $this->createNotFoundException('Unable to find Supplychain entity.');
            }
        }
    }

}
