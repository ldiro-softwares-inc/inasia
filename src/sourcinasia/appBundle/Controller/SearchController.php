<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{
    /*
     * Router
     */

    public function indexAction(Request $request)
    {
        $seach = $request->request->get('qsearch');

        $letter = strtolower(substr($seach, 0, 1));

        if ($letter == 'p' && $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $seach = substr($seach, -(strlen($seach) - 1));
            $product = $this->getDoctrine()->getManager()->getRepository('appBundle:Product')->find((int)$seach);

            if ($product)
                return $this->redirect($this->generateUrl('product_show', array('id' => $product->getId())));
            else
                return $this->redirect($this->generateUrl('product'));
        }

        if ($letter == 's' && $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $seach = substr($seach, -(strlen($seach) - 1));
            $supplier = $this->getDoctrine()->getManager()->getRepository('appBundle:Supplier')->find((int)$seach);
            if ($supplier)
                return $this->redirect($this->generateUrl('supplier_show', array('id' => $supplier->getID())));
            else
                return $this->redirect($this->generateUrl('supplier'));
        }

        if ($letter == 'c' && $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $customer = $this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->find((int)$seach);
            if ($customer)
                return $this->redirect($this->generateUrl('customer_show', array('id' => $customer->getId())));
            else
                return $this->redirect($this->generateUrl('customer'));
        }

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')) {
            $supplier = $this->getDoctrine()->getManager()->getRepository('appBundle:Supplier')->find((int)$seach);
            if ($supplier)
                return $this->redirect($this->generateUrl('supplier_show', array('id' => $supplier->getID())));
            else
                return $this->redirect($this->generateUrl('supplier'));
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $customer = $this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->find((int)$seach);

            if ($customer)
                return $this->redirect($this->generateUrl('customer_show', array('id' => $customer->getId())));
            else
                return $this->redirect($this->generateUrl('customer'));
        }
    }

    /*
     * Autocompletion
     */

    public function autocompleteAction(Request $request)
    {
        $seach = $request->request->get('q');
        $letter = strtolower(substr($seach, 0, 1));
        $seach = substr($seach, -(strlen($seach) - 1));

        switch ($letter) {
            case 'p':
                $result = $this->getDoctrine()
                    ->getRepository('appBundle:Product')
                    ->findById($seach);
                break;
            default :
                $result = $this->getDoctrine()
                    ->getRepository('appBundle:Product')
                    ->findById($seach);
                break;
        }

        $response = new Response();
        $response->setContent(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
