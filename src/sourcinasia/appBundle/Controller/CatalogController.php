<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Catalog;

/**
 * Catalog controller.
 *
 */
class CatalogController extends Controller
{

    public function addtoselectionAction(Request $request, $action)
    {
        $content = 0;
        $em = $this->getDoctrine()->getManager();

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
            if ($request->request->get('catalog') && $request->request->get('selectionids')) {
                if ($Catalog = $em->getRepository('appBundle:Catalog')->find($request->request->get('catalog'))) {
                    $Products = $em->createQuery('SELECT p FROM appBundle:Product p WHERE p.id in (:products)')
                        ->setParameters(array('products' => $request->request->get('selectionids')))
                        ->GetResult();
                    foreach ($Products as $Product) {
                        switch ($action) {
                            case 'add':
                                if (!$Catalog->getProducts()->contains($Product)) {
                                    $Catalog->addProduct($Product);
                                    $em->persist($Catalog);
                                    $em->flush();
                                }
                                break;
                            case 'remove':
                                if ($Catalog->getProducts()->contains($Product)) {
                                    $Catalog->removeProduct($Product);
                                    $em->persist($Catalog);
                                    $em->flush();
                                }
                                break;
                        }
                    }
                    $this->get('session')->set('selectionids', array());
                    $this->get('session')->set('selection', '');
                    $content = $this->generateUrl('catalog', array('catalog' => $Catalog->getId()));
                };
            }
        }

        return new JsonResponse(array('content' => $content));
    }

    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Catalog entities.
     */
    public
    function indexAction()
    {
        return $this->render('appBundle:Catalog:index.html.twig', array(
            'Catalogs' => $Catalog = $this->GetSecurityCatalogs()
        ));
    }

    /**
     * Finds and displays a Catalog entity.
     */
    public
    function showAction($id)
    {
        $Catalog = $this->GetSecurityCatalog($id);

        if (!$Catalog) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Catalog:show.html.twig', array(
            'Catalog' => $Catalog,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Catalog entity.
     */
    public
    function newAction()
    {
        return $this->render('appBundle:Catalog:form.html.twig', array(
            'Catalog' => $this->GetSecurityCatalog(),
            'form' => $this->buildForm($this->GetSecurityCatalog())->createView(),
        ));
    }

    /**
     * Creates a new Catalog entity.
     */
    public
    function createAction(Request $request)
    {
        $Catalog = $this->GetSecurityCatalog();
        $form = $this->buildForm($Catalog)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Catalog);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('catalog'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Catalog:form.html.twig', array(
            'Catalog' => $Catalog,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Catalog entity.
     *
     */
    public
    function editAction($id)
    {
        $Catalog = $this->GetSecurityCatalog($id);

        if (!$Catalog)
            throw $this->createNotFoundException('Unable to find Catalog entity.');

        return $this->render('appBundle:Catalog:form.html.twig', array(
            'Catalog' => $Catalog,
            'form' => $this->buildForm($Catalog, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Catalog entity.
     */
    public
    function updateAction(Request $request, $id)
    {
        $Catalog = $this->GetSecurityCatalog($id);

        if (!$Catalog)
            throw $this->createNotFoundException('Unable to find Catalog entity');

        $form = $this->buildForm($Catalog, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Catalog);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('catalog'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Catalog:form.html.twig', array(
            'Catalog' => $Catalog,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Catalog entity.
     *
     */
    public
    function deleteAction($id)
    {

        $Catalog = $this->GetSecurityCatalog($id);

        if (!$Catalog)
            throw $this->createNotFoundException('Unable to find Catalog entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Catalog);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('catalog'));
    }

    /**
     * Creates a form to delete a Catalog entity by id.
     */
    private
    function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('catalog_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Catalog entity.
     */
    private function buildForm(Catalog $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\CatalogType', $entity, array(
                'action' => $this->generateUrl("catalog_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\CatalogType', $entity, array(
                'action' => $this->generateUrl("catalog_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Catalog
     */
    private
    function GetSecurityCatalogs()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSOURCING'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Catalog')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Catalog.');
    }

    /**
     * Gets Catalog
     */
    private
    function GetSecurityCatalog($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Catalog')->find($id);
            else
                return new Catalog();
        else
            throw $this->createNotFoundException('Unable to find Catalog.');
    }

}
