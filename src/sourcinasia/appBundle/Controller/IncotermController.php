<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Incoterm;

/**
 * Incoterm controller.
 *
 */
class IncotermController extends Controller
{

    /**
     * Lists all Incoterm entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Incoterm:index.html.twig', array(
            'Incoterms' => $Incoterm = $this->GetSecurityIncoterms()
        ));
    }

    /**
     * Finds and displays a Incoterm entity.
     */
    public function showAction($id)
    {
        $Incoterm = $this->GetSecurityIncoterm($id);

        if (!$Incoterm) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Incoterm:show.html.twig', array(
            'Incoterm' => $Incoterm,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Incoterm entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Incoterm:form.html.twig', array(
            'Incoterm' => $Incoterm,
            'form' => $this->buildForm($this->GetSecurityIncoterm())->createView(),
        ));
    }

    /**
     * Creates a new Incoterm entity.
     */
    public function createAction(Request $request)
    {
        $Incoterm = $this->GetSecurityIncoterm();
        $form = $this->buildForm($Incoterm)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Incoterm);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'CREATED!');
            return $this->redirect($this->generateUrl('incoterm_show', array('id' => $Incoterm->getId())));
        }
        $this->get('session')->getFlashBag()->add('error', 'NOT CREATED!');
        return $this->render('appBundle:Incoterm:form.html.twig', array(
            'Incoterm' => $Incoterm,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Incoterm entity.
     *
     */
    public function editAction($id)
    {
        $Incoterm = $this->GetSecurityIncoterm($id);

        if (!$Incoterm)
            throw $this->createNotFoundException('Unable to find Incoterm entity.');

        return $this->render('appBundle:Incoterm:form.html.twig', array(
            'Incoterm' => $Incoterm,
            'form' => $this->buildForm($Incoterm, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Incoterm entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Incoterm = $this->GetSecurityIncoterm($id);

        if (!$Incoterm)
            throw $this->createNotFoundException('Unable to find Incoterm entity.');

        $form = $this->buildForm($Incoterm, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Incoterm);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'UPDATED!');
            return $this->redirect($this->generateUrl('incoterm_show', array('id' => $id)));
        }
        $this->get('session')->getFlashBag()->add('error', 'NOT UPDATED!');

        return $this->render('appBundle:Incoterm:form.html.twig', array(
            'Incoterm' => $Incoterm,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Incoterm entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->DeleteForm($id)->handleRequest($request);

        if ($form->isValid()) {

            $Incoterm = $this->GetSecurityIncoterm($id);

            if (!$Incoterm)
                throw $this->createNotFoundException('Unable to find Incoterm entity.');

            $em = $this->getDoctrine()->getManager();
            $em->remove($Incoterm);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('incoterm'));
    }

    /**
     * Creates a form to delete a Incoterm entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('incoterm_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Incoterm entity.
     */
    private function buildForm(Incoterm $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\IncotermType', $entity, array(
                'action' => $this->generateUrl("incoterm_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\IncotermType', $entity, array(
                'action' => $this->generateUrl("incoterm_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Incoterms
     */
    private function GetSecurityIncoterms()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Incoterm')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Incoterm.');
    }

    /**
     * Gets Suppliers
     */
    private function GetSecurityIncoterm($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Incoterm')->find($id);
            else
                return new Incoterm();
        else
            throw $this->createNotFoundException('Unable to find Incoterm.');
    }

}
