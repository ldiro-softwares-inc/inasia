<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CategoryController extends Controller
{


    /*
     * Page tree
     */
    public function indexAction()
    {
        return $this->render('appBundle:Category:index.html.twig');
    }

    /*
     * Manage Nomenclature
     */
    public function manageAction(Request $request)
    {
        $result = array();

        if ($request->isMethod('POST')) {

            $id = $request->request->get('id');
            switch ($request->request->get('operation')) {
                case 'rename_node':
                    $title = $request->request->get('title');
                    $result = $this->getDoctrine()
                        ->getRepository('appBundle:Categorie')
                        ->rename_node($id, $title);
                    break;
                case 'create_node':
                    $title = $request->request->get('title');
                    $position = $request->request->get('position');
                    $type = $request->request->get('type');
                    $result = $this->getDoctrine()
                        ->getRepository('appBundle:Categorie')
                        ->create_node($id, $title);
                    break;
                case 'remove_node':
                    $result = $this->getDoctrine()
                        ->getRepository('appBundle:Categorie')
                        ->remove_node($id);
                    break;
                case 'move_node':
                    $ref = $request->request->get('position');
                    $position = $request->request->get('position');
                    $copy = $request->request->get('copy');
                    $result = $this->getDoctrine()
                        ->getRepository('appBundle:Categorie')
                        ->move_node($id, $ref, $copy);
                    break;
            }
        } else
            $result = $this->getDoctrine()
                ->getRepository('appBundle:Categorie')
                ->get_children($request->query->get('id'));

        $response = new Response();
        $response->setContent(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


}
