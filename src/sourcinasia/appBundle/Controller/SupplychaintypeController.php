<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Supplychaintype;

/**
 * Supplychaintype controller.
 *
 */
class SupplychaintypeController extends Controller
{
    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Supplychaintype entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Supplychaintype:index.html.twig', array(
            'Supplychaintypes' => $Supplychaintype = $this->GetSecuritySupplychaintypes()
        ));
    }

    /**
     * Finds and displays a Supplychaintype entity.
     */
    public function showAction($id)
    {
        $Supplychaintype = $this->GetSecuritySupplychaintype($id);

        if (!$Supplychaintype) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Supplychaintype:show.html.twig', array(
            'Supplychaintype' => $Supplychaintype,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Supplychaintype entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Supplychaintype:form.html.twig', array(
            'Supplychaintype' => $this->GetSecuritySupplychaintype(),
            'form' => $this->buildForm($this->GetSecuritySupplychaintype())->createView(),
        ));
    }

    /**
     * Creates a new Supplychaintype entity.
     */
    public function createAction(Request $request)
    {
        $Supplychaintype = $this->GetSecuritySupplychaintype();
        $form = $this->buildForm($Supplychaintype)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Supplychaintype);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('supplychaintype'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Supplychaintype:form.html.twig', array(
            'Supplychaintype' => $Supplychaintype,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Supplychaintype entity.
     *
     */
    public function editAction($id)
    {
        $Supplychaintype = $this->GetSecuritySupplychaintype($id);

        if (!$Supplychaintype)
            throw $this->createNotFoundException('Unable to find Supplychaintype entity.');

        return $this->render('appBundle:Supplychaintype:form.html.twig', array(
            'Supplychaintype' => $Supplychaintype,
            'form' => $this->buildForm($Supplychaintype, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Supplychaintype entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Supplychaintype = $this->GetSecuritySupplychaintype($id);

        if (!$Supplychaintype)
            throw $this->createNotFoundException('Unable to find Supplychaintype entity.');

        $form = $this->buildForm($Supplychaintype, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Supplychaintype);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('supplychaintype'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Supplychaintype:form.html.twig', array(
            'Supplychaintype' => $Supplychaintype,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Supplychaintype entity.
     *
     */
    public function deleteAction($id)
    {

        $Supplychaintype = $this->GetSecuritySupplychaintype($id);

        if (!$Supplychaintype)
            throw $this->createNotFoundException('Unable to find Supplychaintype entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Supplychaintype);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('supplychaintype'));
    }

    /**
     * Creates a form to delete a Supplychaintype entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('supplychaintype_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Supplychaintype entity.
     */
    private function buildForm(Supplychaintype $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\SupplychaintypeType', $entity, array(
                'action' => $this->generateUrl("supplychaintype_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\SupplychaintypeType', $entity, array(
                'action' => $this->generateUrl("supplychaintype_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Supplychaintype
     */
    private function GetSecuritySupplychaintypes()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Supplychaintype')->findBy(array(), array('step' => 'asc'));
        else
            throw $this->createNotFoundException('Unable to find Supplychaintype.');
    }

    /**
     * Gets Supplychaintype
     */
    private function GetSecuritySupplychaintype($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Supplychaintype')->find($id);
            else
                return new Supplychaintype();
        else
            throw $this->createNotFoundException('Unable to find Supplychaintype.');
    }

}
