<?php

namespace sourcinasia\appBundle\Controller;


use sourcinasia\appBundle\Entity\Document;
use sourcinasia\appBundle\Entity\Supplier;
use sourcinasia\appBundle\Form\DocumentSupplierTypeType;
use sourcinasia\appBundle\Form\DocumentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class DocumentController extends Controller
{

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(DocumentType::class);
        $form
            ->add('save', SubmitType::class, array('label' => 'Save'))
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $document = $form->getData();
            $em->persist($document);
            $em->flush();

            return $this->redirectToRoute('add_document_to_supplier');
        }


        return $this->render('appBundle:Document:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function addToSupplierAction(Request $request, Supplier $supplier,string $type)
    {
        if(
            !(
                $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')
                || (
                    ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSOURCING')
                        || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')
                        || $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING')
                    )
                    && $type !== Document::CUSTOMER_OFFER_TYPE
                )
            )
        ) {
            $response = $this->render('appBundle:Error:403.html.twig');
            $response->setStatusCode(403);

            return $response;
        }


        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(DocumentSupplierTypeType::class);
        $form->get('documentType')->setData($type);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Document $document */
            $document = $form->getData();
            $document->setSupplier($supplier);
            $em->persist($document);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'ENTITY_DOCUMENT_FLASH_SAVED');
            if ($document->getDocumentType() === Document::CATALOG_TYPE) {
                return $this->redirect($this->generateUrl('supplier_show', array('id' => $supplier->getId())) . '#catalogs');
            } else {
                return $this->redirect($this->generateUrl('supplier_show', array('id' => $supplier->getId())) . '#quotations');
            }

        }


        if ($form->getErrors()->count() > 0) {
            $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        }

        return $this->render('appBundle:Document:form.html.twig', array(
            'form' => $form->createView(),
            'supplier_label' => sprintf('%s (#%s)', $supplier->getName(),$supplier->getId()),
            'type' => $type
        ));
    }

    public function deleteAction(Request $request, Document $document) {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('delete-document-'.$document->getId(), $submittedToken)
        &&
            (
                $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')
                || (
                    ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN'))
                    && $document->getDocumentType() !== Document::CUSTOMER_OFFER_TYPE
                )
            )
        ) {
            $em = $this->getDoctrine()->getManager();
            $supplier_id = $document->getSupplier()->getId();
            $document_type = $document->getDocumentType();
            $em->remove($document);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'ENTITY_DOCUMENT_FLASH_REMOVED');
            if ($document_type === Document::CATALOG_TYPE) {
                return $this->redirect($this->generateUrl('supplier_show', array('id' => $supplier_id)) . '#catalogs');
            } else {
                return $this->redirect($this->generateUrl('supplier_show', array('id' => $supplier_id)) . '#quotations');
            }
        } else {
            $response = $this->render('appBundle:Error:403.html.twig');
            $response->setStatusCode(403);

            return $response;
        }
    }


}
