<?php

namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\Cadencier;
use sourcinasia\appBundle\Entity\Command;
use sourcinasia\appBundle\Entity\History;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Command controller.
 *
 */
class CommandController extends Controller
{
    /*
     * Save filters
     */

    public function savefiltersAction(Request $request)
    {
        $request = $request->request->get('request');
        $response = new Response();
        $result = 0;
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            if (is_array($request)) {
                if (array_key_exists('steps', $request)) {
                    $session = $this->get('session');
                    foreach ($request['steps'] as $key => $value) {
                        $request['steps'][$key] = (int)$value;
                    }
                    $session->set('statefilters', $request['steps']);
                    $result = 1;
                }
            }
        }
        $response->setContent(json_encode(array('result' => $result)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /*
     * Rassemble commandes dans le meme conteneur
     */

    public function mergeAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')
        ) {
            $selection = $request->request->get('selection');
            $cadenciers = $this->getDoctrine()->getRepository('appBundle:Cadencier')->GetCadenciers($selection);

            if (count($cadenciers) > 1) {
                $test = true;
                foreach ($cadenciers as $key => $cadencier) {
                    if ($key == 0) {
                        $maincadencier = $cadencier;
                    }
                    $nb = count($cadencier->GetCommand()->getSupplychains());
                    if ($nb > 0) {
                        $test = false;
                        $this->get('session')->getFlashBag()->add(
                            'warning',
                            'Impossible - Order num' . $cadencier->getId() . ' have ' . $nb . ' document' . $nb > 1 ? 's' : '' . ' linked to logistic. To merge orders please remove documents before'
                        );
                    }
                }

                if ($test) {
                    $em = $this->getDoctrine()->getManager();
                    foreach ($cadenciers as $key => $cadencier) {
                        if ($key != 0) {
                            if ($cadencier->GetCommand()->getId() != $maincadencier->GetCommand()->getId()) {
                                $oldcommand = $cadencier->GetCommand();
                                $cadencier->SetCommand($maincadencier->GetCommand());
                                $em->persist($cadencier);
                                $em->flush();

                                if (count($oldcommand->getCadenciers()) == 0) {
                                    $em->remove($oldcommand);
                                    $em->flush();
                                }
                            }
                        }
                    }
                }

                return $this->redirect($this->generateUrl('command_show', array('id' => $maincadencier->getId())));
            } else {
                $this->get('session')->getFlashBag()->add('warning', 'Not enough order selected');

                return $this->redirect($this->generateUrl('command'));
            }
        }
    }

    /*
     * Retirer commande du conteneur
     */

    public function unmergeAction(Cadencier $cadencier)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') || $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')
        ) {
            $Command = new Command();
            $cadencier->setCommand($Command);
            $Command->setContainer($cadencier->getContainer());
            $Command->setShipper('-');
            $Command->setConsignee('-');
            $Command->setNotify('-');
            $Command->setFreightpayableat('');
            $Command->setBookingnumber('-');
            $Command->setEtd(new \DateTime());
            $Command->setEta(new \DateTime());
            $Command->setVessel('-');
            $Command->setTcnum('-');
            $Command->setSealnum('-');
            $Command->setLocked(0);
            $Command->setModified(new \DateTime());
            $Command->setCreated(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($cadencier);
            $em->flush();

            return $this->redirect($this->generateUrl('command_show', array('id' => $cadencier->getId())));
        }
    }

    /*
     * Retourne les orders filtrer
     */

    public function requestAction(Request $request)
    {
        $request = $request->query->get('request');
        $profil = $this->get('Profils')->get();

        $customersids = array();
        $userid = false;

        if ($profil['cutomer_id'] && $this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $customersids[] = $profil['cutomer_id'];
            $customers = array();

            foreach ($customers as $customer) {
                $customersids[] = $customer->getId();
            }
        } elseif (($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted('ROLE_SOURCING') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN'))
        ) {
            $customers = $this->getDoctrine()->getRepository('appBundle:Customer')->GetCommercialCustomers('all', true);

            foreach ($customers as $customer) {
                $customersids[] = $customer->getId();
            }
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $customers = $this->getDoctrine()->getRepository('appBundle:Customer')->GetCommercialCustomers(
                $this->get('security.token_storage')->getToken()->getUser()->getId(),
                true
            );

            foreach ($customers as $customer) {
                $customersids[] = $customer->getId();
            }
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && $profil['contact_type'] == 0) {
            $customer = $this->get('security.token_storage')->getToken()->getUser()->GetContact()->GetCustomer();
            $customersids[] = $customer->getId();
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && $profil['contact_type'] == 4) {
            $customer = $this->get('security.token_storage')->getToken()->getUser()->GetContact()->GetCustomer();
            $customersids[] = $customer->getId();
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && $profil['contact_type'] == 1) {
            $customer = $this->get('security.token_storage')->getToken()->getUser()->GetContact()->GetCustomer();
            $customersids[] = $customer->getId();
            $userid = $this->get('security.token_storage')->getToken()->getUser()->getId();
        }


        $session = $this->get('session');
        if (array_key_exists('supplierid', $request)) {
            $session->set('supplierid', $request['supplierid']);
        } else {
            $session->remove('supplierid');
        }

        if (array_key_exists('customersid', $request)) {
            $session->set('customersid', $request['customersid']);
        } else {
            $session->remove('customersid');
        }

        if (array_key_exists('invoiceid', $request)) {
            $session->set('invoiceid', $request['invoiceid']);
        } else {
            $session->remove('invoiceid');
        }

        if (array_key_exists('containerid', $request)) {
            $session->set('containerid', $request['containerid']);
        } else {
            $session->remove('containerid');
        }

        if (array_key_exists('orderid', $request)) {
            $session->set('orderid', $request['orderid']);
        } else {
            $session->remove('orderid');
        }

        if (array_key_exists('supplierCategoryFilter', $request)) {
            $session->set('supplierCategoryFilter', $request['supplierCategoryFilter']);
        } else {
            $session->remove('supplierCategoryFilter');
        }

        if (array_key_exists('containerSize', $request)) {
            $session->set('containerSize', $request['containerSize']);
        } else {
            $session->remove('containerSize');
        }

        // Gros hack pas super propre mais c'était la seul solution possible
        if (array_key_exists('productFilter', $request)) {
            $session->set('productFilter', $request['productFilter']);

            if(!empty($request['productFilter'])) {
                // Hack pour passer l'info au repo qu'il faut retourner un tableau vide si pas de donnés sur les produits (à cause de array_filer)
                $request['emptyData'] = true;

                $tempoData = $this->getDoctrine()->getRepository('appBundle:Product')->getProductBySearch($request['productFilter']);

                if(!empty($tempoData)) {
                    $request['emptyData'] = false;
                    $request['productFilterIds'] = array_map(function($value) { return $value['id']; }, $tempoData);
                }
            }
        } else {
            $session->remove('productFilter');
        }

        $request = $this->getDoctrine()
            ->getRepository('appBundle:Cadencier')
            ->search(
                array_filter($request),
                $customersids,
                $userid,
                $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')
            );

        return $this->render(
            'appBundle:Command:listCommand.html.twig',
            array(
                'Cadenciers' => $request,
                'tous' => true
            )
        );
    }

    public function listAction()
    {
        return $this->render(
            'appBundle:Command:produce.html.twig',
            array(
                'Commands' => $Command = $this->GetSecurityCommands()
            )
        );
    }

    /*
     * STEP2 1.1 COMMERCIAL COMFIRM SELECTION
     */

    public function step01Action(Cadencier $cadencier)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $em = $this->getDoctrine()->getManager();
            $cadencier->setState(2);
            $em->persist($cadencier);
            $em->flush();
            $this->get('event_dispatcher')->dispatch(
                'sendmail',
                new GenericEvent($cadencier, array('step' => 'SALERCONFIRMSELECTION'))
            );
            $this->get('session')->getFlashBag()->add('notice', 'STEP11');

            return $this->redirect($this->generateUrl('command'));
        }
    }

    /*
     * STEP4 1.3 PRODUCTION COMFIRM PRICES
     */

    public function step12Action(Cadencier $cadencier)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {


            $em = $this->getDoctrine()->getManager();
            $cadencier->setState(4);
            $em->persist($cadencier);
            $em->flush();
            $this->get('event_dispatcher')->dispatch(
                'sendmail',
                new GenericEvent($cadencier, array('step' => 'PRODUCTIONCONFIRMPRICES'))
            );
            $this->get('session')->getFlashBag()->add('notice', 'STEP13');

            return $this->redirect($this->generateUrl('command'));
        }
    }

    /*
     * STEP3 1.2 PRICES REJECTED
     */

    public function stepreject12Action(Cadencier $cadencier)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
            $em = $this->getDoctrine()->getManager();
            $cadencier->setState(3);
            $em->persist($cadencier);
            $em->flush();
            $this->get('event_dispatcher')->dispatch(
                'sendmail',
                new GenericEvent($cadencier, array('step' => 'PRICESREJECTED'))
            );
            $this->get('session')->getFlashBag()->add('notice', 'PI SUPPLIER REJECTED');
        }

        return $this->redirect($this->generateUrl('command'));
    }

    /*
     * STEP5 1.2 PRICES CONFIRMED
     */

    public function step13Action(Cadencier $cadencier)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            if ($this->isValidSelection($cadencier->getSelection())) {
                $test = true;
                switch (strtolower($cadencier->getIncoterm()->getTitle())) {
                    case 'fob':
                        foreach ($cadencier->getSelection() as $selection) {

                            $offer = $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->find(
                                $selection['offer']
                            );

                            if (!$offer->getFobnegociation() > 0 && $selection['confirm'] == 0) {
                                $test = false;
                                $this->get('session')->getFlashBag()->add(
                                    'warning',
                                    'Error - FOB - Supplier PI price is empty for offer ' . $offer->getId() . ' - Supplier Item number ' . $offer->getItemnumber()
                                );
                            };

                            if ($selection['price'] < $offer->getFobnegociation()) {
                               // $test = false;
                                $this->get('session')->getFlashBag()->add(
                                    'warning',
                                    'Error - FOB - Customer Pi price is < Supplier PI price for offer ' . $offer->getId() . ' - Supplier Item number ' . $offer->getItemnumber()
                                );
                            };
                        }
                        break;
                    case 'exw':
                        foreach ($cadencier->getSelection() as $selection) {
                            $offer = $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->find(
                                $selection['offer']
                            );

                            if (!$offer->getExwnegociation() > 0 || $selection['confirm'] == 0) {
                                $test = false;
                                $this->get('session')->getFlashBag()->add(
                                    'warning',
                                    'Error - EXW - Supplier PI price is empty for offer ' . $offer->getId() . ' - Supplier Item number ' . $offer->getItemnumber()
                                );
                                continue; // when exwnegociation == null...
                            };

                            if ($selection['price'] < $offer->getExwnegociation() * $offer->getCurrency()->getUsd()) {
                                //$test = false;
                                $this->get('session')->getFlashBag()->add(
                                    'warning',
                                    'Error - EXW - Customer Pi price is < Supplier PI price for offer' . $offer->getId() . ' - Supplier Item number ' . $offer->getItemnumber()
                                );
                            };
                        }
                        break;
                }

                if ($test) {
                    $em = $this->getDoctrine()->getManager();
                    $cadencier->setState(5);
                    $em->persist($cadencier);
                    $em->flush();
                    $this->get('event_dispatcher')->dispatch(
                        'sendmail',
                        new GenericEvent($cadencier, array('step' => 'ADMINCONFIRMPRICES'))
                    );
                    $this->get('session')->getFlashBag()->add('notice', 'STEP14');

                    return $this->redirect(
                        $this->generateUrl('cadencier_show', array('id' => $cadencier->getId(), 'action' => 'genpi'))
                    );
                } else {
                    return $this->redirect(
                        $this->generateUrl('cadencier_show', array('id' => $cadencier->getId(), 'action' => 'production'))
                    );
                }
            }
        }

        return $this->redirect($this->generateUrl('command'));
    }

    /*
     * STEP6 1.4 SALER CONFIRM PI
     */

    public function step142Action(Cadencier $cadencier)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') || $this->get('security.authorization_checker')->isGranted(
                'ROLE_ADMINSUPPLYCHAIN'
            )
        ) {
            if ($this->isValidSelection($cadencier->getSelection())) {
                $em = $this->getDoctrine()->getManager();
                $cadencier->setState(6);
                $em->persist($cadencier);
                $em->flush();
                $this->get('event_dispatcher')->dispatch(
                    'sendmail',
                    new GenericEvent($cadencier, array('step' => 'SALERCOMFIRMPI'))
                );
                $this->get('session')->getFlashBag()->add('notice', 'STEP15');
            }
        }

        return $this->redirect($this->generateUrl('command'));
    }

    /*
     * STEP7 1.5 ADMIN CONFIRM PI
     */

    public function step14Action(Cadencier $cadencier)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            if ($cadencier->getCommand() && $this->isValidSelection($cadencier->getSelection())) {
                $cadencier->setState(7);
                $cadencier->setStepValidation(new \datetime());
                $em->persist($cadencier);
                $em->flush();
                $this->get('event_dispatcher')->dispatch(
                    'sendmail',
                    new GenericEvent($cadencier, array('step' => 'ADMINCOMFIRMPI'))
                );
                $this->get('session')->getFlashBag()->add('notice', 'STEP2');
            }
        }

        return $this->redirect($this->generateUrl('command'));
    }

    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Command entities.
     */
    public function indexAction(Request $request)
    {
        if (($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') || $this->get('security.authorization_checker')->isGranted(
                'ROLE_ADMINSUPPLYCHAIN'
            ))
        ) {
            $customers = $this->getDoctrine()->getRepository('appBundle:Customer')->GetCommercialCustomers('all');
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $customers = $this->getDoctrine()->getRepository('appBundle:Customer')->GetCommercialCustomers(
                $this->get('security.token_storage')->getToken()->getUser()->getId()
            );
        }
        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
            $profil = $this->get('Profils')->get();
            if (!in_array($profil['contact_type'], array('0', '1', '4'))) {
                return $this->redirect($this->generateUrl('catalog'));
            }
        }
        $customers = array();

        $session = $this->get('session');

        $statefilters = (array)$session->get('statefilters');

        if (empty($statefilters) || $request->query->get('filter') == "raz") {
            if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') and in_array(
                    $profil['contact_type'],
                    array('4')
                )
            ) {
                $statefilters = array_merge($statefilters, array(1, 7));
            }
            if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') and $profil['commercialtools']) {
                $statefilters = array_merge($statefilters, array(0, 1, 7, 8, 9, 10));
            }
            if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
                $statefilters = array_merge($statefilters, array(1, 7, 8, 9, 10));
            }
            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
                $statefilters = array_merge($statefilters, array(1, 4, 6, 8));
            }
            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                $statefilters = array_merge($statefilters, array(1, 5, 7));
            }
            if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
                $statefilters = array_merge($statefilters, array(2, 3, 8));
            }
            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                $statefilters = array_merge($statefilters, array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)); //todo tout selectionner pour admin
            }

            $session = $this->get('session');
            $session->set('statefilters', $statefilters);
            $session->remove('supplierid');
            $session->remove('customersid');
            $session->remove('invoiceid');
            $session->remove('containerid');
            $session->remove('productFilter');
            $session->remove('supplierCategoryFilter');
            $session->remove('containerSize');
            $session->remove('orderid');
        }

        // On va récupérer la liste des suppliers categories pour afficher le filtre
        $suppliersCategories = $this->getDoctrine()->getManager()->getRepository('appBundle:Suppliercategory')->findBy(array(), array('title' => 'ASC'));

        return $this->render(
            'appBundle:Command:index.html.twig',
            array(
                'statefilters' => $statefilters,
                'Customers' => $customers,
                'session' => $this->get('session'),
                'suppliersCategories' => $suppliersCategories,
            )
        );
    }

    /**
     * Finds and displays a Order entity.
     */
    public function showAction($id)
    {
        $cadencier = $this->GetCadencier($id);

        if (!$cadencier) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);

            return $response;
        }

        $Supplychains = array();
        foreach ($cadencier->getSupplychains() as $supplychain) {
            $Supplychains['r' . $supplychain->getSupplychaintype()->GetId()][] = $supplychain;
        }
        /*
          foreach ($cadencier->getCommand()->getSupplychains() as $supplychain) {
          $Supplychains['r' . $supplychain->getSupplychaintype()->GetId()][] = $supplychain;
          } */

        $Supplychaintypes = array();
        foreach ($this->getDoctrine()->getManager()->getRepository('appBundle:Supplychaintype')->findAll() as $Supplychaintype) {
            $Supplychaintypes['r' . $Supplychaintype->getSupplychaincat()->GetId()][] = $Supplychaintype;
        }


        $AllOffers = array();
        foreach ($cadencier->getCommand()->getCadenciers() as $order) {
            $AllOffers[$order->getId()] = $this->getDoctrine()->getManager()->getRepository('appBundle:Offer')->OfferIn(
                $order->GetOfferslist(),
                $this->get('Profils')->get()
            );
        }

        $customer = $cadencier->GetCustomer();

        return $this->render(
            'appBundle:Command:show.html.twig',
            array(
                'cadencier' => $cadencier,
                'Command' => $cadencier->getCommand(),
                'cbmtotalvolumeformdoctine' => $this->getDoctrine()->getRepository('appBundle:Command')->GetTotalValue(
                    $cadencier->GetCommand()->getId()
                ),
                'Supplychaintypes' => $Supplychaintypes,
                'Supplychaincats' => $this->getDoctrine()->getManager()->getRepository(
                    'appBundle:Supplychaincat'
                )->findAll(),
                'Supplychains' => $Supplychains,
                'myuser' => array(
                    'cutomer_id' => $customer->getId(),
                    'customer_coef' => $customer->getMargecoef(),
                    'commercial_coef' => 0,
                ),
                'documents' => $this->getDoctrine()->getManager()->getRepository(
                    'appBundle:Supplychain'
                )->getCustomerDocuments($cadencier),
                'checked' => true,
                'Offers' => $AllOffers[$cadencier->getId()],
                'AllOffers' => $AllOffers,
            )
        );
    }

    /**
     * Displays a form to edit an existing Order entity.
     *
     */
    public function editAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
            $Command = $this->GetSecurityCommand($id);

            if (!$Command) {
                throw $this->createNotFoundException('Unable to find Order entity.');
            }

            return $this->render(
                'appBundle:Command:form.html.twig',
                array(
                    'Command' => $Command,
                    'form' => $this->buildForm($Command, 'edit')->createView()
                )
            );
        }
    }

    /**
     * Edits an existing Order entity.
     */
    public function updateAction(Request $request, $id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
            $Command = $this->GetSecurityCommand($id);

            if (!$Command) {
                throw $this->createNotFoundException('Unable to find Order entity.');
            }

            $form = $this->buildForm($Command, 'edit')->handleRequest($request);

            $check = $form->get('eta')->getData() > $form->get('etd')->getData();
            if (!$check) {
                $this->get('session')->getFlashBag()->add('warning', 'Error: ETA>ETD');
            }

            if ($form->isValid() && $check) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($Command);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
                $cadenciers = $Command->getCadenciers();
                if (count($cadenciers)) {
                    return $this->redirect(
                        $this->generateUrl('command_show', array('id' => $cadenciers[0]->getId())) . '#Logistic'
                    );
                } else {
                    return $this->redirect($this->generateUrl('command'));
                }
            }
            $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

            return $this->render(
                'appBundle:Command:form.html.twig',
                array(
                    'Command' => $Command,
                    'form' => $form->createView()
                )
            );
        }
    }

    public function archiveAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') || $this->get('security.authorization_checker')->isGranted(
                'ROLE_COMMERCIAL'
            ) || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')
        ) {

            $cadencier = $this->GetCadencier($id);

            if (!$cadencier) {
                throw $this->createNotFoundException('Unable to find Order entity.');
            }

            if (count($cadencier->GetCommand()->getCadenciers()) <= 2) {
                if (
                (
                    ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && in_array(
                            (int)$cadencier->getState(),
                            array(0)
                        )) || // CLIENT commercial
                    ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && in_array(
                            (int)$cadencier->getState(),
                            array(0, 1)
                        )) ||
                    ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') && in_array(
                            (int)$cadencier->getState(),
                            array(1)
                        )) ||
                    ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') && in_array(
                            (int)$cadencier->getState(),
                            array(2, 3, 4, 5, 6, 7)
                        )) ||
                    ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') && in_array(
                            (int)$cadencier->getState(),
                            array(0, 1, 2, 3, 4, 5, 6, 7)
                        )) ||
                    ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && in_array(
                            (int)$cadencier->getState(),
                            array(0, 1, 2, 3, 4, 5, 6, 7)
                        ))
                )
                ) {

                    $em = $this->getDoctrine()->getManager();
                    $cadencier->setArchive(true);
                    $em->persist($cadencier);
                    $em->flush();

                    $this->get('session')->getFlashBag()->add('notice', 'Order archive !');

                    return $this->redirect($this->generateUrl('command'));
                } else {
                    if (count($cadencier->getSupplychains())) {
                        $this->get('session')->getFlashBag()->add(
                            'warning',
                            'DOCUMENTS ARE LINKED TO ORDER. PLEASE DELETE BEFORE'
                        );
                    } else {
                        $this->get('session')->getFlashBag()->add('warning', 'ERROR_DELETE_ALREADY_CONFIRM');
                    }

                    return $this->redirect($this->generateUrl('command'));
                }
            } else {
                $this->get('session')->getFlashBag()->add('warning', 'ERROR_DELETE_UNMERGE_FIRST');

                return $this->redirect($this->generateUrl('command'));
            }
        }
        throw new AccessDeniedException();
    }

    /**
     * Deletes a Order entity.
     *
     */
    public function deleteAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') || $this->get('security.authorization_checker')->isGranted(
                'ROLE_COMMERCIAL'
            ) || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')
        ) {

            $cadencier = $this->GetCadencier($id);

            if (!$cadencier) {
                throw $this->createNotFoundException('Unable to find Order entity.');
            }

            if (count($cadencier->GetCommand()->getCadenciers()) <= 2) {

                if (
                    (
                        ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && in_array(
                                (int)$cadencier->getState(),
                                array(0)
                            )) || // CLIENT commercial
                        ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && in_array(
                                (int)$cadencier->getState(),
                                array(0, 1)
                            )) ||
                        ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') && in_array(
                                (int)$cadencier->getState(),
                                array(1)
                            )) ||
                        ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION') && in_array(
                                (int)$cadencier->getState(),
                                array(2, 3, 4, 5, 6, 7)
                            )) ||
                        ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN') && in_array(
                                (int)$cadencier->getState(),
                                array(0, 1, 2, 3, 4, 5, 6, 7)
                            )) ||
                        ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') && in_array(
                                (int)$cadencier->getState(),
                                array(0, 1, 2, 3, 4, 5, 6, 7)
                            ))
                    ) && !count($cadencier->getSupplychains())
                ) {

                    $em = $this->getDoctrine()->getManager();
                    $em->remove($cadencier);
                    $em->flush();

                    $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

                    return $this->redirect($this->generateUrl('command'));
                } else {
                    if (count($cadencier->getSupplychains())) {
                        $this->get('session')->getFlashBag()->add(
                            'warning',
                            'DOCUMENTS ARE LINKED TO ORDER. PLEASE DELETE BEFORE'
                        );
                    } else {
                        $this->get('session')->getFlashBag()->add('warning', 'ERROR_DELETE_ALREADY_CONFIRM');
                    }

                    return $this->redirect($this->generateUrl('command'));
                }
            } else {
                $this->get('session')->getFlashBag()->add('warning', 'ERROR_DELETE_UNMERGE_FIRST');

                return $this->redirect($this->generateUrl('command'));
            }
        }
        throw new AccessDeniedException();
    }

    /**
     * Creates a form to delete a Order entity by id.
     */
    private function DeleteForm($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            return $this->createFormBuilder()
                ->setAction($this->generateUrl('command_delete', array('id' => $id)))
                ->setMethod('DELETE')
                ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
                ->getForm();
        }
    }

    /**
     * Creates a form to create/edit a Order entity.
     */
    private function buildForm(Command $entity, $task = "create")
    {
        if ($task == "create") {
            $form = $this->createForm(
                'sourcinasia\appBundle\Form\CommandType',
                $entity,
                array(
                    'action' => $this->generateUrl("command_create"),
                    'method' => 'POST',
                    'attr' => array('class' => 'fill-up')
                )
            );
        } else {
            $form = $this->createForm(
                'sourcinasia\appBundle\Form\CommandType',
                $entity,
                array(
                    'action' => $this->generateUrl("command_update", array('id' => $entity->GetID())),
                    'method' => 'POST',
                    'attr' => array('class' => 'fill-up')
                )
            );
        }

        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Command
     */
    private function GetSecurityCommands()
    {
        $profils = $this->get('profils')->get();
        $customerid = $profils['cutomer_id'];
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            if ($customerid) {
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Command')->findBy(
                    array('customer' => $customerid)
                );
            } else {
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Command')->findAll();
            }
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Command')->findAll();
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Command')->findBy(
                array('customer' => $customerid)
            );
        }
    }

    /**
     * Gets Command
     */
    private function GetSecurityCommand($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            if ($id) {
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Command')->find($id);
            } else {
                throw $this->createNotFoundException('Unable to find Command.');
            }
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
            if ($id) {
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Command')->find($id);
            } else {
                throw $this->createNotFoundException('Unable to find Command.');
            }
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
            if ($id) {
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Command')->findOneBy(
                    array(
                        'id' => $id,
                        'customer' => $this->get('security.token_storage')->getToken()->getUser()->getContact()->getCustomer()->getId()
                    )
                );
            } else {
                throw $this->createNotFoundException('Unable to find Command.');
            }
        }
    }

    /*
     * R�cup�re le cadencier en fonction des droits
     * Delete, show , generate ACTION, CommandControler, CadencierController
     */

    private function GetCadencier($id)
    {
        $profil = $this->get('profils')->get();
        $em = $this->getDoctrine()->getRepository('appBundle:Cadencier');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINSUPPLYCHAIN')) {
            return $em->getCommercialCadencier($id);
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            return $em->getCommercialCadencier($id);
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
            return $em->getCommercialCadencier($id);
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT') && $profil['cutomer_id']) {
            return $em->getCustomerCadencier(
                $this->get('security.token_storage')->getToken()->getUser()->GetId(),
                $profil['cutomer_id'],
                $id
            );
        } else {
            return false;
        }
    }

    private function isValidSelection($selections)
    {
        if (empty($selections) && !is_array($selections)) {
            $this->get('session')->getFlashBag()->add(
                'warning',
                'Prices not confirmed. Please before valid supplier prices '
            );

            return false;
        } else {
            foreach ($selections as $selection) {
                if (array_key_exists('confirm', $selection)) {
                    if (empty($selection['confirm'])) {
                        $this->get('session')->getFlashBag()->add(
                            'warning',
                            'Prices not confirmed. Please before valid supplier prices '
                        );

                        return false;
                    }
                } else {
                    $this->get('session')->getFlashBag()->add(
                        'warning',
                        'Prices not confirmed. Please before valid supplier prices '
                    );

                    return false;
                }
            }
        }

        return true;
    }

}
