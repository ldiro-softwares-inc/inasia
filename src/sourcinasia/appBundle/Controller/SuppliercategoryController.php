<?php

namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\Supplier;
use sourcinasia\appBundle\Entity\Suppliercategory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class SuppliercategoryController
 * @package sourcinasia\appBundle\Controller
 */
class SuppliercategoryController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        // Nous allons rechercher tous les suppliers categories pour les afficher sur l'index
        $suppliersCategories = $em->getRepository('appBundle:Suppliercategory')->findBy(array(), array('created_at' => 'DESC'));

        return $this->render('appBundle:Suppliercategory:index.html.twig', array(
            'suppliersCategories' => $suppliersCategories
        ));
    }

    /**
     * @param Request $request
     * @param null $supplierCategoryId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function formAction(Request $request, $supplierCategoryId = null)
    {
        $em = $this->getDoctrine()->getManager();

        // Nous sommes dans le cas d'une édition
        if(!empty($supplierCategoryId)) {
            $supplierCategory = $em->getRepository('appBundle:Suppliercategory')->find($supplierCategoryId);
        } else {
            $supplierCategory = new Suppliercategory();
        }

        if(!$supplierCategory instanceof Suppliercategory) {
            throw $this->createNotFoundException('Impossible to find SupplierCategory');
        }

        $form = $this->createForm('sourcinasia\appBundle\Form\SuppliercategoryType', $supplierCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if($form->isValid()) {
                $em->persist($supplierCategory);
                $em->flush();

                $this->get('session')->getFlashBag()->add('notice', 'Supplier category has been created.');
                return $this->redirectToRoute('suppliercategory');
            } else {
                $this->get('session')->getFlashBag()->add('warning', 'The information are not correct.');
                return $this->redirectToRoute('suppliercategory_new');
            }
        }

        return $this->render('appBundle:Suppliercategory:form.html.twig', array(
            'form'             => $form->createView(),
            'supplierCategory' => $supplierCategory,
        ));
    }

    /**
     * @param Request $request
     * @param Suppliercategory $supplierCategory
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Suppliercategory $supplierCategory)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        if (!empty($supplierCategory->getSuppliers())) {
            foreach ($supplierCategory->getSuppliers() as $supplier) {
                /** @var $supplier Supplier */
                $supplier->setSupplierCategory(null);
                $em->persist($supplier);
                $em->flush();
            }
        }

        $em->remove($supplierCategory);
        $em->flush();

        return $this->redirectToRoute('suppliercategory');
    }
}
