<?php
namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\History;
use sourcinasia\appBundle\Entity\Image;
use sourcinasia\appBundle\Entity\Offer;
use sourcinasia\appBundle\Entity\Product;
use sourcinasia\appBundle\Entity\Supplier;
use sourcinasia\appBundle\Services\ImageRotateService;
use sourcinasia\appBundle\Services\Read\ReadCsv;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;



/**
 *Imagerotate Controller.
 *
 */

class ImagerotateController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function rotateImageAction(Request $request)
    {
        $itemNumber = $request->get('supplieritemnumber');
        
        $image =  strtok($request->get('image'),'?');
        $destination =  strtok($request->get('image'),'?');
        $angle = 90;
        $source = __SOURCINASIA__."htdocs$image";

        $imageRotateService = $this->get('ImageRotate');
        $imageRotateService->rotateImage( $source,$angle);

        return new JsonResponse($image);

    }
}
