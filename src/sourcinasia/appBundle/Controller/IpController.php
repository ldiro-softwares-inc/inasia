<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class IpController extends Controller
{

    public function indexAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ALLOWIP')) {
            return $this->render('appBundle:Security:index.html.twig', array(
                'form' => $this->createFormBuilder()
                    ->add('ip', 'text')
                    ->add('save', 'Symfony\Component\Form\Extension\Core\Type\SubmitType')
                    ->setAction($this->generateUrl('ip_update'))
                    ->setMethod('POST')
                    ->getForm()->createView()
            ));
        } else
            throw $this->createNotFoundException('Unable to find Unit entity.');
    }

    public function updateAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ALLOWIP')) {

            $form = $this->createFormBuilder()
                ->add('ip', 'text')
                ->add('save', 'Symfony\Component\Form\Extension\Core\Type\SubmitType')
                ->getForm();

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $ip = __DIR__ . '/../ac';
                $fp = fopen($ip, 'w');
                fwrite($fp, $data['ip']);
                fclose($fp);
                $this->get('session')->getFlashBag()->add('notice', 'Ip Updated !');
            }
        }
        return $this->redirect($this->generateUrl('home'));
    }

}
