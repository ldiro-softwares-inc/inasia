<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Container;

/**
 * Container controller.
 *
 */
class ContainerController extends Controller
{
    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Container entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Container:index.html.twig', array(
            'Containers' => $Container = $this->GetSecurityContainers()
        ));
    }

    /**
     * Finds and displays a Container entity.
     */
    public function showAction($id)
    {
        $Container = $this->GetSecurityContainer($id);

        if (!$Container) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Container:show.html.twig', array(
            'Container' => $Container,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Container entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Container:form.html.twig', array(
            'Container' => $this->GetSecurityContainer(),
            'form' => $this->buildForm($this->GetSecurityContainer())->createView(),
        ));
    }

    /**
     * Creates a new Container entity.
     */
    public function createAction(Request $request)
    {
        $Container = $this->GetSecurityContainer();
        $form = $this->buildForm($Container)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Container);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('container'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Container:form.html.twig', array(
            'Container' => $Container,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Container entity.
     *
     */
    public function editAction($id)
    {
        $Container = $this->GetSecurityContainer($id);

        if (!$Container)
            throw $this->createNotFoundException('Unable to find Container entity.');

        return $this->render('appBundle:Container:form.html.twig', array(
            'Container' => $Container,
            'form' => $this->buildForm($Container, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Container entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Container = $this->GetSecurityContainer($id);

        if (!$Container)
            throw $this->createNotFoundException('Unable to find Container entity.');

        $form = $this->buildForm($Container, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Container);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('container'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Container:form.html.twig', array(
            'Container' => $Container,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Container entity.
     *
     */
    public function deleteAction($id)
    {

        $Container = $this->GetSecurityContainer($id);

        if (!$Container)
            throw $this->createNotFoundException('Unable to find Container entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Container);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('container'));
    }

    /**
     * Creates a form to delete a Container entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('container_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Container entity.
     */
    private function buildForm(Container $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\ContainerType', $entity, array(
                'action' => $this->generateUrl("container_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\ContainerType', $entity, array(
                'action' => $this->generateUrl("container_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Container
     */
    private function GetSecurityContainers()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Container')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Container.');
    }

    /**
     * Gets Container
     */
    private function GetSecurityContainer($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Container')->find($id);
            else
                return new Container();
        else
            throw $this->createNotFoundException('Unable to find Container.');
    }

}
