<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class CodebarController extends Controller
{

    public function generateAction($id)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $Codebar = $this->GetSecurityCodeBar($id);

            if (!$Codebar)
                throw $this->createNotFoundException('Unable to find Unit entity1');

            $gencode = $this->get('Gencode');
            $gencode->setCode($Codebar->getCodebar());
            $gencode->setType('EAN');
            $gencode->setSize(80, 150, 10);
            $gencode->setText('AUTO');
            $gencode->hideCodeType();
            $gencode->setColors('#123456', '#F9F9F9');
            $gencode->setFiletype('PNG');
            if (!file_exists(__SOURCINASIA__ . 'htdocs/barcodes/' . $Codebar->getCodebar() . '.png'))
                $gencode->writeBarcodeFile('barcodes/' . $Codebar->getCodebar() . '.png');
            $gencode->showBarcodeImage('barcode.png');
            die;
        } else
            throw $this->createNotFoundException('Unable to find Unit entity2');
    }

    /**
     * Gets City
     */
    private function GetSecurityCodeBar($id = null)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Codebar')->find($id);
            else
                throw $this->createNotFoundException('Unable to find City.');
    }

}
