<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Activity;

/**
 * Activity controller.
 *
 */
class ActivityController extends Controller
{
    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Activity entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Activity:index.html.twig', array(
            'Activitys' => $Activity = $this->GetSecurityActivitys()
        ));
    }

    /**
     * Finds and displays a Activity entity.
     */
    public function showAction($id)
    {
        $Activity = $this->GetSecurityActivity($id);

        if (!$Activity) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Activity:show.html.twig', array(
            'Activity' => $Activity,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Activity entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Activity:form.html.twig', array(
            'Activity' => $this->GetSecurityActivity(),
            'form' => $this->buildForm($this->GetSecurityActivity())->createView(),
        ));
    }

    /**
     * Creates a new Activity entity.
     */
    public function createAction(Request $request)
    {
        $Activity = $this->GetSecurityActivity();
        $form = $this->buildForm($Activity)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Activity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('activity'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Activity:form.html.twig', array(
            'Activity' => $Activity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Activity entity.
     *
     */
    public function editAction($id)
    {
        $Activity = $this->GetSecurityActivity($id);

        if (!$Activity)
            throw $this->createNotFoundException('Unable to find Activity entity.');

        return $this->render('appBundle:Activity:form.html.twig', array(
            'Activity' => $Activity,
            'form' => $this->buildForm($Activity, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Activity entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Activity = $this->GetSecurityActivity($id);

        if (!$Activity)
            throw $this->createNotFoundException('Unable to find Activity entity.');

        $form = $this->buildForm($Activity, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Activity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('activity'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Activity:form.html.twig', array(
            'Activity' => $Activity,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Activity entity.
     *
     */
    public function deleteAction($id)
    {

        $Activity = $this->GetSecurityActivity($id);

        if (!$Activity)
            throw $this->createNotFoundException('Unable to find Activity entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Activity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('activity'));
    }

    /**
     * Creates a form to delete a Activity entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('activity_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Activity entity.
     */
    private function buildForm(Activity $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\ActivityType', $entity, array(
                'action' => $this->generateUrl("activity_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\ActivityType', $entity, array(
                'action' => $this->generateUrl("activity_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Activity
     */
    private function GetSecurityActivitys()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Activity')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Activity.');
    }

    /**
     * Gets Activity
     */
    private function GetSecurityActivity($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Activity')->find($id);
            else
                return new Activity();
        else
            throw $this->createNotFoundException('Unable to find Activity.');
    }

}
