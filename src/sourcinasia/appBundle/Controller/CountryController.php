<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use sourcinasia\appBundle\Entity\Country;

/**
 * Country controller.
 *
 */
class CountryController extends Controller
{
    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Country entities.
     */
    public function indexAction()
    {
        return $this->render('appBundle:Country:index.html.twig', array(
            'Countrys' => $Country = $this->GetSecurityCountrys()
        ));
    }

    /**
     * Finds and displays a Country entity.
     */
    public function showAction($id)
    {
        $Country = $this->GetSecurityCountry($id);

        if (!$Country) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        return $this->render('appBundle:Country:show.html.twig', array(
            'Country' => $Country,
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    /**
     * Displays a form to create a new Country entity.
     */
    public function newAction()
    {
        return $this->render('appBundle:Country:form.html.twig', array(
            'Country' => $this->GetSecurityCountry(),
            'form' => $this->buildForm($this->GetSecurityCountry())->createView(),
        ));
    }

    /**
     * Creates a new Country entity.
     */
    public function createAction(Request $request)
    {
        $Country = $this->GetSecurityCountry();
        $form = $this->buildForm($Country)->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Country);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('country'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        return $this->render('appBundle:Country:form.html.twig', array(
            'Country' => $Country,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Country entity.
     *
     */
    public function editAction($id)
    {
        $Country = $this->GetSecurityCountry($id);

        if (!$Country)
            throw $this->createNotFoundException('Unable to find Country entity.');

        return $this->render('appBundle:Country:form.html.twig', array(
            'Country' => $Country,
            'form' => $this->buildForm($Country, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Country entity.
     */
    public function updateAction(Request $request, $id)
    {
        $Country = $this->GetSecurityCountry($id);

        if (!$Country)
            throw $this->createNotFoundException('Unable to find Country entity.');

        $form = $this->buildForm($Country, 'edit')->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($Country);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('country'));
        }
        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        return $this->render('appBundle:Country:form.html.twig', array(
            'Country' => $Country,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Country entity.
     *
     */
    public function deleteAction($id)
    {
        $Country = $this->GetSecurityCountry($id);

        if (!$Country)
            throw $this->createNotFoundException('Unable to find Country entity.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($Country);
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');

        return $this->redirect($this->generateUrl('country'));
    }

    /**
     * Creates a form to delete a Country entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('country_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Country entity.
     */
    private function buildForm(Country $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\CountryType', $entity, array(
                'action' => $this->generateUrl("country_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\CountryType', $entity, array(
                'action' => $this->generateUrl("country_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    /**
     * Gets all Country
     */
    private function GetSecurityCountrys()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Country')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Country.');
    }

    /**
     * Gets Country
     */
    private function GetSecurityCountry($id = null)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SOURCING'))
            if ($id)
                return $this->getDoctrine()->getManager()->getRepository('appBundle:Country')->find($id);
            else
                return new Country();
        else
            throw $this->createNotFoundException('Unable to find Country.');
    }

}
