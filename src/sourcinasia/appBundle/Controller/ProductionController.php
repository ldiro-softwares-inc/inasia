<?php

namespace sourcinasia\appBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ProductionController extends Controller
{

    public function indexAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {
            return $this->render('appBundle:Production:index.html.twig');
        }
    }

    /*
     * Retourne les suppliers filtrer
     */

    public function requestsAction(Request $request)
    {

        $request = $request->query->get('request');
        $profil = $this->get('Profils')->get();

        $customersids = array();

        if ($this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION')) {

            $request = $this->getDoctrine()
                ->getRepository('appBundle:Supplychain')
                ->search(array_filter($request));

            return $this->render('appBundle:Command:listCommand.html.twig', array(
                'Cadenciers' => $request
            ));
        }
    }

    public function listAction()
    {
        return $this->render('appBundle:Command:produce.html.twig', array(
            'Commands' => $Command = $this->GetSecurityCommands()
        ));
    }

}
