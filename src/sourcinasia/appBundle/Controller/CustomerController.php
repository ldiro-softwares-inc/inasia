<?php

namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\Chat;
use sourcinasia\appBundle\Entity\Contact;
use sourcinasia\appBundle\Entity\Customer;
use sourcinasia\appBundle\Entity\History;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Customer controller.
 *
 */
class CustomerController extends Controller
{
    /*
     * Retourne le json 
     */

    public function filtersAction(Request $request,$filter)
    {
        if (!is_array($request->query->get('request'))) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        $content = $this->getDoctrine()
            ->getRepository('appBundle:Customer')
            ->seach(array_filter($request->query->get('request')), $filter);

        if ($filter == "nomenclature") {
            $content = $this->getDoctrine()
                ->getRepository('appBundle:Categorie')
                ->getCategroies(array_filter($content), $request->getLocale());
        }

        $response = new Response();
        $response->setContent(json_encode($content));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /*
     * Retourne la liste des customer en fonction des cirt�res de recherche 
     */

    public function requestsAction(Request $request)
    {

        $request = $request->query->get('request');

        if (!is_array($request)) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }

        $request = $this->getDoctrine()
            ->getRepository('appBundle:Customer')
            ->seach(array_filter($request), false, $this->get('security.token_storage')->getToken()->getUser()->getId(), $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'));

        return $this->render('appBundle:Customer:list.html.twig', array(
            'Customers' => $request
        ));
    }

    /*
     * SYMFONY CRUD //////////////////////////////////////////////////////////////////////////////////
     */

    /**
     * Lists all Customer entities.
     */
    public function customerslistAction()
    {
        return $this->render('appBundle:Customer:index.html.twig', array('Activites' => $this->getDoctrine()->getManager()->getRepository('appBundle:Activity')->findAll(),
            'leads' => false
        ));
    }

    /**
     * Lists all Customer entities.
     */
    public function leadslistAction()
    {
        return $this->render('appBundle:Customer:index.html.twig', array('Activites' => $this->getDoctrine()->getManager()->getRepository('appBundle:Activity')->findAll(),
            'leads' => true
        ));
    }

    /**
     * Finds and displays a Customer entity.
     */
    public function showAction($id)
    {

        $Customer = $this->GetSecurityCustomer($id);

        if (!$Customer) {
            $response = $this->render('appBundle:Error:404.html.twig');
            $response->setStatusCode(404);
            return $response;
        }
        /* $chatform = $this->createForm(new ChatType(), new Chat(), array(
          'action' => $this->generateUrl("chat_create"),
          'method' => 'POST',
          'attr' => array('class' => 'fill-up'),
          )); */

        //$chatform->get('customer')->setData($Customer);

        return $this->render('appBundle:Customer:show.html.twig', array(
            'Customer' => $Customer,
            'today' => new \dateTime(),
            // 'Chat_form' => $chatform->createView(),
            'delete_form' => $this->DeleteForm($id)->createView(),));
    }

    public function quickaddAction()
    {

        $cutosmer = new Customer();
        $contact = new Contact();
        $contact->setFrequencenews(14);
        $cutosmer->addContact($contact);

        $cutosmer->setLeads(true);

        $form = $this->createForm('sourcinasia\appBundle\Form\QuickType', $cutosmer, array(
            'action' => $this->generateUrl("customer_quickcreate"),
            'method' => 'POST',
            'attr' => array('class' => 'fill-up')
        ))->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));

        $em = $this->getDoctrine()->getManager();
        $Nomenclature = $em->getRepository('appBundle:Categorie')->getJsonData();

        return $this->render('appBundle:Customer:quickform.html.twig', array(
            'Customer' => $this->GetSecurityCustomer(),
            'activites' => $this->getDoctrine()->getManager()->getRepository('appBundle:Activity')->findAll(),
            'Nomenclatures' => $Nomenclature,
            'form' => $form->createView(),
        ));

    }

    /**
     * Creates a new Customer entity.
     */
    public function quickcreateAction(Request $request)
    {
        $Customer = new Customer();
        $Customer->addContact(new Contact());

        $form = $this->createForm('sourcinasia\appBundle\Form\QuickType', $Customer, array(
            'action' => $this->generateUrl("customer_quickcreate"),
            'method' => 'POST',
            'attr' => array('class' => 'fill-up')
        ))->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));

        $form->handleRequest($request);

        if ($Customer->getNomenclatures() && count($Customer->getZones()) && $form->isValid()) {

            $Customer->setMainsaler($this->get('security.token_storage')->getToken()->getUser());
            $Customer->setDeleverytime(30);
            $Customer->setPaymentcondition($this->getDoctrine()->getManager()->getRepository('appBundle:Paymentcondition')->find(3));


            $test = false;
            foreach ($Customer->getOwners() as $owner)
                $test = $test || $owner->getId() == $Customer->getMainsaler()->getId();

            if (!$test)
                $Customer->addOwner($Customer->getMainsaler());

            $Contact = $Customer->getContacts()[0];

            $em = $this->getDoctrine()->getManager();
            $em->persist($Customer);
            $em->flush();

            $Contact->setCustomer($Customer);
            $em->persist($Contact);
            $em->flush();

            $History = new History();
            $History->SetTitle('HISTORY_NEW_CUSTOMER');
            $History->SetDescription('HISTORY_NEW_CUSTOMER_DESCRIPTION' . $Customer->getName());
            $History->SetIco('<i class="icon-plus-sig"></i>');
            $History->SetDate(new \DateTime());
            $History->SetCustomer($Customer);
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
                if ($request->get('OpenAccess')) {
                    if (!$this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneByEmail($Contact->getEmail())) {
                        $login = $Contact->getEmail();
                        $password = rand(100000, 1000000);
                        $userManager = $this->container->get('fos_user.user_manager');
                        $user = $userManager->createUser();
                        $user->setUsername($Contact->getEmail());
                        $user->setEmail($Contact->getEmail());
                        $user->setPlainPassword($password);
                        $user->setContact($Contact);
                        $user->setRoles(array('ROLE_CLIENT', 'ROLE_USER'));
                        $user->setEnabled(true);
                        $user->setLocale($Contact->getCustomer()->getLocale());
                        $user->setModeonlogin('buy');
                        $user->setToken($password * 1981);
                        $user->setName($Contact->getName());
                        $user->setCustomermargecoef('5');
                        if ($request->get('DemoAcess')) {
                            $limit = new \datetime();
                            $limit->modify("+1 week");
                            $user->setExpired(false);
                            $user->setExpiresAt($limit);
                        }
                        $userManager->updateUser($user, true);

                        $this->get('event_dispatcher')->dispatch(
                            'sendemailuseraccess',
                            new GenericEvent('sendemailuseraccess', array('ContactUser' => $user,
                                'User' => $this->get('security.token_storage')->getToken()->getUser(),
                                'Password' => $password,
                                'TITLE' => 'MAIL_TITLE_NEWACCESS',
                                'MESSAGE' => 'MAIL_MESSAGE_NEWACCESS'

                            ))
                        );

                        $this->get('session')->getFlashBag()->add('notice', 'Access sent by mail');
                    } else {
                        $this->get('session')->getFlashBag()->add('warning', 'Cannot create access because this email already use by another user');
                    }
                }
            }

            return $this->redirect($this->generateUrl('customer_show', array('id' => $Customer->getId())));
        }
        if (!$Customer->getNomenclatures())
            $this->get('session')->getFlashBag()->add('warning', 'PLEASE SELECT NOMENCLATURE');

        if (!count($Customer->getZones()))
            $this->get('session')->getFlashBag()->add('warning', 'PLEASE SELECT ZONES');

        $this->get('session')->getFlashBag()->add('warning', 'ERRORS');
        $Nomenclatures = $this->getDoctrine()->getManager()->getRepository('appBundle:Categorie')->getJsonData();
        return $this->render('appBundle:Customer:quickform.html.twig', array(
            'Customer' => $Customer,
            'activites' => $this->getDoctrine()->getManager()->getRepository('appBundle:Activity')->findAll(),
            'Nomenclatures' => $Nomenclatures,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to create a new Customer entity.
     */
    public function newAction()
    {
        $form = $this->buildForm(new Customer());
        $em = $this->getDoctrine()->getManager();
        $Nomenclature = $em->getRepository('appBundle:Categorie')->getJsonData();

        $form->get('mainsaler')->setData($this->get('security.token_storage')->getToken()->getUser());

        return $this->render('appBundle:Customer:form.html.twig', array(
            'Customer' => $this->GetSecurityCustomer(),
            'activites' => $this->getDoctrine()->getManager()->getRepository('appBundle:Activity')->findAll(),
            'Nomenclatures' => $Nomenclature,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Customer entity.
     */
    public function createAction(Request $request)
    {
        $Customer = new Customer();
        $form = $this->buildForm($Customer)->handleRequest($request);

        if ($Customer->getNomenclatures() && count($Customer->getZones()) && $form->isValid()) {

            $test = false;
            foreach ($Customer->getOwners() as $owner)
                $test = $test || $owner->getId() == $Customer->getMainsaler()->getId();

            if (!$test)
                $Customer->addOwner($Customer->getMainsaler());

            $em = $this->getDoctrine()->getManager();
            $em->persist($Customer);
            $em->flush();
            $History = new History();
            $History->SetTitle('HISTORY_NEW_CUSTOMER');
            $History->SetDescription('HISTORY_NEW_CUSTOMER_DESCRIPTION' . $Customer->getName());
            $History->SetIco('<i class="icon-plus-sig"></i>');
            $History->SetDate(new \DateTime());
            $History->SetCustomer($Customer);
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'FLASH_CREATED');
            return $this->redirect($this->generateUrl('customer_show', array('id' => $Customer->getId())));
        }

        if (!$Customer->getNomenclatures())
            $this->get('session')->getFlashBag()->add('warning', 'PLEASE SELECT NOMENCLATURE');

        if (!count($Customer->getZones()))
            $this->get('session')->getFlashBag()->add('warning', 'PLEASE SELECT ZONES');

        $this->get('session')->getFlashBag()->add('error', 'FLASH_CREATED_ERROR');
        $Nomenclatures = $this->getDoctrine()->getManager()->getRepository('appBundle:Categorie')->getJsonData();
        return $this->render('appBundle:Customer:form.html.twig', array(
            'Customer' => $Customer,
            'activites' => $this->getDoctrine()->getManager()->getRepository('appBundle:Activity')->findAll(),
            'Nomenclatures' => $Nomenclatures,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Customer entity.
     *
     */
    public function editAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $Customer = $em->getRepository('appBundle:Customer')->find($id);

        if (!$Customer) {
            throw $this->createNotFoundException('Unable to find Customer entity.');
        }

        $Nomenclature = $em->getRepository('appBundle:Categorie')->getJsonData($Customer->getNomenclatures());

        return $this->render('appBundle:Customer:form.html.twig', array(
            'Customer' => $Customer,
            'Nomenclatures' => $Nomenclature,
            'activites' => $this->getDoctrine()->getManager()->getRepository('appBundle:Activity')->findAll(),
            'form' => $this->buildForm($Customer, 'edit')->createView()
        ));
    }

    /**
     * Edits an existing Customer entity.
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $Customer = $this->GetSecurityCustomer($id);

        if (!$Customer) {
            throw $this->createNotFoundException('Unable to find Customer entity.');
        }

        $logo = $Customer->getLogo();
        $form = $this->buildForm($Customer, 'edit')->handleRequest($request);


        if ($Customer->getNomenclatures() && count($Customer->getZones()) && $form->isValid()) {
            if ($logo)
                $Customer->setLogo($logo);

            $test = false;
            foreach ($Customer->getOwners() as $owner)
                $test = $test || $owner->getId() == $Customer->getMainsaler()->getId();

            if (!$test)
                $Customer->addOwner($Customer->getMainsaler());


            $this->UpdateExclusivity($Customer);
            $em->persist($Customer);

            $History = new History();
            $History->SetTitle('HISTORY_UPDATE_CUSTOMER');
            $History->SetDescription('HISTORY_UPDATE_CUSTOMER_DESCRIPTION ' . $Customer->getName());
            $History->SetIco('<i class="icon-pencil"></i>');
            $History->SetDate(new \DateTime());
            $History->SetCustomer($this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->find($Customer->getId()));
            $History->SetUser($this->getDoctrine()->getManager()->getRepository('appBundle:User')->findOneById($this->get('security.token_storage')->getToken()->getUser()->getId()));
            $em->persist($History);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'FLASH_UPDATE');
            return $this->redirect($this->generateUrl('customer_show', array('id' => $id)));
        }

        if (!$Customer->getNomenclatures())
            $this->get('session')->getFlashBag()->add('warning', 'PLEASE SELECT NOMENCLATURE');

        if (!count($Customer->getZones()))
            $this->get('session')->getFlashBag()->add('warning', 'PLEASE SELECT ZONES');

        $this->get('session')->getFlashBag()->add('error', 'FLASH_UPDATE_ERROR');

        $Nomenclature = $em->getRepository('appBundle:Categorie')->getJsonData($Customer->getNomenclatures());

        return $this->render('appBundle:Customer:form.html.twig', array(
            'Customer' => $Customer,
            'activites' => $this->getDoctrine()->getManager()->getRepository('appBundle:Activity')->findAll(),
            'Nomenclatures' => $Nomenclature,
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a Customer entity.
     *
     */
    public function deleteAction($id)
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_DELETE')) {
            $em = $this->getDoctrine()->getManager();

            $Customer = $this->GetSecurityCustomer($id);

            if (!$Customer) {
                throw $this->createNotFoundException('Unable to find Customer entity.');
            }

            $nbr_orders = $this->getDoctrine()->getManager()->getRepository('appBundle:Command')->GetOrderCustomer($Customer->getId());

            if ($nbr_orders == 0) {

                foreach ($Customer->getContacts() as $contact) {
                    if ($contact->getUser()) {
                        $em->remove($contact->getUser());
                    }
                    $em->remove($contact);
                    $em->flush();
                }

                $em->remove($Customer);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', 'FLASH_DELETE');
            } else {
                $this->get('session')->getFlashBag()->add('warning', 'CANNOT DELETE - THIS CUSTOMER HAS ' . $nbr_orders . ' ORDERS');
            }


            return $this->redirect($this->generateUrl('customer'));
        }
    }

    /**
     * Creates a form to delete a Customer entity by id.
     */
    private function DeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('customer_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Creates a form to create/edit a Customer entity.
     */
    private function buildForm(Customer $entity, $task = "create")
    {
        if ($task == "create")
            $form = $this->createForm('sourcinasia\appBundle\Form\CustomerType', $entity, array(
                'action' => $this->generateUrl("customer_create"),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));
        else
            $form = $this->createForm('sourcinasia\appBundle\Form\CustomerType', $entity, array(
                'action' => $this->generateUrl("customer_update", array('id' => $entity->GetID())),
                'method' => 'POST',
                'attr' => array('class' => 'fill-up')
            ));

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
            $form->remove('xlsexport');

        return $form->add('submit', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => "APPLICATION_SAVE"));
    }

    private function GetSecurityCustomers()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
            return $this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->findAll();
        else
            throw $this->createNotFoundException('Unable to find Address.');
    }

    private function GetSecurityCustomer($id = null)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL'))
            if ($id) {
                if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                    $customer = $this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->find($id);
                else
                    $customer = $this->getDoctrine()->getManager()->getRepository('appBundle:Customer')->GetCommercialCustomer($id, $this->get('security.token_storage')->getToken()->getUser()->getId());

                if ($customer)
                    return $customer;
                else
                    throw $this->createNotFoundException('Unable to find Customer.');
            } else
                return new Customer();
        else
            throw $this->createNotFoundException('Unable to find Customer.');
    }

    private function UpdateExclusivity($Customer)
    {
        foreach ($Customer->getZones() as $zone) {
            $exlu = '[' . $zone->GetId() . ':' . $Customer->GetId() . ']';
            foreach ($Customer->getProducts() as $Product) {
                $save = false;
                $ExclusivitiesProduct = $Product->getExclusivity();
                if (!in_array($exlu, $ExclusivitiesProduct)) {
                    $ExclusivitiesProduct[] = $exlu;
                    $save = true;
                }
                if ($save) {
                    $Product->setExclusivity($ExclusivitiesProduct);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($Product);
                    $em->flush();
                }
            }
        }
    }

}
