<?php

namespace sourcinasia\appBundle\Controller;

use sourcinasia\appBundle\Entity\Newsletter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Newsletter controller.
 *
 */
class NewsletterController extends Controller
{

    public function consultAction(Newsletter $newsletter)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            return new Response($newsletter->getMessage());
        }
    }

    public function generateAction(Request $Request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            return $this->render(
                'appBundle:Newsletter:generate.html.twig',
                array(
                    'newsletter' => $this->generateNewsletter(),
                    'to' => implode(', ', $this->getGuessEmails())
                )
            );
        }
    }

    private function getGuessEmails(Request $request, $leads = false)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {

            $user = $this->get('security.token_storage')->getToken()->getUser();
            $session = $this->get('session');
            $nomenclatures = array();

            if (is_array($session->get('selectionids'))) {
                foreach ($session->get('selectionids') as $product) {

                    $product = $this->getDoctrine()
                        ->getRepository('appBundle:Product')->find($product);

                    if ($product) {
                        $nomenclatures[$product->getCategorie()->GetId()] = "";
                    }
                }
            }

            if ($leads) {
                $emails = $this->getDoctrine()
                    ->getRepository('appBundle:Customer')
                    ->GetCustomersEmailsFormNomenclature(array_keys($nomenclatures), $user, $this->get('profils')->get(), true);
            } else
                $emails = $this->getDoctrine()
                    ->getRepository('appBundle:Customer')
                    ->GetCustomersEmailsFormNomenclature(array_keys($nomenclatures), $user, $this->get('profils')->get());

            return $emails;
        }
    }

    public function sendAction(Request $Request)
    {

        $em = $this->getDoctrine()->getManager();
        $emails = explode(',', $Request->request->get('emails'));
        $title = $Request->request->get('title');
        $newsletter = $Request->request->get('message');
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $i = 0;
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') && count($emails) && $newsletter) {

            foreach ($emails as $email) {
                $contact = $this->getDoctrine()->getRepository('appBundle:Contact')->findOneBy(array('email' => $email));
                $message = array(
                    'TITLE' => $title,
                    'FROM' =>'info@inasia-corp.com',
                    'TO' => trim($email),
                    'MESSAGE' => $newsletter
                );
                $this->get('event_dispatcher')->dispatch('sendemailbase', new GenericEvent($message));

                if ($contact) {
                    $news = new Newsletter();
                    $news->setTitle($title);
                    $news->setMessage($newsletter);
                    $news->setDate(new \datetime());
                    $news->setContact($contact);
                    $em->persist($news);
                    $em->flush();
                }


                $i++;

            }
        }

        if ($Request->request->get('leads'))
            $i = $i + $this->sendLeads($title, $newsletter);

        if ($i) {
            $this->get('session')->getFlashBag()->add('notice', $i . ' message' . ($i > 2 ? 's' : '') . ' sent !');
        } else {
            $this->get('session')->getFlashBag()->add('warning', 'Error : 0 message sent');
        }

        return $this->redirect($this->generateUrl('catalog'));

    }


    private function sendLeads($title, $newsletter)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $i = 0;

            $emails = $this->getGuessEmails(true);
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $em = $this->getDoctrine()->getManager();
            if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL') && count($emails) && $newsletter) {

                foreach ($emails as $email) {
                    if ($i < 10) {
                        $contact = $this->getDoctrine()->getRepository('appBundle:Contact')->findOneBy(array('email' => $email));
                        $limit = new \dateTime();
                        $frequency = $contact->getFrequencenews() ? $contact->getFrequencenews() : 3;
                        $limit->modify('-' . $frequency . ' day' . ($frequency > 1 ? 's' : ''));
                        //if ($contact && !$contact->getLastemail() || $contact->getLastemail() < $limit) {
                        if ($contact) {
                            $newsletter = $this->generateNewsletter($contact, $this->get('profils')->setCustomer($contact->getCustomer(), true));
                            $contact->setToken(md5($user->getEmail() . date('dmYs')));
                            $message = array(
                                'TITLE' => $title,
                                'FROM' =>'info@inasia-corp.com',
                                'TO' => $contact->getEmail(),
                                'MESSAGE' => $newsletter
                            );
                            $contact->setLastemail(new \dateTime());
                            $em->persist($contact);
                            $em->flush();

                            $this->get('event_dispatcher')->dispatch('sendemailbase', new GenericEvent($message));

                            if ($contact) {
                                $news = new Newsletter();
                                $news->setTitle($title);
                                $news->setMessage($newsletter);
                                $news->setDate(new \datetime());
                                $news->setContact($contact);
                                $em->persist($news);
                                $em->flush();
                            }

                            $i++;
                        }
                    }
                }
            }

            return $i;

        }
    }


    private
    function generateNewsletter($contact = null, $profil = null)
    {
        if (!$profil) {
            $profil = $this->get('Profils')->get();
        }

        $locale = strtolower($profil['contact_locale']);
        if (!in_array($locale, array('fr,en'))) {
            $locale = 'en';
        }

        if ($this->get('security.authorization_checker')->isGranted('ROLE_COMMERCIAL')) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $session = $this->get('session');

            $offers = array();
            $nomenclatures = array();
            $exw = $fob = false;

            // Commercial
            $datas = array();
            $datas['{{commercialname}}'] = $user->getName();
            $datas['{{commercialemail}}'] = $user->GetEmail();

            $offers = $this->getDoctrine()
                ->getRepository('appBundle:Offer')
                ->getProductsCatalog($session->get('selectionids'), $this->get('Profils')->get());

            foreach ($offers as $offer) {
                $nomenclatures[$offer->getProduct()->getCategorie()->GetId()] = "";
                $exw = $exw || $offer->getSaleprice(strtolower('exw'), $profil) != '0.00';
                $fob = $fob || $offer->getSaleprice(strtolower('fob'), $profil) != '0.00';
            }

            $line = array();
            foreach ($offers as $offer) {
                $url = 'https://market.inasia-corp.com' . $this->generateUrl(
                        'catalog',
                        array(
                            'n' => $offer->getProduct()->getCategorie()->getId(),
                            'o' => $offer->getId()
                        )
                    );
                $vals = array();
                $vals[] = '<td style="text-align:center;padding:5px; font-family:Arial, Helvetica, sans-serif; color:#000">' . $offer->getProduct()->GetId() . '</td>';
                $vals[] = '<td style="text-align:center;padding:5px; font-family:Arial, Helvetica, sans-serif; color:#000"><a href="' . $url . '" style="color:fff;text-decoration:none;"><img src="https://market.inasia-corp.com/' . str_replace(
                        'original.jpg',
                        'small.jpg',
                        $offer->getProduct()->getMainimage()
                    ) . '" alt="" /></a></td>';
                $vals[] = '<td style="padding:5px;text-align:left;font-family:Arial, Helvetica, sans-serif; color:#000">' . (($locale == "fr") ? $offer->getProduct()->GetFrenchdescription() : $offer->getProduct()->GetDescription()) . '</td>';
                $vals[] = '<td style="text-align:center;padding:5px; font-family:Arial, Helvetica, sans-serif; color:#000">' . $offer->getMoq() . '</td>';
                if ($exw) {
                    $vals[] = '<td style="text-align:center;padding:5px; font-family:Arial, Helvetica, sans-serif; color:#000">' . (0!=$offer->getSaleprice(
                            strtolower('exw'),
                            $profil
                        ) ? $offer->getSaleprice(
                            strtolower('exw'),
                            $profil
                        ) : '-') . '</td>';
                }
                if ($fob) {
                    $vals[] = '<td style="text-align:center;padding:5px; font-family:Arial, Helvetica, sans-serif; color:#000">' . (0!=$offer->getSaleprice(
                            strtolower('fob'),
                            $profil
                        ) ? $offer->getSaleprice(
                            strtolower('fob'),
                            $profil
                        ) : '-') . '</td>';
                }


                $vals[] = '<td style="text-align:center;padding:5px;font-family:Arial, Helvetica, sans-serif; color:#000"><a href="' . $url . '" style="font-size:bold;color:red;text-decoration:none;">' . strtoupper($this->get('translator')->trans('NEWSLETTER_CONSULTER', array(), 'messages', $locale)) . '</a></td>';


                $line[] = '<tr>' . implode('', $vals) . '</tr>';
            }

            $datas['heads'] = '<tr>
        <td width="10%" style="text-transform:uppercase;text-align:center;font-family:Arial, Helvetica, sans-serif;  color:#fff;background-color: red;">No</td>
        <td width="10%" style="text-align:center;font-family:Arial, Helvetica, sans-serif;  color:#fff;background-color: red;">' . $this->get(
                    'translator'
                )->trans('ENTITY_USER_PICTURE') . '</td>
        <td width="40%" style="text-transform:uppercase;text-align:center;font-family:Arial, Helvetica, sans-serif;  color:#fff;background-color: red;">' . $this->get(
                    'translator'
                )->trans('TITLE_DESCRIPTION') . '</td>
        <td width="10%" style="text-transform:uppercase;text-align:center;font-family:Arial, Helvetica, sans-serif;  color:#fff;background-color: red;">' . $this->get(
                    'translator'
                )->trans('ENTITY_OFFER_MOQ') . '</td>' .
                ($exw ? '<td style="text-transform:uppercase;text-align:center;font-family:Arial, Helvetica, sans-serif;  color:#fff;background-color: red;" width="10%">EXW USD</td>' : '') .
                ($fob ? '<td style="text-transform:uppercase;text-align:center;font-family:Arial, Helvetica, sans-serif;  color:#fff;background-color: red;" width="10%">FOB USD</td>' : '') .
                '<td style="text-transform:uppercase;text-align:center;font-family:Arial, Helvetica, sans-serif;  color:#fff;background-color: red;">' . $this->get('translator')->trans('NEWSLETTER_CONSULTER', array(), 'messages', $locale) . '</td></tr>';

            $datas['{{rows}}'] = implode('', $line);

            /*if ($halt){
                dump($profil);
                dump($datas);
                die;
            }
                else{*/

            return $this->renderView(
                'appBundle:Newsletter:template.html.twig',
                array(
                    'locale' => $locale,
                    'contact' => $contact,
                    'head' => $datas['heads'],
                    'body' => $datas['{{rows}}'],
                    'nbrOffer' => $this->getDoctrine()->getRepository('appBundle:Offer')->getNbOffer(),
                    'NbrSupplier' => $datas['{{rows}}'],
                )
            );
            /* }*/

        }
    }
}
