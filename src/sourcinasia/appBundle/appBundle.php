<?php

namespace sourcinasia\appBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class appBundle extends Bundle {

    public function getParent() {
        return 'FOSUserBundle';
    }

}
