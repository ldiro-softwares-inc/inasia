$(document).ready(function() {

    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        scrollDistance: 300, // Distance from top/bottom before showing element (px)
        scrollFrom: 'top', // 'top' or 'bottom'
        scrollSpeed: 300, // Speed back to top (ms)
        easingType: 'linear', // Scroll to top easing (see http://easings.net/)
        animation: 'fade', // Fade, slide, none
        animationSpeed: 200, // Animation speed (ms)
        scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
        scrollTarget: false, // Set a custom target element for scrolling to. Can be element or number
        scrollText: '<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>', // Text for element, can contain HTML
        scrollTitle: false, // Set a custom <a> title if required.
        scrollImg: false, // Set true to use image
        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        zIndex: 2147483647           // Z-Index for the overlay
    });

    $('.reloadAction').click(function() {
        window.location.reload();
    });

    $('.BackAction').click(function() {
        window.history.back();
    });

    $('.ConfirmAction').click(function() {
        if ($(this).data('confirm'))
            message = $(this).data('confirm');
        else
            message = 'Vous confirmez ?';

        if (confirm(message)) {
            if ($(this).attr('href') == '#') {
                window.location = $(this).data('href');
            } else {
                window.location = $(this).attr('href');
            }
        }else
            return false;
    });

    if (window.location.hash) {
        $(function() {
            $('.nav-tabs a[href="' + window.location.hash + '"]').tab('show');

        })
    }

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 300) {
            $(".fixedbox").addClass("fixedbox-active");
            $(".staticbox").addClass("staticbox-active");

        }
        if (scroll <= 300) {
            $(".fixedbox").removeClass("fixedbox-active");
            $(".staticbox").removeClass("staticbox-active");
        }
    });

    $(".chzn-select").select2();

    //29-10-19 Olivier Seems not used
    //$('a.product-thumb').touchTouch();

    $('.showtooltip').tooltip();

    $('form.fill-up').each(function() {
        $(this).validate();
        $('.box-content button[type=submit]').hide();
        if ($('#btn-form').length)
            target = $('#btn-form');
        else
            target = $('form.fill-up');
        target.prepend('<div class="form-actions" style="text-align:right"><button type="button" class="btn btn-default BackAction" style="margin-right:5px">Cancel</button><button class="btn btn-blue SubmitAction">Save changes</button></div>');
        $('.BackAction').click(function() {
            window.history.back();
        });
    });

    $('form.message-form').each(function() {
        $(this).validate();
        text = $('.box-content button[type=submit]').html();
        $('.box-content button[type=submit]').hide();
        if ($('#btn-form').length)
            target = $('#btn-form');
        else
            target = $('form.message-form');
        target.prepend('<div class="form-actions" style="text-align:right"><button class="btn btn-blue SubmitAction">' + text + '</button></div>');
        $('.BackAction').click(function() {
            window.history.back();
        });
    });


    $('.SubmitAction').click(function() {
        $('.box-content button[type=submit]').trigger("click");
    });




    $('form.fill-up label+ul>li').each(function() {
        $(this).parent().parent().after('<div class="alert alert-error" style="margin-top:5px;"><button type="button" class="close" data-dismiss="alert"></button><strong>Attention : </strong>' + $(this).html() + '</div>');
        $(this).remove();
    });



});



