$(document).ready(function () {

    $('.selection-box').hide();

    if ($('#filters').data('uripol')) {
        $('.pol-fltr').addClass('d-block');
    }

    if ($('#filters').data('uriproduct')) {
        $('.uriproduct-fltr').addClass('d-block');
    }

    if ($('#filters').data('pol')) {
        $('#pol.request').html('<option value="' + $('#filters').data('pol') + '" selected>' + $('#filters').data('pol') + '</option>');
    }

    // $(".chzn-select3").select2();
    /*
     * Actualisation de la requete
     */
    function updatecatefilters(obj) {

        if ($('#filters').data('uriproduct') != undefined) {
            $.get($('#filters').data('uriproduct'), $(".request").serializeArray()).done(function (data) {

                //D�finition de la variable en sauvegardant la value
                departement = ($('#departement.request').val() != 0) ? $('#departement.request').val() : 0;
                shelves = ($('#shelves.request').val() != 0) ? $('#shelves.request').val() : 0;
                familly = ($('#familly.request').val() != 0) ? $('#familly.request').val() : 0;
                subfamilly = ($('#subfamilly.request').val() != 0) ? $('#subfamilly.request').val() : 0;

                //Modification de la liste en mode loading

                $('#departement.request').html('<option value="0">' + $('#filters').data('alldep') + '</option>');
                $('#shelves.request').html('<option value="0">' + $('#filters').data('allshe') + '</option>');
                $('#familly.request').html('<option value="0">' + $('#filters').data('allfam') + '</option>');
                $('#subfamilly.request').html('<option value="0">' + $('#filters').data('allsub') + '</option>');

                //Traitement du json
                $.each(data.department, function (key, value) {

                    if (value.name) {
                        $('#departement.request').append($('<option data-categorie="' + value.id + '"></option>').val(key).text(value.name));
                        $('#nav_department').html(value.name);
                        window.history.replaceState(null, null, '?n=' + value.id);

                    }

                });
                $.each(data.shelves, function (key, value) {
                    if (value.name) {

                        $('#shelves.request').append($('<option data-categorie="' + value.id + '"></option>').val(key).text(value.name));
                        $('#nav_shelves').html(value.name);
                    }
                });
                $.each(data.familly, function (key, value) {
                    if (value.name) {
                        $('#familly.request').append($('<option data-categorie="' + value.id + '"></option>').val(key).text(value.name));
                        $('#nav_famillies').html(value.name);
                    }
                });
                $.each(data.subfamilly, function (key, value) {
                    if (value.name) {

                        $('#subfamilly.request').append($('<option data-categorie="' + value.id + '"></option>').val(key).text(value.name));
                        $('#nav_subfamillies').html(value.name);
                    }
                });


                /*  $.each(data, function (key,value) {
                      if (value.name){

                          window.history.replaceState(null, null, '?n='+data.department.id+'?s='+data.shelves.id+'?f='+data.familly.id+'?u='+data.subfamilly.id);
                      }
                  });*/

                //insertion de la value
                $('#departement.request').select2("val", departement)
                $('#shelves.request').select2("val", shelves)
                $('#familly.request').select2("val", familly)
                $('#subfamilly.request').select2("val", subfamilly)


                //Si autocomplete selection de la 2ieme val
                if ($('#filters').data('autocomplete') == true) {


                    if ($('#departement.request option').length == 2) {
                        $("#departement.request").select2("val", $('#departement.request option:eq(1)').val());
                    } else {
                        $("#departement.request").select2("val", "0");
                    }
                    if ($('#shelves.request option').length == 2)
                        $("#shelves.request").select2("val", $('#shelves.request option:eq(1)').val());
                    else
                        $("#shelves.request").select2("val", "0");

                    if ($('#familly.request option').length == 2)
                        $("#familly.request").select2("val", $('#familly.request option:eq(1)').val());
                    else
                        $("#familly.request").select2("val", "0");

                    if ($('#subfamilly.request option').length == 2)
                        $("#subfamilly.request").select2("val", $('#subfamilly.request option:eq(1)').val());
                    else
                        $("#subfamilly.request").select2("val", "0");

                    countSelectedProduct();

                }
            }).fail(function () {
                //  alert("Erreur de transmistion. Merci s&eacute;l&eacute;ctionnez &agrave; nouveau votre choix.");
            });
        }

        if ($('#filters').data('uripol') != undefined) {

            $.get($('#filters').data('uripol'), $(".request").serializeArray(), function (data) {
                //Modification de la liste en mode loading
                $('#pol.request').html('<option value="0">' + $('#filters').data('allpol') + '</option>');

                //Traitement du json
                $.each(data, function (key, value) {
                    if (value.title)
                        $('#pol.request').append($('<option></option>').val(value.id).text(value.title));
                });

                // Bug avec le pol

                /*    if ($('#pol.request option').length == 2)
                        $("#pol.request").select2("val", $('#pol.request option:eq(1)').val());
                    else
                        $("#pol.request").select2("val", "0");*/
            });

            $.get($('#offers_count').val(), $(".request").serializeArray(), function (data) {
                $('#nbrproduct').html(data + ' product' + (parseInt(data) > 1 ? 's' : ''));
            });


        }
    }

    if ($('.request').length) {
        //$(".chzn-select").select2();
        if ($('#filters').data('depval')) {
            $('#departement.request').html('<option value="' + $('#filters').data('depval') + '" selected></option>');
        }
        if ($('#filters').data('sheval')) {
            $('#shelves.request').html('<option value="' + $('#filters').data('sheval') + '" selected></option>');
        }
        if ($('#filters').data('famval')) {
            $('#familly.request').html('<option value="' + $('#filters').data('famval') + '" selected></option>');
        }
        if ($('#filters').data('subval')) {
            $('#subfamilly.request').html('<option value="' + $('#filters').data('subval') + '" selected></option>');
        }

        loadingfilters();
    }


    /*
     * Modification des filtres du catalogue
     */
    $('input[type=checkbox].request').change(function () {

        if (typeof pressbtn != 'undefined') {
            clearTimeout(pressbtn);
        }
        pressbtn = setTimeout(function () {
            filterchange(this);
        }, 700);
    });

    $('select.request').on('select2:select', function (e) {
        if ($(this).val() == 0) {
            select = $(this).attr('id');
            if (select == "departement" || select == "pol") {

                $("#departement.request").select2("val", "0");
                $("#shelves.request").select2("val", "0");
                $("#familly.request").select2("val", "0");
                $("#subfamilly.request").select2("val", "0");
                $("#pol.request").select2("val", "0");

            } else if (select == "shelves") {
                $("#shelves.request").select2("val", "0");
                $("#familly.request").select2("val", "0");
                $("#subfamilly.request").select2("val", "0");
                $("#pol.request").select2("val", "0");
            } else if (select == "familly") {
                $("#familly.request").select2("val", "0");
                $("#subfamilly.request").select2("val", "0");
                $("#pol.request").select2("val", "0");
            } else if (select == "subfamilly") {
                $("#subfamilly.request").select2("val", "0");
                $("#pol.request").select2("val", "0");
            }
        }

        if (typeof pressbtn != 'undefined') {
            clearTimeout(pressbtn);
        }

        pressbtn = setTimeout(function () {
            filterchange(this);
        }, 700);
    });

    $('input.request').keyup(function () {
        if (typeof pressbtn != 'undefined') {
            clearTimeout(pressbtn);
        }
        pressbtn = setTimeout(function () {
            $('#departement.request').select2("val", "0")
            $('#shelves.request').select2("val", "0")
            $('#familly.request').select2("val", "0")
            $('#subfamilly.request').select2("val", "0")
            filterchange();
        }, 700);
    });


    function filterchange(obj) {

        resetPagination();

        $('.chzn-select filterselect').each(function () {
            if ($(obj).val() == 0)
                $(obj).html('<option value="0">-LOADING-</option>');
            $(obj).attr('disabled');
        });

        if ($(obj).attr('id') == 'departement') {
            $('#shelves.request').select2("val", 0);
            $('#familly.request').select2("val", 0);
            $('#subfamilly.request').select2("val", 0);
            $("#pol.request").select2("val", "0");
            $("input.request:not(.locked)").val('');
        }

        if ($(obj).attr('id') == 'shelves') {
            $('#familly.request').select2("val", 0);
            $('#subfamilly.request').select2("val", 0);
            $("#pol.request").select2("val", "0");
            $("input.request:not(.locked)").val('');
        }

        if ($(obj).attr('id') == 'familly') {
            $('#subfamilly.request').select2("val", 0);
            $("#pol.request").select2("val", "0");
            $("input.request:not(.locked)").val('');
        }

        if ($(obj).attr('id') == 'subfamilly') {
            $("#pol.request").select2("val", "0");
            $("input.request:not(.locked)").val('');
        }

        if ($(obj).attr('id') == 'orderby') {
            $("#pol.request").select2("val", "0");
        }

        loadingfilters(obj);
    }

    function loadingfilters(obj) {
        resetPagination();
        fitlers = $('#filters_result');
        fitlers.addClass('loading');

        if ($('#filters').attr('data-categorie-level') && $('#filters').attr('data-categorie-selected')) {

            if ($('#filters').attr('data-categorie-level') == "departments") {
                $('#departement').html('<option value="' + $('#filters').attr('data-categorie-selected') + '" selected>-SELECTED-</option>');
            }

            if ($('#filters').attr('data-categorie-level') == "shelves") {
                $('#shelves').html('<option value="' + $('#filters').attr('data-categorie-selected') + '" selected>-SELECTED-</option>');
            }

            if ($('#filters').attr('data-categorie-level') == "families") {
                $('#familly').html('<option value="' + $('#filters').attr('data-categorie-selected') + '" selected>-SELECTED-</option>');
            }

            $('#filters').attr('data-categorie-selected', '');
        }


        requestdatas = $(".request").serializeArray();
        updatecatefilters(obj);
        //updatecatefilters(obj);
        //a chasue fois que le cadenicer s'ouvre
        $.get($('#uri_request').val(), requestdatas, function (data) {
            fitlers.html(data);
            $('#loader-infinite-scroll').removeClass('active').hide();
            $('.showtooltip').tooltip();
            fitlers.removeClass('loading');


            $('.ConfirmAction').click(function () {
                if ($(this).data('confirm'))
                    message = $(this).data('confirm');
                else
                    message = 'Vous confirmez ?';

                if (confirm(message)) {
                    if ($(this).attr('href') == '#') {

                        window.location = $(this).data('href');
                    } else {
                        window.location = $(this).attr('href');
                    }
                } else
                    return false;
            });

        });
    }

    /*
     * Filter menu
     */
    $('#RazFiltersActions').click(function () {
        resetFilters(true)
        resetPagination();
        loadingfilters();
    });

    function resetFilters(soucinasiaItemNumber) {
        $("#departement.request").select2("val", "0");
        $("#shelves.request").select2("val", "0");
        $("#familly.request").select2("val", "0");
        $("#subfamilly.request").select2("val", "0");
        $("#pol.request").select2("val", "0");
        $("select.chzn-select").select2("val", "0");
        $("input.request:not([type=hidden])").val('');
        if (soucinasiaItemNumber){
            $("#soucinasiaItemNumber").val('');
        }
    }

    if ($('#filters').data('autocomplete') == true) {
        $('.chzn-select filterselect').change(function () {
            value = $(this).find('option:selected').data('categorie');
            if (value) {
                $('#sourcinasia_appbundle_product_categorie option[value=' + value + ']').attr("selected", "selected");
                $('.updatefiltervalue option[value=' + value + ']').attr("selected", "selected");
            }
        });
    }

    /*
     * Catalogue
     */

    $(document).on("click", "label.items", function (event) {
        console.log(event);
        event.preventDefault();
        checkbox = $(this).prev();
        if ($(checkbox).prop('checked')) {
            console.log('ask product remove');
            actionProduct('remove', checkbox.val());
        } else {
            console.log('ask product add');
            actionProduct('add', checkbox.val());
        }
    });

    $(document).on("click", '#addProduct', function (event) {
        console.log('ask product add');
        actionProduct('add', $(this).data('offer'));
        $('#offerProductDetails').modal('hide');
    });

    $(document).on("click", '#hideProduct', function (event) {
        btn = $(this);
        $.ajax({
            url: btn.data('action'), success: function (result) {
                productCatalog = $('.selectedzone[data-offer=' + btn.data('offer') + ']');
                if (result == 1) {
                    productCatalog.find('.articlethumbs').addClass('disabled');
                } else {
                    productCatalog.find('.articlethumbs').removeClass('disabled');
                }
                $('#offerProductDetails').modal('hide');
            }
        });
    });

    $(document).on('show.bs.modal', function (event) {
        console.log('open modal');
        $.ajax({
            url: $(event.relatedTarget).data('action'), success: function (result) {
                $('#offerProductDetails .modal-content').html(result);
            }
        });
    });

    function actionProduct(action, offerId) {
        productCatalog = $('.products-box .selectedzone[data-offer=' + offerId + ']');
        productInSelectionScreen = $('.selection-box .selectedzone[data-offer=' + offerId + ']');

        if (action == 'add') {
            if (productInSelectionScreen.length == 0) {
                productionselected = productCatalog.clone();
                productionselected.appendTo('#selections');
                console.log('product ' + offerId + ' added!')
                productionselected.find('input[type=checkbox]').prop('checked', true);
            }
            productCatalog.find('input[type=checkbox]').prop('checked', true);
        } else if (action == 'remove') {
            if (productInSelectionScreen.length >= 1) {
                productInSelectionScreen.remove();
                console.log('product ' + offerId + ' remove!')
                productCatalog.find('input[type=checkbox]').prop('checked', false);
            }
        }
        countSelectedProduct();
        saveSelection();
    }

    $('.deleteSelection').click(function () {
        if (confirm('Confirmation ?')) {
            $('#selections').html('')
            countSelectedProduct();
            saveSelection();
            return true;
        }
        return false;
    });

    function saveSelection() {
        if (typeof saveselection != 'undefined') {
            clearTimeout(saveselection);
        }

        selectionids = [];
        $('#selections input[type=checkbox]').each(function (i,) {
            selectionids.push($(this).data('product'))
        });

        console.log(selectionids);

        saveselection = setTimeout(function () {
            $.post($('#uri_savecatalog').val(), {'selection': $('#selections').html(), 'selectionids': selectionids});
        }, 700);
    };


    function countSelectedProduct() {
        $(".selectioncount").html($('#filters_result input.checkbox:checked').length)
        $(".selectioninchartcount").html($('#selectionsform input.checkbox:checked').length)
        updatenbrcadencier()
    }

    function updatenbrcadencier() {
        var e = new Array();
        $('.selection-box .selectedzone').each(function () {
            e.push($(this).data('supplier') + $(this).data('pol'));
        });
        e = $.grep(e, function (v, k) {
            return $.inArray(v, e) === k;
        });

        $('#blocCadencierCount').html('<strong>' + e.length + '</strong> cadencier' + ((e.length >= 2) ? 's' : ''));
    }

    function resetPagination() {
        $('#firstResult').val(0);
        $('.fh-breadcrumb').animate({scrollTop: 0});
    }

    // Show-Hide screens
    $('.switch-screen').click(function () {
        $('.products-box').toggle();
        $('.selection-box').toggle();
        $('.toggle-btn').toggle();
    });

    $('#scrollUp').click(function () {
        $('.fh-breadcrumb').animate({scrollTop: 0}, 'slow');
    });

    $('.hideselection-btn').click(function () {
        selection = $('.products-box input[type=checkbox]:checked');
        console.log(selection);
    });

    $('.selectionTool').click(function () {
        selection = prompt('Please enter SOUCINASIA item numbers', $('#soucinasiaItemNumber').val());
        resetFilters(false);
        $('#soucinasiaItemNumber').val(selection.replace(/\n|\r|(\n\r)/g, ','));
        filterchange();
    });

    $('.checkall').click(function () {
        if (confirm('Select {n} produts ?'.replace('{n}', $('#filters_result INPUT[type=checkbox]').not(":checked").length)))
            $('#filters_result INPUT[type=checkbox]').not(":checked").each(function () {
                if (!$(this).is(':checked')) {
                    actionProduct('add', $(this).data('offer'));
                }
            });
    });

    $('.btn-confirmation').click(function () {
        if ($(this).attr('data-title')) {
            message = $(this).attr('data-title')
        } else {
            message = "confirmation ?"
        }

        if (confirm(message)) {
            document.location.href = $(this).attr('data-url')
            return true;
        }

        return false;
    });


    // if ($('#selectiondemasquerade')) {
    //     $('#selectiondemasquerade').attr('data-action', function (index, currentvalue) {
    //         return currentvalue.substring(0, currentvalue.indexOf("hideselection") + 13) + '/' + newselection.toString() + '/unhide';
    //     });
    //     $('#selectiondemasquerade').attr('data-products', newselection.toString());
    //     $('.hideselection-btn').off().on('click', function () {
    //         var btn = $(this);
    //         $.get(btn.attr('data-action'), function (result) {
    //             if (result == 1) { // 1 = success
    //                 var productids = btn.attr('data-products').split(',');
    //                 if (btn.data('action').endsWith("/1")) {// = flip all
    //                     productids.forEach(function (product) {
    //                         if ($('.articlethumbs[data-product=' + product + ']').css('opacity') == 1) {
    //                             $('.articlethumbs[data-product=' + product + ']').css('opacity', '0.4');
    //                         } else {
    //                             $('.articlethumbs[data-product=' + product + ']').css('opacity', '1.0');
    //                         }
    //                     });
    //                 } else { // unhide all
    //                     productids.forEach(function (product) {
    //                         if ($('.articlethumbs[data-product=' + product + ']').css('opacity') == 0.4) {
    //                             $('.articlethumbs[data-product=' + product + ']').css('opacity', '1.0');
    //                         }
    //                     });
    //                 }
    //             }
    //         })
    //     });
    // }
});

$(document).ready(function () {
    $(".fh-breadcrumb").on("scroll", function () {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight - 300) {
            if (!$('#noMore').length && !$('#loader-infinite-scroll').hasClass('active')) {
                $('#loader-infinite-scroll').addClass('active').show();

                console.log('infinite scroll next')
                if (typeof delay != 'undefined') {
                    clearTimeout(delay);
                }

                delay = setTimeout(function () {
                    if ($('#firstResult').val()==0){
                        console.log('first page')
                        $('#firstResult').val($('#pagination').val());
                    }
                    $.get($('#uri_request').val(), $(".request").serializeArray(), function (data) {
                        fitlers.append(data);
                        $('#loader-infinite-scroll').removeClass('active').hide();
                        $("#firstResult").val(parseInt($('#pagination').val()) + parseInt($('#firstResult').val()));
                    });
                }, 300);
            }
        }
    });

    $('#selections .articlethumbs input[type=checkbox]').prop('checked', true);
});
