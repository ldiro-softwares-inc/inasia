$(document).ready(function() {
    $('#generatebtn').click(function() {
        if ($('#selections .articlethumbs').length == 0)
            alert('Please select at least one article');
        else
        if (confirm('Do you want to generate ' + $('#blocCadencierCount strong').html() + ' cadencier' + (($('#blocCadencierCount strong').html() >= 2) ? 's' : '') + ' ?')) {
            $('#selectionsform').submit();
        }
    });
    $('.razbtn').click(function() {
        if (confirm('Confirm ?')) {
            $('#selections .articlethumbs').trigger('click');
            $('.nbrcadencier').html('0 cadencier');
            if ($('#selectionmasquerade')) {
                $('#selectionmasquerade').attr('data-action', function (index, currentvalue) {
                    return currentvalue.substring(0, currentvalue.indexOf("hideselection") + 13) + '/none';
                });
                $('#selectionmasquerade').attr('data-products', 'none');
            }
        }
    });
});
