$(document).ready(function () {
    if ($('#filters').data('uripol'))
        $('#filters').parent().after('<li style="width:13%" class="tool"><select name="request[pol]" id="pol" class="chzn-select request" ></select></li>');
    if ($('#filters').data('uriproduct'))
        $('#filters').parent().after('<li style="width:19%" class="tool"><select data-placeholder="test" value="" id="departement" name="request[departementid]" class="request chzn-select filterselect"></select></li><li style="width:19%" class="tool"><select data-placeholder="test" value="" id="shelves" name="request[shelvesid]" class="request chzn-select filterselect"></select></li><li class="tool" style="width:19%"><select value=""  name="request[famillyid]" id="familly" class="request chzn-select filterselect"></select></li><li class="tool" style="width:19%"><select value="" name="request[subfamillyid]" id="subfamilly" class="request chzn-select filterselect"></select></li>');
    $('#filters').parent().after('<li class="tool"><span style="margin-left:5px;" id="RazFiltersActions" class="btn btn-default"><i class="icon-refresh"></i></a></li>');
    $("li.tool SELECT").select2({
        placeholder: "Select a State"
    });
    if ($('#filters').data('pol')) {
        $('#pol.request').html('<option value="' + $('#filters').data('pol') + '" selected>' + $('#filters').data('pol') + '</option>');
    }

    // $(".chzn-select3").select2();
    /*
     * Actualisation de la requete
     */
    function updatecatefilters(obj) {
        if ($('#filters').data('uriproduct') != undefined) {
            $.get($('#filters').data('uriproduct'), $(".request").serializeArray()).done(function (data) {
                //D�finition de la variable en sauvegardant la value
                departement = ($('#departement.request').val() != 0) ? $('#departement.request').val() : 0;
                shelves = ($('#shelves.request').val() != 0) ? $('#shelves.request').val() : 0;
                familly = ($('#familly.request').val() != 0) ? $('#familly.request').val() : 0;
                subfamilly = ($('#subfamilly.request').val() != 0) ? $('#subfamilly.request').val() : 0;

                //Modification de la liste en mode loading
                $('#departement.request').html('<option value="0">' + $('#filters').data('alldep') + '</option>');
                $('#shelves.request').html('<option value="0">' + $('#filters').data('allshe') + '</option>');
                $('#familly.request').html('<option value="0">' + $('#filters').data('allfam') + '</option>');
                $('#subfamilly.request').html('<option value="0">' + $('#filters').data('allsub') + '</option>');

                //Traitement du json
                $.each(data.department, function (key, value) {
                    if (value.name)
                        $('#departement.request').append($('<option data-categorie="' + value.id + '"></option>').val(key).text(value.name));
                });
                $.each(data.shelves, function (key, value) {
                    if (value.name)
                        $('#shelves.request').append($('<option data-categorie="' + value.id + '"></option>').val(key).text(value.name));
                });
                $.each(data.familly, function (key, value) {
                    if (value.name)
                        $('#familly.request').append($('<option data-categorie="' + value.id + '"></option>').val(key).text(value.name));
                });
                $.each(data.subfamilly, function (key, value) {
                    if (value.name)
                        $('#subfamilly.request').append($('<option data-categorie="' + value.id + '"></option>').val(key).text(value.name));
                });

                //insertion de la value
                $('#departement.request').select2("val", departement)
                $('#shelves.request').select2("val", shelves)
                $('#familly.request').select2("val", familly)
                $('#subfamilly.request').select2("val", subfamilly)

                //Si autocomplete selection de la 2ieme val
                if ($('#filters').data('autocomplete') == true) {
                    // if ($('#departement.request option').length == 2)
                    //     $("#departement.request").select2("val", $('#departement.request option:eq(1)').val());
                    // else
                    //     $("#departement.request").select2("val", "0");

                    if ($('#shelves.request option').length == 2)
                        $("#shelves.request").select2("val", $('#shelves.request option:eq(1)').val());
                    else
                        $("#shelves.request").select2("val", "0");

                    if ($('#familly.request option').length == 2)
                        $("#familly.request").select2("val", $('#familly.request option:eq(1)').val());
                    else
                        $("#familly.request").select2("val", "0");

                    if ($('#subfamilly.request option').length == 2)
                        $("#subfamilly.request").select2("val", $('#subfamilly.request option:eq(1)').val());
                    else
                        $("#subfamilly.request").select2("val", "0");
                }
            }).fail(function () {
                //  alert("Erreur de transmistion. Merci s&eacute;l&eacute;ctionnez &agrave; nouveau votre choix.");
            });
        }

        if ($('#filters').data('uripol') != undefined) {

            $.get($('#filters').data('uripol'), $(".request").serializeArray(), function (data) {
                //Modification de la liste en mode loading
                $('#pol.request').html('<option value="0">' + $('#filters').data('allpol') + '</option>');

                //Traitement du json
                $.each(data, function (key, value) {
                    if (value.title)
                        $('#pol.request').append($('<option></option>').val(value.id).text(value.title));
                });

                // Bug avec le pol

                /*    if ($('#pol.request option').length == 2)
                        $("#pol.request").select2("val", $('#pol.request option:eq(1)').val());
                    else
                        $("#pol.request").select2("val", "0");*/
            });

            $.get($('#offers_count').val(), $(".request").serializeArray(), function (data) {
                $('#nbrproduct').html(data + ' product' + (parseInt(data) > 1 ? 's' : ''));
            });


        }
    }

    if ($('.request').length) {
        //$(".chzn-select").select2();
        if ($('#filters').data('depval')) {
            $('#departement.request').html('<option value="' + $('#filters').data('depval') + '" selected></option>');
        }
        if ($('#filters').data('sheval')) {
            $('#shelves.request').html('<option value="' + $('#filters').data('sheval') + '" selected></option>');
        }
        if ($('#filters').data('famval')) {
            $('#familly.request').html('<option value="' + $('#filters').data('famval') + '" selected></option>');
        }
        if ($('#filters').data('subval')) {
            $('#subfamilly.request').html('<option value="' + $('#filters').data('subval') + '" selected></option>');
        }

        loadingfilters();
    }


    /*
     * Modification des filtres du catalogue
     */
    $('input[type=checkbox].request').change(function () {
        if (typeof pressbtn != 'undefined') {
            clearTimeout(pressbtn);
        }
        pressbtn = setTimeout(function () {
            filterchange(this);
        }, 700);
    });

    $('#updateCatalog').click(function () {
        filterchange();
    });

    $('#updateOfferSupplier').click(function () {
        filterchange();
    });

    $('select.request').change(function () {
        if ($(this).val() == 0) {
            select = $(this).attr('id');
            if (select == "departement") {
                $("#departement.request").select2("val", "0");
                $("#shelves.request").select2("val", "0");
                $("#familly.request").select2("val", "0");
                $("#subfamilly.request").select2("val", "0");
                $("#pol.request").select2("val", "0");
            } else if (select == "shelves") {
                $("#shelves.request").select2("val", "0");
                $("#familly.request").select2("val", "0");
                $("#subfamilly.request").select2("val", "0");
                $("#pol.request").select2("val", "0");
            } else if (select == "familly") {
                $("#familly.request").select2("val", "0");
                $("#subfamilly.request").select2("val", "0");
                $("#pol.request").select2("val", "0");
            } else if (select == "subfamilly") {
                $("#subfamilly.request").select2("val", "0");
                $("#pol.request").select2("val", "0");
            } else if (select == "pol") {
                $("#departement.request").select2("val", "0");
                $("#shelves.request").select2("val", "0");
                $("#familly.request").select2("val", "0");
                $("#subfamilly.request").select2("val", "0");
                $("#pol.request").select2("val", "0");
            }
        }

        if (typeof pressbtn != 'undefined') {
            clearTimeout(pressbtn);
        }
        pressbtn = setTimeout(function () {
            filterchange(this);
        }, 700);
    });

    $('input.request').keyup(function () {
        if (typeof pressbtn != 'undefined') {
            clearTimeout(pressbtn);
        }
        pressbtn = setTimeout(function () {
            $('#departement.request').select2("val", "0")
            $('#shelves.request').select2("val", "0")
            $('#familly.request').select2("val", "0")
            $('#subfamilly.request').select2("val", "0")
            filterchange();
        }, 700);
    });


    function filterchange(obj) {
        $('.chzn-select filterselect').each(function () {
            if ($(obj).val() == 0)
                $(obj).html('<option value="0">-LOADING-</option>');
            $(obj).attr('disabled');
        });

        if ($(obj).attr('id') == 'departement') {
            $('#shelves.request').select2("val", 0);
            $('#familly.request').select2("val", 0);
            $('#subfamilly.request').select2("val", 0);
            $("#pol.request").select2("val", "0");
            $("input.request:not(.locked)").val('');
        }

        if ($(obj).attr('id') == 'shelves') {
            $('#familly.request').select2("val", 0);
            $('#subfamilly.request').select2("val", 0);
            $("#pol.request").select2("val", "0");
            $("input.request:not(.locked)").val('');
        }

        if ($(obj).attr('id') == 'familly') {
            $('#subfamilly.request').select2("val", 0);
            $("#pol.request").select2("val", "0");
            $("input.request:not(.locked)").val('');
        }

        if ($(obj).attr('id') == 'subfamilly') {
            $("#pol.request").select2("val", "0");
            $("input.request:not(.locked)").val('');
        }

        if ($(obj).attr('id') == 'orderby') {
            $("#pol.request").select2("val", "0");
        }

        loadingfilters(obj);
    }

    function loadingfilters(obj) {
        fitlers = $('#filters_result');
        fitlers.addClass('loading');
        fitlers.html('<div class = "row"><div class="col-md-12" style="text-align:center;"><img src="/bundles/app/images/loading2.gif"></div></div>');

        if ($('#filters').data('nselected')) {
            $('#subfamilly').html('<option value="' + $('#filters').data('nselected') + '" selected>-SELECTED-</option>');
            $('#filters').data('nselected', '')
        }


        requestdatas = $(".request").serializeArray();
        updatecatefilters(obj);
        //updatecatefilters(obj);
        //a chasue fois que le cadenicer s'ouvre
        $.get($('#uri_request').val(), requestdatas, function (data) {
            fitlers.html(data);
            $('.showtooltip').tooltip();
            fitlers.removeClass('loading');
            if (fitlers.data('effect') == 'catalog')
                catalog(fitlers.data('loaditem'));

            if (fitlers.data('effect') == 'cadencier' || fitlers.data('effect') == 'cadencierbuild') {
                catalog(fitlers.data('loaditem'));
                maj();
            }
            if (fitlers.data('effect') == 'supplier') {
                ;
            }

            $('.ConfirmAction').click(function () {
                if ($(this).data('confirm'))
                    message = $(this).data('confirm');
                else
                    message = 'Vous confirmez ?';

                if (confirm(message)) {
                    if ($(this).attr('href') == '#') {
                        window.location = $(this).data('href');
                    } else {
                        window.location = $(this).attr('href');
                    }
                }
                else
                    return false;
            });

        });
    }


    /*
     * Filter menu
     */
    $('#RazFiltersActions').click(function () {
        $("#departement.request").select2("val", "0");
        $("#shelves.request").select2("val", "0");
        $("#familly.request").select2("val", "0");
        $("#subfamilly.request").select2("val", "0");
        $("#pol.request").select2("val", "0");
        $("select.chzn-select").select2("val", "0");
        $("input.request").val('');

        loadingfilters();
    });

    if ($('#filters').data('autocomplete') == true) {
        $('.chzn-select filterselect').change(function () {
            value = $(this).find('option:selected').data('categorie');
            if (value) {
                $('#sourcinasia_appbundle_product_categorie option[value=' + value + ']').attr("selected", "selected");
                $('.updatefiltervalue option[value=' + value + ']').attr("selected", "selected");
            }
        });
    }

    $('#advancedsearchbtn').click(function () {
        $('#advancedsearch').toggle('slow')
    });


    /*
     * Catalogue
     */
    function catalog(loaditem) {
        displaySelectionFilter();

        if ($('#filters_result').data('loaditem')) {
            $('.articlethumbs').hover(function () {
                articlethumbs = $(this);
                timeout = setTimeout(function () {
                    loadingproduct(articlethumbs);
                }, 1000);
            }, function () {
                clearTimeout(timeout);
            });
        }

        $('.articlethumbs.selectedzone').click(function () {
            productclic($(this));

        });

        $('.localselectedzone2').click(function () {
            if ($(this).closest('table').attr('id') == "cadencierselection") {
                productclic($(this).closest('.articlethumbs'));
            } else {
                productclic($(this).closest('.articlethumbs'));
            }
        });
    }

    function productclic(product) {

        if ($(product).hasClass('selected'))
            removeProduct($(product))
        else
            addProduct($(product));

        //enregistrement de la selection
        if ($('#filters_result').data('loaditem')) {
            if (typeof saveselection != 'undefined') {
                clearTimeout(saveselection);
            }
            saveselection = setTimeout(function () {
                var selection = [];
                $('#selections .articlethumbs').each(function () {
                    selection.push($(this).data('product'));
                });
                $.post($('#uri_savecatalog').val(), {'selection': $('#selections').html(), 'selectionids': selection});
                updatemasqueradebutton(selection);
            }, 2000);
        } else if ($('#filters_result').data('effect') == 'cadencierbuild') {
            majcadencier();
        } else if ($('#filters_result').data('effect') == 'cadencier') {
            majsuppliercadencier();
        }
    }

    function updatemasqueradebutton(selection) {
        if ($('#selectionmasquerade')) {
            $('#selectionmasquerade').attr('data-action', function (index, currentvalue) {
                return currentvalue.substring(0, currentvalue.indexOf("hideselection") + 13) + '/' + selection.toString() + '/1';
            });
            $('#selectionmasquerade').attr('data-products', selection.toString());
            $('.hideselection-btn').off().on('click', function () {
                var btn = $(this);
                $.get(btn.attr('data-action'), function (result) {
                    if (result == 1) { // 1 = success
                        var productids = btn.attr('data-products').split(',');
                        productids.forEach(function(product) {
                            if ($('.articlethumbs[data-product=' + product + ']').css('opacity') == 1) {
                                $('.articlethumbs[data-product=' + product + ']').css('opacity', '0.4');
                            } else {
                                $('.articlethumbs[data-product=' + product + ']').css('opacity', '1.0');
                            }
                        });
                    }
                })
            });
        }
        if ($('#selectiondemasquerade')) {
            $('#selectiondemasquerade').attr('data-action', function (index, currentvalue) {
                return currentvalue.substring(0, currentvalue.indexOf("hideselection") + 13) + '/' + selection.toString() + '/unhide';
            });
            $('#selectiondemasquerade').attr('data-products', selection.toString());
            $('.hideselection-btn').off().on('click', function () {
                var btn = $(this);
                $.get(btn.attr('data-action'), function (result) {
                    if (result == 1) { // 1 = success
                        var productids = btn.attr('data-products').split(',');
                        if (btn.context.dataset.action.endsWith("/1")) {// = flip all
                            productids.forEach(function(product) {
                                if ($('.articlethumbs[data-product=' + product + ']').css('opacity') == 1) {
                                    $('.articlethumbs[data-product=' + product + ']').css('opacity', '0.4');
                                } else {
                                    $('.articlethumbs[data-product=' + product + ']').css('opacity', '1.0');
                                }
                            });
                        } else { // unhide all
                            productids.forEach(function (product) {
                                if ($('.articlethumbs[data-product=' + product + ']').css('opacity') == 0.4) {
                                    $('.articlethumbs[data-product=' + product + ']').css('opacity', '1.0');
                                }
                            });
                        }
                    }
                })
            });
        }
    }

    function updatenbrcadencier() {
        var e = new Array();
        $('#selections .articlethumbs').each(function () {
            e.push($(this).data('supplier') + $(this).data('pol'));
        });
        e = $.grep(e, function (v, k) {
            return $.inArray(v, e) === k;
        });

        $('.nbrcadencier').html(e.length + ' cadencier' + ((e.length >= 2) ? 's' : ''));
    }

    function displaySelectionFilter() {
        $('#selections .articlethumbs.selected').each(function () {
            addProduct($('#selections .articlethumbs[data-product=' + $(this).data('product') + ']'));
        });
    }

    function addProduct(product) {
        productsfilter = $('#filters_result .articlethumbs[data-product=' + $(product).data('product') + ']');
        productsselection = $('#selections .articlethumbs[data-product=' + $(product).data('product') + ']');

        if (!$(productsfilter).hasClass('selected')) {
            productsfilter.addClass('selected');
            productsfilter.find('input[type=checkbox]').prop('checked', true);
        }

        if (productsselection.length != 1) {
            productsselection.remove();

            selection = productsfilter.clone();
            selection.appendTo('#selections');

            $('#selections').find('div[data-product=' + productsfilter.data('product') + ']').bind('click', function () {
                removeProduct($(this));
            });

            $('#cadencierselection').find('.articlethumbs[data-product=' + productsfilter.data('product') + '] .localselectedzone2').bind('click', function () {
                removeProduct($(this).closest('.articlethumbs'));
            });

            /*
             selection.find('.localselectedzone2').unbind('click').bind('click', function() {
             removeProduct(selection);
             });

             if (selection.hasClass('selectedzone')) {
             selection.unbind('click').bind('click', function() {
             removeProduct(selection);
             });
             }*/


            if ($('#filters_result').data('loaditem')) {
                $('#selections').find('div[data-product=' + productsfilter.data('product') + ']').hover(function () {
                    articlethumbs = $(this);
                    timeout = setTimeout(function () {
                        loadingproduct(articlethumbs);
                    }, 500);
                }, function () {
                    clearTimeout(timeout);
                });
            }
        }
        updatenbrcadencier();
    }

    /* function removeProduct(product) {
     productsselection = $('#selections .articlethumbs[data-product=' + $(product).data('product') + ']');
     productsfilter = $('#filters_result .articlethumbs[data-product=' + $(product).data('product') + ']');
     productsfilter.removeClass('selected');
     productsfilter.find('input[type=checkbox]').prop('checked', false);
     productsselection.remove();
     updatenbrcadencier();
     }*/

    function removeProduct(product) {

        if ($('#iscandencier').length) {
            removeAction = (confirm('Confirmation ?'));
        } else {
            removeAction = true;
        }

        if (removeAction) {
            productsselection = $('#selections .articlethumbs[data-product=' + $(product).data('product') + ']');
            productsfilter = $('#filters_result .articlethumbs[data-product=' + $(product).data('product') + ']');
            productsfilter.removeClass('selected');
            productsfilter.find('.select').remove();
            productsfilter.find('input[type=checkbox]').prop('checked', false);
            productsselection.remove();
            updatenbrcadencier();
        } else {
            product.find('input[type=checkbox]').each(function (k, i) {
                $(i).prop('checked', true);
            });
        }

    }


    /*
     * Affichage de la fiche produit
     */
    function loadingproduct(selection) {
        produit = $('#product');
        if ($('#productbox.col-md-4').css('display') == 'none') {
            $('#products.col-md-12').removeClass('col-md-12').addClass('col-md-8');
            $('#productbox.col-md-4').toggle();
        }
        produit.addClass('loading');
        produit.html('<img src="/bundles/app/images/loading2.gif" alt="">');
        $.get($('#product_request').val(), {'id': selection.data('product')}, function (data) {
            produit.html(data);
            $('.overflowtab').css('height', $(window).height() * 0.8);
            produit.removeClass('loading');
            $('#galleryOverlay').remove();
            $('#data-gallery').remove();
            $('a.product-thumb').touchTouch();
            selection.clone().appendTo("#product #offeropened");
            $('#selectproductbtn').click(function () {
                addProduct($('#product #offeropened .articlethumbs'));
            });

        });
    }

    $('.selectionbtn').click(function () {
        $('#products').toggle();
        $('#selectionbox').toggle();
    });
});
