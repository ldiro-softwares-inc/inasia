//Fonctions

function incoterchange() {
    incoterm = $("[name^='form[incoterms]']:checked").val();
    if (incoterm == 1) {
        $('.fob').hide();
        $('.exw').show();
    } else if (incoterm == 2) {
        $('.fob').show();
        $('.exw').hide();
    }
}
function containerchange() {
    incoterm = $("[name^='form[containers]']:checked").val();
    if (incoterm == 1) {
        $('.progress-bar').hide();
    } else {
        $('.progress-bar').show();
    }
}

function update() {
    if (typeof pressbtn != 'undefined') {
        clearTimeout(pressbtn);
    }
    pressbtn = setTimeout(function () {
        updatefx()
    }, 1000);
}

function updatefx() {
    $('.dataupdate').html('<img src="/bundles/app/images/loadingbar.gif">');
    $('#marge').html('<img src="/bundles/app/images/loadingbar.gif">');
    $('#txmarge').html('<img src="/bundles/app/images/loadingbar.gif">');

    $.post($('#url').data('gencad'), $("#cadencierselection INPUT.order, .order INPUT, textarea.order, #form_customerpaymenterms, #form_supplierpaymenterms").serialize()).done(function (data) {
        if (data.Containeroccupation > 100) {
            changeColor('red');
        } else if (data.Containeroccupation > 80)
            changeColor('green');
        else
            changeColor('blue');
        $('#progress-bar').css('width', data.Containeroccupation > 100 ? 100 + '%' : data.Containeroccupation + '%');
        $('#progress-bar').data('Containeroccupation', data.Containeroccupation);
        $('#progress-bar').data('Containervolmue', data.Container.volume);
        $('#progress-bar').data('free', Math.ceil(data.Container.volume - data.Cbm));
        $('#progress-bar').html(Math.ceil(data.Containeroccupation) + '%  | ' + Math.ceil(data.Cbm) + 'M3 / ' + data.Container.volume + 'M3  | ' + data.Container.name);
        $('#Cbm').html(data.Cbm);
        $('#Totalvalue').html(data.Totalvalue);
        if ($('#Totalvaluepi').length)
            $('#Totalvaluepi').html(data.Totalvaluepi);
        $('#Quantpieces').html(data.Quantpieces);
        $('#Quantpackages').html(data.Quantpackages);
        $('#Gw').html(data.Gw);
        $('#Nw').html(data.Nw);

        var total = 0;
        $('.marge').each(function () {
            total += parseFloat($(this).data("marge"));
        });
        $('#marge').html(total.toFixed(2));
        $('#txmarge').html(((total / parseFloat($("#Totalvalue").html())) * 100).toFixed(2));

    }).fail(function () {
        console.log('Erreur de connexion serveur')
    }).done(function (data) {
        if ($('#confirmationtest').val() && data.validation) {
            document.location.href = $('#confirmationurl').val();
        }
    });
}

function changeColor(color) {
    if ($('#progress-bar').hasClass('progress-green'))
        $('#progress-bar').removeClass('progress-green');
    if ($('#progress-bar').hasClass('progress-orange'))
        $('#progress-bar').removeClass('progress-orange');
    if ($('#progress-bar').hasClass('progress-blue'))
        $('#progress-bar').removeClass('progress-blue');
    if ($('#progress-bar').hasClass('progress-red'))
        $('#progress-bar').removeClass('progress-red');
    $('#progress-bar').addClass('progress-' + color);
}


function fullcontainer(btn) {
    tr = btn.closest('tr');
    input = tr.find('input.qty');
    free = $('#progress-bar').data('free') * 0.95;
    cbmtotal = $('#Cbm').html();
    containervolume = $('#progress-bar').data('Containervolmue');
    cbm = input.data('cbm');
    pcb = input.data('pcb');

    console.log(input);
    console.log(cbmtotal);
    console.log(containervolume);
    console.log(cbm);
    console.log(pcb);

    if (free > 0) {
        qtyforfull = free / cbm * pcb;
        qtyforfull = Math.floor(qtyforfull / pcb) * pcb;
        newval = parseInt(parseInt(input.val()) + Math.ceil(qtyforfull));
    } else {
        qtyforfull = ((cbmtotal - containervolume) / cbm) * pcb;
        qtyforfull = Math.floor(qtyforfull / pcb) * pcb;
        newval = parseInt(parseInt(input.val()) - Math.ceil(qtyforfull));
    }

    console.log(newval);
    if (newval != "NaN") {
        input.val(newval);
        checksaveqtyvalue(input);
        update();
    }
}

function checksaveqtyvalue(input) {
    if (input.hasClass('free')) {
        update();
    } else if ($('#unlockPck').is(':checked')) {
        update();
    } else {
        tr = input.closest('tr');
        moq = input.data('moq');
        qty = Math.ceil(input.val() / input.data('pcb')) * input.data('pcb');
        if (qty < 0) {
            input.val(0);
        } else if (qty < moq) {
            if (confirm($('#transmoq').data('moqmessage'))) {
                if (qty != "NaN") {
                    input.val(qty);
                }
            } else {
                if (moq != "NaN") {
                    input.val(moq);
                }
            }
        } else {
            if (qty != "NaN") {
                input.val(qty);
            }
        }
        update();
    }
}

//Dans le cas des maj
var maj = function () {

//les colonnes inctoerm
    var display = function (input, data) {
        input.val(data.result);
        if (data.type == "free") {
            input.removeClass('forced');
        } else {
            input.addClass('forced');
        }
    }

    $('.actionclic').unbind('click').click(function () {
        action = $(this).data('action');
        incoterm = $(this).closest('.articlethumbs').data('incoterm');
        idoffer = $(this).closest('.articlethumbs').data('offer');
        offer = $('.o' + idoffer);
        idcustomer = $('#customer').val();
        if (idoffer && idcustomer) {

            if (action == "published" && $('#url').data('published') !== undefined) {
                eyes = $('.o' + idoffer + ' .publishedAction');
                $.post($('#url').data('published'),
                    {
                        'idoffer': idoffer,
                        'idcustomer': idcustomer,
                    }
                ).done(function (data) {
                        update();
                        if (data == 1) {
                            offer.removeClass('unpublished');
                            eyes.removeClass('icon-eye-close');
                            eyes.addClass('icon-eye-open');
                        }
                        else {
                            offer.addClass('unpublished');
                            eyes.addClass('icon-eye-close');
                            eyes.removeClass('icon-eye-open');
                        }
                    });
            }


            if ((action == "valid" || action == "reject") && $('#url').data('etat') !== undefined) {
                var confirmfield = $('.o' + idoffer + ' INPUT.confirm');
                var confirmfieldmessage = $('.o' + idoffer + ' TEXTAREA.confirmfieldmessage');
                var confirmbtn = $('.o' + idoffer + ' .confirm-btn');
                var unvalidbtn = $('.o' + idoffer + ' .unvalid-btn');
                var archive = $('.o' + idoffer + ' .archive').is(':checked');
                var prixnegociation = $('.o' + idoffer + ' .prixnegociation').data('prixachatnegoarchive');
                var prixachat = $('.o' + idoffer + ' .prixachat');
                var cadencier = $('.o' + idoffer + ' .prixnegociation').data('cadencier');

                if (confirm(action + ' ?')) {
                    if (action == "valid") {
                        confirmfield.val('1');
                        if (archive) {
                            $.post($('#url').data('etat'),
                                {
                                    'idoffer': idoffer,
                                    'etat': 'archive',
                                    'incoterm': incoterm,
                                    'cadencier': cadencier,
                                    'prixnegociation': prixnegociation,
                                }, function (data) {
                                    prixachat.html(prixnegociation);
                                });
                        } else {
                            $.post($('#url').data('etat'),
                                {
                                    'idoffer': idoffer,
                                    'etat': 'clear',
                                    'incoterm': incoterm,
                                    'cadencier': cadencier,
                                    'prixnegociation': prixnegociation,
                                }, function (data) {
                                    //prixachat.html(prixnegociation);
                                });
                            unvalidbtn.show();
                        }
                        offer.addClass("selected");
                        confirmfieldmessage.hide();
                        confirmbtn.hide();
                        $('.o' + idoffer + ' .archive').hide();
                        $('.o' + idoffer + ' .archive-label').hide();
                    } else {
                        confirmfield.val('0');
                    }
                    update();
                }
            }

            if ((action == "unvalid")) {
                var confirmbtn = $('.o' + idoffer + ' .confirm-btn');
                var confirmfieldmessage = $('.o' + idoffer + ' TEXTAREA.confirmfieldmessage');
                var unvalidbtn = $('.o' + idoffer + ' .unvalid-btn');
                var confirmfield = $('.o' + idoffer + ' INPUT.confirm');
                if (confirm(action + ' ?') && offer.hasClass("selected")) {
                    offer.removeClass("selected");
                    confirmfieldmessage.show();
                    confirmbtn.show();
                    unvalidbtn.hide();
                    confirmfield.val('2');
                    update();
                }
            }
        }
    });

    $('.action').unbind('change').change(function () {
        field = $(this);
        action = $(this).data('action');
        idoffer = $(this).closest('.articlethumbs').data('offer');
        offer = $('.o' + idoffer);
        idcustomer = $('#customer').val();

        if (idoffer && idcustomer) {
            if ((action == "fob" || action == "exw") && $('#url').data('price') !== undefined) {
                field.closest('.articlethumbs').find('.prixnegodif').html('<img src="/bundles/app/images/loadingbar.gif">');
                field.closest('.articlethumbs').find('.prixnegovalue').html('<img src="/bundles/app/images/loadingbar.gif">');
                field.closest('.articlethumbs').find('.marge').html('<img src="/bundles/app/images/loadingbar.gif">');

                $.post($('#url').data('price'),
                    {
                        'idoffer': idoffer,
                        'idcustomer': idcustomer,
                        'cadencier': $(this).data('cadencier'),
                        'incoterm': action,
                        'value': $(this).val()
                    }, function (data) {
                        display(field, data);
                        update();
                        prixvente = field.closest('.articlethumbs').data('prixvente');

                        prixachatnego = field.closest('.articlethumbs').data('prixachatnego')
                      /*  if (!parseInt(prixachatnego))
                            prixachatnego = field.closest('.articlethumbs').data('prixachat');*/

                        qty = field.closest('.articlethumbs').data('qty');

                        prixnegodif = (100 * (data.result - prixachatnego) / data.result);
                        marge = (qty * (data.result - prixachatnego));

                        field.closest('.articlethumbs').find('.prixnegodif').html(prixnegodif.toFixed(2) + "%");
                        field.closest('.articlethumbs').find('.prixnegovalue').html(prixnegodif.toFixed(2));

                        field.closest('.articlethumbs').find('.marge').html(marge.toFixed(2));
                        field.closest('.articlethumbs').find('.marge').data("marge", marge.toFixed(2));


                    });
            }

            if (action == "moq" && $('#url').data('moq') !== undefined) {
                $.post($('#url').data('moq'),
                    {
                        'idoffer': idoffer,
                        'idcustomer': idcustomer,
                        'value': $(this).val()
                    }, function (data) {
                        display(field, data);
                    }
                );
            }

            if (action == "prixnegociation" && $('#url').data('pricenego') !== undefined) {
                field.closest('.articlethumbs').find('.prixnegodif').html('<img src="/bundles/app/images/loadingbar.gif">');
                field.closest('.articlethumbs').find('.prixnegovalue').html('<img src="/bundles/app/images/loadingbar.gif">');
                $.post($('#url').data('pricenego'),
                    {
                        'idoffer': idoffer,
                        'idcustomer': idcustomer,
                        'incoterm': $(this).data('incoterm'),
                        'idcadencier': $(this).data('cadencier'),
                        'value': $(this).val()
                    }, function (data) {
                        display(field, data);
                        prixachat = field.closest('.articlethumbs').data('prixachat');
                        qty = field.closest('.articlethumbs').data('qty');
                        prixnegodif = (100 * (data.result - prixachat) / prixachat)
                        prixnegovalue = (qty * (data.result - prixachat))
                        field.closest('.articlethumbs').find('.prixnegodif').html(prixnegodif.toFixed(2) + "%");
                        field.closest('.articlethumbs').find('.prixnegovalue').html(prixnegovalue.toFixed(2));
                    }
                );
            }

        }
    });

    $('INPUT.qty').unbind("keypress.key13").bind("keypress.key13", function () {
        update();
    });


    incoterchange();
    containerchange();

    $('#galleryOverlay').remove();
    $('#data-gallery').remove();
    $('a.product-thumb').touchTouch();
};

//Lorsque id cadencier, lorsqu'on selection
var majsuppliercadencier = function () {
    $('#cadencierselection .qty').unbind("change").change(function () {
        checksaveqtyvalue($(this));
    });

    $('#cadencierselection .fullcontainer').remove();

    maj();
};

//Lorsque id cadencier, lorsqu'on selection
var majcadencier = function () {

//mettre � jour la commande
    $("#cadencierselection INPUT.order, .order INPUT").unbind("change").change(function () {
        majcadencier();
    })

    $('#cadencierselection .qty').unbind("change").change(function () {
        checksaveqtyvalue($(this));
    });

//les fullcontainer
    $('#cadencierselection .fullcontainer').unbind("click").click(function () {
        fullcontainer($(this));
    });

//Actualiser le bloc container
    update();
};

// A l'ouverture:
majcadencier();
maj();

//evenements
$("[name^='form[incoterms]']").click(function () {
    incoterchange();
});

$("[name^='form[containers]']").click(function () {
    containerchange();
});

$("[name^='form[customerpaymenterms]']").change(function () {
    update();
});

$("[name^='form[supplierpaymenterms]']").change(function () {
    update();
});


$('#razAction').click(function () {
    if (confirm('Confirm ?')) {
        $('#cadencierselection .articlethumbs INPUT[type=checkbox]').trigger('click');
    }
});

$('textarea.order').change(function () {
    updatefx();
});




