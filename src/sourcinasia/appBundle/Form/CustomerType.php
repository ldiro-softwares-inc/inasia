<?php

namespace sourcinasia\appBundle\Form;

use Doctrine\ORM\EntityRepository;
use sourcinasia\appBundle\Entity\Supplier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('leads')
            ->add('name')
            ->add('paymentcondition', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Paymentcondition',
                'choice_label' => 'title',
                'attr' => array('class' => 'chzn-select'),
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.title', 'ASC');
                },
            ))
            ->add('activity', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Activity',
                'attr' => array('class' => 'chzn-select'),
                'choice_label' => 'title',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.title', 'ASC');
                },
            ))
            ->add('siret')
            ->add('xlsexport')
            ->add('exportsalername')
            ->add('exportsaleremail')
            ->add('address')
            ->add('city')
            ->add('locale', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices' => array(
                    'APPLICATION_FR' => 'fr',
                    'APPLICATION_UK' => 'en',
                ), 'attr' => array('class' => 'chzn-select'),
                'choices_as_values' => true
            ))
            ->add('pc')
            ->add('deleverytime')
            ->add('country', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Country',
                'choice_label' => 'title',
                'attr' => array('class' => 'chzn-select')
            ))
            ->add('commercialtools')
            ->add('margecoef')
            ->add('nomenclatures', 'Symfony\Component\Form\Extension\Core\Type\HiddenType', array('required' => true))
            ->add('suppliersrestriction', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Supplier',
                'choice_label' => function (Supplier $supplier) {
                    return $supplier->getId().'-'.$supplier->getName();
                },
                'multiple' => true,
                'expanded' => false,
                'required' => false,
                'attr' => array('class' => 'chzn-select'),
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.id', 'ASC');
                }
            ))
            ->add('suppliersexception', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Supplier',
                'choice_label' => function (Supplier $supplier) {
                    return $supplier->getId().'-'.$supplier->getName();
                },
                'multiple' => true,
                'expanded' => false,
                'required' => false,
                'attr' => array('class' => 'chzn-select'),
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.id', 'ASC');
                }
            ))
            ->add('zones', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Zone',
                'choice_label' => 'title',
                'multiple' => true,
                'attr' => array('class' => 'chzn-select'),
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.title', 'ASC');
                }
            ))
            ->add('owners', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:User',
                'choice_label' => 'username',
                'multiple' => true,
                'attr' => array('class' => 'chzn-select'),
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.roles like :role')
                        ->setParameters(array('role' => '%commercial%'));
                }
            ))
            ->add('catalogs', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Catalog',
                'choice_label' => 'titleEn',
                'multiple' => true,
                'attr' => array('class' => 'chzn-select'),
                'required' => false
            ))
            ->add('mainsaler', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:User',
                'choice_label' => 'username',
                'attr' => array('class' => 'chzn-select'),
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.roles like :role')
                        ->setParameters(array('role' => '%commercial%'));
                }
            ))
            ->add('logo', new LogoType(), array('required' => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Customer'
        ));
    }
}
