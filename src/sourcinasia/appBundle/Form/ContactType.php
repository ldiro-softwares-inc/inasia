<?php

namespace sourcinasia\appBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('phone')
            ->add('mobile')
            ->add('fax')
            ->add('email', 'email')
            ->add('exportnbrmax')
            ->add('job')
            ->add('qq')
            ->add('skype')
            ->add('type', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices' => array(
                    'Admin' => '0',
                    'Agent Co(SOURCINASIA)' => '4',
                    'Saler (Saler mode with PO)' => '1',
                    'Customer (Saler mode)' => '2',
                    'Presentation (Presentation mode)' => '3',
                ), 'attr' => array('class' => 'chzn-select'),
                'choices_as_values' => true
            ))
            ->add('supplier', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Supplier',
                'choice_label' => 'name',
                'attr' => array('class' => 'chzn-select'),
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                }
            ))
            ->add('customer', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Customer',
                'choice_label' => 'name',
                'attr' => array('class' => 'chzn-select'),
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Contact'
        ));
    }
}
