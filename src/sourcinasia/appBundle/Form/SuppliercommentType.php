<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SuppliercommentType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $builder
            ->add('title')
            ->add('comments')
            ->add('supplier', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Supplier',
                'choice_label' => 'name'));
        }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Suppliercomment'
            ));
    }
}
