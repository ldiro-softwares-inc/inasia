<?php

namespace sourcinasia\appBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SupplierType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('website')
            ->add('comments')
            ->add('mark', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices' => array(
                    'N/A' => '0',
                    'GREEN' => '1',
                    'ORANGE' => '2',
                    'RED' => '3',
                ),
                'choices_as_values' => true
            ))
            ->add('note')
            ->add('offername', 'text', array(
                    'required' => true
                )
            )
            ->add('offernamefr', 'text', array(
                'required' => true
            ))
            ->add('containers', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Container',
                'choice_label' => 'title',
                'multiple' => true,
                'required' => true,
                'expanded' => true
            ))
            ->add('paymenterms', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Paymentcondition',
                'choice_label' => 'title',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.title', 'ASC');
                }))
            ->add('infobank')
            ->add('exportlicence', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices' => array(
                    'No' => '0',
                    'Yes' => '1'),
                'choices_as_values' => true
            ))
            ->add('businesslicence')
            ->add('incoterms', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Incoterm',
                'choice_label' => 'title',
                'multiple' => true,
                'required' => true,
                'expanded' => true
            ))
            ->add('shopNumber', 'Symfony\Component\Form\Extension\Core\Type\TextType', array(
                'label'    => 'Shop number',
                'required' => false,
                'attr'     => array(
                    'class'       => 'form-control',
                    'placeholder' => 'Shop number',
                )
            ))
            ->add('supplierCategory', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'required' => false,
                'placeholder' => '-',
                'class' => 'appBundle:Suppliercategory',
                'choice_label' => 'title',
            ))
            ->add('deleverytime', 'text', array(
                'required' => true
            ))
            ->add('validityoffer')
            ->add('nomenclatures', 'Symfony\Component\Form\Extension\Core\Type\HiddenType', array('required' => true));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Supplier'
        ));
    }
}
