<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommandType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('shipper')
                ->add('consignee')
                ->add('notify')
                ->add('freightpayableat')
                ->add('descriptionofgoods')
                ->add('bookingnumber')
                ->add('pod', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Pod',
                    'choice_label' => 'title',
                    'attr' => array('class' => 'chzn-select'),
                ))
                ->add('etd', 'date', array(
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'required' => false,
                    'format' => 'dd/MM/yyyy',
                    'attr' => array('class' => 'datepicker'),
                ))
                ->add('eta', 'date', array(
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'required' => false,
                    'format' => 'dd/MM/yyyy',
                    'attr' => array('class' => 'datepicker'),
                ))
                ->add('vessel')
                ->add('tcnum')
                ->add('sealnum')
                ->add('shippingcompany', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Shippingcompany',
                    'choice_label' => 'title',
                    'attr' => array('class' => 'chzn-select'),
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Command'
        ));
    }
}
