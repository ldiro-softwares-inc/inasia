<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class OfferType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('itemnumber', 'text', array('label' => 'Supplier Item Number'))
                ->add('fob')
                ->add('exw')
                ->add('unpublished')
                ->add('currency', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'attr' => array('class' => 'chzn-select'),
                    'class' => 'appBundle:Currency',
                    'choice_label' => 'title'
                ))
                ->add('moq', null, array('required' => true))
                ->add('moqpacking')
                ->add('moqunit', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Unit',
                    'attr' => array('class' => 'chzn-select'),
                    'choice_label' => 'title',
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('u')
                        ->where('u.moqunit=1');
            },
                ))
                ->add('moqpackingunit', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Unit',
                    'attr' => array('class' => 'chzn-select'),
                    'choice_label' => 'title',
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('u')
                        ->where('u.moqpackingunit=1');
            },
                ))
                ->add('deal', null, array('attr' => array('class' => 'icheck')))
                
                ->add('pol', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Pol',
                    'choice_label' => 'title',
                    'attr' => array('class' => 'chzn-select'),
                    'required' => false,
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('u')
                        ->orderBy('u.title', 'ASC');
            }
                ))
                ->add('supplier', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Supplier',
                    'attr' => array('class' => 'chzn-select'),
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
            },
                    'choice_label' => 'idname'
                ))

        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Offer'
        ));
    }
}
