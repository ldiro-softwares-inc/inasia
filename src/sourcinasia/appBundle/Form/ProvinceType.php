<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProvinceType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title')
                ->add('country', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Country',
                    'choice_label' => 'title',
                    'attr' => array('class' => 'chzn-select'),
                ))
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Province'
        ));
    }
}
