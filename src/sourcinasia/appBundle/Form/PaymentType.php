<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('date', 'date', array(
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array('class' => 'datepicker'),
                ))
                ->add('type', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Paymenttype',
                    'choice_label' => 'title',
                    'attr' => array('class' => 'chzn-select'),
                ))
                ->add('amount')
                ->add('comment')

        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Payment'
        ));
    }
}
