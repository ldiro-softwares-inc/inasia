<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SuppliercategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('title', 'Symfony\Component\Form\Extension\Core\Type\TextType', array(
            'label'    => 'Title',
            'required' => true,
            'attr'     => array(
                'class'       => 'form-control',
                'placeholder' => 'Title',
            )
        ))
        ->add('marginRate', 'Symfony\Component\Form\Extension\Core\Type\NumberType', array(
            'label'    => 'Margin rate',
            'required' => true,
            'attr'     => array(
                'class'       => 'form-control',
                'placeholder' => 'Margin rate',
            )
        ))
        ->add('save', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array(
            'label' => 'Validation',
            'attr'  => array(
                'class' => 'btn btn-success',
            ),
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Suppliercategory'
        ));
    }
}
