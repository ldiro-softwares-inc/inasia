<?php

namespace sourcinasia\appBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuickType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('leads')
            ->add('name')
            ->add('siret')
            ->add('activity', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Activity',
                'attr' => array('class' => 'chzn-select'),
                'choice_label' => 'title',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.title', 'ASC');
                },
            ))
            ->add('margecoef')
            ->add('address')
            ->add('pc')
            ->add('country', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Country',
                'choice_label' => 'title',
                'attr' => array('class' => 'chzn-select')
            ))
            ->add('city')
            ->add('zones', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Zone',
                'choice_label' => 'title',
                'multiple' => true,
                'attr' => array('class' => 'chzn-select'),
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.title', 'ASC');
                }
            ))
            ->add('locale', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices' => array(
                    'APPLICATION_FR' => 'fr',
                    'APPLICATION_UK' => 'en',
                ), 'attr' => array('class' => 'chzn-select'),
                'choices_as_values' => true
            ))
            ->add('nomenclatures', 'Symfony\Component\Form\Extension\Core\Type\HiddenType', array('required' => true))
            ->add('contacts', 'contact_type', array(
                'context' => 'default',
                'data_class' => 'Application\Sonata\MediaBundle\Entity\Media',
                'required' => false,
                'label' => 'Image'
            ))
            ->add('contacts', 'collection', array('type' => new QuickcontactType(),
                'allow_add' => false,
                'allow_delete' => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Customer'
        ));
    }
}
