<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SupplychaintypeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'Symfony\Component\Form\Extension\Core\Type\TextType', array('label' => 'Title'))
            ->add('validation', 'Symfony\Component\Form\Extension\Core\Type\CheckboxType', array('required' => false, 'label' => 'Require validation'))
            ->add('step')
            ->add('customer', 'Symfony\Component\Form\Extension\Core\Type\CheckboxType', array('required' => false, 'label' => 'Display for customer'))
            ->add('supplychaincat', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'appBundle:Supplychaincat',
                'choice_label' => 'title',
                'attr' => array('class' => 'chzn-select'),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Supplychaintype'
        ));
    }
}
