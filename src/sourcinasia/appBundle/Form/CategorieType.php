<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategorieType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('id')
                ->add('public')
                ->add('type', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                    'choices' => array(
                        'SEC' => 'SEC',
                        'RAY' => 'RAY',
                        'FAM' => 'FAM',
                        'SSF' => 'SSF',
                    ),
                    'choices_as_values' => true
                ))
                ->add('name')
                ->add('nom')
                ->add('margesourcin')
                ->add('indexation')
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Categorie'
        ));
    }

}
