<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CadencierType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('state', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
            'choices' => array(
                '1.0 - Customer PI request received ' => '1',
                '1.1 - Customer PI request confirmed' => '2',
                '1.2 - Record Supplier PI' => '3',
                '1.3 - Supplier PI to be confirmed (Admin)' => '4',
                '1.4 - Edit customer PI (sales)' => '5',
                '1.5 - Confirm customer PI (Admin)' => '6',
                '2 - Customer PI to be confirmed BY customer' => '7',
                '3 - Order confirmed - Production processing' => '8',
                '4 - Customer order on board ' => '9',
                '5 - Order delivered at destination' => '10',
            ),
            'choices_as_values' => true
        ));


        $builder->add('stepProduction', 'date', array(
            'widget' => 'single_text',
            'input' => 'datetime',
            'required' => false,
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepicker'),
        ));

        $builder->add('cargoreadydate', 'date', array(
            'widget' => 'single_text',
            'input' => 'datetime',
            'required' => false,
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepicker'),
        ));
        $builder->add('qcschedule', 'date', array(
            'widget' => 'single_text',
            'input' => 'datetime',
            'required' => false,
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepicker'),
        ));

        $builder->add('loadingdate', 'date', array(
            'widget' => 'single_text',
            'input' => 'datetime',
            'required' => false,
            'format' => 'dd/MM/yyyy',
            'attr' => array('class' => 'datepicker'),
        ));

        $builder->add('customerforwarder');
        $builder->add('supplierpaymenterms', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
            'class' => 'appBundle:Paymentcondition',
            'choice_label' => 'title',
            'multiple' => false,
            'required' => true
        ));
        $builder->add('customerpaymenterms', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
            'class' => 'appBundle:Paymentcondition',
            'choice_label' => 'title',
            'multiple' => false,
            'required' => true
        ));
        $builder->add('finalincoterm');
        $builder->add('finalpol');
        $builder->add('finaldevis');
        $builder->add('supplierpi');
        $builder->add('supplierci');
        $builder->add('deleverytime');
        $builder->add('nbr');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Cadencier'
        ));
    }

}
