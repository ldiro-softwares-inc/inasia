<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class AddressType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('customer', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Customer',
                    'choice_label' => 'name',
                    'attr' => array('class' => 'chzn-select'),
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
            }
                ))
                ->add('supplier', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Supplier',
                    'choice_label' => 'name',
                    'attr' => array('class' => 'chzn-select'),
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
            }
                ))
                ->add('type', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                    'choices' => array(
                        'Office' => 'Office',
                        'Factory' => 'Factory',
                    ),
                    'choices_as_values' => true
                ))
                ->add('address')
                         ->add('city', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:City',
                    'attr' => array('class' => 'chzn-select'),
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('c')
                        ->leftJoin('c.province', 'province')
                        ->leftJoin('province.country', 'country')
                        ->orderBy('country.title,province.title,c.title', 'ASC');
            },
                    'choice_label' => 'addressname'
                ))
                
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Address'
        ));
    }

}
