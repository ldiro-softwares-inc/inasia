<?php

namespace sourcinasia\appBundle\Form;

use sourcinasia\appBundle\Entity\Document;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'supplier',
                EntityType::class,
                [
                    'class' => 'appBundle:Supplier'
                ]
            )
            ->add(
                'title',
                TextType::class
            )
            ->add(
                'documentFile',
                'file',
                [
                    'label' => 'Document',
                ]
            )
            ->add(
                'documentType',
                ChoiceType::class,
                [
                    'label' => 'Type de document',
                    'choices' =>
                        [
                            'quotation' => Document::QUOTATION_TYPE,
                            'catalog'   => Document::CATALOG_TYPE,
                            'customer_offer'   => Document::CUSTOMER_OFFER_TYPE
                        ]
                ]
            )
            ->add(
                'Valider',
                SubmitType::class,
                ['label' => 'Valider']
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Document'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sourcinasia_appbundle_document';
    }
}
