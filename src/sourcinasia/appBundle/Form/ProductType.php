<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class ProductType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('codebar', new CodebarType(), array(
                    'required' => false))
                ->add('description')
                ->add('smalldescription')
                ->add('frenchsmalldescription')
                ->add('frenchdescription')
                ->add('coloris')
                ->add('material')
                ->add('packing')
                ->add('comments')
                ->add('ce')
                ->add('packingsize')
                ->add('productsize')
                ->add('nwproduct')
                ->add('gwproduct')
                ->add('gwpackage')
                ->add('nwpackage')
                ->add('qty20gp')
                ->add('qty40gp')
                ->add('qty40hq')
                ->add('pcb')
                ->add('cbm')
                ->add('hidden')
                ->add('mainimage')
                ->add('ce', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                    'choices' => array(
                        'N/A' => 'N/A',
                        'YES' => 'YES',
                        'NO' => 'NO',
                    ),
                    'choices_as_values' => true,
                ))
                ->add('categorie', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Categorie',
                    'choice_label' => 'idname',
                    'attr' => array('class' => 'chzn-select'),
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('c')
                        ->andwhere('c.subfamilly>0')
                        ->orderBy('c.id', 'ASC');
            }
                ))
                ->add('customers', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                    'class' => 'appBundle:Customer',
                    'choice_label' => 'name',
                    'multiple' => true,
                    'expanded' => false,
                    'required' => false,
                    'attr' => array('class' => 'chzn-select'),
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
            }
                ))

        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Product'
        ));
    }

}
