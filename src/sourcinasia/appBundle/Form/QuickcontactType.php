<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuickcontactType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email')
            ->add('name')
            ->add('phone')
            ->add('job')
            ->add('frequencenews')
            ->add('type', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices' => array(
                    'Admin' => '0',
                    'Agent Co(SOURCINASIA)' => '4',
                    'Saler (Saler mode with PO)' => '1',
                    'Customer (Saler mode)' => '2',
                    'Presentation (Presentation mode)' => '3',
                ), 'attr' => array('class' => 'chzn-select'),
                'choices_as_values' => true
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Contact'
        ));
    }
}
