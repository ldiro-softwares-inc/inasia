<?php

namespace sourcinasia\appBundle\Form;

use sourcinasia\appBundle\Entity\Document;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentSupplierTypeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'documentFile',
                'file',
                [
                    'label' => 'Document',
                ]
            )
            ->add(
                'title',
                TextType::class,
                [
                    'label' => 'ENTITY_DOCUMENT_TITLE',
                ]
            )
            ->add(
                'documentType',
                HiddenType::class
            )
            ->add(
                'Valider',
                SubmitType::class,
                ['label' => 'APPLICATION_SAVE']
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\Document'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sourcinasia_appbundle_document_supplier_type';
    }
}
