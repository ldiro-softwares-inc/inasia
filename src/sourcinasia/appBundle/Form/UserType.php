<?php

namespace sourcinasia\appBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           // ->add('id')
            ->add('name')
            ->add('username')
            ->add('phone')
            ->add('email')
            ->add('password')
            ->add('logo', new LogoType(), array('required' => false))
            ->add('roles', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices' => array(
                    'Commercial' => 'ROLE_COMMERCIAL',
                    'Production' => 'ROLE_PRODUCTION',
                    'Sourcing' => 'ROLE_SOURCING',
                    'POL' => 'ROLE_ADDPOL',
                    'Allow IP' => 'ROLE_ALLOWIP',
                    'Delete' => 'ROLE_DELETE',
                    'Export' => 'ROLE_EXPORT',
                    'Admin supplychain' => 'ROLE_ADMINSUPPLYCHAIN',
                    'Admin sourcing' => 'ROLE_ADMINSOURCING',
                    'Admin user' => 'ROLE_ADMIN'
                ),
                'multiple' => true,
                'expanded' => true,
                'choices_as_values' => true
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'sourcinasia\appBundle\Entity\User'
        ));
    }
}
