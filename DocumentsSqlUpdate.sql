-- for supplier_docs_update
CREATE TABLE Documents (id INT AUTO_INCREMENT NOT NULL, supplier_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, documentFileName VARCHAR(255) NOT NULL, documentType ENUM('catalog', 'quotation'), updatedAt DATETIME NOT NULL, INDEX IDX_2041F02B2ADD6D8C (supplier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE Documents ADD CONSTRAINT FK_2041F02B2ADD6D8C FOREIGN KEY (supplier_id) REFERENCES Suppliers (id);
ALTER TABLE Documents MODIFY COLUMN documentType enum('catalog','quotation','customer_offer') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
